# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe import msgprint, throw,_

def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	cols = [_("UVC")+ "::80",_("Reference/Style")+ "::80",_("Colours")+ "::80",_("Theme")+ "::80",\
	_("Description")+ "::80",_("Quantity/Pcs")+ "::80",_("Unit Price")+ "::80",\
	_("Unit price less Discount")+ "::175",_("Total amount less discount")+ "::175",_("Shipment Date")+ "::100"]

	return cols


def get_all_proforma(filters=None):

	datas = frappe.db.sql("""SELECT total_amount_discount,order_qty,proforma_order\
		FROM `tabPI Child` WHERE parent = '{}'
		""".format(filters['pi']) , as_list=1)
	return datas
	

def get_all_proforma_detail(pro_names):


	result = frappe.db.sql("""SELECT uvc,style_name,colors,theme,description,qty_pcs,\
		unit_price,shipment_date\
		FROM `tabProforma Order Child` WHERE parent = '{}'
		""".format(pro_names.encode('ascii','ignore')),as_list=1)
	return result


def get_main_row(filters=None):
	datas = frappe.db.sql("""SELECT total_qty,total_amount_order,amount_in_word,terms\
		FROM `tabPI` WHERE name = '{}'
		""".format(filters['pi']) , as_list=1)
	return datas.pop()

def cal_discount_total(qty,unit,discount):
	unit = float(unit)
	qty = int(qty)
	discount = float(discount)
	unit_less = unit - (unit*discount)/100
	return [unit_less,unit_less*qty]



def data(filters):

	main_row = get_main_row(filters)

	all_proforma = get_all_proforma(filters)

	##throw(all_proforma)

	proforma_detail = []

	total_price = 0
	total_qty = 0

	for x in all_proforma:
		data = get_all_proforma_detail(x[2])

		for i,j in enumerate(data):
			y = data[i].pop()
			less_info = cal_discount_total(data[i][5],data[i][6],main_row[3])
			total_qty += int(data[i][5])
			total_price += less_info[1]
			proforma_detail.append(data[i]+less_info+[y])

	proforma_detail.append(["","","","","<strong>Total</strong>",total_qty,"","",total_price,""])		
	proforma_detail.append(["Amount In Word",main_row[2],"","","","","","","",""])		

	#throw(proforma_detail)
	#proforma_detail.append(["<strong>Amount in Word</strong>","","","","","",main_row[2],""])		
	return proforma_detail