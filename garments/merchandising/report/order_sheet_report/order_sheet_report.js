// Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Order Sheet Report"] = {
	"filters": [
		{
				fieldname:"order",
				label:"Order Sheet",
				fieldtype:"Link",
				options:"Order Sheet",
				reqd:1
		}

	]
}
