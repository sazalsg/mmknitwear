# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import msgprint, throw,_
import collections
import json

parent_data = []

total_rows = 0

consumption_row = []

rib_dia = 0

def execute(filters=None):
	return columns(filters), data(filters)


def get_size_list(filters=None):

	datas = frappe.db.sql("""SELECT order_quantity_data,fabric_requirement_data,ratio_data,consumption_workout\
		FROM `tabOrder Sheet` WHERE name = '{}'
		""".format(filters['order']) , as_dict=1).pop()

	#throw(datas)

	global parent_data
	parent_data = datas

	global consumption_row
	consumption_row = frappe.db.sql("""SELECT consumption_data\
		FROM `tabConsumption Workout Child` WHERE name = '{}'
		""".format(datas["consumption_workout"]), as_list=1)
	if consumption_row:
		consumption_row = consumption_row.pop().pop()
		consumption_row = json.loads(consumption_row)['CONSUMPTION :']

	datas = frappe.db.sql("""SELECT size,IFNULL(rib_dia,0)\
		FROM `tabConsumption Workout` WHERE name = '{}'
		""".format(datas["consumption_workout"]) , as_list=1).pop()


	global rib_dia
	rib_dia = datas[1]

	datas = datas[0]

	datas = frappe.db.sql("""SELECT size\
		FROM `tabSize Format Child` WHERE parent = '{}'
		""".format(datas) , as_list=1)


	return datas


def columns(filters):

	get_all_size = get_size_list(filters)

	get_all_size = map(lambda x: x.pop(),get_all_size)

	global total_rows
	total_rows = len(get_all_size) + 2
	
	colms = []
	for x in get_all_size:
		colms.append( x + "::80" )

	return 	["Size::100"] + colms + ["Total::80","RIB::80","GrandTotal::80",]


	# colms = ["::100"]
	# for x in list(range(10)):
	# 	#if i == 4 or i==3:
	# 	colms.append("::70")
		
	# 	# else:
	# 	# 	colms.append( x.pop() + "::50" )
		
	# return colms


def col_list(size_list):
	
	colms = ["SIZE"]
	for i,x in enumerate(size_list):
		#if i == 4 or i==3:
		colms.append( x  )
	colms = colms + ['Total']
		
	return 	colms


def get_empty_row():
	global total_rows
	return ['<div style="background-color:#A6C5E6;"></div>']*total_rows


def data(filters):
	get_all_size = get_size_list(filters)

	final_data = []

	get_all_size = map(lambda x: x.pop(),get_all_size)

	#cols = col_list(get_all_size)

	empty_row = get_empty_row()
	empty_row.append("<strong>Ratio</strong>")

	final_data.append(  empty_row )

	rat_data = parent_data["ratio_data"]

	final_data = final_data + ratio_displayOrderQ(rat_data,get_all_size)


	empty_row = get_empty_row()
	empty_row.append("<strong>Order Quantity</strong>")

	final_data.append(  empty_row )

	rat_data = parent_data["order_quantity_data"]

	final_data = final_data + ratio_displayOrderQ(rat_data,get_all_size)

	empty_row = get_empty_row()
	empty_row.append("<strong>Fabric Requirement</strong>")

	final_data.append(  empty_row )
	rat_data = parent_data["fabric_requirement_data"]
	final_data.append(display_consumption(get_all_size))
	final_data = final_data + ratio_displayOrderQ(rat_data,get_all_size,True)
	return final_data



def display_consumption(get_all_size):
	temp = ['CONSUMPTION :']
	global consumption_row
	temp = temp + consumption_row
	
	temp.append('-')
	global rib_dia
	temp.append(str(rib_dia) + "%")
	temp.append('-')
	return temp


	
def ratio_displayOrderQ(rat_data,get_all_size,fab=None):
	final_data = []
	rat_data = json.loads(rat_data)
	total_cols = rat_data["total_row"]
	rat_data = rat_data["data_row"]
	rat_data = collections.OrderedDict(sorted(rat_data.items()))
	
	for _, x in rat_data.iteritems():
		temp = render_row(get_all_size,x,fab)
		final_data.append(temp)

	temp = render_row(get_all_size,total_cols,fab,True)
	final_data.append(temp)	

	# temp = []
	# for i in get_all_size:
	# 	temp.append(total_cols[i])

	# temp.append(total_cols["total"])
	# if fab:
	# 	temp.append(total_cols["total_rib"])
	# 	temp.append(total_cols["total_gtotal"])
	# temp = ["Total"] + temp
	#final_data.append(temp)	

	return final_data	



def render_row(get_all_size,x,fab,isTotal=None):
	temp = []
	for i in get_all_size:
		temp.append(x[i])
	temp.append(x["total"])
	if fab:
		temp.append(x["total_rib"])
		temp.append(x["total_gtotal"])
	if not isTotal:
		temp = [x["color"]] + temp
	else:
		temp = ["Total"] + temp
	return temp

	