# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _ , msgprint


def execute(filters=None):
	##msgprint(filters)
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("PO")+":Data:30",
		_("UVC")+":Data:40",
		_("STD P Code")+":Data:80",
		_("Product Name")+":Style Name/Link:90",
		_("Description")+":Data:80",
		_("Color")+":Color/Link:55",
		_("Theme")+":Theme/Link:55",
		_("AGE")+":Data:40",
		_("STD")+":Data:40",
		_("Shipment date")+":Data:80",
		_("Shipment Destination")+":Data:90",
		_("Composition")+":Composition/Link:80",
		_("FOB")+":Data:30",
		_("2 ANS")+":Data:30",
		_("3 ANS")+":Data:30",
		_("4 ANS")+":Data:30",
		_("5 ANS")+":Data:30",
		_("6 ANS")+":Data:30",
		_("7 ANS")+":Data:30",
		_("8 ANS")+":Data:30",
		_("10 ANS")+":Data:30",
		_("12 ANS")+":Data:30",
		_("14 ANS")+":Data:30",
		_("TOTAL")+":Data:30"
	]

def data(filters):
	datas = frappe.db.sql("""SELECT po, uvc, std_p_code, product_name, description, colors,\
		theme, age, std, shipment_date, shp_dest, composition, fob, ans2, ans3, ans4,\
		ans5, ans6, ans7, ans8, ans10, ans12, ans14, ttl, parentfield \
		 FROM `tabPO Child` WHERE parent = '{}' \
		 """.format(filters['po report']))

	# total = frappe.db.sql("""SELECT ttl_ans2,ttl_ans3,ttl_ans4,ttl_ans5,ttl_ans6,ttl_ans7, \
	# 	ttl_ans8,ttl_ans10,ttl_ans12,ttl_ans14,ttl_ttl\
	# 	 FROM `tabPO` WHERE name = '{}' \
	# 	 """.format(filters['po report']))
	# #msgprint(total)

	total = frappe.db.sql("""SELECT *
		 FROM `tabPO` WHERE name = '{}' \
		 """.format(filters['po report']))

	datas = datas 
	total = tuple(["","","","","","","","","","","","<b>Total</b>",""]+list(total[0]))
	total = (total,)
	#msgprint(total)
	return datas + total