// Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["PO Report"] = {
	"filters": [
		{
			fieldname:"po report",
			label:"PO Report",
			fieldtype:"Link",
			options:"PO",
			reqd:1
		}

	]
}
