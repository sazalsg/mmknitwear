# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, json
from frappe import _

def execute(filters=None):
	return columns(filters), all_data(filters)

def columns(filters):
	return [
		{
			"fieldname":"i_code",
			"fieldtype":"Link",
			"label":_("ITEM CODE"),
			"options":"Item",
			"width":190 if filters.get('costing') else 300,
            "is_tree":1
		},
		{
			"fieldname":"item_description",
			"fieldtype":"Data",
			"label":_("ITEM DESCRIPTION"),
			"width":150
		},
		{
			"fieldname":"qty",
			"fieldtype":"Data",
			"label":_("QTY/CONS"),
			"width":150
		},
		{
			"fieldname":"unit_price",
			"fieldtype":"Currency",
			"label":_("UNIT PRICE"),
			"width":150
		},
		{
			"fieldname":"total",
			"fieldtype":"Currency",
			"label":_("TOTAL"),
			"width":150
		}

	] 
	


def all_data(filters):
	if filters.get("costing"):
		return dummy_data(filters)
	else:
		rows = []
		costings = frappe.db.get_values("Costing",{"name":("!=","")},"name")
		for cost in costings:
			rows += dummy_data(filters,cost[0])

		return rows

def dummy_data(filters,name=None):
	if name:
		data = frappe.get_doc("Costing", name)
	elif filters.get('costing'):
		data = frappe.get_doc("Costing", filters['costing'])
	else:
		return []

	rows = [
		{
			"i_code":"<strong>GRAND TOTAL{}</string>".format(" ("+str(name)+")" if name else ""),
			"item_description":"",
			"qty":"	",
			"unit_price":"",
			"total":data.g_total,
			"indent":0
		},
		{
			"i_code":"<strong>TOTAL{}</string>".format(" ("+str(name)+")" if name else ""),
			"parent_field":"<strong>GRAND TOTAL{}</string>".format(" ("+str(name)+")" if name else ""),
			"item_description":"",
			"qty":"",
			"unit_price":"",
			"total":data.total,
			"indent":1
		},
		{
			"i_code":"<strong>TOTAL FABRICS{}</string>".format(" ("+str(name)+")" if name else ""),
			"parent_field":"<strong>TOTAL{}</string>".format(" ("+str(name)+")" if name else ""),
			"item_description":"",
			"qty":"",
			"unit_price":"",
			"total":data.total_fabrics_cost,
			"indent":2
		}
	]
	for fab in data.fabric_table:
		row = {}
		row['i_code'] = fab.costing_child_item
		row['parent_field'] = "<strong>TOTAL FABRICS{}</string>".format(" ("+str(name)+")" if name else "")
		row['item_description'] = fab.costing_child_item_name
		row['qty'] = fab.costing_child_qty
		row['unit_price'] = fab.costing_child_unit_price
		row['total'] = fab.costing_child_total_amount
		row['indent'] = 3
		rows.append(row)

	rows.append({
			"i_code":"<strong>TOTAL ACCESSORIES{}</string>".format(" ("+str(name)+")" if name else ""),
			"parent_field":"<strong>TOTAL{}</string>".format(" ("+str(name)+")" if name else ""),
			"item_description":"",
			"qty":"",
			"unit_price":"",
			"total":data.total_accessories_cost,
			"indent":2
		})

	for acc in data.accessories_table:
		row = {}
		row['i_code'] = acc.costing_child_item
		row['parent_field'] = "<strong>TOTAL ACCESSORIES{}</string>".format(" ("+str(name)+")" if name else "")
		row['item_description'] = acc.costing_child_item_name
		row['qty'] = acc.costing_child_qty
		row['unit_price'] = acc.costing_child_unit_price
		row['total'] = acc.costing_child_total_amount
		row['indent'] = 3
		rows.append(row)


	rows += [
		{
			"i_code":"<strong>Total Foreign Commission{}</string>".format(" ("+str(name)+")" if name else ""),
			"parent_field":"<strong>GRAND TOTAL{}</string>".format(" ("+str(name)+")" if name else ""),
			"item_description":"",
			"qty":"",
			"unit_price":"",
			"total":data.total_foreign_commission,
			"indent":1
		},
		{
			"i_code":"<strong>Total Local Commission{}</string>".format(" ("+str(name)+")" if name else ""),
			"parent_field":"<strong>GRAND TOTAL{}</string>".format(" ("+str(name)+")" if name else ""),
			"item_description":"",
			"qty":"",
			"unit_price":"",
			"total":data.total_local_commission,
			"indent":1
		}
	]
	return rows
	