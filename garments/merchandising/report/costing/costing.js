// Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Costing"] = {
	"filters": [
		{
			fieldname:"costing",
			label:"Costing",
			fieldtype:"Link",
			options:"Costing"
		}

	],
	"tree": true,
	"name_field": "i_code",
	"parent_field": "parent_field",
	"initial_depth": 4
}
