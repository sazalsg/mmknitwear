# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Reference/Style")+":Link/Style Name:100",
		_("FABRICS")+":Data:150",
		_("GSM")+":Data:150",
		_("Color")+":Link/Color:100",
		_("Samples")+":Data:100",
		_("Size")+":Data:90",
		_("Qty")+":Data:100"
	]

def data(filters):
	data = frappe.db.sql("""SELECT style, fabric, gsm,color,samples,size,qty, parentfield\
		FROM `tabPP Sample Child` WHERE parent = '{}'""".format(filters['pp_sample']))
	# frappe.throw("data")
	return data