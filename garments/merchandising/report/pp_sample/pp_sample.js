// Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["PP Sample"] = {
	"filters": [
		{	
			fieldname:"pp_sample",
			label:"PP Sample",
			fieldtype:"Link",
			options:"PP Sample",
			reqd:1
		}
	]
}
