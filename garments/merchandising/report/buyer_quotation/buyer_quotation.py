# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe import msgprint, throw
from frappe.utils import flt
from frappe import _

def execute(filters=None):
	if not filters: filters = {}

	columns = add_columns()
	data =  get_return(filters)
	return columns, data

def add_columns():
	return [
		_("Reference/Style")+":Link/Style Name:120",
		_("Description")+":Data:120",
		_("Size Range")+":Data:90",
		_("Photo")+":Image:150",
		_("Quantity(PCS)")+":Data:70",
		_("Per Piece Price")+":Currency:100",
		_("Target Price")+":Currency:100",
		_("Shipment Date")+":Date:100",
		_("Remark")+":Data:120",
		_("Status")+":Data:90"
	]

def get_return(filters):

	photos = []
	condition=""
	if filters.get("ref"):
		condition += " AND `tabBuyer Quotation Child`.`ref` ='{}'".format(filters['ref'])
	if filters.get("status"):
		condition += " AND `tabBuyer Quotation Child`.`status` ='{}'".format(filters['status'])

	# return data from database
	datas = frappe.db.sql(""" SELECT 
		`tabBuyer Quotation Child`.`ref`,
		`tabBuyer Quotation Child`.`item`,
		`tabBuyer Quotation Child`.`s_rng`,
		`tabBuyer Quotation Child`.`photo`,
		`tabBuyer Quotation Child`.`qty`,
		`tabBuyer Quotation Child`.`per_pcs_price`,
		`tabBuyer Quotation Child`.`t_prc`,
		`tabBuyer Quotation Child`.`s_date`,
		`tabBuyer Quotation Child`.`rmk`,
		`tabBuyer Quotation Child`.`status`,
		`tabStyle Name`.`buyer`
		FROM `tabBuyer Quotation Child`
		LEFT JOIN `tabStyle Name` 
		ON `tabBuyer Quotation Child`.`ref` = `tabStyle Name`.`style_name`
		WHERE `tabStyle Name`.`buyer` ='{}' {fill}
		""".format(filters['buyer'], fill=condition), as_list = 1)
	for pht in datas :
		if pht[3]:
			pht[3] = '<img src="'+pht[3]+'" width="30">'
			photos.append(pht[3])

	return datas