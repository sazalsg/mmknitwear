// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Buyer Quotation"] = {
	"filters": [
		{
			fieldname:"buyer",
			label: __("Buyer"),
			fieldtype: "Link",
			options: "Customer",
			reqd: 1,
		},
		{
			fieldname:"ref",
			label: __("Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 0,
		},
		{
			fieldname:"status",
			label: __("Status"),
			fieldtype: "Select",
			options: "Pending"+NEWLINE+"Confirm"+NEWLINE+"Cancel",
			reqd: 0,
		}
	]
}