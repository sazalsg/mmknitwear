// Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Consumption Workout Report"] = {
	"filters": [
		{

			fieldname:"consumption",
			label:"Consumption Workout",
			fieldtype:"Link",
			options:"Consumption Workout",
			reqd:1

		}
	]
}
