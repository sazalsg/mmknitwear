# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import msgprint, throw,_

import json

def execute(filters=None):
	return columns(filters), data(filters)


def get_main_row(filters=None):

	datas = frappe.db.sql("""SELECT avg_rib,IFNULL(rib_dia,'0.0000'),avg_consumption,size\
		FROM `tabConsumption Workout` WHERE name = '{}'
		""".format(filters['consumption']) , as_list=1)
	return datas.pop()

def get_size(size):
	datas = frappe.db.sql("""SELECT size\
		FROM `tabSize Format Child` WHERE parent = '{}'
		""".format(size) , as_list=1)
	return datas


def get_child_row(name):
	datas = frappe.db.sql("""SELECT consumption_data\
		FROM `tabConsumption Workout Child` WHERE parent = '{}'
		""".format(name) , as_list=1)

	return datas


def columns(filters):
	main_row = get_main_row(filters)
	size_list = get_size(main_row[3])
	colms = ["SIZE::120"]
	for i,x in enumerate(size_list):
		if i == 4 or i==3:
			colms.append( x.pop() + "::100" )
		
		else:
			colms.append( x.pop() + "::50" )
		

	return colms	


def data(filters):
	child_row = get_child_row(filters['consumption']).pop().pop()

	main_row = get_main_row(filters)

	#throw(main_row)

	final_data = []

	rows = ['BODY LEN :','SLEEVE LEN :', 'ALOWANCE :', 'DIA :','GSM :','DOZ/PCS :','WASTAGE :','CONSUMPTION :','RIB :']
	unit = {'BODY LEN :':'Cm','SLEEVE LEN :':'','ALOWANCE :':'','DIA :':'Inch','GSM :':'Gm','DOZ/PCS :':'Pcs','WASTAGE :':'%','CONSUMPTION :':'','RIB :':''}

	child_row = json.loads(child_row)
	
	for x in rows:
		final_data.append([x]+map(lambda i: i +" " + unit[x],child_row[x]) )

	last_row = []
	
	final_data.append(["","","","","","<strong>"+_("RIB DIA")+"</strong>","",main_row[1]])	
	final_data.append(["","","","","<strong>"+_("AVERAGE")+"</strong>","<strong>"+_("RIB")+"</strong>","",main_row[0]])
	final_data.append(["","","","","<strong>"+_("AVERAGE") +"</strong>","<strong>"+_("CONSUMPTION") +"</strong>","",main_row[2]])

	return final_data
	

	