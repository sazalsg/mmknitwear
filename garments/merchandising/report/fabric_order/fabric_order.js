// Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Fabric Order"] = {
	"filters": [
		{
			fieldname:"fabric order",
			label:"Fabric Order",
			fieldtype:"Link",
			options:"Fabric Order",
			reqd:1
		}
	]
}