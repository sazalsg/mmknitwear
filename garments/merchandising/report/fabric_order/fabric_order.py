# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _ , msgprint

def execute(filters=None):
	#msgprint(filters)
	return columns(filters), data(filters)

def columns(filters):
	return [
		_('Item')+':Item/Link:100',
		_('35"(MBROGR)')+':Data:150',
		_('33"(MMBRO)')+':Data:150',
		_('30"(Minbre)')+':Data:100',
		_('36"(Buldon)')+':Data:100',
		_('Total Body')+':Data:90',
		_('RIB')+':Data:100',
		_('Total Body+RIB')+':Data:100'
	]
	
def data(filters):
	data = frappe.db.sql("""SELECT fo_child_item, mbr, mmb, min, bul, t_body, rib, total, parentfield\
		FROM `tabFO Child` WHERE parent = '{}'""".format(filters['fabric order']))
	#frappe.throw("data")
	return data