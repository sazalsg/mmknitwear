# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import msgprint, throw,_

def execute(filters=None):
	return columns(filters), data(filters) 

def columns(filters):
	return [
		_("Reference/Style")+":Link/Style Name:120",
		_("Item")+":Data:90",
		_("PO Sheet Price")+":Data:100",
		_("Shipment Date")+":Date:100",
		_("Quantity")+":Data:70",
		_("Size Range")+":Data:150",
		_("Our Price")+":Currency:100",
		_("Foreign Commission")+":Data:90",
		_("Local Commission")+":Data:90",
		_("Grand Total")+":Data:90"
	]
def data(filters):

	# datas = frappe.db.sql("""SELECT `tabPO Sheet Child Child`.`product_name`,
	#  `tabPO Sheet Child Child`.`description`, 
	#  `tabPO Sheet Child Child`.`purchase_price`,
	#  `tabPO Sheet Child Child`.`etd_shipment_date`,
	#  `tabPO Sheet Child Child`.`qty`, 
	#  `tabPO Sheet Child Child`.`size_format`,
	#  `tabCosting`.`total`,
	#  `tabCosting`.`total_foreign_commission`,
	#  `tabCosting`.`total_local_commission`,
	#  `tabCosting`.`g_total`
	#  FROM `tabPO Sheet Child Child` LEFT JOIN `tabCosting` 
	#  ON `tabPO Sheet Child Child`.`product_name` = `tabCosting`.`reference`
	#  WHERE `tabPO Sheet Child Child`.`product_name` = '{}' """.format(filters["product_name"], as_list = 1)
	


	datas=frappe.db.sql("""SELECT `tabPO Sheet Child Child`.product_name,
	 `tabPO Sheet Child Child`.description, 
	 `tabPO Sheet Child Child`.purchase_price,
	 `tabPO Sheet Child Child`.etd_shipment_date,
	 `tabPO Sheet Child Child`.qty, 
	 `tabPO Sheet Child Child`.size_format,
	 `tabCosting`.total,
	 `tabCosting`.total_foreign_commission,
	 `tabCosting`.total_local_commission,
	 `tabCosting`.g_total FROM `tabPO Sheet Child Child` INNER JOIN `tabCosting` 
	 ON `tabPO Sheet Child Child`.product_name = `tabCosting`.reference 
	 WHERE `tabPO Sheet Child Child`.`product_name` = '{}'
	""".format(filters["product_name"]), as_list = 1)
	return datas