// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Order ReCap Report"] = {
	"filters": [
		{
			fieldname:"product_name",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 1,
			default: frappe.defaults.get_user_default("product_name")
		},
	]
}
