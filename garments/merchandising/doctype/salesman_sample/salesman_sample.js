frappe.provide("garments.merchandising");

garments.merchandising.SalesmanSampleController = frappe.ui.form.Controller.extend({
	onload:function (doc) {
		this.frm.set_query('fabrics','salesman_sample_child',function () {
			return {
				filters:[
					["Item",'item_group','=',"Fabrics"]
				]
			}
		});
		frappe.ui.form.on("Salesman Sample Child","salesman_sample_child_remove",function(frm){
		  cur_frm.cscript.total(frm.doc);
		});
	},
	qty:function (doc) {
		this.total(doc);
	},
	total:function (doc) {
		var total = 0;
		for (var i = doc.salesman_sample_child.length - 1; i >= 0; i--) {
			total += doc.salesman_sample_child[i].qty;
		};
		this.frm.set_value('total',total);
	}
})

cur_frm.cscript = new garments.merchandising.SalesmanSampleController({frm: cur_frm});
