# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.model.naming import make_autoname
from frappe.model.mapper import get_mapped_doc

class BuyerInquiry(Document):
	def autoname(self):
		self.name = make_autoname("BI-{b}-{m}/{y}-".format(b=self.byr,m=date.today().month,y=str(date.today().year)[2:4])+".##")

@frappe.whitelist()
def make_costing(source_name, target_doc=None, item=None, size=None):

	doc = get_mapped_doc("Buyer Inquiry", source_name, {
		"Buyer Inquiry": {
			"doctype": "Costing",
			"field_map":{
				"reference":"reference"
			}
		}
	}, target_doc)

	return doc