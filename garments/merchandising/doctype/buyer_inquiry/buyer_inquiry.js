cur_frm.add_fetch("reference","buyer","byr");
cur_frm.add_fetch("reference", "item", "item");
cur_frm.cscript.refresh = function (doc) {
	cur_frm.add_custom_button(__("Make Costing"),function () {
		frappe.model.open_mapped_doc({
			method:"garments.merchandising.doctype.buyer_inquiry.buyer_inquiry.make_costing",
			frm:cur_frm,
			run_link_triggers:true
		})
	}).addClass('btn-primary');
}