# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import msgprint, throw,_
from datetime import date
from frappe.model.naming import make_autoname
import time



class ConsumptionWorkout(Document):

	def autoname(self):
		cname=frappe.db.get_value("Customer", {"name":self.buyer}, ["customer_name"])
		self.name =  make_autoname('{0}-{1}-{2}{3}{4}-'.format("CW",cname.split()[0], str(date.today().month), '/',str(date.today().year)[2:4] ) + ".#")

	def validate(self):
		consumpStatus = frappe.db.get_value("Consumption Workout", {"name":("!=", self.name),"style":self.style,"item":self.item}, ["name"])
		if consumpStatus :
			throw(_('Item "{}"" Consumption already created at <strong><a href="#Form/Consumption Workout/{}" target="_blink">{}</a></strong>'.format(self.item, consumpStatus[0][0], consumpStatus[0][0])))
		
		


	def before_insert(self):
		if not hasattr(self, 'measure_info'):
			frappe.throw("Please Input Consumption Work Out")

	
	def on_update(self):
		if hasattr(self, 'measure_info'):
			if frappe.db.exists("Consumption Workout Child", self.name):
				doc = frappe.get_doc("Consumption Workout Child",self.name)
			else:
				doc = frappe.new_doc("Consumption Workout Child")
			doc.set("consumption_data" , self.measure_info)
			doc.set("parent" , self.name)
			doc.set("parenttype" , "Consumption Workout")
			doc.save()


@frappe.whitelist()
def order_query(name):
	return frappe.db.get_values("Consumption Workout Child", {"parenttype": "Consumption Workout", "parent": name}, "consumption_data")

@frappe.whitelist()
def csheet_query(ref_nm):
	if ref_nm:
		# return filters
		sql = frappe.db.get_value("Consumption Sheet", {"reference":ref_nm}, ["name"])

		return sql
	else :
		return []

@frappe.whitelist()
def csheet_size(it_nm,cns_it):
	if it_nm and cns_it:
		# return filters
		sql = frappe.db.get_value("Consumption Sheet Child", {"parent":cns_it,"item":it_nm}, ["size","consump_type","dia_width"])

		return sql
	else :
		return []
