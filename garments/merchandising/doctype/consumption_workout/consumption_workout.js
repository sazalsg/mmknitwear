cur_frm.add_fetch("style","buyer","buyer")

var totl_cols;

var col_list = [];





var rib_percent =0;

var is_edit = false;

var consump_type = false;

var dia_width = 'DIA';

function render_table(tab_row, sum_row ){


					// Main Section (1)
					var consump_list = "";
					// consump_list += '<div class="row" id="consump_list"><div class="col-xs-12 text-muted"></div><div>';

					// // Child Body Section (2)
					// consump_list += '<div class="form-column col-xs-12"><div class="frappe-control ui-front" data-fieldtype="Table" title=""><div data-fieldname="add_routine"><div class="form-grid">';

					// // Header Section (3:1)
					// consump_list += '<div class="grid-heading-row"><div class="grid-row"><div class="data-row row sortable-handle"><div class="col col-xs-2 grid-overflow-ellipsis grid-static-col" align="center" data-fieldname="size" style="">SIZE :</div>';

					consump_list += '<div class="table-responsive"><table class="table table-hover table-bordered"><thead>';


				    // consump_list += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" style="">';

			    	consump_list += '<tr class="grid-heading-row"><td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size" style="font-size: 13px;padding:8px;">SIZE :</td>';



				 

					var size_head = '';


					for( var x in col_list){

						var hd = '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size_'+x+'" style="font-size: 13px;padding:8px;">'

						size_head +=  hd + col_list[x] +'</td>';

						//consump_list +=

					}

					   consump_list += size_head+'</tr></thead>';


					// consump_list += size_head+'</div></div></div></div>';
				
					// --- Header Section (3:1)

					// Rows Section (3:2)


					// // Main Row Section (4)
					
						var rows = rowRet();


					 consump_list += '<tbody>';

				
					consump_list += drawRow(rows,tab_row,'transparent');



					

					consump_list += drawRow1(['CONSUMPTION :','RIB :'],sum_row,'#669999');


					// consump_list += '</div></div></div></div>';
					// // --- Child Body Section (2)

					consump_list += '</tbody></table>';




					 setTimeout(function(){ $("body").find("#size_info").html(consump_list); 

					 		$('body').find('#0-2').trigger('change');

					 		if(!is_edit){

					 			cur_frm.set_value("avg_consumption", 0.00 );
								cur_frm.set_value("avg_rib", 0.00 );
								cur_frm.set_value("rib_dia", 0.00 );

					 		} else{

					 			is_edit = false;
					 		}
					 		

							make_json();

							// cur_frm.set_value("buyer", "" );
							// cur_frm.set_value("date", "" );
							// cur_frm.set_value("item", "" );
							// cur_frm.set_value("style", "" );
							

					}, 100);
					



					


}



$.extend(cur_frm.cscript, {
	onload: function(doc) {

		

	},
	refresh: function(doc,doctype,docname) {

		this.frm.set_query("size",function () {
			return {
				filters:[
					["Size Format","docstatus","=",1],
					
				]
			}
		})


		if(!doc.__islocal){


			if( doc.consumer_sheet){

			cur_frm.set_query("item",  function(doc, dt, dn)  {
		
			
				return{
					query: "garments.merchandising.doctype.consumption_sheet.consumption_sheet.get_items_lst",
					filters: {'cs_nm': doc.consumer_sheet}
				}


		  	});
		}else{

				

				cur_frm.set_query("item",function () {
					return {
					}
				});
		}


			frappe.call({
				method: "garments.merchandising.doctype.consumption_workout.consumption_workout.order_query",
				async:false,
				cache:false,
				args:{
					"name" : doc.name,
				},
				callback: function(r) {
					console.log(r)
				
				data = JSON.parse( r.message[0][0] );


				totl_cols = data["all_cols"].length;

				col_list = data["all_cols"];




					var tab_row = [];

					var sum_row = [];

					var rows = rowRet();

					for( var x in rows){

						tab_row[x] = data[rows[x]];

						
					}


					var rows = ['CONSUMPTION :','RIB :']

					for( var x in rows ){

						sum_row[x] = data[rows[x]];

						
					}

					is_edit = true;

				
					render_table(tab_row, sum_row );

					



				 }

			})

		}	
}});


var sheet_signal = false;

cur_frm.cscript.consumer_sheet = function(doc,doctype,docname) {
	

	cur_frm.set_value("item", "" );
		cur_frm.set_value("size", "" );



	if( doc.consumer_sheet){

			cur_frm.set_query("item",  function(doc, dt, dn)  {
		
			
				return{
					query: "garments.merchandising.doctype.consumption_sheet.consumption_sheet.get_items_lst",
					filters: {'cs_nm': doc.consumer_sheet}
				}


		  	});
	}else{

		cur_frm.set_query("item",function () {
			return {
				
			}
		});
	}
	

}

function reset_table(){


	
		setTimeout(function(){ 

			$("body").find("#size_info").html("");

		});

		cur_frm.doc['measure_info']= null;

		cur_frm.doc['dia_width']= 'DIA';


		
}


// cur_frm.cscript.size = function(doc,doctype,docname) {


// 		sheet_signal = true;
// 		drawSheet(doc,doctype,docname);


// }

function drawStart(doc,doctype,docname){
	
	var pro_name = doc.consumer_sheet ? doc.consumer_sheet: "";
	var it = doc.item ? doc.item : "";
	var sz = doc.size ? doc.size : "";

	//if(pro_name){

			frappe.call({

				cache:false,
				method:"garments.merchandising.doctype.consumption_sheet.consumption_sheet.sheet_by_name",
				args:{
					prt:pro_name,
					itm:it,
					siz:sz
				},
				callback:function (data, doc1) {

					if(jQuery.isEmptyObject(data)){

						drawNew(doc,doctype,docname)
					}else{

						data = JSON.parse(data.message);

						drawRowBySheetName(data);
					}
					
				}
			});


	// }else{
	// 	// setLocal.total_amount_discount = 0;
	// 	// refresh_field("total_amount_discount", setLocal.name, setLocal.parentfield);
	// } 		


}

function setfilds(info){

	//["buyer","date","item","style","size"]

	cur_frm.set_value("buyer", info[0] );
	cur_frm.set_value("date", info[1] );
	cur_frm.set_value("item", info[2] );
	cur_frm.set_value("style", info[3] );
	cur_frm.set_value("size", info[4] );


}


function drawRowBySheetName(data){


	
	col_list =[];

	var vals = {};

	for( var x in data.header.format_child){

		 col_list.push(data.header.format_child[x].size );

	}


	var indx = [0,3,5];

	for( var x in data.body.body){

		var sing = data.body.body[x].format_child;

			var temp = [];

			for( var j in sing){


				if(x==1 && consump_type == 'Tube'){

					temp.push( parseFloat(sing[j].val));


				}else{

					temp.push( sing[j].val );

				}



			}


			vals[indx[x]] = temp


	}

					totl_cols = col_list.length;

					var tab_row = [];

					var sum_row = [];


					var rows = rowRet();


					for( var x in rows){

						tab_row[x] = [];

						if(x==0 || x==3 || x==5){

							tab_row[x] = vals[x];

						}else{

							for( var i in col_list){
								tab_row[x][i] = 0;
							}

						}

						
					}


					for( var x in [0,1] ){

						sum_row[x] = [];

						for( var i in col_list){
							sum_row[x][i] = 0;

						}
					}

					render_table(tab_row, sum_row);


}

function drawRowBySheet(cols){


	//msgprint(cols);

	cols = JSON.parse(cols);

	if(cur_frm.doc['dia_width']=='DIA'){


		unit = ['Cm','','','Inch','Gm','Pcs','%'];
	}else{

		unit = ['Cm','','','Yard','Gm','Pcs','%'];

	}	

		// var hd = '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size_'+x+'" style=" padding:0px;">';

	if(cur_frm.doc['dia_width']=='DIA'){
	
	var rows = ['BODY LEN :','DIA :','DOZ/PCS :'];

	var row_pos = {'BODY LEN :' : 0 , 'DIA :' : 3 , 'DOZ/PCS :' : 5}	

}else{

	var rows = ['BODY LEN :','WIDTH :','DOZ/PCS :'];

	var row_pos = {'BODY LEN :' : 0 , 'WIDTH :' : 3 , 'DOZ/PCS :' : 5}	

}

	var consump_list = '';

					for(st in rows){

						// consump_list += '<div class="grid-body"><div class="rows">';
						// consump_list += '<div style="background:'+ color+'" class="grid-row" data-idx="1"><div class="data-row row"><div class="col col-xs-2 grid-overflow-ellipsis grid-static-col" align="right" data-fieldname="student_id">'+rows[st]+'</div>';

					//consump_list += '<tr id="'+st+'">';							

				    // consump_list += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" style="">';

				    consump_list += '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+rows[st]+
				    '</td>';

				   

				 //    console.log(rows[st]);

					// console.log(row_pos[rows[st]]);				    

				 //    console.log(cols[rows[st]]);

				 //    console.log(unit[row_pos[rows[st]]]);

				 //    console.log(row_pos[rows[st]]);

				    var pos = row_pos[rows[st]];


					consump_list += drawCol(cols[rows[st]],unit[pos],pos);	


						 (function  (pos,consump_list) {
						 		setTimeout(function(){ $("body").find("#row-"+pos).html(consump_list); }, 100);

						 })(pos,consump_list);

						 consump_list = '';

					}


					setTimeout(function(){ $("body").find('#0-2').trigger('change'); }, 100);	

					
					



return	consump_list;

}


cur_frm.cscript.size = function(doc,doctype,docname){

		reset_table();


	drawStart(doc,doctype,docname);
}


function drawNew(doc,doctype,docname){

	console.log('eeee');


	var pro_name = doc.size;

	if(pro_name && !sheet_signal){


			frappe.call({

				cache:false,

				method:"garments.merchandising.doctype.size_format.size_format.size_info",
				args:{
					
					name:pro_name
				},
				callback:function (data, doc1) {

					//json_out = [];



					totl_cols = data.message.length;

					col_list = data.message;

					for( var x in data.message){


						col_list[x] = data.message[x][0];
					}

					var tab_row = [];

					var sum_row = [];

					console.log( 'sss' );
					console.log( col_list );

					var rows = rowRet();


					for( var x in rows){

						tab_row[x] = [];

						for( var i in col_list){
							tab_row[x][i] = 0;

						}
					}


					for( var x in [0,1] ){

						sum_row[x] = [];

						for( var i in col_list){
							sum_row[x][i] = 0;

						}
					}


					console.log(tab_row);

					render_table(tab_row, sum_row);

					//cur_frm.set_value("consumer_sheet", "" );


					//make_json();

					// }
					
				}
			});

			//cur_frm.set_df_property("consumer_sheet", "hidden", false);


	}else{

			sheet_signal = false;


		//cur_frm.set_df_property("consumer_sheet", "hidden", true);
		// setLocal.total_amount_discount = 0;
		// refresh_field("total_amount_discount", setLocal.name, setLocal.parentfield);
	} 		

}


cur_frm.cscript.style = function(doc,doctype,docname){

	var pro_name = doc.style;

	if(pro_name){

			frappe.call({
				cache:false,
				method:"garments.merchandising.doctype.consumption_workout.consumption_workout.csheet_query",
				args:{
					
					ref_nm:pro_name
				},
				callback:function (data, doc1) {


					if(data){
						cur_frm.set_value("consumer_sheet", data.message );


					}
					//render_table(tab_row, sum_row);
					
				}
			});
	}	
}

cur_frm.cscript.item = function(doc,doctype,docname){

	var pro_name = doc.item;
	var cns_sheet = doc.consumer_sheet;

	if(pro_name && cns_sheet){

			frappe.call({
				cache:false,
				method:"garments.merchandising.doctype.consumption_workout.consumption_workout.csheet_size",
				args:{
					
					it_nm:pro_name,
					cns_it:cns_sheet
				},
				callback:function (data, doc1) {

					if(data){
						cur_frm.set_value("size", data.message[0] );

						consump_type = data.message[1];

						cur_frm.doc['dia_width']= data.message[2];



						
						cur_frm.set_value("cons_type", consump_type);

						drawStart(doc,doctype,docname);

					}
					
				}
			});
	}	
}



function drawRow(rows,cols,color){

	if(cur_frm.doc['dia_width']=='DIA'){


		unit = ['Cm','','','Inch','Gm','Pcs','%'];
	}else{


	unit = ['Cm','','','Yard','Gm','Pcs','%'];

	}

	var consump_list = '';

					for(st in rows){

					consump_list += '<tr id="row-'+st+'">';							

				    consump_list += '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+rows[st]+
				    '</td>';


					consump_list += drawCol(cols[st],unit[st],st);	


					consump_list += '</tr>';

					
					}
					



return	consump_list;

}


function drawRow1(rows,cols,color){


color = ['#6CA7A7','#669999'];

if(cur_frm.doc["dia_width"] == "DIA"){

	unit = ['Kg','Kg'];


}

if(cur_frm.doc["dia_width"] == "WIDTH"){

	unit = ['Yard','Yard'];
}



	var consump_list = '';

					for(st in rows){

					

					consump_list += '<tr>';							
				    consump_list += '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+rows[st]+
				    '</td>';


					consump_list += drawCol1(cols[st],unit[st],st);	


					consump_list += '</tr>';
					}
					



return	consump_list;

}



function drawCol1(cols,unit,row_num){

var consump_list = '';


var cls = 'cons-all cons_col-';

if(row_num==1){

	cls = 'rib-all rib_col-';

}

	for (var i in cols){

						console.log('chk' + cls);

						//if(i==0){

					var inp = '<input  type="text" class="form-control setMarks col-xs-12 '+cls+i+'" style="font-size: 13px;padding:1px 5px;" size="15" name="'+st+i+'" id="'+st+'-'+i+'"  value="'+cols[i]+'" disabled="disabled">';


							lbl = '';
							//if(i==0){

								lbl = '<label class="setMarks col-xs-12">'+unit+'</label>';
							//}
						

				    consump_list += '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+inp+lbl+
				    '</td>';
		}



	return consump_list;
}





function drawCol(cols,unit,row_num){

var consump_list = '';


var cls = 'col-';

if(row_num==5){

	cls = 'doz_col-';

}

st = row_num;

	for (var i in cols){

						if(i==0){

							var inp = '<input type="text" class="form-control setMarks col-xs-12 input_row_first input_row '+cls+i+'" style="padding:1px 5px;" size="15" name="'+st+i+'" value="'+cols[i]+'" id="'+st+'-'+i+'">';
						} else{


								var inp = '<input type="text" class="form-control setMarks col-xs-12 input_row '+cls+i+'" style="padding:1px 5px;" size="15" name="'+st+i+'" value="'+cols[i]+'" id="'+st+'-'+i+'">';
						}

							lbl = '';
							//if(i==0){

								lbl = '<label class="setMarks col-xs-12">'+unit+'</label>';
							

				    consump_list += '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+inp+lbl+
				    '</td>';

		}



	return consump_list;
}


var json_out = {};

function make_json(){
	buildJSON();
	json_out['all_cols'] = col_list;


	cur_frm.doc['measure_info']= JSON.stringify(json_out);



}

function confirmation(obj) {
	var answer = confirm("Set same Value To All Columns?")
	if (answer){
		$(obj).parent().siblings().find('input').val($(obj).val());
	}
}


$('body').on('change','.input_row_first',function(e){
	
	confirmation(this);

	$(this).parent().siblings().find('input').trigger('change');
	
	
	make_json();


});



function json_helper(clsnm,rows){

	var idx = 0;

	var track = [];


	$('.'+clsnm).each(function(index){

		var r_c = this.id.split('-');

		var r = r_c[0];
		var c = r_c[1];

		var key = rows[r];

		


			if( key in json_out){


				json_out[key][c] = $(this).val() ;

				//idx++;

			}else{

				json_out[key] = [];


				json_out[key][c] = $(this).val() ;	

			}

		 		

	});

}

function rowRet(){
	 if(cur_frm.doc['dia_width']=='DIA'){

	 	var rows = ['BODY LEN :','SLEEVE LEN :', 'ALOWANCE :', 'DIA :','GSM :','DOZ/PCS :','WASTAGE :'];



	 }else{
	 	var rows = ['BODY LEN :','SLEEVE LEN :', 'ALOWANCE :', 'WIDTH :','GSM :','DOZ/PCS :','WASTAGE :'];


	 }

		

	return rows;
}

function buildJSON(){

	var rows = rowRet();
	json_out = {};

	json_helper('input_row',rows);

	console.log(json_out);


	var rows = ['CONSUMPTION :','RIB :']

	json_helper('cons-all',['CONSUMPTION :']);

	json_helper('rib-all',['','RIB :']);

}




$('body').on('change','.input_row',function(e){


	var col_indx = this.id.split('-')[1];

	var tst = $('.col-'+col_indx);


	var sum = calcCol('col-'+col_indx);

	$('.cons_col-'+col_indx).val(sum);

	$('.rib_col-'+col_indx).val(calcPercent(sum));


	calcAvgConRib();

	make_json();

});



cur_frm.cscript.rib_dia = function(doc,doctype,docname){

	rib_percent = doc.rib_dia;

	$('.cons-all').each(function( index ) {

		
		var val = $( this ).val();

		if(val){

			var col_indx = this.id.split('-')[1];

			$('.rib_col-' + col_indx).val(calcPercent(val));

		}

	});

	calcAvgConRib();

	make_json();



}

function calcCol(clsnm){

	var sum = 0;

	var arr = [];

	$('.'+clsnm).each(function( index ) {
		
		var val = $( this ).val();

		if(val){

			arr.push( parseFloat(val) );			

		} else{

			arr.push( 0 );

		}

	});

	if(cur_frm.doc["dia_width"]=="WIDTH"){

		sum = (arr[0] / 2.54)/arr[3];
		
	}


	if(cur_frm.doc["dia_width"]=="DIA"){



		sum = ((arr[0] + arr[1] + arr[2]) * (arr[3]*2.54) * (arr[4]*2)) / 10000000 ;

		sum = sum + (sum*arr[5])/100; 
	}


	

	

	return sum.toFixed(4);


}


function calcPercent(val){


	var per = rib_percent;

	if(val && per){

		val = parseFloat(val);

		per = parseFloat(per);

		val = val - (val*per)/100;

		return val.toFixed(4);

	} else{

		return 0;
	}

}


function calcAvgConRib(){

	cur_frm.set_value("avg_consumption", (sumAllRow('cons-all')/totl_cols).toFixed(4) );
	cur_frm.set_value("avg_rib", (sumAllRow('rib-all')/totl_cols).toFixed(4) );
}


function sumAllRow(clsnm){

		var sum = 0;

		$('.'+clsnm).each(function( index ) {

		
		var val = $( this ).val();

		if(val){

			sum += parseFloat(val);

		}

	});

	return sum.toFixed(4);	



}