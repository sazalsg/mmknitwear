var totl_cols;

var col_list = [];

var rib_percent =0;

var fixed_cols = ['TOTAL BODY','RIB 27',"TOTAL BODY<br />+ RIB"];

var total_row = 0;

var total_row_dis = 0;

var current_table = '';

frappe.ui.form.on("Fabric Order", "after_save", function(frm) {

	msgprint('Saving ...');
	location.reload();
});

var custom_col_range = [];

var color_list = [];


function render_table(tab_row, sum_row,tab_id,summation_row ){


					// Main Section (1)
					var consump_list = "";
					// consump_list += '<div class="row" id="consump_list"><div class="col-xs-12 text-muted"></div><div>';

					// // Child Body Section (2)
					// consump_list += '<div class="form-column col-xs-12"><div class="frappe-control ui-front" data-fieldtype="Table" title=""><div data-fieldname="add_routine"><div class="form-grid">';

					// // Header Section (3:1)
					// consump_list += '<div class="grid-heading-row"><div class="grid-row"><div class="data-row row sortable-handle"><div class="col col-xs-2 grid-overflow-ellipsis grid-static-col" align="center" data-fieldname="size" style="">SIZE :</div>';

					consump_list += '<div class="table-responsive"><table class="table table-hover table-bordered"><thead>';


				    // consump_list += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" style="">';

			    	consump_list += '<tr class="grid-heading-row"><td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size" style="font-size: 13px;padding:8px;">COLOR/DIA :</td>';



				 

					var size_head = '';


					for( var x in col_list){

						var hd = '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size_'+x+'" style="font-size: 13px;padding:8px;">'

						size_head +=  hd + col_list[x] +'</td>';

						//consump_list +=



					}


					for( var x in fixed_cols){

						var hd = '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size_'+x+'" style="font-size: 13px;padding:8px;">'

						size_head +=  hd + fixed_cols[x] +'</td>';

						//consump_list +=

					}

					   consump_list += size_head+'</tr></thead>';


					// consump_list += size_head+'</div></div></div></div>';
				
					// --- Header Section (3:1)

					// Rows Section (3:2)


					// // Main Row Section (4)


					
					//var rows = [0];

					consump_list += '<tbody>';

					current_table = tab_id;
				
					consump_list += drawRow(sum_row,tab_row,'transparent');

					consump_list += drawSumRow(summation_row);



					

					//consump_list += drawRow1(['CONSUMPTION :','RIB :'],sum_row,'#669999');


					// consump_list += '</div></div></div></div>';
					// // --- Child Body Section (2)

					consump_list += '</tbody></table>';

					if(tab_id == 'fin_fo_child'){


					consump_list += '<div><button class="btn btn-xs btn-default" id="fin_fo_child_row">AddRow</button></div>';

					}


					// // consump_list += r.message;
					// consump_list += '</div></div>';
					// // --- Main Section (1)


					//console.log(consump_list);


					 setTimeout(function(){ $("body").find("#"+ tab_id).html(consump_list); 

					 		//$('body').find('#0-2').trigger('change');

					 	// 	cur_frm.set_value("avg_consumption", 0.00 );
							// cur_frm.set_value("avg_rib", 0.00 );

							// cur_frm.set_value("rib_dia", 0.00 );

							// cur_frm.set_value("buyer", "" );
							// cur_frm.set_value("date", "" );
							// cur_frm.set_value("item", "" );
							// cur_frm.set_value("style", "" );
							

					}, 100);
					



					


}



$.extend(cur_frm.cscript, {
	onload: function(doc) {

		

	},
	refresh: function(doc,doctype,docname) {

		this.frm.set_query("fabric_size",function () {
			return {
				filters:[
					["Fabric Size Format","docstatus","=",1],
					
				]
			}
		})

		frappe.call({
				method: "garments.merchandising.doctype.color.color.color_info",
				async:false,
				cache:false,
				args:{
					"name" : doc.name,
				},
				callback: function(r) {
				
				r = r.message;

				

				//color_list += '<select>';


				for(var k in r){

					console.log( r[k] );

					color_list.push(r[k].name);

					// color_list += '<option value="'+r[k]["name"]+'">' +r[k].name+'</option>'; 
				}
				//color_list += '</select>';

		}});	


		if(!doc.__islocal){

			frappe.call({
				method: "garments.merchandising.doctype.fabric_order.fabric_order.order_query",
				async:false,
				cache:false,
				args:{
					"name" : doc.name,
				},
				callback: function(r) {
				
				data = JSON.parse( r.message[0][0] );

				console.log( data );


				// totl_cols = data["all_cols"].length;

				// col_list = data["all_cols"];

				tot_row = data.total_row;

				col_list = data.all_cols;

				var cut_rng = 1;

					for(var j in col_list){

						custom_col_range.push(cut_rng);
						cut_rng = cut_rng +1;

					}


				//console.log(data);


					var tab_row = [];

					var tab_row_dis = [];
					var sum_row = [];

					
					for (var i = 1; i <= tot_row; i++) {

						//console.log( data['input_row-' + i] );
						
						tab_row[i-1] = data['input_row-' + i];
						sum_row.push(i-1);

					};


					for (var i = 1; i <= tot_row; i++) {

						//console.log( data['input_row-' + i] );
						
						tab_row_dis[i-1] = data['input_row_dis-' + i];
						//sum_row.push(i-1);

					};

				

					console.log( tab_row );

					console.log(tab_row_dis);

					console.log( sum_row );

					current_table = 'fin_fo_child';

					var tdis = data["total_row-total_row"] ;

				
					render_table(tab_row,sum_row ,'fin_fo_child',tdis );

					current_table = 'knit_fo_child';

					var tdis = data["total_row_dis-total_row_dis"] ;

					render_table(tab_row_dis, sum_row,'knit_fo_child',tdis);					



				 }

			})

		}	
}});


var sheet_signal = false;




cur_frm.cscript.fabric_size = function(doc,doctype,docname){

	var pro_name = doc.fabric_size;

	if(pro_name && !sheet_signal){


			frappe.call({

				cache:false,

				method:"garments.merchandising.doctype.fabric_size_format.fabric_size_format.size_info",
				args:{
					
					name:pro_name
				},
				callback:function (data, doc1) {

					totl_cols = data.message.length;

					col_list = data.message;

					for( var x in data.message){


						col_list[x] = data.message[x][0];
					}

					var cut_rng = 1;

					for(var j in col_list){

						custom_col_range.push(cut_rng);
						cut_rng = cut_rng +1;

					}



					var tab_row = [];

					var sum_row = [];

					var summation_row = [];




					var rows = [0];

					//total_row = total_row +1;

				    var count = true;

					for( var x in rows){

						tab_row[x] = [];

						tab_row[x].push( 0 );

						for( var i in col_list){
							tab_row[x].push( 0 );

						}

						for( var i in fixed_cols){
							//tab_row[x][i] = 0;

							tab_row[x].push( 0 );

						}

						if(count){

							summation_row = tab_row[x];

							count = false;	
						}
					}

					console.log( tab_row);


					// for( var x in [0,1] ){

					// 	sum_row[x] = [];

					// 	for( var i in col_list){
					// 		sum_row[x][i] = 0;

					// 	}
					// }

					current_table = 'fin_fo_child';
					
					render_table(tab_row, [0],'fin_fo_child',summation_row);

					current_table = 'knit_fo_child';
					
					render_table(tab_row, [0],'knit_fo_child',summation_row);

					//cur_frm.set_value("consumer_sheet", "" );


					// }
					
				}
			});

			//cur_frm.set_df_property("consumer_sheet", "hidden", false);


	}else{

			sheet_signal = false;


		//cur_frm.set_df_property("consumer_sheet", "hidden", true);
		// setLocal.total_amount_discount = 0;
		// refresh_field("total_amount_discount", setLocal.name, setLocal.parentfield);
	} 		


}





function drawRow(rows,cols,color){


	unit = ['','','','','','',''];

		

	var consump_list = '';


					var tot_r = 0;
					for(st in rows){

						if(current_table == 'fin_fo_child'){


							total_row = total_row + 1;

							tot_r = total_row;	

						} else
						{

							total_row_dis = total_row_dis +1;

							tot_r = total_row_dis;
						}


					consump_list += '<tr id="row-'+tot_r+'">';		


					consump_list += drawCol(cols[st],unit[st],st);	


					consump_list += '</tr>';


					}
					



return	consump_list;

}


function drawSumRow(sum_cols){

	var consump_list = '';

					consump_list += "<tr class='total_row'>";		
				    for(var st in sum_cols){

						consump_list += drawSumCol(sum_cols[st],st);	

					}

					consump_list += '</tr>';
										
return	consump_list;

}



function drawSumCol(val,pos){


	var tt_r = '';

	if( current_table == 'fin_fo_child' ){

		tt_r = 'total_row';

	}else{

		tt_r = 'total_row_dis';
	}


						if(pos==0){

							val = 'Total:';
						}

							var inp = '<input  type="text" class="form-control setMarks col-xs-12 total-'+pos+' '+tt_r+'" style="font-size: 13px;padding:1px 5px;" size="15" name="'+pos+'" id="'+tt_r+'-'+pos+'"  value="'+val+'" disabled="disabled">';



							lbl = '';
							

				    var consump_list = '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+inp+lbl+
				    '</td>';



	return consump_list;
}





function drawCol(cols,unit,row_num){


//console.log( cols );

var consump_list = '';


var cls = '';

if(row_num==5){

	//cls = 'doz_col-';

}

st = row_num;

//var cls = 'col-';

var tot_r = 0;
var disabled = 'disabled';
if(current_table== 'fin_fo_child'){

	cls = 'input_row';
	disabled = '';

	tot_r = total_row;

}else{

		cls = 'input_row_dis';

		tot_r = total_row_dis;


}


var tot_ts = custom_col_range[custom_col_range.length-1] + 1;

var tot_rs = custom_col_range[custom_col_range.length-1] + 3; 

	var temp = disabled;

	for (var i in cols){



		if( i == tot_ts || i == tot_rs){

			disabled = 'disabled';

		}

						if(i==0){

							console.log( color_list );

							var inp = '<select  class="form-control setMarks col-xs-12 input_row_first cls-'+i+' '+cls+'" name="'+st+i+'"'+ disabled +' id="'+tot_r+'-'+i+'">';

							for( var z in color_list){

								if(cols[i]==color_list[z]){

									inp += '<option selected value="'+color_list[z]+'">' +color_list[z]+'</option>'; 

								}else{

									inp += '<option value="'+color_list[z]+'">' +color_list[z]+'</option>'; 

								}

								
							}

							
							//inp += color_list;

							inp += '</select>';
						} else{


								var inp = '<input type="text" class="form-control setMarks col-xs-12 cls-'+i+' '+cls+'" style="padding:1px 5px;" size="15" name="'+st+i+'" value="'+cols[i]+'" '+ disabled +' id="'+tot_r+'-'+i+'">';
						}

							lbl = '';
							//if(i==0){

								// lbl = '<label class="setMarks col-xs-12">'+unit+'</label>';
							//}
						

							// consump_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col" data-fieldname="studnet_name" style="padding:0px;">'+inp+lbl+'</div>';


				    consump_list += '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+inp+lbl+
				    '</td>';


		 disabled = temp;


		}



	return consump_list;
}


var json_out = {};

function make_json(){
	buildJSON();
	json_out['all_cols'] = col_list;
	json_out['total_row'] = total_row;
	var js_out = JSON.stringify(json_out);
	cur_frm.doc['measure_info']= js_out;

	// if( !cur_frm.doc.__islocal ){

	// frappe.call({
	// 			method: "garments.merchandising.doctype.fabric_order.fabric_order.fabric_save",
	// 			async:false,
	// 			cache:false,
	// 			args:{
	// 				"name" : cur_frm.doc["name"],
	// 				"measure_info" : json_out
	// 			},
	// 			callback: function(r) {
	// 				// console.log(r);
	// 				// console.log( cur_frm.doc["name"] );


	// 				console.log( js_out );
				
	// 			//data = JSON.parse( r.message[0][0] );
	// 		}
	// 	});

	//}


	console.log(js_out);

	//console.log( JSON.stringify(json_out) );

}

function confirmation(obj) {
	var answer = confirm("Set same Value To All Columns?")
	if (answer){
		$(obj).parent().siblings().find('input').val($(obj).val());
	}
}


$('body').on('change','.input_row',function(e){
	
	
	//confirmation(this);	

	//$(this).parent().siblings().find('input').trigger('change');
	
	var vid = $(this).attr('id');

	var r_c = vid.split('-');

		var r = r_c[0];
		var c = r_c[1];

		


	var vl = parseFloat($(this).val());

	var incl = parseFloat( cur_frm.doc["include_percent"] );


	if( c!= 0){

		vl = (vl * incl) / 100  + vl; 

	}

	//console.log( vid +'::'+ vl );

	$('#knit_fo_child').find('input[id="' + vid + '"]').val(vl);

	count_sum(this);

	make_json();


});


function count_sum(obj){

		//$(obj)


		var r_c = obj.id.split('-');

		var r = parseInt( r_c[0] );
		var c = parseInt(r_c[1]);

		if( c!= 0){

			//custom_col_range

			col_sum(c);
			//console.log( custom_col_range );
			// console.log( custom_col_range.indexOf(c) > -1 );

			if( custom_col_range.indexOf(c) > -1 ){

				//console.log( 'here ....' );

				calc_total_body('fin_fo_child',r,c);
				calc_total_body('knit_fo_child',r,c);
			}else{

				var temp = c;

				var x = parseInt($('#fin_fo_child').find('#'+r+'-'+c).val());

				c = c-1;	
				var y = parseInt($('#fin_fo_child').find('#'+r+'-'+c).val()); 
				c = c+2;
				$('#fin_fo_child').find('#'+r+'-'+c).val(x+y);

				c = temp; 


				var x = parseInt($('#knit_fo_child').find('#'+r+'-'+c).val());

				c = c-1;	
				var y = parseInt($('#knit_fo_child').find('#'+r+'-'+c).val()); 
				c = c+2;
				$('#knit_fo_child').find('#'+r+'-'+c).val(x+y);



			}
			

		} else{

			var x = $('#fin_fo_child').find('#'+r+'-'+c).val();

			$('#knit_fo_child').find('#'+r+'-'+c).val(x);
		}

}

function col_sum(c){

	var total = 0;

			$('#fin_fo_child').find('.cls-'+c).each(function(index,elem){


					total += parseFloat($(elem).val());

			});

			total = total.toFixed(2);


			$('#fin_fo_child').find('.total-'+c).val(total);

			total  = 0;


			$('#knit_fo_child').find('.cls-'+c).each(function(index,elem){


					total += parseFloat($(elem).val());

			});

			total = total.toFixed(2);


			$('#knit_fo_child').find('.total-'+c).val(total);
			
}

function calc_total_body(tab_id,rw,cl){


	var total = 0;

	for( var z in custom_col_range ){

		var rg = custom_col_range[z];

		var vl = $('#'+tab_id).find('#'+rw+'-'+rg).val();

		//console.log( vl );

		total += parseFloat(vl);
	}

	total = total.toFixed(2);

	var lst = parseFloat(custom_col_range[custom_col_range.length -1])+1;

	$('#'+tab_id).find('#'+rw+'-'+lst ).val( total );


		col_sum(lst);


	lst = lst +1 ;

	 var rb = parseFloat($('#'+tab_id).find('#'+rw+'-'+lst ).val());

	lst = lst +1 ;

	var cp = parseFloat(total)+ parseFloat(rb);
	cp = cp.toFixed(2);
	//console.log( cp );

	 $('#'+tab_id).find('#'+rw+'-'+lst ).val( cp );

	 col_sum(lst);

}






function json_helper(clsnm,rows){

	var idx = 0;

	var track = [];


	$('.'+clsnm).each(function(index,elem){

		var r_c = elem.id.split('-');

		var r = r_c[0];
		var c = r_c[1];

		var key = clsnm +'-' + r;

		console.log( key );

			if( key in json_out){


				json_out[key][c] = $(this).val() ;

				//idx++;

			}else{

				json_out[key] = [];


				json_out[key][c] = $(this).val() ;	

			}

		 		

	});

}

function buildJSON(){


	// var rows = ['BODY LEN :','SLEEVE LEN :', 'ALOWANCE :', 'DIA :','GSM :','DOZ/PCS :','WASTAGE :'];

	json_helper('input_row');

	json_helper('input_row_dis');

	json_helper('total_row');
	json_helper('total_row_dis');

	//console.log(json_out);

	// var rows = ['CONSUMPTION :','RIB :']

	// json_helper('cons-all',['CONSUMPTION :']);

	json_helper('rib-all',['','RIB :']);

}


$('body').on('click','#fin_fo_child_row',function(e){

	var tbd = $(this).parent().prev().find('tbody');

	//console.log();

						// consump_list += drawRow(rows,tab_row,'transparent');

	//total_row = total_row +1;					

	var row = [0];
	var tab_row =[]; 

	for(var x in row){

		tab_row[x] = [];
		tab_row[x].push(0);

		for(var i in col_list){

			tab_row[x].push(0);
		}

		for(var i in fixed_cols){

			tab_row[x].push(0);
		}	
	

	}

	current_table = 'fin_fo_child';

	var arow = drawRow(row,tab_row,'transparent');


	current_table = 'knit_fo_child';

	var brow = drawRow(row,tab_row,'transparent');


	setTimeout(function(){

		$(tbd).find('tr:last').before(arow);
		$('#knit_fo_child').find('table tbody tr:last').before(brow);

		make_json();


	}, 100);

	// render_table(tab_row, [],'fin_fo_child');
	// render_table(tab_row, [],'knit_fo_child');




});


