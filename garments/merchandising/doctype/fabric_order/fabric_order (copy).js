cur_frm.add_fetch("fo_child_item","description","clr_dia")


cur_frm.cscript.mbr = function(doc,doctype,docname){


		calcTotal(doc,doctype,docname);

		sumTotal(doc,doctype,docname);
}

cur_frm.cscript.mmb = function(doc,doctype,docname){
	

	calcTotal(doc,doctype,docname);

	sumTotal(doc,doctype,docname);

}

cur_frm.cscript.min = function(doc,doctype,docname){
	

	calcTotal(doc,doctype,docname);

	sumTotal(doc,doctype,docname);

}

cur_frm.cscript.bul = function(doc,doctype,docname){
	

	calcTotal(doc,doctype,docname);

	sumTotal(doc,doctype,docname);

}

cur_frm.cscript.rib = function(doc,doctype,docname){
	

	calcTotal(doc,doctype,docname);

	sumTotal(doc,doctype,docname);

}

function sumTotal(doc,doctype,docname){

		var mbr_total = 0;

		var mmb_total = 0;

		var min_total = 0;

		var bul_total = 0;

		var total_body = 0;

		var total_body_rib = 0;

			for(var key in doc.knit_fo_child){

				if(parseFloat(doc.knit_fo_child[key].mbr)){
					mbr_total+=(parseFloat(doc.knit_fo_child[key].mbr));
				}

				if(parseFloat(doc.knit_fo_child[key].mmb)){
					mmb_total+=(parseFloat(doc.knit_fo_child[key].mmb));
				}

				if(parseFloat(doc.knit_fo_child[key].min)){
					min_total+=(parseFloat(doc.knit_fo_child[key].min));
				}

				if(parseFloat(doc.knit_fo_child[key].bul)){
					bul_total+=(parseFloat(doc.knit_fo_child[key].bul));
				}

				if(parseFloat(doc.knit_fo_child[key].t_body)){
					total_body+=(parseFloat(doc.knit_fo_child[key].t_body));
				}

				if(parseFloat(doc.knit_fo_child[key].bul)){
					total_body_rib+=(parseFloat(doc.knit_fo_child[key].total));
				}
			}

			cur_frm.set_value("mbr_total", mbr_total);
			cur_frm.set_value("mmb_total", mmb_total);
			cur_frm.set_value("min_total", min_total);
			cur_frm.set_value("bul_total", bul_total);

			cur_frm.set_value("total_body", total_body);
			cur_frm.set_value("total_body_rib", total_body_rib);
			
}

function calcTotal(doc,doctype,docname){

	var setLocal = locals[doctype][docname];

	var mbr = parseFloat(setLocal.mbr || 0);

	var mmb = parseFloat(setLocal.mmb || 0);

	var min = parseFloat(setLocal.min || 0);

	var bul = parseFloat(setLocal.bul || 0);

	var rib = parseFloat(setLocal.rib || 0);

		setLocal.t_body = mbr + mmb + min + bul;
		refresh_field("t_body", setLocal.name, setLocal.parentfield);

		setLocal.total =setLocal.t_body + rib;
		refresh_field("total", setLocal.name, setLocal.parentfield);
}