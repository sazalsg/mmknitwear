# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import msgprint, throw,_
from datetime import date
from frappe.model.naming import make_autoname
import time


class FabricOrder(Document):
	def autoname(self):
		cname=frappe.db.get_value("Customer", {"name":self.buyer}, ["customer_name"])
		self.name =  make_autoname('{0}-{1}-{2}{3}{4}-'.format("FBO",cname.split()[0], str(date.today().month), '/',str(date.today().year)[2:4] ) + ".#")

	def validate(self):
		pass

	def before_insert(self):
		if not hasattr(self, 'measure_info'):
			frappe.throw("Please Input Finish Fabric Quantity")


	# def after_insert(self):
	# 	doc = frappe.new_doc("Fabric Order Child")
	# 	doc.set("fabric_data" , self.measure_info)
	# 	doc.set("name" , self.name)
	# 	doc.set("parent" , self.name)
	# 	doc.set("parenttype" ,"Merchandising")
	# 	doc.save()

	
	def on_update(self):
		if hasattr(self, 'measure_info'):
			#msgprint(self.measure_info)
			if frappe.db.exists("Fabric Order Child", self.name):
				doc = frappe.get_doc("Fabric Order Child",self.name)
			else:
				doc = frappe.new_doc("Fabric Order Child")
			doc.set("fabric_data" , self.measure_info)
			doc.set("parent" , self.name)
			doc.set("parenttype" , "Fabric Order")
			doc.save()




@frappe.whitelist()
def order_query(name):

	if name :
		# return filters
		sql = frappe.db.get_values("Fabric Order Child", {"parenttype":"Fabric Order", "parent":name}, ["fabric_data"])

		return sql
	else :
		return []


@frappe.whitelist()
def fabric_save(name,measure_info):
	# # doc = frappe.get_doc("Fabric Order Child",name)
	# # doc.set("fabric_data" , measure_info)
	# # doc.save()
	# return doc	
	return []
