# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class StyleName(Document):
	pass


@frappe.whitelist()
def style_info():
	style=frappe.db.get_values("Style Name", {"name":("!=", "")}, ["style_name","style_name"],as_dict=1)
	return style

