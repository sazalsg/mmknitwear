frappe.provide("garments.merchandising");

garments.merchandising.StyleNameController = frappe.ui.form.Controller.extend({
	onload:function (doc) {

	},
	refresh:function(doc){
		var me = this
		if(!doc.__islocal){
			/*setTimeout(function(){
				me.frm.form_wrapper.find('[data-fieldname="img"]').removeClass('hide-control')
			}, 100)*/
			this.frm.add_custom_button(__("Pattern Cutting"),function () {
				me.new_doc(doc,"Pattern Cutting",{"reference":doc.name})
			})
			this.frm.add_custom_button(__("Size Set"),function () {
				// body...
			})
			this.frm.add_custom_button(__("Size Set QC"))
			this.frm.add_custom_button(__("Test Cutting"),function () {
				me.queary_repoty(doc,"Test Cutting Order",{"ss.style":doc.name})
			})
			this.frm.add_custom_button(__("Test Cutting QC"),function () {
				me.page_view(doc,"Test Cutting Approval")
			})
			this.frm.add_custom_button(__("Bulk Cutting"),function () {
				me.queary_repoty(doc,"Bulk Cutting Order",{"tc.styl":doc.name})
			})
			this.frm.add_custom_button(__("Cutting Alter"),function () {
				me.set_route(doc,"Form","Cutting Alter Entry")
			})
			this.frm.add_custom_button(__("Bundle"),function () {
				me.new_doc(doc,"Bundle")
			})
			this.frm.add_custom_button(__("Embellishment"),function () {
				me.new_doc(doc,"Embellishment")
			})
			this.frm.add_custom_button(__("Embellishment Receive"),function () {
				me.new_doc(doc,"Embellishment Receive")
			})
			this.frm.add_custom_button(__("Embellishment QC"),function () {
				me.page_view(doc,"embellishment-qc")
			})
			this.frm.add_custom_button(__("Production Line Setup"),function () {
				me.new_doc(doc,"Production Line Setup")
			})
			this.frm.add_custom_button(__("Production Line"),function () {
				// me.queary_repoty(doc,"Production Line Setup")
			})
			this.frm.add_custom_button(__("Finished Goods Process"),function () {
				me.new_doc(doc,"Finished Goods Process",{"stl":doc.name})
			})
			this.frm.add_custom_button(__("Measurement Check"),function () {
				me.new_doc(doc,"Measurement Check")
			})
			this.frm.add_custom_button(__("Inspection"),function () {
				me.new_doc(doc,"Inspection")
			})
		}
		frappe.route_options = null;
	},
	new_doc:function (doc,doctype,args) {
		if (!args) {
			args = {"style":doc.name}
		};
		frappe.route_options=args
		new_doc(doctype)		
	},
	queary_repoty:function (doc,report,args) {

		if (!args) {
			args = {"style":doc.name}
		};
		frappe.route_options=args
		frappe.set_route("query-report",report)	
		// body...
	},
	page_view:function (doc,page,args) {
		if (!args) {
			args = {"style":doc.name}
		};
		frappe.route_options=args
		frappe.set_route(page)		
	},
	set_route:function (doc,page,form,name,args) {
		if (!args) {
			args = {"style":doc.name}
		};
		frappe.route_options=args
		frappe.set_route(page,form,name)		
	}
});



cur_frm.cscript = new garments.merchandising.StyleNameController({frm:cur_frm})