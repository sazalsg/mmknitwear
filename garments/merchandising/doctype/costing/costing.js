cur_frm.add_fetch("costing_child_item","item_name","costing_child_item_name")
cur_frm.add_fetch("costing_child_item","stock_uom","costing_child_uom")
// cur_frm.add_fetch("reference","item","costing_item")



cur_frm.cscript.onload = function(doc,doctype,docname){
	// cur_frm.set_query("costing_item", function() {
 //        return {
 //            "filters": {
 //                "item_group": "Products",
 //            }
 //        };
 //    });
    cur_frm.set_query("costing_child_item","fabric_table", function(){
        return {
            "filters": {
                "item_group": "Fabrics",
            }
        };
    });
    cur_frm.set_query("costing_child_item","accessories_table", function(){
		return {
            "filters": {
				"item_group": "Accessories",
            }
        };
    });
}


cur_frm.cscript.costing_item_description = function(doc,doctype,docname){


	// msgprint(doc.accessories_table)

}







cur_frm.cscript.costing_child_qty = function(doc,doctype,docname){

		calcTotal(doc,doctype,docname);

		sumTotal(doc,doctype,docname);
}


cur_frm.cscript.costing_child_unit_price = function(doc,doctype,docname){
	
	calcTotal(doc,doctype,docname);

	sumTotal(doc,doctype,docname);
}


//Calculation After Quantity (pcs)
cur_frm.cscript.total_qty = function(doc,doctype,docname){
	
	sumTotal(doc,doctype,docname);

}


//Calculation After change CM
cur_frm.cscript.cm = function(doc,doctype,docname){
	
	sumTotal(doc,doctype,docname);

}


//Calculation After change Foreign Commission
cur_frm.cscript.foreign_commission = function(doc,doctype,docname){

	sumTotal(doc,doctype,docname);

}


//Calculation After change Local Commission
cur_frm.cscript.local_commission = function(doc,doctype,docname){
	
	sumTotal(doc,doctype,docname);

}



frappe.confirm = function(message, ifyes, ifno) {
	var d = new frappe.ui.Dialog({
		title: __("Select Accessories List"),
		fields: [
			{fieldtype:"HTML", options:"<p class='frappe-confirm-message'>" + message + "</p>"}
		],
		primary_action_label: __("SET"),
		primary_action: function() {
			ifyes();
			d.hide();
		}
	});
	d.show();

	// no if closed without primary action
	if(ifno) {
		d.onhide = function() {
			if(!d.primary_action_fulfilled) {
				ifno();
			}
		};
	}
	return d;
}



cur_frm.cscript.accessories_list = function(doc, dt, dn){

	var all_checkboxes = $("input:checkbox[name=acc_list]:checked"); // choose & store all checkboxes
	all_checkboxes.prop('checked', false); // uncheck all of them



	frappe.call({
		method:"frappe.client.get_list",
		args:{
			doctype:"Item",
			filters: [
				["item_group","=", 'Accessories'],
			],
			fields: ["name as item_id", "name", "item_name", 'weight_uom']
		},
		cache:false,
		callback: function(r) {

			var list = '<ul style="text-align: left;" id="acc_list">';

			$.each(r.message, function( index, value ) {
				list += '<li style="list-style:none;"><input type="checkbox" name="acc_list" value="'+value['name']+'"> '+value['item_name']+'</li>';
			});

			list += '</ul>';

			list += '<div class="clearfix"></div>';


			frappe.confirm(__(list),
				function() {
					
					doc.accessories_table = ""

					$("input:checkbox[name=acc_list]:checked").each(function(){

						var checkedVal = $(this).val();

						var itemList = r.message.filter(function (el) {
							return el.name == checkedVal;
						});

						var row = frappe.model.add_child(doc, "CostingChild", "accessories_table");
						row.costing_child_item = itemList[0].item_name;
						row.costing_child_item_name =  itemList[0].item_name;
						row.costing_child_uom =  itemList[0].stock_uom;

					});

					refresh_field("accessories_table");

				}
			);	


		}
	});

}





cur_frm.cscript.reference = function(doc, dt, dn){
	
	// var ii = get_item_list(doc);

		frappe.call({
			method:"garments.merchandising.doctype.costing.costing.fabric_list",
			args:{
				'reference':doc.reference
			},
			callback: function(r) {

				doc.fabric_table = ""
				$.each(r.message, function(i, d) {
					var rowa = frappe.model.add_child(doc, "CostingChild", "fabric_table");
					rowa.costing_child_item = d.name;
					rowa.costing_child_item_name =  d.item_name;
					rowa.costing_child_uom =  d.stock_uom;
					rowa.costing_child_qty =  d.avg_consumption;
				});
				refresh_field("fabric_table");
				
			}
		});
		
		frappe.call({
			method:'frappe.client.get_value',
			args:{
				doctype:"Buyer Inquiry",
				fieldname:"desc",
				filters:{
					reference:doc.reference,
				}
			},
			callback:function (data) {
				if (data.message) {
					console.log(data.message)
					doc.costing_item_description = data.message.desc;
					refresh_field('costing_item_description');
				};
			}
		});
}



//Costing child tables total field
function sumTotal(doc,doctype,docname){


	
			if(!doc.total_qty)
				doc.total_qty =  0;
	
			if(!doc.cm)
				doc.cm =  0;
	
			if(!doc.foreign_commission)
				doc.foreign_commission =  0;
	
			if(!doc.local_commission)
				doc.local_commission =  0;





			var fabric_sum = 0;
			// Check Current Row Duplicate Entry
			for(var key in doc.fabric_table){

				if(parseFloat(doc.fabric_table[key].costing_child_total_amount)){
					fabric_sum+=(parseFloat(doc.fabric_table[key].costing_child_total_amount));
				}
			}
			//Fabrics Cost
			cur_frm.set_value("total_fabrics_cost", fabric_sum);
			
			var accessories_sum = 0;
			// Check Current Row Duplicate Entry
			for(var key in doc.accessories_table){

				if(parseFloat(doc.accessories_table[key].costing_child_total_amount)){
					accessories_sum+=(parseFloat(doc.accessories_table[key].costing_child_total_amount));
				}
			}
			//Accessories Cost
			cur_frm.set_value("total_accessories_cost", accessories_sum); 

			// Set Total Amount
			cur_frm.set_value("total", fabric_sum+accessories_sum+doc.cm);

			// Total Foreign Commission
			var total_foreign_commission = (doc.total * doc.foreign_commission) / 100;
			cur_frm.set_value("total_foreign_commission", total_foreign_commission);

			// Total Local Commission
			var total_local_commission = doc.total_qty * doc.local_commission;
			cur_frm.set_value("total_local_commission", total_local_commission);

			// Set Grand Total
			var g_total = doc.total + total_foreign_commission + total_local_commission
			cur_frm.set_value("g_total", g_total);


			// Set Per Pcs Price
			cur_frm.set_value("per_pcs_price", g_total/doc.total_qty);

}



function calcTotal(doc,doctype,docname){

	var setLocal = locals[doctype][docname];

	var qty = parseFloat(setLocal.costing_child_qty);

	var unit = parseFloat(setLocal.costing_child_unit_price);

	if( qty && unit ){

		setLocal.costing_child_total_amount = qty*unit;
		refresh_field("costing_child_total_amount", setLocal.name, setLocal.parentfield);
	} else {

		setLocal.costing_child_total_amount = "";
		refresh_field("costing_child_total_amount", setLocal.name, setLocal.parentfield);
	}
}