# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import throw,session,_
from frappe.utils import getdate,flt
from frappe.model.mapper import get_mapped_doc


class Costing(Document):
	pass



@frappe.whitelist()
def fabric_list(reference):
		item_list= frappe.db.sql("SELECT `tabItem`.`name`, `tabItem`.`item_name`, `tabItem`.`stock_uom`, `tabConsumption Workout`.`avg_consumption` FROM `tabItem` LEFT JOIN `tabConsumption Workout` ON `tabConsumption Workout`.`item` = `tabItem`.`name` WHERE `tabConsumption Workout`.`style`= '{}'".format(reference), as_dict=1)

		# default_option= frappe.db.sql("SELECT `tabItem`.`name`, `tabItem`.`item_name`, `tabItem`.`stock_uom` FROM `tabItem` WHERE `tabItem`.`name` in ('CM', 'KNITTING & DYEING')", as_dict=1)


		listed_row = item_list

		return listed_row



@frappe.whitelist()
def make_costing(source_name, target_doc=None):
	def set_missing_values(source, target):
		quotation = frappe.get_doc(target)
		# quotation.currency = None # set it as default from customer
		quotation.run_method("set_missing_values")
		# quotation.run_method("calculate_taxes_and_totals")
	# checkHavePrevUnsubmitRec= frappe.db.get_value("Library Acquisition Receive",{"aq_id":source_name,"docstatus":0})
	# if checkHavePrevUnsubmitRec:
	# 	throw ("You already recoerded a received item but doesn't 'Submit'. Before New Record Please Submit This record <a href='/desk#Form/Library Acquisition Receive/{link}' title='{link}'>{link}</a>".format(link=checkHavePrevUnsubmitRec))
	# def update_item(source, target, source_parent):
	# 	target.received_qty = (flt(source.granted_qty) - flt(source.received_qty))
	# 	target.pre_receive_qty = flt(source.received_qty)

	target_doc = get_mapped_doc("Costing", source_name, {
		"Costing":{
			"doctype":"Buyer Quotation",
			"field_map":{
				"costing_item":"quote_item",
				"costing_item_name":"quote_item_name",
				"costing_item_description":"quote_item_description",
				"total_qty":"total_qty",
				"total_fabrics_cost":"total_fabrics_cost",
				"total_accessories_cost":"total_accessories_cost",
				"g_total":"g_total",
				"per_pcs_price":"per_pcs_price",
				# "fabric_table":"fabric_table",
				# "accessories_table":"accessories_table"

			}
		},
		"CostingChild":{
			"doctype":"BuyerQChild",
			"field_map":{

				"costing_child_item":"quote_child_item",
				"costing_child_item_name":"quote_child_item_name",
				"costing_child_qty":"quote_child_qty",
				"costing_child_uom":"quote_child_uom",
				"costing_child_unit_price":"quote_child_unit_price",
				"costing_child_total_amount":"quote_child_total_amount",

			},

		},

		"CostingChild1":{
			"doctype":"BuyerQChild1",
			"field_map":{

				"costing_child_item":"quote_child_item",
				"costing_child_item_name":"quote_child_item_name",
				"costing_child_qty":"quote_child_qty",
				"costing_child_uom":"quote_child_uom",
				"costing_child_unit_price":"quote_child_unit_price",
				"costing_child_total_amount":"quote_child_total_amount",

			},

		},
			"add_if_empty": True
			
		
	}, target_doc, set_missing_values)



	return target_doc
