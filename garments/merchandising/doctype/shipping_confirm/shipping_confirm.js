cur_frm.cscript.qty = function (doc) {
	cur_frm.cscript.total_qty(doc)
}
cur_frm.cscript.total_qty = function (doc) {
	var total = 0;
	for (var i = doc.shipment_confirm_child.length - 1; i >= 0; i--) {
		total += doc.shipment_confirm_child[i].qty;
	};
	cur_frm.set_value('total_qty',total);
}
frappe.ui.form.on("Shipping Confirm Child","shipment_confirm_child_remove",function(frm){
  cur_frm.cscript.total_qty(frm.doc);
});