# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, os, ast, openpyxl, time, json
from frappe.model.document import Document
from datetime import date
from frappe.model.naming import make_autoname
from frappe import _

class POSheet(Document):
	def autoname(self):
		self.name = make_autoname("PS-{b}-{y}/{m}-.##".format(b=self.buyer or "", y = str(date.today().year)[2:4],m=date.today().month))


def check_size_format(dt,dn,rows,size_col_idx):
	if not frappe.db.exists(dt,dn):
		doc = frappe.new_doc("Size Format")
		doc.set('format_name',dn)
		format_childs = []
		for row in rows:
			if row.value and row.col_idx > size_col_idx:
				if not frappe.db.exists("Size",row.value):
					size = frappe.new_doc("Size")
					size.set('size_name',row.value)
					size.save()
				format_childs.append({"size":row.value})
		doc.set("format_child",format_childs)
		doc.save()


def check_size_format_child(dt,dn,size):
	if frappe.db.count(dt,{"parent":dn,"size":size}) <=0:
		frappe.throw(_("Size Format '{0}'' doesn't have size '{1}'.").format(dn,size))


		
@frappe.whitelist()
def import_sheet(doc):

	doc = ast.literal_eval(doc)
	doc = frappe.get_doc(doc['doctype'],doc['name'])

	current_site = frappe.local.site_path;
	public_path = os.path.abspath(os.path.join(frappe.local.site_path, 'public'))
	try:
		xls = openpyxl.load_workbook(os.path.join(public_path,str(doc.xls_file[1:])))
	except Exception, e:
		frappe.throw(_("File '{}' not found.".format(doc.xls_file)))
	sheet = xls.get_active_sheet()
	columns, child_rows, sections =[], [], []

	child_meta =frappe.get_meta(doc.doctype).get_field('childs')
	child_child_meta=frappe.get_meta(doc.doctype).get_field('childs_child')

	for row in sheet.rows[1]:
		if row.value:
			columns.append(row.value)
		if row.value == "~~":
			size_col_idx = row.col_idx
			break
	for x in xrange(3,sheet.get_highest_row()+1):		
		if sheet.cell(row=x,column=1).value =="New Section":
			pre_parent = 'parent'+str(time.time()).split('.')[0]+str(x)
			sections.append({
				"parnet":doc.name,
				"pre_parent":pre_parent,
				"total_qty":0,
				"total_amount":0,
				"total_std_packing_qty":0,
				"total_std_parcel_number":0,
				"total_solid_size_qty":0,
				})
			continue
		elif sheet.cell(row=x,column=1).value =="sizes":
			size_row_idx = x
			continue
		if not sheet.cell(row=x,column=2).value:
			continue
		sizes = {};
		child_row = {}			
		for row in sheet.rows[x-1]:
			if row.col_idx < size_col_idx and row.value:
				if sheet.cell(row=2,column=row.col_idx).value == "size_format":
					size_format_idx = row.col_idx
					check_size_format("Size Format",row.value,sheet.rows[size_row_idx-1],size_col_idx)

				elif sheet.cell(row=2,column=row.col_idx).value == "color":
					set_missing_val("Color",row.value,{"color_name":row.value})

				elif sheet.cell(row=2,column=row.col_idx).value == "theme":
					set_missing_val("Theme",row.value,{"theme_name":row.value})

				elif sheet.cell(row=2,column=row.col_idx).value == "composition":
					set_missing_val("Composition",row.value,{"composition":row.value})

				elif sheet.cell(row=2,column=row.col_idx).value == "product_name":
					set_missing_val("Style Name",row.value,{"style_name":row.value,"buyer":doc.buyer})

				elif sheet.cell(row=2,column=row.col_idx).value == "description":
					if not frappe.db.exists("Item",row.value):
						frappe.throw(_("Please create Item '{}' Before Update Po Sheet".format(row.value)))

				child_row[sheet.cell(row=2,column=row.col_idx).value] = row.value
			elif 'size_row_idx' in locals() and row.col_idx > size_col_idx and row.value:
				check_size_format_child("Size Format Child",sheet.cell(row=x,column=9).value,sheet.cell(row=size_row_idx,column=row.col_idx).value)
				sizes[sheet.cell(row=size_row_idx,column=row.col_idx).value] = row.value
		# Set missing value
		if 'size_format' in child_row:
			sections[-1]['size']=child_row['size_format']
		if 'qty' in child_row:
			sections[-1]['total_qty']= sections[-1]['total_qty']+int(child_row['qty'])

		if 'solid_size_qty' in child_row and child_row['solid_size_qty'] > 0:
			child_row['size_breakdown_for'] = "Solid Packing"
			sections[-1]['total_solid_size_qty']= sections[-1]['total_solid_size_qty']+int(child_row['solid_size_qty'])
		else:
			child_row['size_breakdown_for'] = "Standard Packing"
			sections[-1]['total_std_packing_qty']= sections[-1]['total_std_packing_qty']+int(child_row['std_packing_qty'])
			if 'std_parcel_number' in child_row:
				sections[-1]['total_std_parcel_number']= sections[-1]['total_std_parcel_number']+int(child_row['std_parcel_number'])
		if 'amount' in child_row:
			sections[-1]['total_amount']= sections[-1]['total_amount'] + frappe.utils.data.flt(child_row['amount'])

		child_row['datas']=json.dumps(sizes)
		child_row['pre_parent']=pre_parent
		child_row["parnet"]=doc.name
		child_row["etd_shipment_date"]=frappe.utils.data.get_datetime(child_row['etd_shipment_date'])

		child_rows.append(child_row)


	doc.set('childs',sections)
	doc.set('childs_child',child_rows)
	if doc.save():
		frappe.msgprint(_("Updated sheet Succesfully"))
		return doc.as_dict()
	else:
		return "Sorry"




def set_missing_val(doctype,name,field_values = None):
	if frappe.db.exists(doctype,name):
		return True
	else:
		doc = frappe.new_doc(doctype)
		doc.set('name',name)
		for key, val in field_values.iteritems():
			doc.set(key,val)
		doc.insert(ignore_permissions=1)
		return doc.as_dict()