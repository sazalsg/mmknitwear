// -*- coding: utf-8 -*-
//  Copyright (c) 2015, Techbeeo Software Ltd. and contributors
//  For license information, please see license.txt
frappe.ui.form.on("PO Sheet", "onload", function(frm) {
	$(frm.wrapper).append('\
		<div id="freeze" class="freeze loading_freeze">\
			<div class="freeze-message-container modal-backdrop">\
				<div class="freeze-message">\
					<h4 style="color:#fff">Loading...</h4>\
				</div>\
			</div>\
		</div>\
		')
});


frappe.provide('garments.merchandising');
frappe.require("assets/garments/js/controllers/po_sheet_child_fields.js",function (r) {});
frappe.require("assets/garments/js/controllers/po_sheet_import.js",function (r) {});
//{% include "garments/public/js/controllers/po_sheet_child_fields.js" %}
//{% include "garments/public/js/controllers/po_sheet_import.js" %}
garments.merchandising.PoSheetController = frappe.ui.form.Controller.extend({
	onload:function (doc) {
		if (typeof fieldsLength === "undefined") {
			fieldsLength = this.get_childs_child_fields('childs_child').length;
		};
	},
	refresh:function (doc) {
		$('.loading_freeze').removeClass('hide').addClass('in');
		setTimeout(function (doc) {
			doc = cur_frm.doc;
			me = cur_frm.cscript;
			me.section_render(doc)
			$('.loading_freeze').removeClass('in').addClass('hide');
		},100);
	},
	section_render:function (doc) {
		var me = this;
		this.html = $('body').find('#html_rander');
		this.html.html($(frappe.render_template("po_sheet_section_body", {}))
			.attr('date-fieldname', 'childs'));
		// Bind Add Table Event 
		$(this.html).find(".sec-add-table").click(function() {
			var sel = $(this).parents(".grid-body");
			me.parent_row(doc,sel);
			return false;
		});

		if (typeof doc.childs !== "undefined" && doc.childs.length > 0 ) {
			for (var i = 0; i < doc.childs.length; i++) {
				me.parent_row(doc,$('#html_rander').find('.grid-body'),doc.childs[i])
			};
		};
	},
	parent_row:function (doc,selector,row) {
		var me = this;
		var df = me.frm.get_field('childs').df;
		var row_fields = frappe.get_meta(df.options).fields;
		if (typeof row === "undefined") {
			row = frappe.model.add_child(this.frm.doc, df.options, df.fieldname);
			row['pre_parent'] = 'parent'+new Date().getTime();
		};
		var wrapper_row = $($(selector).find('.rows').append(frappe.render_template("po_sheet_table_render",{})));
			wrapper_row.find('.item:last').attr('data-parent', row.name);
			wrapper_row.find('.item:last .table-responsive').before('<div class="size col-sm-6"></div> <div class="clearfix"></div>');
		var size = frappe.ui.form.make_control({
			parent: wrapper_row.find('.size:last'),
			df: {
				label: "Size",
				fieldtype: "Link",
				options: "Size Format",
				fieldname: "size",
				reqd:1
			},
		});
		size.refresh();
		if ('size' in row) {
			size.$input.val(row.size);			
			me.check_mandetory(size.$input)
			var fields = me.get_sizes(size.$input.val())
			me.render_header(size.$input.parents('.item'),fields)
			if (typeof doc.childs_child !== "undefined" && doc.childs_child.length > 0) {
				for (var i = 0; i < doc.childs_child.length; i++) {
					if (doc.childs_child[i].pre_parent === row.pre_parent) {
						me.render_body(size.$input.parents('.item'),fields,doc.childs_child[i])
					};
				};
			};
			me.render_footer(size.$input.parents('.item'),row_fields,row)
		};
		size.$input.on('change',function(event) {
			row['size']=$(this).val();
			var fields = me.get_sizes($(this).val())
			// console.log(fields);
			if ($(this).val()=='') {
				$(this).parents('.item').find('.tb-body, .tb-footer, .tb-header').html('');
			}else{
				$(this).parents('.item').find('.tb-body').html('');
				me.render_header($(this).parents('.item'),fields)
				me.render_body($(this).parents('.item'),fields)
				me.render_footer($(this).parents('.item'),row_fields,row)
				me.check_mandetory($(this))
			};
		});

	},
	get_childs_child_fields:function (field) {
		var df = me.frm.get_field(field).df;
		return frappe.get_meta(df.options).fields;
	},
	get_sizes:function (sel) {
		var me = this;
		var fields = me.get_childs_child_fields('childs_child');
		fields.length = fieldsLength;
		frappe.call({
			method:"frappe.client.get",
			async:false,
			args:{
				doctype:"Size Format",
				name:sel
			},
			callback:function (r) {
				if (r.message) {
					for (var i = 0; i < r.message.format_child.length; i++) {
						fields.push({
							'fieldname':r.message.format_child[i].size,
							'label':r.message.format_child[i].size,
							'fieldtype':'Int',
							'h_cal':1,
							'childs_field':1
						})
					};
				};
			}
		});
		return fields
	},
	render_header:function (selector,data) {
		var parent =  $(selector);
		data['idx'] = $(parent).index()+1;
		$(parent).find('.tb-header').html('<tr class="data-row row sortable-handle"></tr>');
		var tr = $(parent).find('.tb-header tr');
		$(tr).append('<td></td>')
		for (var i = 0; i < data.length; i++) {
			$(tr).append('<td>'+data[i].label+'</td>');			
			var td = $(tr).find('td:last');
			if ('css' in data[i]) {
				td.attr('style', data[i].css);
			};
			if ('hidden' in data[i] && data[i].hidden == 1) {
				td.hide();
			};
			/*if ('width' in data[i] && data[i].hidden != '') {
				td.css('min-width', data[i].width);
			};*/
		};
		$(tr).append('<td></td>')

	},
	render_footer:function (selector,row_fields,row) {
		var parent =  $(selector);
		var total_td = $(parent).find('thead tr.data-row.row.sortable-handle:first td').length;
		$(parent).find('.tb-footer').html($('<tr></tr>').attr('data-name', row.name))
		var tr = $(parent).find('.tb-footer').find('[data-name="'+row.name+'"]');
		// Set empty td in frist
		for (var i = 0; i < 12; i++) {
			tr.append('<td></td>')
		};
		tr.append('<td>Total:</td>')
		// Set Total Fields
		for (var i = 0; i < row_fields.length; i++) {
			tr.append('<td></td>');
			var td = $(tr).find('td:last');
			if (($.inArray(row_fields[i].fieldname , ['size','pre_parent','size_for_break']) == '-1')) {
				var field = frappe.ui.form.make_control({
					parent: td,
					df: row_fields[i],
					only_input: true
				});
				field.refresh();
				field.$input.attr('disabled', '1');
				if ( typeof row !== "undefined" && (row_fields[i].fieldname in row) && !('childs_field' in row_fields[i])) {
					$(field.input).val(row[row_fields[i].fieldname]);
					if (row_fields[i].reqd == '1') {
						me.check_mandetory($(field.input))
					};
				}
			};
		};
		// Add empty td in last
		for (var i = 0; i < (total_td -(row_fields.length+15)); i++) {
			tr.append('<td></td>');
		};
		$(tr).find('input[data-fieldname], select[data-fieldname]').on('change', function(event) {
			event.preventDefault();
			row[$(this).data('fieldname')] = $(this).val();
		});
	},
	render_body:function (selector,data,values) {
		var me = this
		var doc = me.frm.doc;
		var parent =  $(selector);
		if (typeof values === "undefined") {
			var df = me.frm.get_field('childs_child').df;
			var values = frappe.model.add_child(this.frm.doc, df.options, df.fieldname);
			for (var i = 0; i < doc.childs.length; i++) {
				if(doc.childs[i].name === parent.data('parent')){
					values['pre_parent'] = doc.childs[i].pre_parent;
					break;
				};
			};
		}else{
			if ( typeof values.datas === "string") {
				values['datas'] = JSON.parse(values.datas);
			};
			var etd_shipment_date = frappe.datetime.str_to_user(values.etd_shipment_date);
			if (etd_shipment_date !== "Invalid date") {
				values['etd_shipment_date'] = etd_shipment_date;
			};
		};
		$(parent).find('.tb-body').append($('<tr></tr>').attr('data-name', values.name));
		var tr = $(parent).find('.tb-body tr:last');
		if ($(parent).find('.tb-body tr').length >1) {
			$(tr).append($('<td></td>').addClass('text-center').append(
				$('<button>Copy</button>').addClass('btn btn-xs btn-default').on('click', function(event) {
					event.preventDefault();
					me.copy_from_previous(tr);
				})
				));
		}else{
			$(tr).append('<td></td>')
		};
		if (!('datas' in values)) {
			values['datas']={};
		};
		for (var i = 0; i < data.length; i++) {
			$(tr).append('<td></td>');
			var td = $(tr).find('td:last');
			var field = frappe.ui.form.make_control({
				parent: td,
				df: data[i],
				only_input: true
			});
			field.refresh();



			if ('hidden' in data[i] && data[i].hidden == 1) {
				td.hide();
			};

			if ('childs_field' in data[i] && data[i].childs_field == 1) {
				field.$input.attr({'data-parent-name': values.name, 'data-parent-field':'datas'});
			};

			if (data[i].fieldname == "size_format") {
				$(field.input).val($(parent).find('input[data-fieldname="size"]').val());
				$(field.input).attr('disabled','1');
				me.check_mandetory($(field.input))
			};
			if (data[i].fieldname == "amount") {
				$(field.input).attr('disabled','1')
			};
			if (data[i].fieldname == "qty") {
				$(field.input).attr('disabled','1')
			};
			if (data[i].fieldname == "size_breakdown_for") {
				$(field.input).on('change', function(event) {
					me.changeV($(this))
					event.preventDefault();
					/* Act on the event */
				});
				
			};
			if ('css' in data[i]) {
				td.attr('style', data[i].css);
			};
			if ('h_cal' in data[i] && data[i].h_cal == 1) {
				$(field.input).addClass('day_row').on('change', function(event) {
					totalHorizental(this)
					event.preventDefault();
					/* Act on the event */
				}).trigger('change');
			};			
			if ( typeof values !== "undefined" && (data[i].fieldname in values) && !('childs_field' in data[i])) {
				$(field.input).val(values[data[i].fieldname]);
				if (data[i].reqd == '1') {
					me.check_mandetory($(field.input))
				};
			}else if ( typeof values !== "undefined" && ('datas' in values) && ('childs_field' in data[i])) {
				if (data[i].fieldname in values['datas']) {
					$(field.input).val(values['datas'][data[i].fieldname]);
				};
				if (data[i].reqd == '1') {
					me.check_mandetory($(field.input))
				};
			};
			
		};
		$(tr).append('<td><button onclick="javascript:remove_row(this);" class="button btn btn-xs btn-danger">remove</button></td>')

		$(tr).find('input[data-fieldtype="Link"]').on('change', function(event) {
			me.check_mandetory(this)
			event.preventDefault();
		});
		$(tr).find('input[data-fieldname="purchase_price"], input[data-fieldname="qty"]').on('change', function(event) {
			totalAmount(this)
			event.preventDefault();
		});
		$(tr).find('input[data-fieldname="qty"], input[data-fieldname="amount"], input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"], input[data-fieldname="solid_size_qty"]').on('change', function(event) {
			totalVartical(this)
			event.preventDefault();
		});

		// Set value in array
		$(tr).find('input[data-fieldname], select[data-fieldname]').on('change', function(event) {
			event.preventDefault();
			if ($(this).data('parent-field')) {
				values[$(this).data('parent-field')][$(this).data('fieldname')] =$(this).val();
			}else {
				values[$(this).data('fieldname')] = $(this).val();
			};
		});
		
		$(tr).find('select[data-fieldname="size_breakdown_for"], .day_row, input[data-fieldname="size_format"]').trigger('change')
		// Horizontal Calculation
		$(tr).find('.day_row, input[data-fieldname="std_parcel_number"]').on('change', function(event) {
					totalHorizental(this)
					event.preventDefault();
					/* Act on the event */
				}).trigger('change')

	},
	copy_from_previous:function (row) {
		var me = this;
		var doc = me.frm.doc;
		var curr_row = $(row).data('name');
		var prev_row = $(row).prev().data('name');
		var copy_fiels = ['uvc','std_parcel_code','product_name','description','color','theme','size_format','std','etd_shipment_date','composition','purchase_price'];
		var sl = '';
		for (var i = 0; i < doc.childs_child.length; i++) {
			if (doc.childs_child[i].name == curr_row) {
				sl = i;
			};
			if (doc.childs_child[i].name == prev_row) {				
				if (sl == '') {
					for (var j = 0; j < doc.childs_child.length; j++) {
						if (doc.childs_child[j].name == curr_row) {
							sl = j;
							break;
						};
					};
				}
				for (var k = 0; k < copy_fiels.length; k++) {
					if (copy_fiels[k] in doc.childs_child[i] ) {
						doc.childs_child[sl][copy_fiels[k]] = doc.childs_child[i][copy_fiels[k]];
						$(row).find('input[data-fieldname="'+copy_fiels[k]+'"], select[data-fieldname="'+copy_fiels[k]+'"]').val(doc.childs_child[sl][copy_fiels[k]]).trigger('change');
					};
				};
				break;
			};
		};
	},
	changeV:function (e) {
		if ($(e).val()=="Standard Packing") {
			$(e).parents('tr').find('input[data-fieldname="std_packing_qty"], input[data-fieldname="solid_size_qty"]').attr('disabled','1');
			$(e).parents('tr').find('input[data-fieldname="solid_size_qty"]').val('');
			$(e).parents('tr').find('input[data-fieldname="std_parcel_number"]').removeAttr('disabled');
			$(e).parents('tr').find('input[data-fieldname="qty"]').val('');
			totalVartical($(e).parents('tr').find('input[data-fieldname="solid_size_qty"]'));
		}else if ($(e).val()=="Solid Packing") {
			$(e).parents('tr').find('input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"]').val('').attr('disabled','1');
			$(e).parents('tr').find('input[data-fieldname="solid_size_qty"]').attr('disabled','1');
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_packing_qty"]'));
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_parcel_number"]'));
		}else {
			$(e).parents('tr').find('input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"], input[data-fieldname="solid_size_qty"], input[data-fieldname="qty"]').val('').attr('disabled','1');
			totalVartical($(e).parents('tr').find('input[data-fieldname="solid_size_qty"]'));
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_packing_qty"]'));
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_parcel_number"]'));
		};
		$(e).parents('tr').find('input.day_row:first').trigger('change');
	},
	check_mandetory:function (sel) {
		if ($(sel).val() !="") {
			$(sel).closest('.frappe-control').removeClass('has-error');
		}else{
			$(sel).closest('.frappe-control').addClass('has-error');
		};
	},
	validate:function (doc) {
		if (doc.childs_child) {
			for (var i = 0; i < doc.childs_child.length; i++) {
				if (typeof cur_frm.doc.childs_child[i].datas === "object") {
					doc.childs_child[i].datas = JSON.stringify(doc.childs_child[i].datas)
				};
				cur_frm.doc.childs_child[i].etd_shipment_date = frappe.datetime.user_to_str(cur_frm.doc.childs_child[i].etd_shipment_date)
				
			};
		};
	},
	update_sheet_from_file:function (doc) {
		update_sheet_from_file(doc)
	},
	after_save:function (doc) {
		if (doc.xls_file) {
			frappe.ui.toolbar.clear_cache();
		};
	}
});

cur_frm.cscript = new garments.merchandising.PoSheetController({frm:cur_frm});