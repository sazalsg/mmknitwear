# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import msgprint, throw,_
from datetime import date
from frappe.model.naming import make_autoname
import time


class LoadForms(Document):

	def autoname(self):
		cname=frappe.db.get_value("Customer", {"name":self.buyer}, ["customer_name"])
		self.name =  make_autoname('{0}-{1}-{2}{3}{4}-'.format("LF",cname.split()[0], str(date.today().month), '/',str(date.today().year)[2:4] ) + ".#")

	def validate(self):
		pass

	def before_insert(self):
		# throw(self)
		if not hasattr(self, 'measure_info'):
			frappe.throw("Please Select Atleast One Form ")

	def after_insert(self):
		doc = frappe.new_doc("Load Forms Child")
		doc.set("forms_info" , self.measure_info)
		doc.set("name" , self.name)
		doc.save()
		
	
	def on_update(self):
		if hasattr(self, 'measure_info'):
			doc = frappe.get_doc("Load Forms Child",self.name)
			doc.set("forms_info" , self.measure_info)
			doc.save()


@frappe.whitelist()
def order_query(name):
	if name :
		# return filters
		sql = frappe.db.get_values("Load Forms Child", {"name":name}, ["forms_info"])

		return sql
	else :
		return []
