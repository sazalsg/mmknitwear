frappe.ui.form.on("Others Booking", "after_save", function(frm) {

	msgprint('Saving ...');
	location.reload();
});


// frappe.ui.form.on("Load Forms", "before_save", function(frm) {

// console.log( cur_frm.doc);
	
	
// });


var called = 0;

var is_add_tab = 0;

cur_frm.cscript.form_list = function(doc,doctype,docname) {


			onchange_render(doc,doctype,docname);
						
}

function onchange_render(doc,doctype,docname){

		if(!called){

			registerOnchngeMethod(doc,doctype,docname);

			called =1;
		}

		var format = doc.form_list;						
		if(format !='select'){
				setDescAndSubject(doc)	

				reset_global_vars();
				start_rendering(doc,doctype,docname);



			}else{

				//reset rendering.....
				//current_form = '';
			}
}


cur_frm.cscript.total_tabs = function(doc,doctype,docname) {

		onchange_render(doc,doctype,docname);
							
}

function setDescAndSubject(doc){

		var format = doc.form_list;						
		
		var subject = forms_list[format]['subject'];

		var attns = doc.attn;
		attns = attns ? 'Dear ' + attns + ',\n' : '';

		var description = attns + forms_list[format]['description'];

		cur_frm.set_value("sub", subject );
		cur_frm.set_value("description", description );
}

cur_frm.cscript.attn = function(doc,doctype,docname) {

		//onchange_render(doc,doctype,docname);

		setDescAndSubject(doc);
							
}


$.extend(cur_frm.cscript, {
	onload: function(doc) {

		if(doc.__islocal){

			cur_frm.set_value("br", "");

		}	

	},
	refresh: function(doc,doctype,docname) {

		//console.log( doc );
		//msgprint('xxx::' + doc.__islocal)

		cur_frm.set_df_property("total_tabs", "hidden", true);


				if(!called){

					registerOnchngeMethod(doc,doctype,docname);

					//console.log('once.....');

					called =1;
				}


		if(!doc.__islocal){

			cur_frm.set_df_property("form_list"	, "read_only", true);
			

			frappe.call({
				method: "garments.merchandising.doctype.others_booking.others_booking.order_query",
				async:false,
				cache:false,
				args:{
					"name" : doc.name,
				},
				callback: function(r) {
					if (r.message) {
						
				
						//data = r.message[0][0] ;
						data = JSON.parse( r.message[0][0] );

						reset_global_vars();

						if(!called){

							registerOnchngeMethod(doc,doctype,docname);

							// console.log('once.....');

							called =1;
						}


						//registerOnchngeMethod(doc,doctype,docname);

						//console.log( data );

						current_form = data.form;


						
						tab_names = data.tabs_names;

						// console.log('wwww');
						// console.log( data);

						
						total_table = data.tab_count;

						current_links = data.links;

						//console.log( data.table_info );

						json_str = data.table_info;

					};
				//console.log( json_str );

				//console.log( 'total_count::' + total_table  );

				//json_str = { links:current_links,form:current_form,tab_count:total_table,table_info:json_str}

				start_rendering(doc,doctype,docname);

				 }

			})

		}
}});


/*Current table Global info info */

var current_form = '';
var current_total_col = 0;
var current_total_row = 0;
var current_row = 0;
var total_table = 0;
var total_links = [];

var current_links = [];

var json_str = [];

var tab_names= [];


function reset_global_vars(){

	current_form = '';
	current_total_col = 0;
	current_total_row = 0;
	current_row = 0;
	total_table = 0;
	total_links = [];
	current_links = {};
	json_str = [];

	tab_names= [];
	
}

/* */ 


function start_rendering(doc,doctype,docname){
		

		//if(format =='select') return;

		if(doc.__islocal){

			// console.log('WHY');


				var format = doc.form_list;
				var tab_qty = doc.total_tabs;

				current_form = forms_list[format];


				getLinks();

				for (x in total_links){
					//current_links = [];

					//console.log( total_links[x] );


					var i = getDBval( total_links[x] );

					var key = total_links[x][0];

					current_links[key]=i;
					
				}
		}

	
		var tabs = '';

		tabs += '<button class="btn btn-xs btn-default" id="add_tab">Add Table</button>';

		//console.log( tab_qty );

		if(!doc.__islocal){


			tab_qty = total_table;

			total_table = 0;

			
		}else{

			total_table = 0;

		}
		//total_table = tab_qty;

	

		for( var i=1; i<=tab_qty;i++){

			total_table++;

			

			tabs += render_table(doc,doctype,docname);
			
		}

		 setTimeout(function(){ $("body").find("#render_tabs").html(tabs); 

					 		//$('body').find('#0-2').trigger('change');
					 	
		}, 100);
}


function render_table(doc,doctype,docname,add_new){


				add_new = add_new || '';


					// Main Section (1)
				var consump_list = "";
				consump_list += '<div class="table-responsive grid-body" style="padding:15px;margin:10px;border: 2px solid #ccc;">';

				
				//consump_list += '<label class="control-label">Table :'+total_table+'</label>&nbsp;&nbsp;';

				var tab_name = '';

				if(!doc.__islocal){

					tab_name = tab_names[total_table-1];

					tab_name = tab_name || '';

				}

				//console.log( doc );

				consump_list += '<input type="text" class="form-control tab-name-list" placeholder="Title" value="'+tab_name+'"/>';

				consump_list += '<button class="btn btn-xs btn-default del_tab">X</button><br /><br />';

					consump_list += '<table class="table table-hover table-bordered table-striped"><thead class="tb-header">';


			    	//consump_list += '<tr class="grid-heading-row"><td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size" style="font-size: 13px;padding:8px;">SIZE :</td>';

					var size_head = '';

					

					for (var x in current_form.columns){

						//var x = tab_keys[i];

						var hd = '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="size_'+x+'" style="font-size: 13px;padding:8px;">'

						var units = [current_form.columns[x].prefix,current_form.columns[x].suffix].join('');

						units = units ? '<br/ >( '+ units +' )' : '';
						size_head +=  hd + current_form.columns[x].label+units+'</td>';


					}



					   consump_list += size_head+'<td></td></tr></thead>';


					// // Main Row Section (4)
					

					var tab_row_id = 'tab_row_id_'+ total_table;

					 consump_list += '<tbody class="tab_list" id="'+tab_row_id+'">';

				
					//consump_list += drawRow(rows,tab_row,'transparent');

					var total_info_sub = current_form.calc_cols_val;


					if( add_new ){
						consump_list += drawRow(doc,doctype,docname);
					}
					else if(!doc.__islocal){

						var tab_keys = Object.keys(json_str);

						var ctab = json_str[tab_keys[total_table-1]];

						total_info_sub = ctab.total_sub;

						delete ctab['total_sub'];

						for( var x in ctab ){

							consump_list += drawRow( ctab[x],doc,doctype,docname );

						}

					}else{

						consump_list += drawRow(doc,doctype,docname,add_new);

					}


					var all_cols = current_form['calc_cols'];


					if( all_cols ){

						var tot_row = '<tr class="grad-total">';

						for (var x in current_form.columns){


							if( all_cols[x] != undefined){

									var val;

									if(doc.__islocal || is_add_tab){
										val=0;
										is_add_tab = 0;
									}
									else{
										val = total_info_sub[all_cols[x]];
									}

									// console.log( 'mine:: ' + val );

									tot_row += '<td>Total:<input type="text" disabled="1" class="form-control '+all_cols[x]+'" value="'+val+'"/></td>';
							}else{
								tot_row += '<td></td>';

							}

						}


						tot_row += '</tr>';	

						consump_list += tot_row;

					}
			


					//consump_list += drawRow1(['CONSUMPTION :','RIB :'],sum_row,'#669999');


					consump_list += '</tbody></table>';

					var clsname = 'add-row-'+total_table;
					consump_list += '<button class="btn btn-xs btn-default add-row pull-right" type="button">Add New Row</button>' ;

					consump_list += '</div>';

					// addnew row button js

					AddRow(clsname,tab_row_id,doc,doctype,docname);

					return consump_list;

}


function make_json(doc){

		
			json_str = {};

			$( '.tab_list' ).each(function( ix , ex ) {

				var obj = $(ex).find('tr:not(:last)');

				var curr_tab_id =  $(ex).attr('id');

				var current_tab = {};	

				$( obj ).each(function( ind , elem ) {

					var curr_row = {};

					var all_widget = $(elem).find('.input_row');

					$( all_widget ).each(function( inx , el ) {

						var ob = {};
						var nm = $(el).attr('name');

						var vl = $(el).val();	

						curr_row[nm] = vl;

					});	
				
					current_tab[ind] = curr_row;
				});

				var total_sub = {}

				$(ex).find('tr:last').children().each(function(i,e){

						var ins = $(e).find('input');

						if( $(ins).length > 0 ){
							//console.log( $(ins) );



							total_sub[ $(ins).attr('class') ] = $(ins).val();
						}
						
				});

				current_tab['total_sub'] = total_sub;

				json_str[curr_tab_id] = current_tab;

			});

			var tab_names = {};

			$( '.tab-name-list' ).each(function( ix , ex ) {


				tab_names[ix] = $(ex).val();


			});


			//console.log( current_form['calc_cols'] );

			var final_str = { tabs_names:tab_names,links:current_links,form:current_form,tab_count:total_table,table_info:json_str}
			
			cur_frm.doc['measure_info'] = JSON.stringify(final_str);

			// console.log( cur_frm.doc['measure_info'] );

}

function registerOnchngeMethod(doc,doctype,docname){

	

	$('body').on('change','.input_row',function(e){


			if( $(this).hasClass('is_total') ){


				var curr_td = $(this).parent();


				var column = $(this).parent().parent().children().index(curr_td);
				
				//console.log( column  );

				var total = 0;

				$(this).parent().parent().parent().children().each(function(){  
			    	var val = $(this).find('td').eq(column).find('.is_total').val();

			    	// console.log( val );

			    	if(val)
			    		total += parseInt(val);

			    });

			    // console.log( total );


			    var tot_fld = current_form.calc_cols[column];

			    $(curr_td).parent().parent().find('.'+tot_fld).val(total);

			    current_form.calc_cols_val[tot_fld]  = total;

			    //.val(total);
			}

			make_json(doc);

		});


		$('body').on('click','#add_tab',function(e){

			
			var tabs = '';

			total_table++;


			/* signaling rendering condition not to set current form total value*/
			is_add_tab =1;

			

			//console.log( 'tot_tabs::' + total_table );

			tabs += render_table(doc,doctype,docname,1);

			 setTimeout(function(){ $("body").find("#render_tabs").append(tabs); 

					 	
			}, 100);

			 //

		});

		$('body').on('click','.delete_row',function(e){

			var r = confirm("Are You Sure You Want to Delete This Row?");
			if (r == true) {

					var isTot = $(this).parent().parent().children().find('.is_total');

					// console.log( isTot );

					$(isTot).each(function(i,e){

						$(e).val(0).change();

					});


					$(this).parent().parent().remove();

					make_json(doc);

			} 
			
		});

		$('body').on('click','.del_tab',function(e){

			var r = confirm("Are You Sure You Want to Delete This Table?");
			if (r == true) {

				$(this).parent().remove();

				total_table--;

				make_json(doc);

			}	

		});


	$('body').on('click','.add-row',function(e){
		///alert('a')
		//e.preventDefault();


		var obj_id = $(this).prev().find('tbody').attr('id');


		// console.log( 'follow......' );

		if( $('#' + obj_id).find('tr:not(.grad-total)').length > 0 ){

			
			var lasts = $('#'+obj_id).find('tr:not(.grad-total):last');

			// console.log( lasts );

			var rw = $(lasts).clone();

		    $(rw).find("select").each(function(i){

		        this.selectedIndex = $(lasts).find("select")[i].selectedIndex;
		    })



					

			$('#'+obj_id).find('tr.grad-total').before(rw);


			var isTot = $(rw).find('.is_total');

					// console.log( isTot );

					$(isTot).each(function(i,e){

						var cc = $(e).val();

						$(e).change();

					});

	


		}else{

				$('#'+obj_id).find('tr.grad-total').before(drawRow(doc,doctype,docname));
	
		}
		
		// confirmation(this);

		// $(this).parent().siblings().find('input').trigger('change');
		
		//console.log( 'ccccc' );


		//$('#'+obj_id).find('tr.grad-total').before(drawRow(doc,doctype,docname));

	
		make_json(doc);


		});


	$('body').on('change','.tab-name-list',function(e){

			//console.log('animesh');
	
				make_json(doc);


		});
	
}



function drawRow(row, doc,doctype,docname,add_new){

	row = row ? row : '';

	add_new	 = add_new ? add_new : '';



	//unit = ['Cm','','','Inch','Gm','Pcs','%'];
	var consump_list = '';

	//console.log('ttt::'+current_row);
		
	consump_list += '<tr id="row-'+current_row+'">';


	//if(!row){

			// consump_list += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" style="">';
	consump_list += drawCol(row, doc,doctype,docname);	

		
	//} else{}


	consump_list += '<td style="padding:8px 2px;" data-fieldname="" class="col ir_action_0 grid-overflow-ellipsis grid-static-col"><a name="delete_row_0" class="delete_row" href="javascript:void(0)"><i class="icon-trash icon-white"></i></a></td>';	
		



	consump_list += '</tr>';

					//}
					
return	consump_list;

}





function drawCol(row, doc,doctype,docname,add_new){

	row = row ? row : '';

	add_new	 = add_new ? add_new : '';

var consump_list = '';

var cls = 'col-';

st = current_row;

	for (var i in current_form.columns){


						var pref = current_form.columns[i].prefix;

						pref = '<label class="setMarks col-xs-12">'+pref+'</label>';
						var val = '';
						if(add_new){
							var inp = InputYpes(current_form.columns[i].fld_name,current_form.columns[i].link,current_form.columns[i].type,0,cls,st,i);

						}
						else if( !doc.__islocal ){

							var val = row[current_form.columns[i].fld_name];

							var inp = InputYpes(current_form.columns[i].fld_name,current_form.columns[i].link,current_form.columns[i].type,0,cls,st,i,val);

						}else{

							var inp = InputYpes(current_form.columns[i].fld_name,current_form.columns[i].link,current_form.columns[i].type,0,cls,st,i);

						}

							lbl = current_form.columns[i].suffix;

							lbl = '<label class="setMarks col-xs-12">'+lbl+'</label>';

				    consump_list += '<td class="col grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" style="font-size: 13px;padding:8px 2px;">'+inp+'</td>';

		}



	return consump_list;
}



function getDBval(fn_name){


	var fn_str = [fn_name[0],fn_name[0],fn_name[1]].join(".");


 	var result = '';

	frappe.call({
				method: "garments.merchandising.doctype."+fn_str,
				async:false,
				
				callback: function(r) {
				
				//data = JSON.parse( r.message[0][0] );

					result = r.message;
				 }

			})

	return result;

}


function AddRow(clsname,tabid, doc,doctype,docname){

}




function getLinks(){


//console.log('x::'+current_form);

    var cols = current_form.columns; 	

	for (x in cols){

		if( cols[x].link ){

			total_links.push([cols[x].link,cols[x].fn_name]);

		}
	}



}



/************************************JSON FORMS START*******************************/

function InputYpes(fld_name,link,name,val,cls,st,i,vals){


	html = '';


	var tot_cls = '';

	for ( var i in current_form.calc_cols){

			if( current_form.calc_cols[i] == fld_name+'-total'){

				tot_cls = 'is_total';
			}

	}

	if(fld_name+'-total')

	if(name=='text'){


			vals = vals || '';

			html = '<input type="text" class="form-control setMarks col-xs-12 input_row '+tot_cls+' '+cls+i+'" style="padding:1px 5px;" size="15" name="'+fld_name+'" value="'+vals+'" id="'+st+'-'+i+'">';

	}else if(name=='select'){

		//class="setMarks col-xs-12 input_row
		//style="padding:1px 5px;

		html = '<select  class="form-control setMarks col-xs-12 input_row '+tot_cls+'" name="'+fld_name+'" id="'+st+'-'+i+'">';

		var clink = current_links[link];

		var keys = Object.keys(clink[0]);

		//keys = keys.length==1 ? keys.push(keys[0]) : keys;

		//console.log(clink[0]); 		

		for(x in clink){

			var select = '';

			if(clink[x][keys[0]] == vals){

					select = 'selected';

			}

			if(keys.length==2){


				html +='<option '+ select+' values="'+clink[x][keys[0]]+'">'+ clink[x][keys[1]] +'</option>';

			}else{

				html +='<option '+ select+' values="'+clink[x][keys[0]]+'">'+ clink[x][keys[0]] +'</option>';
			}

		}

		html += '</select>';

	}

	//console.log(html);


	return html;
}


var forms_list = {


		
		'button_revet' : {


						'columns':
								[	{
										'label':'Style',
										'type':'select',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'stl',
										'link':'style_name',
										'fn_name':'style_info'

									},
									
									{
										'label':'Description',
										'type':'text',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'desc',
										'link':'',
										'fn_name':''

									},
									{
										'label':'Color',
										'type':'select',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'color',
										'link':'color',
										'fn_name':'color_info'

									},
									
									{
										'label':'Size/Pic',
										'type':'select',
										'prefix':'Diam.',
										'suffix':'mm',
										'class' :'',
										'fld_name':'size',
										'link':'size',
										'fn_name':'size_all'

									},
									{
										'label':'QTY',
										'type':'text',
										'prefix':'',
										'suffix':'Grs.',
										'class' :'qty-total',
										'fld_name':'ttl_qty',
										'link':'',
										'fn_name':''

									},
									{
										'label':'Rate',
										'type':'text',
										'prefix':'$',
										'suffix':'',
										'class' :'',
										'fld_name':'rate',
										'link':'',
										'fn_name':''

									}

								],

						'subject':'Eyelet order',

						'description':'Please find button & Revet break as per below-.',
								

						'calc_cols' : {'4':'ttl_qty-total'}, 
						'calc_cols_val' : {'ttl_qty-total':0} 
						
					},

					'rib_booking' : {


						'columns':
								[	
									{
										'label':'Used Size',
										'type':'select',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'size',
										'link':'size',
										'fn_name':'size_all'

									},
									{
										'label':'Measurement',
										'type':'text',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'measure',
										'link':'',
										'fn_name':''

									},
									{
										'label':'QTY',
										'type':'text',
										'prefix':'',
										'suffix':'Pcs',
										'class' :'qty-total',
										'fld_name':'ttl_qty',
										'link':'',
										'fn_name':''

									}
									,
									{
										'label':'RECV',
										'type':'text',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'recv',
										'link':'',
										'fn_name':''

									}
									,
									{
										'label':'Due',
										'type':'text',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'due',
										'link':'',
										'fn_name':''

									}
								],

						'subject':'Rib booking for Style# Deterreraj.',

						'description':'Please find rib booking as per below--',
						
						'calc_cols' : {'2':'ttl_qty-total'}, 
						'calc_cols_val' : {'ttl_qty-total':0} 
						
					},
					'tape' : {


						'columns':
								[	{
										'label':'Style',
										'type':'select',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'stl',
										'link':'style_name',
										'fn_name':'style_info'

									},
									
									{
										'label':'Description',
										'type':'text',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'desc',
										'link':'',
										'fn_name':''

									},
									{
										'label':'Color',
										'type':'select',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'color',
										'link':'color',
										'fn_name':'color_info'

									},
									
									{
										'label':'Size',
										'type':'select',
										'prefix':'Ans',
										'suffix':'',
										'class' :'',
										'fld_name':'size',
										'link':'size',
										'fn_name':'size_all'

									},
									{
										'label':'Req',
										'type':'text',
										'prefix':'',
										'suffix':'Cm.',
										'class' :'',
										'fld_name':'req',
										'link':'',
										'fn_name':''

									},
									{
										'label':'Short',
										'type':'text',
										'prefix':'',
										'suffix':'Pcs',
										'class' :'short-total',
										'fld_name':'short',
										'link':'',
										'fn_name':''

									},
									{
										'label':'Rate', 
										'type':'text',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'rate',
										'link':'',
										'fn_name':''

									}

								],

						'subject':'',
						'description':'PLEASE SUPPLY US THE FOLLOWING Harringbone Tape.',

						'calc_cols' : {'5':'short-total'}, 
						'calc_cols_val' : {'short-total':0} 
						
					},
					'poly' : {


						'columns':
								[	{
										'label':'Style',
										'type':'select',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'stl',
										'link':'style_name',
										'fn_name':'style_info'

									},
									
									{
										'label':'Size',
										'type':'select',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'size',
										'link':'size',
										'fn_name':'size_all'

									},
									{
										'label':'Measurement',
										'type':'text',
										'prefix':'',
										'suffix':'',
										'class' :'',
										'fld_name':'measure',
										'link':'',
										'fn_name':''

									},
									{
										'label':'TTL Qty',
										'type':'text',
										'prefix':'',
										'suffix':'Pcs',
										'class' :'qty-total',
										'fld_name':'qty',
										'link':'',
										'fn_name':''

									}
								],

						'subject':'',
						'description':'PLEASE SUPPLY US THE FOLLOWING INDIVIDUAL POLY.',

						'calc_cols' : {'3':'qty-total'}, 
						'calc_cols_val' : {'qty-total':0} 
						
					}






}

/*************************************JSON FORMS END******************************/ 