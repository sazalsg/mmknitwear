# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Declaration(Document):
	pass


@frappe.whitelist()
def dec_info(name):
	if name :
		# return filters
		sql = frappe.db.get_values("Declaration", {"name":name}, ["terms"])
		return sql
	else :
		return []

