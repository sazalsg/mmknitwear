cur_frm.cscript.attn = function(doc,doctype,docname){
	var text = "Dear " + doc.attn + ",<br /> We are sending you following samples-";
	cur_frm.set_value("description", text); 
}

cur_frm.cscript.copy_prev = function(doc,doctype,docname){

	var setLocal = locals[doctype][docname];

	var curr_name = setLocal.name;

	var curr_key;	


	for(var key in doc.frabics_list){
		if(key > 0){

			if(curr_name == doc.frabics_list[key].name){

					curr_key = key;
					break;
			}	
		}
	}

	setLocal.style = doc.frabics_list[curr_key-1].style;
	refresh_field("style", setLocal.name, setLocal.parentfield);

	setLocal.fabric = doc.frabics_list[curr_key-1].fabric;
	refresh_field("fabric", setLocal.name, setLocal.parentfield);

	setLocal.gsm = doc.frabics_list[curr_key-1].gsm;
	refresh_field("gsm", setLocal.name, setLocal.parentfield);


	setLocal.color = doc.frabics_list[curr_key-1].color;
	refresh_field("color", setLocal.name, setLocal.parentfield);

	setLocal.samples = doc.frabics_list[curr_key-1].samples;
	refresh_field("samples", setLocal.name, setLocal.parentfield);

	// setLocal.size = doc.frabics_list[curr_key-1].size;
	// refresh_field("size", setLocal.name, setLocal.parentfield);


	// setLocal.qty = doc.frabics_list[curr_key-1].qty;
	// refresh_field("qty", setLocal.name, setLocal.parentfield);
}