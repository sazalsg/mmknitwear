# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import msgprint, throw,_
from datetime import date
from frappe.model.naming import make_autoname
import time

class ProformaOrder(Document):
	def autoname(self):
		cname=frappe.db.get_value("Customer", {"name":self.buyer}, ["customer_name"])
		self.name =  make_autoname('{0}-{1}-{2}{3}{4}'.format("PO",cname.split()[0], str(date.today().year) , str(date.today().month), str(date.today().day) ) + '.###')

		
@frappe.whitelist()
def order_query(name):

	if name :
		# return filters
		sql = frappe.db.get_values("Proforma Order", {"name":name}, ["total_sum","total_qty"])

		return sql
	else :
		return []

