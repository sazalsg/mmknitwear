frappe.ui.form.on("Proforma Order Child","child_remove",function(frm){
       sumTotal(frm.doc,frm.doctype,frm.docname);
});

cur_frm.cscript.style_name = function(doc,doctype,docname){
	
	var setLocal = locals[doctype][docname];

	frappe.call({
		method:"frappe.client.get_value",
		args: {
			doctype: "PO Sheet Child Child",
			filters: {
				"product_name":setLocal.style_name,
			},
			fieldname : ["uvc","color","theme","description","qty","composition","etd_shipment_date"]
		},
		callback: function(r) {
			if(r.message){
				$.each(doc.child, function() {
			 	if(setLocal.style_name){
			 		setLocal.uvc = r.message["uvc"];
			 		setLocal.colors = r.message["color"];
			 		setLocal.theme = r.message["theme"];
			 		setLocal.description = r.message["description"];
			 		setLocal.qty_pcs = r.message["qty"];
			 		setLocal.composition = r.message["composition"];
			 		setLocal.shipment_date = r.message["etd_shipment_date"];
			 	}
			 });
				}else{
			 		setLocal.uvc = "";
			 		setLocal.colors = "";
			 		setLocal.theme = "";
			 		setLocal.description = "";
			 		setLocal.qty_pcs = "0";
			 		setLocal.composition = "";
			 		setLocal.shipment_date = "";
			 	}

			refresh_field("child");
		}
	});
}

cur_frm.cscript.qty_pcs = function(doc,doctype,docname){


		calcTotal(doc,doctype,docname);

		sumTotal(doc,doctype,docname);
}

cur_frm.cscript.unit_price = function(doc,doctype,docname){
	

	calcTotal(doc,doctype,docname);

	sumTotal(doc,doctype,docname);
}
// calculte child tables total field  
function sumTotal(doc,doctype,docname){

			var sum = 0;

			var sum_qty = 0;
			// Check Current Row Duplicate Entry
			for(var key in doc.child){


				var res = parseFloat(doc.child[key].total);
				if( res ){
					sum+= res;
				}

				var res = parseInt(doc.child[key].qty_pcs);
				if( res ){
					sum_qty+= res;
				}
			}

			cur_frm.set_value("total_sum", sum.toFixed(2));
			cur_frm.set_value("total_qty", sum_qty); 
}
function calcTotal(doc,doctype,docname){

	var setLocal = locals[doctype][docname];

	var qty = parseFloat(setLocal.qty_pcs);

	var unit = parseFloat(setLocal.unit_price);

	if( qty && unit ){

		setLocal.total = qty*unit;
		refresh_field("total", setLocal.name, setLocal.parentfield);
	} else {

		setLocal.total = "";
		refresh_field("total", setLocal.name, setLocal.parentfield);
	}
}

cur_frm.cscript.copy_prev = function(doc,doctype,docname){

	var setLocal = locals[doctype][docname];

	var row_no = (setLocal.idx - 2);

	setLocal.uvc = doc.child[row_no].uvc;
	refresh_field("uvc", setLocal.name, setLocal.parentfield);

	setLocal.style_name = doc.child[row_no].style_name;
	refresh_field("style_name", setLocal.name, setLocal.parentfield);

	setLocal.colors = doc.child[row_no].colors;
	refresh_field("colors", setLocal.name, setLocal.parentfield);

	setLocal.theme = doc.child[row_no].theme;
	refresh_field("theme", setLocal.name, setLocal.parentfield);

	setLocal.description = doc.child[row_no].description;
	refresh_field("description", setLocal.name, setLocal.parentfield);

	setLocal.shipment_date = doc.child[row_no].shipment_date;
	refresh_field("shipment_date", setLocal.name, setLocal.parentfield);

	setLocal.composition = doc.child[row_no].composition;
	refresh_field("composition", setLocal.name, setLocal.parentfield);
}