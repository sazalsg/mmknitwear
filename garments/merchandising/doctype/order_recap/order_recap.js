frappe.ui.form.on("Order ReCap Child", "orc_cld_remove", function(frm){
		sumTotal(frm.doc,frm.doctype,frm.docname);
});

cur_frm.cscript.style = function(doc,doctype,docname){
	
	var setLocal = locals[doctype][docname];
	
	for(var key in doc.orc_cld){
		if(doc.orc_cld[key]['style'] == setLocal.style && doc.orc_cld[key]['name'] != setLocal.name){
			    setLocal.style = ""; 
			    refresh_field("style",setLocal.name,"orc_cld");
			    msgprint("Already Added This Style!")
			    return false;
		}
	}

	frappe.call({
		method: "frappe.client.get_value",
		args: {
			doctype: "Order ReCap Child",
			filters:{
				"style":setLocal.style,
			},
			fieldname : ["style"]
		},
		callback: function (data) {
			if(!data.message){

				frappe.call({
					method:"frappe.client.get_value",
					args: {
						doctype: "PO Sheet Child Child",
						filters: {
							"product_name":setLocal.style,
						},
						fieldname : ["purchase_price","description","size_format","qty"]
					},
					callback: function(r) {
						//msgprint(r.message)
						$.each(doc.orc_cld, function() {
						 	if(setLocal.style){
						 		setLocal.po_prc = r.message["purchase_price"];
						 		setLocal.item = r.message["description"];
						 		setLocal.siz_rng = r.message["size_format"];
						 		setLocal.qty = r.message["qty"];
						 		setLocal.wth_per = r.message["purchase_price"];
						 	}
						 });
						refresh_field("orc_cld");
						sumTotal(doc,doctype,docname);
					}
				});

			}else{
			    setLocal.style = ""; 
			    refresh_field("style",setLocal.name,"orc_cld");
			    msgprint("Already Created This Style!")
			}
		}
	});
			// 	frappe.call({

			// 	method:"frappe.client.get_value",
			// 	args: {
			// 		doctype: "Costing",
			// 		filters: {
			// 			"reference":setLocal.style,
			// 		},
			// 		fieldname : ["total_local_commission","g_total"]
			// 	},
			// 	callback: function(r) {
			// 		//msgprint(r.message)
			// 		$.each(doc.orc_cld, function() {
			// 		 	if(doc.total_local_commission){
			// 		 		setLocal.our_prc = (r.message["purchase_price"]-(r.message["purchase_price"]*(3/100))-r.message["total_local_commission"]);
			// 		 	}
			// 		 });
			// 		refresh_field("orc_cld");
			// 		sumTotal(doc,doctype,docname);
			// 	}
			// });

			// 	frappe.call({

			// 	method:"frappe.client.get_value",
			// 	args: {
			// 		doctype: "Buyer Quotation Child",
			// 		filters: {
			// 			"ref":setLocal.style,
			// 		},
			// 		fieldname : ["per_pcs_price"]
			// 	},
			// 	callback: function(r) {
			// 		//msgprint(r.message)
			// 		$.each(doc.orc_cld, function() {
			// 		 	if(doc.ref){
			// 		 		setLocal.cmsn = (r.message["purchase_price"]-(r.message["purchase_price"]*(3/100))-r.message["g_total"]);
			// 		 	}
			// 		 });
			// 		refresh_field("orc_cld");
			// 		sumTotal(doc,doctype,docname);
			// 	}
			// });
}

cur_frm.cscript.qty = function(doc,doctype,docname){

		sumTotal(doc,doctype,docname);
}
cur_frm.cscript.prcnt = function(doc,doctype,docname){
	if (doc.prcnt <0 ){
		cur_frm.set_value('prcnt', 3)
		msgprint('Invalid Input')
	}
	else if(doc.prcnt >99 ){
		cur_frm.set_value('prcnt', 3)
		msgprint('Limit Cross')
	}
}
function sumTotal(doc,doctype,docname){
	var t_qty = 0;
	for (var key in doc.orc_cld){
		if (doc.orc_cld[key].qty);
		t_qty+=(doc.orc_cld[key].qty);
	}
	cur_frm.set_value("ttl_qty",t_qty,"");
}