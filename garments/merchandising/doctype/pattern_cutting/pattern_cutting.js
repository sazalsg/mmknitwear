
garmentsMerchandisingPatternCuttingSizeRange = frappe.ui.form.Controller.extend({
    onload:function(doc,doctype,docname){

        this.frm.set_query("item", function() {
            return {
                "filters": {
                    "item_group": "Products",
                }
            };
        });

        this.frm.add_fetch("reference","buyer","buyer")
        this.frm.add_fetch("reference", "item", "item")
    },
    refresh: function(doc, cdt, cdn) {

        cur_frm.fields_dict['pattern_child'].grid.get_field('size').get_query = function(doc) {

            var size_format = cur_frm.doc.size_format;

            return {
                query: "garments.merchandising.doctype.pattern_cutting.pattern_cutting.size_filter",
                searchfield: "size",
                doctype: "Size Format Child",
                filters: {
                    "parent": size_format
                },
                txt: 'idx asc'
            }
        };

    },
    refresh:function (doc,cdt,cdn) {
        var me = this;
        setTimeout(function(){me.remove_add_new()},100)

    }, 
    remove_add_new:function () {
        cur_frm.form_wrapper.find('[data-fieldname="pattern_child"] .grid-footer').remove();
    }
});

 cur_frm.cscript = new garmentsMerchandisingPatternCuttingSizeRange({frm:cur_frm})

// calculte child tables total field  

frappe.ui.form.on("Pattern Cutting Child", "pattern_child_remove", function(frm){

        sumTotal(frm.doc,frm.doctype,frm.docname);
});

function sumTotal(doc,doctype,docname){

            var sum_qty = 0;
            // Check Current Row Duplicate Entry
            for(var key in doc.pattern_child){
                var res = parseInt(doc.pattern_child[key].qty);
                if( res ){
                    sum_qty+= res;
                }
            }
            cur_frm.set_value("total_qty", sum_qty); 
}

cur_frm.cscript.qty = function(doc,doctype,docname){
    sumTotal(doc,doctype,docname);
}

// Child auto fillup
frappe.ui.form.on("Pattern Cutting", "size_format", function(frm) {
    // Row render
    frappe.model.with_doc("Size Format",frm.doc.size_format,function () {
        size_format = frappe.model.get_doc("Size Format",frm.doc.size_format);
        frm.doc.pattern_child = "";
        refresh_field('pattern_child');
        $.each(size_format.format_child, function(index, val) {
            var row = frappe.model.add_child(frm.doc,"Pattern Cutting Child","pattern_child");
                row.size = val.size
        });
        refresh_field('pattern_child');
        sumTotal(frm.doc);
    })
});