frappe.provide("garments.merchandising");

cur_frm.add_fetch("item","item_name","item_name")
cur_frm.add_fetch("item","stock_uom","uom")

frappe.confirm = function(message, ifyes, ifno) {
	var d = new frappe.ui.Dialog({
		title: __("Select Accessories List"),
		fields: [
			{fieldtype:"HTML", options:"<p class='frappe-confirm-message'>" + message + "</p>"}
		],
		primary_action_label: __("SET"),
		primary_action: function() {
			ifyes();
			d.hide();
		}
	});
	d.show();
	// no if closed without primary action
	if(ifno) {
		d.onhide = function() {
			if(!d.primary_action_fulfilled) {
				ifno();
			}
		};
	}
	return d;
}

garments.merchandising.SamplingController = frappe.ui.form.Controller.extend({
 	onload:function (doc) {
		
frappe.ui.form.on("Sampling Child","sm_cld_remove",function(frm){
		  
		  cur_frm.cscript.total(frm.doc);

		});
	},
	qty:function (doc) {
		this.total(doc);
	},
	total:function (doc) {
		var total = 0;
		for (var i = doc.sm_cld.length - 1; i >= 0; i--) {
			total += doc.sm_cld[i].qty;
		};
		this.frm.set_value('total',total);
	},
	acc_list:function (doc, dt, dn) {
var all_checkboxes = $("input:checkbox[name=acc_list]:checked"); // choose & store all checkboxes
all_checkboxes.prop('checked', false); // uncheck all of them

frappe.call({
	method:"frappe.client.get_list",
	args:{
		doctype:"Item",
		filters: [
			["item_group","=", 'Accessories'],
		],
		fields: ["name", "item_name", 'stock_uom']
	},
	cache:false,
	callback: function(r) {

		var list = '<ul style="text-align: left;" id="acc_list">';

		$.each(r.message, function( index, value ) {
			list += '<li style="list-style:none;"><input type="checkbox" name="acc_list" value="'+value['name']+'"> '+value['item_name']+'</li>';
		});

		list += '</ul>';

		list += '<div class="clearfix"></div>';

		frappe.confirm(__(list),
			function() {
				
			doc.s_acc = ""

			$("input:checkbox[name=acc_list]:checked").each(function(){

				var checkedVal = $(this).val();

				var itemList = r.message.filter(function (el) {
					return el.name == checkedVal;
				});

				var row = frappe.model.add_child(doc, "Sampling Child Accessories", "s_acc");
				row.item = itemList[0].item_name;
				row.item_name =  itemList[0].item_name;
				row.uom =  itemList[0].stock_uom;
			});
				refresh_field("s_acc");

					}
				);	
			}
		});
	}
})
cur_frm.cscript = new garments.merchandising.SamplingController({frm: cur_frm});