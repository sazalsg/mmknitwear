frappe.provide("garments.merchandising");
frappe.require("assets/garments/js/controllers/consumption_sheet.js",function (r) {

});

cur_frm.add_fetch("reference","buyer","buyer");

var arr = ['LENGTH','DIA','PIECE'];

//cur_frm.cscript.
garments.merchandising.ZipperBookingController = frappe.ui.form.Controller.extend({
    onload:function () {
        spData =[];

    },
    reference : function (doc){
        frappe.call({
            method:"frappe.client.get_list",
            async:false,
            args:{
                doctype:"Consumption Sheet",
                filters:[
                    ['reference', '=' , doc.reference],
                    ['name', '!=' , doc.name]
                ],
                fields:['reference']
            },
            callback:function (r) {
                if( Object.keys(r).length>0){
                    msgprint('Reference/Style Name Already Used');
                    cur_frm.set_value("reference","");

                }
            }
        });
    },
    refresh:function () {

        setTimeout(function (doc) {
            doc = cur_frm.doc;
            me = cur_frm.cscript;
            me.section_render(doc)

            if (!doc.__islocal) {

                var sel = $('#html').find('.grid-body');
                for (var i = 0; i < doc.childs.length; i++) {

                    if(doc.childs[i].dia_width =='DIA'){

                        arr = ['LENGTH','DIA','PIECE'];

                    }else{

                        arr = ['LENGTH','WIDTH','PIECE'];
                    }


                    me.add_new_table(doc,sel);
                    var item = $(sel).find('.item:last');
                    if (typeof doc.childs[i].name !== "undefined") {
                        $(item).attr('data-name', doc.childs[i].name)
                    };
                    var parsejson = JSON.parse(doc.childs[i].datas);
                    parsejson.header['item'] = doc.childs[i].item;
                    me.render_header(item,parsejson.header);

                    me.render_footer(item,parsejson.footer);
                    // $(item).find('.tb-body').html("");

                    for (var j = 0; j < parsejson.body.body.length; j++) {

                        var its = parsejson.body.body[j];its['format_child'][its.format_child.length]=arr[j];
                        me.render_body(item,its)
                    }
                    $(item).find('input[data-fieldname="size"]').val(doc.childs[i].size);
                    $(item).find('input[data-fieldname="item"]').val(doc.childs[i].item);

                    $(item).find('select[data-fieldname="consump_type"]').val(doc.childs[i].consump_type);
                    $(item).find('select[data-fieldname="dia_width"]').val(doc.childs[i].dia_width);



                    me.check_mandetory($(item).find('input[data-fieldname="size"]'));
                    me.check_mandetory($(item).find('input[data-fieldname="item"]'));
                    me.check_mandetory($(item).find('select[data-fieldname="consump_type"]'));
                    me.check_mandetory($(item).find('select[data-fieldname="dia_width"]'));
                    // msgprint(parsejson.body.body[0])
                }
            }
        },100);
    },
    // attn:function (doc) {
    // 	var old_val = (doc.description).split('\n');
    // 	old_val.splice(0,1);
    // 	var value = "Dear "+doc.attn +"\n"+old_val.join('\n');
    // 	this.frm.set_value('description',value);
    // 	// msgprint(newtext);
    // },
    section_render:function (doc) {
        var me = this;
        this.html = $('body').find('#html');
        this.html.html($(frappe.render_template("section_body", {}))
            .attr('date-fieldname', 'childs'));
        // Bind Add Table Event
        $(this.html).find(".sec-add-table").click(function() {
            var sel = $(this).parents(".grid-body");
            me.add_new_table(doc,sel);
            return false;
        });
    },
    add_new_table:function (doc,selector) {
        var me = this;
        var table = $($(selector).find('.rows').append(frappe.render_template("table_render",{})));
        // Make Reference Field
        var reference = frappe.ui.form.make_control({
            parent: table.find(".reference:last"),
            df: {
                fieldtype: "Link",
                options: "Item",
                fieldname: "item",
                reqd:1
            },
            only_input: true
        });
        reference.refresh();
        // Create Size Field
        var size = frappe.ui.form.make_control({
            parent: table.find(".size:last"),
            df: {
                fieldtype: "Link",
                options: "Size Format",
                fieldname: "size",
                get_query: function () {
                    return {
                        filters:[
                            ["Size Format","docstatus","=",1]
                        ]
                    }
                },
                reqd:1
            },
            only_input: true
        });
        size.refresh();

        // Create Size Field
        var consump_type = frappe.ui.form.make_control({
            parent: table.find(".consump_type:last"),
            df: {
                fieldtype: "Select",
                options: "Open\nTube",
                fieldname: "consump_type",
                reqd:1
            },
            only_input: true
        });
        consump_type.refresh();

        // Create Size Field
        var dia_width = frappe.ui.form.make_control({
            parent: table.find(".dia_width:last"),
            df: {
                fieldtype: "Select",
                options: "DIA\nWIDTH",
                fieldname: "dia_width",
                reqd:1
            },
            only_input: true
        });
        dia_width.refresh();

        // SIze Onchange Event
        $(table).find('select[data-fieldname="dia_width"]').change(function(event) {

            var tab = $(this).parent().parent().next().next().find('table');

            // console.log(tab);

            var val = $(this).val();

            if($(tab).find("label:contains('DIA')").length>0){

                $(tab).find("label:contains('DIA')").text(val);

            }

            if($(tab).find("label:contains('WIDTH')").length>0){

                $(tab).find("label:contains('WIDTH')").text(val);

            }

        });

        // SIze Onchange Event
        $(table).find('input[data-fieldname="size"]').change(function(event) {
            var sel = $(this);
            me.get_sizes($(sel).val());
            me.render_header($(sel).parents('.item'),spData.sizes);
            me.render_footer($(sel).parents('.item'),spData.sizes);
            $(this).parents('.item').find('.tb-body').html("");

            var tota_arr = [];

            for(var i = 0 ; i<3;i++){

                var temp = jQuery.extend(true, {}, spData.sizes);

                temp.format_child.push(arr[i]);

                me.render_body($(sel).parents('.item'),temp);
                //tota_arr.push( temp );

            }


            me.check_mandetory(sel);
        });
        // SIze Onchange Event
        $(table).find('input[data-fieldname="item"]').change(function(event) {
            $(this).parents(".item").find('.ref_id').html($(this).val());
            me.check_mandetory($(this));

            var cpy = $(this).val();

            var cus = $(this);

            var count = 0;

            //console.log( ' cpy :: ' + cpy );

            //if(cpy){

            $(table).find('input[data-fieldname="item"]').each(function( index ) {

                if(cpy == $(this).val()){

                    count++;
                }

            });

            if( count > 1){

                // console.log( $(cus).val() );

                //$(cus).html('');
                msgprint(__("Item Was Chosen Already"));
            }
            //}
            //console.log('aaa');
        });





        // Event : Consumption Type Onchange
        $(table).find('select[data-fieldname="consump_type"]').change(function(event) {
            var myval = $(this).val();

            if(myval == 'Open'){
                $(table).find('select[data-fieldname="dia_width"]').val('WIDTH')
            }else{
                $(table).find('select[data-fieldname="dia_width"]').val('DIA')
            }

            $(table).find('select[data-fieldname="dia_width"]').trigger('change');
        });

    },
    make_consumption_workout:function (doc,selector) {
        var parent = $(selector).parents('.item');
        cur_frm.doc['selected_item'] = $(parent).find('input[data-fieldname="item"]').val();
        cur_frm.doc['selected_size'] = $(parent).find('input[data-fieldname="size"]').val();

        this.open_mapped_doc({
            method: "garments.merchandising.doctype.consumption_sheet.consumption_sheet.make_consumption_workout",
            frm: cur_frm,
            item:$(parent).find('input[data-fieldname="item"]').val(),
            size:$(parent).find('input[data-fieldname="size"]').val(),
            run_link_triggers:true
        })
    },
    open_mapped_doc: function(opts) {
        if (opts.frm && opts.frm.doc.__unsaved) {
            frappe.throw(__("You have unsaved changes in this form. Please save before you continue."));

        } else if (!opts.source_name && opts.frm) {
            opts.source_name = opts.frm.doc.name;
        }

        return frappe.call({
            type: "POST",
            method: opts.method,
            args: {
                "source_name": opts.source_name,
                "item":opts.item,
                "size":opts.size
            },
            freeze: true,
            callback: function(r) {
                if(!r.exc) {
                    frappe.model.sync(r.message);
                    if(opts.run_link_triggers) {
                        frappe.get_doc(r.message.doctype, r.message.name).__run_link_triggers = true;
                    }
                    frappe.set_route("Form", r.message.doctype, r.message.name);
                }
            }
        })
    },

    test:function () {
        msgprint("Test")
    },
    get_sizes:function (sel) {
        frappe.call({
            method:"frappe.client.get",
            async:false,
            args:{
                doctype:"Size Format",
                name:sel
            },
            callback:function (r) {
                if (r.message) {
                    spData['sizes'] = r.message
                }
            }
        });
    },
    render_header:function (selector,data) {
        var parent =  $(selector);
        data['idx'] = $(parent).index()+1;
        if (!('item' in data)) {
            data['item'] = $(parent).find('input[data-fieldname="item"]').val();
        }
        $(parent).find('.tb-header').html(frappe.render_template("header_row",data));

        // // Create Size Field
        // var first_rei_date = frappe.ui.form.make_control({
        // 	parent: parent.find(".first_rei_date"),
        // 	df: {
        // 		fieldtype: "Date",
        // 		fieldname: "first_rei_date",
        // 	},
        // 	only_input: true
        // });
        // first_rei_date.refresh();
        // Create Size Field
        // var sec_rei_date = frappe.ui.form.make_control({
        // 	parent: parent.find(".sec_rei_date"),
        // 	df: {
        // 		fieldtype: "Date",
        // 		fieldname: "sec_rei_date",
        // 	},
        // 	only_input: true
        // });
        // sec_rei_date.refresh();
        // set value if have
        //parent.find('.first_rei_date input').val('first_rei_date' in data ? data.first_rei_date:'')
        //parent.find('.sec_rei_date input').val('sec_rei_date' in data ? data.sec_rei_date:'')
    },
    render_footer:function (selector,data) {
        var parent =  $(selector);
        date = footer_missing_key(data);
        $(parent).find('.tb-footer').html(frappe.render_template("footer_row",data));
    },
    render_body:function (selector,data) {
        this.get_colors();
        var parent =  $(selector);
        data = body_missing_key(data);
        data['idx'] = $(parent).index()+1;
        data['colors'] = spData.colors;

        $(parent).find('.tb-body').append(frappe.render_template("row",data));
    },
    get_colors:function () {
        if (!('colors' in spData)) {
            frappe.call({
                method:'garments.merchandising.doctype.sewing_booking.sewing_booking.colors',
                async:false,
                callback:function (r) {
                    if (r.message) {
                        spData['colors']=r.message;
                    };
                }
            });
        }
    },
    check_mandetory:function (sel) {
        if ($(sel).val() !="") {
            $(sel).parents('.form-group.frappe-control').removeClass('has-error');
        }else{
            $(sel).parents('.form-group.frappe-control').addClass('has-error');
        }
    },
    validate:function (doc) {
        if ($('#html').find('.has-error').length >0) {
            $('#html').find('.has-error').each(function(index, el) {
                msgprint(__("Plese fill mandetory field {0}",[$(el).attr('title')]))
            });
        }
        this.set_child_value(doc)
    },
    set_child_value:function (doc) {
        doc['childs']=[];
        $('#html').find('.item').each(function(key, sel) {
            var item = {};
            item['item']=$(sel).find('input[data-fieldname="item"]').val();
            item['size']=$(sel).find('input[data-fieldname="size"]').val();
            // alert($(sel).find('input[data-fieldname="consump_type"]').val())
            item['consump_type']=$(sel).find('select[data-fieldname="consump_type"]').val();
            item['dia_width']=$(sel).find('select[data-fieldname="dia_width"]').val();
            var table={};
            // Header Value
            var rows = [];
            $(sel).find('.dync_row').each(function(index, el) {
                var row = {};
                row['size'] = $(el).text();
                row['val'] = $(el).parents('.tb-header').find('input[name="'+$(el).text()+'"]').val();
                rows.push(row);
            });
            var header = {};
            header['format_child'] = rows;
            //header['first_rei_date'] = $(sel).find('.first_rei_date input').val()
            //header['sec_rei_date'] = $(sel).find('.sec_rei_date input').val();
            table['header']=header;
            // Footer Value
            var footer = {};
            footer['format_child'] = []
            $(sel).find('.tb-footer input:not([name="ttl-total_col"])').each(function(fidx, fel) {
                var row = {};
                row['size'] = $(fel).attr('name').replace('ttl-','');
                row['val'] = $(fel).val();
                footer['format_child'].push(row);
            });
            footer['ttl_total_col']=$(sel).find('.tb-footer input[name="ttl-total_col"]').val();
            table['footer']=footer;
            // Body Value
            var body = [];
            var static_fields = ['desc_one','desc_two','color','total_col','first_pri','secound_pri','remark'];
            $(sel).find('.tb-body tr').each(function(bidx, bval) {
                b_row = {};
                // set static fields value
                $.each(static_fields,function(i,v) {
                    b_row[v] = $(bval).find('input[name="'+v+'"], select[name="'+v+'"]').val();
                });
                // set format_child
                b_row['format_child']=[];

                $(bval).find('input').each(function(k,rv) {
                    var row ={};
                    if ((static_fields.indexOf($(rv).attr('name'))) == -1) {
                        row['size'] = $(rv).attr('name');
                        row['val'] = $(rv).val();
                        b_row['format_child'].push(row);
                    };
                });
                body.push(b_row);
            });
            table['body']={"body":body};
            item['datas'] = JSON.stringify(table);
            doc['childs'].push(item)
        });
    }



});

cur_frm.cscript = new garments.merchandising.ZipperBookingController ({ frm:cur_frm });