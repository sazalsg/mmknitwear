# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.model.naming import make_autoname
from frappe import msgprint, throw,_
from frappe.model.mapper import get_mapped_doc

class ConsumptionSheet(Document):
	def autoname(self):
		self.name = make_autoname("CNSP-{b}-{y}/{m}-.##".format(b=self.buyer or "", y = str(date.today().year)[2:4],m=date.today().month))

	def before_insert(self):
		cname=frappe.db.get_value("Consumption Sheet", {"reference":self.reference}, ["reference"])

		if(cname):
			frappe.throw('Reference/Style Name Already Used')

@frappe.whitelist()
def get_items_lst(doctype, txt, searchfield, start, page_len,filters):

	cs_nm = filters['cs_nm']
	if cs_nm :
		# return filters
		sql = frappe.db.get_values("Consumption Sheet Child", {"parent":cs_nm}, ["item"])

		return sql
	else :
		return []


@frappe.whitelist()
def sheet_by_name(prt,itm,siz):

	# prt = filters['prt']
	# itm = filters['itm']
	# siz = filters['siz']


		# return filters
	sql = frappe.db.get_value("Consumption Sheet Child", {"parent":prt,"size":siz,"item":itm}, ["datas"])
	return sql


@frappe.whitelist()
def make_consumption_workout(source_name, target_doc=None, item=None, size=None):

	def postprocess(source, doc):
		# pass
		doc.item = item
		doc.size = size
		# doc.run_method('set_missing_values')
		# doc.run_method('reset_table')
	doc = get_mapped_doc("Consumption Sheet", source_name, {
		"Consumption Sheet": {
			"doctype": "Consumption Workout",
			"field_map":{
				"reference":"style",
				"name":"consumer_sheet"
			}
		}
	}, target_doc, postprocess)

	return doc