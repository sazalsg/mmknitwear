# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import throw, msgprint

class OrderSheet(Document):
	
	# def validate(self):
	# 	throw(self.fabric_requirement_data)


	def get_format(self):

		get_format=frappe.db.sql("""SELECT `tabSize Format Child`.`size` FROM `tabConsumption Workout` LEFT JOIN `tabSize Format` ON `tabConsumption Workout`.`size` = `tabSize Format`.`name` LEFT JOIN `tabSize Format Child` ON  `tabSize Format`.`name` = `tabSize Format Child`.`parent` WHERE `tabConsumption Workout`.`name` = '{consumption}' ORDER BY `tabSize Format Child`.`idx` ASC""".format(consumption=self.consumption_workout))

		colors=frappe.db.get_values("Color", {"name":("!=", "")}, ["color_name","name"],as_dict=1)


		consumption=frappe.db.sql("""SELECT `tabConsumption Workout`.`rib_dia`, `tabConsumption Workout Child`.`consumption_data` FROM `tabConsumption Workout` LEFT JOIN `tabConsumption Workout Child` ON `tabConsumption Workout`.`name`=`tabConsumption Workout Child`.`name` WHERE `tabConsumption Workout`.`name` = '{}'""".format(self.consumption_workout), as_dict = 1)


		return {"format":get_format, "colors":colors, "consumption":consumption}
		# throw("get_consumption")
