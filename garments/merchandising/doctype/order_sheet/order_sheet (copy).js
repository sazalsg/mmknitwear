frappe.provide("garments.merchandising");
cur_frm.add_fetch('consumption_workout', 'buyer', 'buyer');
cur_frm.add_fetch('consumption_workout', 'item', 'item');

var total_row = 0;
var cons_dia = [];
var cons_consumption = [];
var cons_rib = 0;

frappe.ui.form.on("Order Sheet", "after_save", function(frm) {
	location.reload();
});

cur_frm.cscript.onload = function (doc, dt, dn) {

	var consumptionLayout = "";


	if(!doc.__islocal){

		cur_frm.set_df_property("consumption_workout", 'read_only', true)
		cur_frm.set_df_property("buyer", 'read_only', true)
		cur_frm.set_df_property("date", 'read_only', true)
		cur_frm.set_df_property("item", 'read_only', true)
		cur_frm.set_df_property("with_ratio", 'read_only', true)


		frappe.call({
			method: "get_format",
			doc: doc,
			async:false,
			callback: function(r, rt) {
				consumptionLayout = r.message
			}
		});

		// msgprint(consumptionLayout)
		if(doc.with_ratio){
			// If click "Add New Raw" ->

			add_new_row("#ratio", 1, consumptionLayout['colors'], consumptionLayout['format'], "", "")
			add_new_row("#ordqty", 0, consumptionLayout['colors'], consumptionLayout['format'], "", "")
			add_new_row("#fabricreq", 0, consumptionLayout['colors'], consumptionLayout['format'], "fabricreq", "")


		}else{
			// If click "Add New Raw" ->
			add_new_row("#ordqty", 1, consumptionLayout['colors'], consumptionLayout['format'], "")
			add_new_row("#fabricreq", 0, consumptionLayout['colors'], consumptionLayout['format'], "fabricreq")
			calculate_ratio(1, consumptionLayout['format'], 'ordqty')
		}



	}
}



cur_frm.cscript.refresh = function (doc, dt, dn) {

	var consumptionLayout = "";


	if(!doc.__islocal){

		cur_frm.set_df_property("consumption_workout", 'read_only', true)
		cur_frm.set_df_property("buyer", 'read_only', true)
		cur_frm.set_df_property("date", 'read_only', true)
		cur_frm.set_df_property("item", 'read_only', true)
		cur_frm.set_df_property("with_ratio", 'read_only', true)


		frappe.call({
			method: "get_format",
			doc: doc,
			async:false,
			callback: function(r, rt) {
				consumptionLayout = r.message
			}
		});



		// msgprint(consumptionLayout)
		if(doc.with_ratio){
			ratio_form(doc, consumptionLayout);
			calculate_ratio(1, consumptionLayout['format'], 'ratio')

		}else{
			cur_frm.set_df_property("ratio", "hidden", true)
		}

		order_qty_form(doc, consumptionLayout);
		fabric_require_form(doc, consumptionLayout);



	}else{
		cur_frm.set_df_property("buyer", 'read_only', true)
		cur_frm.set_df_property("item", 'read_only', true)


		cur_frm.set_df_property("ratio", "hidden", true)
		cur_frm.set_df_property("order_quantity", "hidden", true)
		cur_frm.set_df_property("ratio", "hidden", true)
	}
}





function ratio_form(doc, consumptionLayout){


		var dataRow =  "";
		var color = consumptionLayout['colors'];
		var size = consumptionLayout['format'];



		var headingColl = formated_head(size, consumptionLayout, "ratio");
		
		if(doc.ratio_data){
			var ratio_data = JSON.parse(doc.ratio_data)

			for(rv in ratio_data['data_row']){
				dataRow += formated_row(1, color, size, rv, "", ratio_data['data_row'][rv], "");
			}


		}else{
			dataRow += formated_row(1, color, size, 0, "", "", "");
		}



		var layout = documentFrame(doc, headingColl, dataRow, consumptionLayout, "ratio", "ratio");

		setTimeout(function(){
			$('#ratio_list').html(layout);
			// $( "body" ).find('#ratio').find('input.reqValue').trigger( "change" );
		}, 100)



}



function order_qty_form(doc, consumptionLayout){
	// msgprint(consumptionLayout['format'])
	var fieldType = 1;
	var data_box_name = ""
	if(doc.with_ratio){
		fieldType = 0;
	}else{
		data_box_name = "ordqty";
		$('body').find('.ordqty_total').attr('disable');
		// $('body').find('.ordqty_total').attr('readonly');
	}


		var dataRow =  "";
		var color = consumptionLayout['colors'];
		var size = consumptionLayout['format'];

		var headingColl = formated_head(size, consumptionLayout, "ordqty");
			
		if(doc.order_quantity_data){
			
			var order_quantity_data = JSON.parse(doc.order_quantity_data)

			for(rv in order_quantity_data['data_row']){
				dataRow += formated_row(fieldType, color, size, rv, "", order_quantity_data['data_row'][rv], "");
			}
		}else{
			dataRow += formated_row(fieldType, color, size, 0, "", "", "");
		}



		var layout = documentFrame(doc, headingColl, dataRow, consumptionLayout, data_box_name, "ordqty");
		setTimeout(function(){
			$('#order_quantity_list').append(layout);
			if(fieldType){
				$('body').find('.ordqty_total').attr('readonly', true);
				$('body').find('.ordqty_total').attr('disabled', true);
				// $('body').find('.ordqty_total').attr('readonly');
			}
		}, 100)



}




function fabric_require_form(doc, consumptionLayout){
	// msgprint(doc.fabric_requirement_data)
	var dataRow =  "";
	var color = consumptionLayout['colors'];
	var size = consumptionLayout['format'];
	
	var headingColl = formated_head(size, consumptionLayout, "fabricreq");
			
		if(doc.fabric_requirement_data){
			
			var fabric_requirement_data = JSON.parse(doc.fabric_requirement_data)
			for(rv in fabric_requirement_data['data_row']){
				dataRow += formated_row(0, color, size, rv, "fabricreq", fabric_requirement_data['data_row'][rv], "");
			}
			
		}else{
			dataRow += formated_row(0, color, size, 0, "fabricreq", "", "");
		}



	var layout = documentFrame(doc, headingColl, dataRow, consumptionLayout, "", "fabricreq");
	setTimeout(function(){
		$('#fabric_requirement_list').append(layout);
	}, 100)

		
}



function documentFrame(doc, headingColl, dataRow, consumptionLayout, data_box_name, box_name){

	var fieldType = 1;
	if(doc.with_ratio){
		fieldType = 0;
	}


	// Main Section (1)
	var rows_list = "";

	// Child Body Section (2)
	rows_list += '<div class="form-column col-xs-12"><div class="frappe-control ui-front" data-fieldtype="Table" title=""><div data-fieldname="add_routine"><div class="form-grid">';

	rows_list += headingColl;



	// Output Row Section (3:1)
	rows_list += '<div class="grid-row"><div class="data-row row sortable-handle" id="'+box_name+'">';
	
	rows_list += dataRow // Show Only Render data
		
	rows_list += '</div></div>';
	// Output Row Section (3:1)



	// Set Total Tow of every Box 
	var totalRow = {};
	
	if(box_name == 'ratio'){
		if(doc.ratio_data)
			totalRow = JSON.parse(doc.ratio_data)['total_row']
	}else if(box_name == 'ordqty'){
		if(doc.order_quantity_data)
			totalRow = JSON.parse(doc.order_quantity_data)['total_row']
	}else if(box_name == 'fabricreq'){
		if(doc.fabric_requirement_data)
			totalRow = JSON.parse(doc.fabric_requirement_data)['total_row']
	}

	if(Object.keys(totalRow).length > 0){
		rows_list += formated_row(2, consumptionLayout['colors'], consumptionLayout['format'], 0, box_name, "", totalRow)
	}else{
		rows_list += formated_row(2, consumptionLayout['colors'], consumptionLayout['format'], 0, box_name, "", "")
	}
	// End 



	rows_list += '</div>';

	if(data_box_name == "ratio" || data_box_name == "ordqty"){
		rows_list += '<button type="button" class="btn btn-success pull-right" id="add_row">Add New</button>';
	}

	rows_list += '</div></div></div>';
	// --- Child Body Section (2)




	// ratio_list += r.message;
	rows_list += '</div></div>';

	return rows_list;

}



function formated_head(size, consumption, box_name){

	var consumptionData = JSON.parse(consumption['consumption'][0]['consumption_data']);
	cons_consumption = consumptionData['CONSUMPTION :']
	cons_dia = consumptionData['DIA :']
	cons_rib = consumption['consumption'][0]['rib_dia']



	var rows_list = "";
	rows_list += '<div class="grid-heading-row"><div class="grid-row"><div class="data-row row sortable-handle"><div class="col col-xs-2 grid-overflow-ellipsis grid-static-col" data-fieldname="size" style="font-weight: bold;">SIZE :</div>';

	rows_list += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" style="font-weight: bold;">';

	for ( sizeCol in size){
		rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;">'+size[sizeCol]+'</div>';
	}

	rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;">Total</div>';
	rows_list += '</div>';




	if(box_name == "fabricreq"){
		rows_list += '</div></div><div class="grid-row"><div class="data-row row sortable-handle"><div class="col col-xs-2 grid-overflow-ellipsis grid-static-col" data-fieldname="size" style="font-weight: bold;">Consumption :</div>';



		rows_list += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" style="font-weight: bold;">';

		for ( sizeCol in size){
			rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;">'+cons_consumption[sizeCol]+'</div>';
		}

		rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;"></div>';

		// Add Rib Collumn wITH Rib Value
		rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;">'+cons_rib+'%</div>';

		rows_list += '</div>';

	}



	if(box_name == "fabricreq"){
		rows_list += '</div></div><div class="grid-row"><div class="data-row row sortable-handle"><div class="col col-xs-2 grid-overflow-ellipsis grid-static-col" data-fieldname="size" style="font-weight: bold;">DIA :</div>';


		rows_list += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" style="font-weight: bold;">';

		for ( sizeCol in size){
			rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;">'+cons_dia[sizeCol]+'</div>';
		}

		rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;"></div>';
		
		// Add Rib Collumn
		rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;">RIB</div>';	
		
		
		// Add G. Total Collumn
		rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;">G.Total</div>';	
		

		rows_list += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col text-center" data-fieldname="" 	style="font-weight: bold; padding:0px;"></div>';
		rows_list += '</div>';


	}

	rows_list += '</div></div></div>';

	return rows_list;
}


function formated_row(fieldType, color, size, total_row, box_name, db_value, db_total_value){
	// msgprint(db_total_value)
	// Start Set Colors Collumn
	var dataRow = "";
	dataRow += '<div class="grid-row alert" data-idx="1"><div class="data-row row">';
	if (fieldType == 1){

		dataRow += '<div class="col col-xs-2 grid-overflow-ellipsis grid-static-col" data-fieldname="size"><select class="reqValue" data-field="ir_color_'+total_row+'"><option>Select Color</option>';
		for ( colorSet in color){
			var selectValue = (db_value['color'] == color[colorSet]['name']) ? 'selected': "";
			dataRow += '<option '+selectValue+'>'+color[colorSet]['name']+'</option>';
		}
		dataRow += '</select></div>';

	}else if (fieldType == 2){

		dataRow += '<div class="col col-xs-2 grid-overflow-ellipsis grid-static-col ir_color_'+total_row+'" data-fieldname="size">Total:</div>';

	}else{
		var colorVal = db_value['color'] ? db_value['color']: "";
		dataRow += '<div class="col col-xs-2 grid-overflow-ellipsis grid-static-col ir_color_'+total_row+'" data-fieldname="size">'+colorVal+'</div>';
	}


	// End Set Colors Collumn


	dataRow += '<div class="col col-xs-10 grid-overflow-ellipsis grid-static-col" data-fieldname="size_all" >';

	for ( sizeCol in size){
		var inpValue = db_value[size[sizeCol]] ? db_value[size[sizeCol]] : 0;
		if (fieldType == 1){
			var inp = '<input type="text" class="col-xs-12 reqValue" style="padding:1px 5px;" data-field="ir_'+size[sizeCol]+'_'+total_row+'" size="15" name="'+size[sizeCol]+'" value="'+inpValue+'">';
		}else if (fieldType == 2){
			var totalFvalue = (db_total_value[size[sizeCol]]) ? db_total_value[size[sizeCol]] : 0
			var inp = '<input type="text" class="col-xs-12" style="padding:1px 5px;" data-field="ir_'+box_name+'_'+size[sizeCol]+'_total" size="15" name="'+sizeCol+'" value="'+totalFvalue+'" readonly disabled>';


		}else{
			var inp = inpValue;
		}



		dataRow += '<div class="col col-xs-1 ir_'+size[sizeCol]+'_'+total_row+' grid-overflow-ellipsis grid-static-col" data-field="" style="padding:0px;">'+inp+'</div>';
	}


	if(box_name == "ordqty"){

		var totalOrdQty = db_total_value['total'] ? db_total_value['total'] : 0;
		dataRow += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col" data-fieldname="" 	style="padding:0px;"><input type="text" class="col-xs-12 ordqty_total" style="padding:1px 5px;" data-field="ir_total_ordqty" size="15" name="" value="'+totalOrdQty+'"></div>';

	}else if (fieldType == 2){
	
		var totalAllvalue = (db_total_value['total']) ? db_total_value['total'] : 0
		dataRow += '<div class="col col-xs-1 ir_total_'+total_row+' grid-overflow-ellipsis grid-static-col" data-fieldname="" 	style="padding:0px;"><input type="text" class="col-xs-12" style="padding:1px 5px;" data-field="ir_total_'+box_name+'" size="15" value="'+totalAllvalue+'" readonly disabled></div>';

		// Add custom column for fabric requirement (Row)
		if(box_name == "fabricreq"){
			var totalRibValue = db_total_value['total_rib'] ? db_total_value['total_rib'] : 0;
			dataRow += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col" data-fieldname="" 	style="padding:0px;"><input type="text" class="col-xs-12" style="padding:1px 5px;" data-field="ir_total_rib" size="15" name="" value="'+totalRibValue+'" readonly disabled></div>';
			

			var totalGtotal_value = (db_total_value['total_gtotal']) ? db_total_value['total_gtotal'] : 0
			dataRow += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col" data-fieldname="" 	style="padding:0px;"><input type="text" class="col-xs-12" style="padding:1px 5px;" data-field="ir_total_gtotal" size="15" name="" value="'+totalGtotal_value+'" readonly disabled></div>';

		}

	}else{
		var rowTotalValue = db_value['total'] ? db_value['total'] : 0;
		dataRow += '<div class="col col-xs-1 ir_total_'+total_row+' grid-overflow-ellipsis grid-static-col" data-fieldname="" 	style="padding:0px;"><input type="text" class="col-xs-12" style="padding:1px 5px;" data-field="ir_total_'+total_row+'" size="15" value="'+rowTotalValue+'" readonly disabled></div>';
		

		if(box_name == "fabricreq"){
			// Add custom column for fabric requirement (Total)
			var rowtotalRib_value = (db_value['total_rib']) ? db_value['total_rib'] : 0
			dataRow += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col" data-fieldname="" 	style="padding:0px;"><input type="text" class="col-xs-12 ir_total_rib_'+total_row+'" style="padding:1px 5px;" data-field="ir_total_rib_'+total_row+'" size="15" name="" value="'+rowtotalRib_value+'" readonly disabled></div>';

			var rowtotalGTotalValue = db_value['total_gtotal'] ? db_value['total_gtotal'] : 0;
			dataRow += '<div class="col col-xs-1 grid-overflow-ellipsis grid-static-col" data-fieldname="" 	style="padding:0px;"><input type="text" class="col-xs-12 ir_total_gtotal_'+total_row+'" style="padding:1px 5px;" data-field="ir_total_gtotal_'+total_row+'" size="15" name="" value="'+rowtotalGTotalValue+'" readonly disabled></div>';
		}
	}

	// dataRow += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

	dataRow += "</div></div></div>";
	return dataRow;
}



function add_new_row(box_id, fieldType, color, size, box_name){

	$('body').on('click', '#add_row', function(){
		if(fieldType)
			total_row += 1;
		var newrow = formated_row(fieldType, color, size, total_row, box_name, "", "")
		$(box_id).append(newrow);
	})


}



function calculate_ratio(fieldType, sizeCol, mainId){

	setTimeout(function(){

		$('body').find('#'+mainId).on('change', '.reqValue', function(){

			var dataField = $(this).attr('data-field');
			var fieldValue = $(this).val();

			var rowInputLength = $('body').find('#'+mainId).find('.reqValue');
			var rowNo = 1;

			var dataFormat = {};

			var columnTotal = {};

			var rowTotal = 0;

			var allTotal = 0;
			for(r = 1; r<rowInputLength.length; r++){
				if(r == (rowNo * (sizeCol.length+1)) ){
					r += 1;
					rowTotal = 0;
					rowNo += 1;
				}
				// msgprint(r)

				var fieldName = rowInputLength.eq((r)).attr('data-field').split("_");
				var inputValue = parseFloat(rowInputLength.eq(r).val());
				// msgprint(rowInputLength.eq(r).attr('data-field'))

				if(fieldName[2] in dataFormat){
					dataFormat[fieldName[2]][fieldName[1]] = inputValue;
				}else{
					dataFormat[fieldName[2]] = {};
					dataFormat[fieldName[2]][fieldName[1]] = inputValue;
					dataFormat[fieldName[2]]['color'] = rowInputLength.eq(r-1).val();
				}
				rowTotal += inputValue

				dataFormat[fieldName[2]]['total'] = rowTotal;


				if(fieldName[1] in columnTotal){
					columnTotal[fieldName[1]] += inputValue;
				}else{
					columnTotal[fieldName[1]] = 0;
					columnTotal[fieldName[1]] += inputValue;
					columnTotal['total'] = 0;
				}
				

				// msgprint('[data-field="ir_'+mainId+'_'+fieldName[1]+'_total"]')
				allTotal += inputValue;

				columnTotal['total'] = allTotal;
				
				$('body').find('[data-field="ir_'+mainId+'_'+fieldName[1]+'_total"]').val(columnTotal[fieldName[1]])
				$('body').find('[data-field="ir_total_'+mainId+'"]').val(allTotal)
				$('body').find('#'+mainId).find('[data-field="ir_total_'+fieldName[2]+'"]').val(rowTotal)

			}

				// msgprint(dataFormat)
				var finalJson = {"data_row":dataFormat, 'total_row':columnTotal};

				if(mainId == "ratio"){

					cur_frm.doc['ratio_data']= JSON.stringify(finalJson);


					if($('body').find('[data-field="ir_total_ordqty"]').val() > 0){
						calculate_ordqty(dataFormat, 'ordqty')
					}else{
						msgprint('Please Set Order Quantity Total First');
						// $(this).val(0).trigger( "change" );
					}

					// $('body').find("#ordqty").find('.'+dataField).html(fieldValue)

				}else{

					cur_frm.doc['order_quantity_data']= JSON.stringify(finalJson);
					calculate_fabricreq(dataFormat, 'fabricreq')

				}

		})

	}, 500)

	return "";
}




/*
 -- 
*/
function calculate_ordqty(ratio_data, mainId){
	var dataFormat = {};
	var columnTotal = {};
	for (oq in ratio_data){

		for(oqc in ratio_data[oq]){
			amount = ratio_data[oq][oqc];
			
			if(oqc == "color"){
				$('body').find('#'+mainId).find('.ir_'+oqc+'_'+oq).html(amount)
				dataFormat[oq]['color'] = amount;
				
			}else{

				var totalRatio = $('body').find('[data-field="ir_total_ratio"]').val()
				var totalOrdqty = $('body').find('[data-field="ir_total_ordqty"]').val()
				var parentField = $('body').find('#ratio').find('[data-field="ir_'+oqc+'_'+oq+'"]').val()

				var fieldVal = parseFloat(totalOrdqty / totalRatio * parentField).toFixed(2);



				if(oq in dataFormat){
					dataFormat[oq][oqc] = fieldVal;
				}else{
					dataFormat[oq] = {};
					dataFormat[oq][oqc] = fieldVal;
				}



				if(oqc in columnTotal){
					columnTotal[oqc] += parseFloat(fieldVal)
				}else{
					columnTotal[oqc] = 0;
					columnTotal[oqc] += parseFloat(fieldVal)
					columnTotal['total'] = 0;
				}



				$('body').find('#'+mainId).find('.ir_'+oqc+'_'+oq).html(fieldVal)
				var setTotalValue = parseFloat(columnTotal[oqc]);
				$('body').find('[data-field="ir_ordqty_'+oqc+'_total"]').val(setTotalValue.toFixed(2))
				columnTotal['total'] = $('body').find('.ordqty_total').val();
			}


		}

	}

	var finalJson = {"data_row":dataFormat, 'total_row':columnTotal};
	// msgprint(finalJson)

	cur_frm.doc['order_quantity_data']= JSON.stringify(finalJson);
	calculate_fabricreq(dataFormat, 'fabricreq');

}




function calculate_fabricreq(ordqty_data, mainId){

	var columnTotal = {"total_rib":0, "total_gtotal":0, "total":0};
	var dataFormat = {};
	for(r in ordqty_data){

		var currentCol = 0;

		var rowtotal = 0;
		var rowRib = 0;
		var rowGTotal = 0;

		for(f in ordqty_data[r]){

			if(f == "color"){
				$('body').find('#'+mainId).find('.ir_'+f+'_'+r).html(ordqty_data[r][f])
				dataFormat[r]['color'] = ordqty_data[r][f];
			}else{

				if(f != "total"){
					var currentFieldValue = parseFloat(ordqty_data[r][f] * cons_consumption[currentCol]);
					rowtotal += parseFloat(currentFieldValue);
					$('body').find('#'+mainId).find('.ir_'+f+'_'+r).html(currentFieldValue.toFixed(2))

					currentCol += 1;


					if(f in columnTotal){
						columnTotal[f] += parseFloat(currentFieldValue.toFixed(2));
					}else{
						columnTotal[f] = 0;
						columnTotal[f] += parseFloat(currentFieldValue.toFixed(2));
					}



					if(r in dataFormat){
						dataFormat[r][f] = parseFloat(currentFieldValue.toFixed(2));
					}else{
						dataFormat[r] = {};
						dataFormat[r][f] = 0;
						dataFormat[r][f] = parseFloat(currentFieldValue.toFixed(2));
						dataFormat[r]['total'] = 0;
						dataFormat[r]['total_rib'] = 0;
						dataFormat[r]['total_gtotal'] = 0;
					}
					

				}


				// $('body').find('#'+mainId).find('.ir_'+f+'_'+r).html(currentFieldValue.toFixed(2))

			}

			$('body').find('#'+mainId).find('.ir_total_'+r).html(rowtotal.toFixed(2));

			// Calculate Total / Row Total RIB
			rowRib = parseFloat((rowtotal / 100) * cons_rib);
			$('body').find('#'+mainId).find('.ir_total_rib_'+r).val(rowRib.toFixed(2));

			// Calculate Total / Row Total Amount
			rowGTotal = parseFloat(rowtotal + rowRib);
			$('body').find('#'+mainId).find('.ir_total_gtotal_'+r).val(rowGTotal.toFixed(2));

			$('body').find('[data-field="ir_fabricreq_'+f+'_total"]').val(columnTotal[f]);

		}



			columnTotal['total'] += rowtotal;
			columnTotal['total_rib'] += rowRib;
			columnTotal['total_gtotal'] += rowGTotal;



			dataFormat[r]['total'] = rowtotal;
			dataFormat[r]['total_rib'] = rowRib;
			dataFormat[r]['total_gtotal'] = rowGTotal;
	


			// Set Total, Total Rib, G. Total Amount in field
			$('body').find('[data-field="ir_total_fabricreq"]').val(columnTotal['total'].toFixed(2))
			$('body').find('[data-field="ir_total_rib"]').val(columnTotal['total_rib'].toFixed(2))
			$('body').find('[data-field="ir_total_gtotal"]').val(columnTotal['total_gtotal'].toFixed(2))
	}

	var finalJson = {"data_row":dataFormat, 'total_row':columnTotal};
	// msgprint(finalJson)
	cur_frm.doc['fabric_requirement_data']= JSON.stringify(finalJson);
}