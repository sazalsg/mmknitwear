# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
from datetime import date
from frappe.model.naming import make_autoname

class SewingBooking(Document):
	def autoname(self):
		self.name = make_autoname("SB-{b}-{y}/{m}-.##".format(b=self.buyer, y= str(date.today().year)[2:4],m=date.today().month))

@frappe.whitelist()
def colors():
	return frappe.db.get_values("Color",{"name":("!=","")},"name")