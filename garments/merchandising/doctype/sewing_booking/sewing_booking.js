
frappe.provide("garments.merchandising");
{% include "garments/public/js/controllers/sewing_booking.js" %}
garments.merchandising.SewingBookingController = frappe.ui.form.Controller.extend({
	onload:function (doc) {
		spData = [];
		if (doc.__islocal) {
			doc.b_r = ""
		};
	},
	refresh:function (doc) {
		frappe.call({
			method:'garments.merchandising.doctype.sewing_booking.sewing_booking.colors',
			context:this,
			callback:function (r) {
				if (r.message) {
					spData['colors']=r.message;
					// Edit Mode
					if (!doc.__islocal) {
						cur_frm.cscript.load_data(doc,r.message)
					};
					// -- End Edit Mode
				};
			}
		});
	},
	b_r:function (doc) {
		this.frm.refresh_field('emp_name')
	},
	attn:function (doc) {
		var old_val = (doc.description).split('\n');
		old_val.splice(0,1);
		var value = "Dear "+doc.attn +"\n"+old_val.join('\n');
		this.frm.set_value('description',value);
		// msgprint(newtext);
	},
	load_data:function (doc,colors) {
		$('body').find('#s_b_body').html(" ");
		var data = JSON.parse(doc.style_format_data);
		set_table_header(data[0].header);
		for (var i =  0; i <= data[0].body.length - 1; i++) {				
			set_table_body(data[0].body[i],colors,'','');
		};
		set_table_footer(data[0].footer);
		spData['styles'] = data[0].header;
	},
	style_format:function (doc) {
		this.style_format_child(doc)
	},
	style_format_child:function (doc) {
		frappe.call({
			method:"frappe.client.get",
			args:{
				doctype:"Style Format",
				name:doc.style_format
			},
			callback:function (r) {
				if (r.message) {
					$('body').find('#s_b_head').html(" ");
					$('body').find('#s_b_body').html(" ");
					$('body').find('#s_b_footer').html(" ");
					$('body').find('button.btn.btn-xs.btn-default.pull-right').remove();
					spData['styles']=r.message;
					set_table_header(r.message);
					set_table_footer(r.message);
					set_table_body(r.message,spData.colors,'','');
				};
			}
		});		
	},
	add_new_row:function (doc) {
		set_table_body(doc.styles,doc.colors,'','');
	},
	validate:function (doc,cdt,cdn) {
		this.format_custom_value(doc,cdt,cdn)
	},
	format_custom_value:function(doc,cdt,cdn){
		this.frm.set_df_property("style_format_data", "reqd", doc.style_format ? 1:0);
		if (doc.style_format) {
			var header_col =[]
			var footer_col =[]
			for (var i = 0;  i <= spData.styles['format_child'].length - 1; i++) {
				row = {};
				row1 = {};
				row['name']=spData.styles['format_child'][i].name;
				row1['name']=spData.styles['format_child'][i].name;
				row['style']=spData.styles['format_child'][i].style;
				row1['value']=$('input[name="total-'+spData.styles['format_child'][i].name+'"]').val();
				header_col.push(row)
				footer_col.push(row1)
			};
			var header = {"format_child":header_col};
			var footer = {"format_child":footer_col};

			footer['total_cone'] = $('input[name="total-cone"]').val();
			
			 datas = [{"header":header,"body":body_values(),"footer":footer}];
			 doc.style_format_data = JSON.stringify(datas)
		};
	}
	
});

cur_frm.cscript = new garments.merchandising.SewingBookingController({frm:cur_frm})
