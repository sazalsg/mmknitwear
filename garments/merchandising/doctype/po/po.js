// -*- coding: utf-8 -*-
//  Copyright (c) 2015, Techbeeo Software Ltd. and contributors
//  For license information, please see license.txt
frappe.ui.form.on("PO", "onload", function(frm) {
	$(frm.wrapper).append('\
		<div id="freeze" class="freeze loading_freeze">\
			<div class="freeze-message-container modal-backdrop">\
				<div class="freeze-message">\
					<h4 style="color:#fff">Loading...</h4>\
				</div>\
			</div>\
		</div>\
		')
});
frappe.provide('garments.merchandising');
{% include "garments/public/js/controllers/po_child_fields.js" %}
garments.merchandising.PoController = frappe.ui.form.Controller.extend({
	onload:function (doc) {
		spData =[];
	},
	refresh:function (doc) {
		$('.loading_freeze').removeClass('hide').addClass('in');
		setTimeout(function (doc) {
			doc = cur_frm.doc;
			me = cur_frm.cscript;
			if (!doc.__islocal) {
				me.reference(doc)
			};
			$('.loading_freeze').removeClass('in').addClass('hide');
		},100);
	},
	reference:function (doc) {
		var me = this
		me.get_child(doc);
		me.section_render(doc);
	},
	get_child:function (doc) {
		frappe.call({
			method:'frappe.client.get',
			async:false,
			args:{
				doctype:"PO Sheet",
				name:doc.reference
			},
			callback:function (r) {
				doc.buyer = r.message.buyer;
				refresh_field('buyer');
				doc['childs'] = r.message.childs
				doc['childs_child'] = r.message.childs_child
			}
		})
	},
	section_render:function (doc) {
		var me = this;
		this.html = $('body').find('#html');
		this.html.html($(frappe.render_template("section_body", {}))
			.attr('date-fieldname', 'childs'));
		// Bind Add Table Event 
		/*$(this.html).find(".sec-add-table").click(function() {
			var sel = $(this).parents(".grid-body");
			me.parent_row(doc,sel);
			return false;
		});*/
		if (typeof doc.childs !== "undefined" && doc.childs.length > 0 ) {
			for (var i = 0; i < doc.childs.length; i++) {				
				me.parent_row(doc,$('#html').find('.grid-body'),doc.childs[i])
			};
		};
	},
	parent_row:function (doc,selector,row) {
		var me = this;
		var row_fields = new po_childs_fields();
		if ('size' in row) {		
			var fields = me.get_sizes(row.size)
			if (typeof doc.childs_child !== "undefined" && doc.childs_child.length > 0) {
				var newArray = {};
				/*for (var j = 0; j < doc.childs_child.length; j++) {
					
				};*/
				// var childs_child_this_row = [];
				for (var i = 0; i < doc.childs_child.length; i++) {
					if (doc.childs_child[i].pre_parent === row.pre_parent) {
						var key = (doc.childs_child[i].product_name+doc.childs_child[i].color).toLowerCase().replace(/ /g,'_')
						if (!(key in newArray )) {
							newArray[key] = [doc.childs_child[i]];
						}else{
							newArray[key].push(doc.childs_child[i]);
						};
						// childs_child_this_row.push(doc.childs_child[i])
						// me.render_body(wrapper_row.find('.item:last'),fields,doc.childs_child[i])
					};
				};


				$.each(newArray, function(index, val) {
					var wrapper_row = $($(selector).find('.rows').append(frappe.render_template("table_render",{})));
					me.render_header(wrapper_row.find('.item:last'),fields)
					var footer_row = [];
					var footer_val = [];
						footer_val['name'] = index;
					for (var j = 0; j < val.length; j++) {
						var datas = val[j].datas = JSON.parse(val[j].datas);
						$.each(datas, function(k, v) {
							if (val[j].size_breakdown_for === "Standard Packing") {
								val[j][k]= val[j].std_parcel_number*v
							}else{
								val[j][k]=v
							};
							if (footer_row.indexOf(k) == '-1') {
								footer_row.push(k)
							};
							if (k in footer_val) {
								footer_val[k] += Number(val[j][k])
							}else {
								footer_val[k] = Number(val[j][k])
							};
						});
						if (footer_row.indexOf('qty') == '-1') {
							footer_row.push('qty');
						};
						if ('qty' in footer_val) {
							footer_val['qty'] += Number(val[j].qty)
						}else {
							footer_val['qty'] = Number(val[j].qty)
						};
						me.render_body(wrapper_row.find('.item:last'),fields,val[j])
					};
					me.render_footer(wrapper_row.find('.item:last'),footer_row,footer_val)
				});


			};
		};
	},
	get_sizes:function (sel) {
		var fields = [];
		var fields = new po_child_fileds();
		frappe.call({
			method:"frappe.client.get",
			async:false,
			args:{
				doctype:"Size Format",
				name:sel
			},
			callback:function (r) {
				if (r.message) {
					spData['sizes'] = r.message
					for (var i = 0; i < r.message.format_child.length; i++) {
						fields.push({
							'fieldname':r.message.format_child[i].size,
							'label':r.message.format_child[i].size,
							'fieldtype':'Int',
							'h_cal':1,
							'auto_row':1
						})
					};
				};
			}
		});

		fields.push({
		   "fieldname": "qty", 
		   "fieldtype": "Int", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "TTL", 
		   "read_only": 0, 
		   "reqd": 1,
		   "css":"border-left:4px solid #DFEBF3;"

		});
		return fields
	},
	render_header:function (selector,data) {
		var parent =  $(selector);
		data['idx'] = $(parent).index()+1;
		$(parent).find('.tb-header').html('<tr class="data-row row sortable-handle"></tr>');
		var tr = $(parent).find('.tb-header tr');
		// $(tr).append('<td></td>')
		for (var i = 0; i < data.length; i++) {
			$(tr).append('<td>'+data[i].label+'</td>');			
			var td = $(tr).find('td:last');
			if ('hidden' in data[i] && data[i].hidden == 1) {
				td.hide();
			};
		};

	},
	render_footer:function (selector,row_fields,row) {
		var parent =  $(selector);
		var total_td = $(parent).find('thead tr.data-row.row.sortable-handle:first td').length;
		$(parent).find('.tb-footer').html($('<tr></tr>').attr('data-name', row.name))
		var tr = $(parent).find('.tb-footer').find('[data-name="'+row.name+'"]');
		// Set empty td in frist
		for (var i = 0; i < 12; i++) {
			tr.append('<td></td>')
		};
		tr.append('<td>Total:</td>')
		// Set Total Fields
		for (var i = 0; i < row_fields.length; i++) {
			tr.append('<td></td>');
			var td = $(tr).find('td:last');
			var field = frappe.ui.form.make_control({
				parent: td,
				df: {
					fieldtype: "Int",
					options: "Size Format",
					fieldname: row_fields[i],
					reqd:0
				},
				only_input: true
			});
			field.refresh();
			field.$input.attr('disabled', '1');
			if ( typeof row !== "undefined" && (row_fields[i] in row)) {
				$(field.input).val(row[row_fields[i]]);
			}
		};
	},
	render_body:function (selector,data,values) {
		var me = this
		var doc = me.frm.doc;
		var parent =  $(selector);
		if ( typeof values.datas === "string") {
			values['datas'] = JSON.parse(values.datas);
		};
		var etd_shipment_date = frappe.datetime.str_to_user(values.etd_shipment_date);
		if (etd_shipment_date !== "Invalid date") {
			values['etd_shipment_date'] = etd_shipment_date;
		};
		$(parent).find('.tb-body').append($('<tr></tr>').attr('data-name', values.name));
		var tr = $(parent).find('.tb-body tr:last');
		for (var i = 0; i < data.length; i++) {
			$(tr).append('<td></td>');
			var td = $(tr).find('td:last');
			var field = frappe.ui.form.make_control({
				parent: td,
				df: data[i],
				only_input: true
			});
			field.refresh();



			if ('hidden' in data[i] && data[i].hidden == 1) {
				td.hide();
			};

					
			if ( typeof values !== "undefined" && (data[i].fieldname in values) && !('childs_field' in data[i])) {
				$(field.input).val(values[data[i].fieldname]);
			}else if ( typeof values !== "undefined" && ('datas' in values) && ('childs_field' in data[i])) {
				if (data[i].fieldname in values['datas']) {
					$(field.input).val(values['datas'][data[i].fieldname]);
				};
			};
			
		};


		// remove mandetory sign
		tr.find('input, select').attr('disabled', '1').parents('.has-error').removeClass('has-error');

	},
	validate:function (doc) {
		
		delete doc['childs']
	}
});

cur_frm.cscript = new garments.merchandising.PoController({frm:cur_frm});