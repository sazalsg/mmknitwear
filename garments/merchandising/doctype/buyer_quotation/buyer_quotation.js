cur_frm.add_fetch("ref","pht","photo")

cur_frm.cscript.ref = function(doc,doctype,docname){
	
	var setLocal = locals[doctype][docname];

	frappe.call({
		method:"frappe.client.get_value",
		args: {
			doctype: "Buyer Inquiry",
			filters: {
				"reference":setLocal.ref,
			},
			fieldname : ["item","siz_rng"]
		},
		callback: function(r) {
			//msgprint(r.message)
			$.each(doc.bq_child, function() {
			 	if(setLocal.ref){
			 		setLocal.item = r.message["item"];
			 		setLocal.s_rng = r.message["siz_rng"];
			 	}
			 });
			 refresh_field("bq_child");
		 }
	});

	frappe.call({
		method:"frappe.client.get_value",
		args: {
			doctype: "Costing",
			filters: {
				"reference":setLocal.ref,
			},
			fieldname : "per_pcs_price"
		},
		callback: function(r) {
			if (!r.message){
				msgprint(__("There is no Costing with this Style"))
				return	
			}
			$.each(doc.bq_child, function() {
			 	if(setLocal.ref){
			 		console.log(r.message["per_pcs_price"])
			 		setLocal.per_pcs_price = r.message["per_pcs_price"];
			 	}
			 });
			 refresh_field("bq_child");
		 }
	});
}