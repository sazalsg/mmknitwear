# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class BuyerQuotation(Document):
	pass





@frappe.whitelist()
def quotation_confirmation(name, status):
		
		frappe.db.sql("""update `tabBuyer Quotation Child` set status='{}' where name='{}'""".format(status, name))

		return 'ok'


@frappe.whitelist()
def buyer_quation_data(name_qut,status_qut):
	return frappe.get_all("Buyer Quotation Child", fields=["name", "ref", "item", "t_prc", 'per_pcs_price'], filters={'status':'Pending'});