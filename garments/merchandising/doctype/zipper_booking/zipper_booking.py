# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.model.naming import make_autoname


class ZipperBooking(Document):
	def autoname(self):
		self.name = make_autoname("ZB-{b}-{y}/{m}-.##".format(b=self.buyer or "", y = str(date.today().year)[2:4],m=date.today().month))

@frappe.whitelist()
def get_size(ref_id):
	if ref_id:
		return frappe.db.get_value("PO Sheet Child Child",{"product_name":ref_id},"size_format")

