frappe.provide("garments.merchandising");
{% include "garments/public/js/controllers/zipper_booking.js" %}
garments.merchandising.ZipperBookingController = frappe.ui.form.Controller.extend({
	onload:function (doc) {
		spData =[];
	},
	refresh:function (doc) {
		setTimeout(function (doc) {
			doc = cur_frm.doc;
			me = cur_frm.cscript;
			me.section_render(doc)

			if (!doc.__islocal) {
				var sel = $('#html').find('.grid-body');
				for (var i = 0; i < doc.childs.length; i++) {	
					me.add_new_table(doc,sel);
					var item = $(sel).find('.item:last');

					var parsejson = JSON.parse(doc.childs[i].datas)
					parsejson.header['reference'] = doc.childs[i].reference;
					me.render_header(item,parsejson.header)
					me.render_footer(item,parsejson.footer)
					// $(item).find('.tb-body').html("");
					for (var j = 0; j < parsejson.body.body.length; j++) {
						me.render_body(item,parsejson.body.body[j])
					};
					$(item).find('input[data-fieldname="size"]').val(doc.childs[i].size)
					$(item).find('input[data-fieldname="reference"]').val(doc.childs[i].reference)
					me.check_mandetory($(item).find('input[data-fieldname="size"]'));
					me.check_mandetory($(item).find('input[data-fieldname="reference"]'));
					// msgprint(parsejson.body.body[0])
				};

				// me.print_childs(doc)
			};
		},100);
	},
	attn:function (doc) {
		var old_val = (doc.description).split('\n');
		old_val.splice(0,1);
		var value = "Dear "+doc.attn +"\n"+old_val.join('\n');
		this.frm.set_value('description',value);
		// msgprint(newtext);
	},
	section_render:function (doc) {
		var me = this;
		this.html = $('body').find('#html');
		this.html.html($(frappe.render_template("section_body", {}))
			.attr('date-fieldname', 'childs'));
		// Bind Add Table Event 
		$(this.html).find(".sec-add-table").click(function() {
			var sel = $(this).parents(".grid-body");
			me.add_new_table(doc,sel);
			return false;
		});	
	},
	add_new_table:function (doc,selector) {
		var me = this;
		var table = $($(selector).find('.rows').append(frappe.render_template("table_render",{})));
		// Make Reference Field
		var reference = frappe.ui.form.make_control({
			parent: table.find(".reference:last"),
			df: {
				fieldtype: "Link",
				options: "Style Name",
				fieldname: "reference",
				reqd:1,
				get_query:function () {
					return {
						filters:{
							'buyer':doc.buyer
						}
					}
				}
			},
			only_input: true
		});
		reference.refresh();
		// Create Size Field
		var size = frappe.ui.form.make_control({
			parent: table.find(".size:last"),
			df: {
				fieldtype: "Link",
				options: "Size Format",
				fieldname: "size",
				reqd:1
			},
			only_input: true
		});
		size.refresh();
		size.$input.attr('disabled', '1');
		// SIze Onchange Event
		size.$input.on('change', function(event) {	
			var sel = $(this);
			if ($(sel).val() !== "") {
			me.get_sizes($(sel).val())		
			me.render_header($(sel).parents('.item'),spData.sizes)
			me.render_footer($(sel).parents('.item'),spData.sizes)
			$(this).parents('.item').find('.tb-body').html("");
			me.render_body($(sel).parents('.item'),spData.sizes)
			}else{
				$(this).parents('.item').find('.tb-body, .tb-header, .tb-footer').html("");
			};
			me.check_mandetory(sel);
		});
		// SIze Onchange Event
		reference.$input.change(function(event) {
			frappe.call({
				method:"garments.merchandising.doctype.zipper_booking.zipper_booking.get_size",
				args:{
					ref_id:$(this).val()
				},
				callback:function (r) {
					if (r.message) {
						size.$input.val(r.message).trigger('change');
					}else{
						size.$input.val('').trigger('change');
					};
				}
			})
			$(this).parents(".item").find('.ref_id').html($(this).val());
			me.check_mandetory($(this));
		});


	},
	get_sizes:function (sel) {
		frappe.call({
			method:"frappe.client.get",
			async:false,
			args:{
				doctype:"Size Format",
				name:sel
			},
			callback:function (r) {
				if (r.message) {
					spData['sizes'] = r.message
				};
			}
		});
	},
	render_header:function (selector,data) {
		var parent =  $(selector);
		data['idx'] = $(parent).index()+1;
		if (!('reference' in data)) {
			data['reference'] = $(parent).find('input[data-fieldname="reference"]').val();
		};
		$(parent).find('.tb-header').html(frappe.render_template("header_row",data));

		// Create Size Field
		var first_rei_date = frappe.ui.form.make_control({
			parent: parent.find(".first_rei_date"),
			df: {
				fieldtype: "Date",
				fieldname: "first_rei_date",
			},
			only_input: true
		});
		first_rei_date.refresh();
		// Create Size Field
		var sec_rei_date = frappe.ui.form.make_control({
			parent: parent.find(".sec_rei_date"),
			df: {
				fieldtype: "Date",
				fieldname: "sec_rei_date",
			},
			only_input: true
		});
		sec_rei_date.refresh();
		// set value if have
		parent.find('.first_rei_date input').val('first_rei_date' in data ? data.first_rei_date:frappe.datetime.str_to_user(frappe.datetime.get_today()))
		parent.find('.sec_rei_date input').val('sec_rei_date' in data ? data.sec_rei_date:frappe.datetime.str_to_user(frappe.datetime.get_today()))
	},
	render_footer:function (selector,data) {
		var parent =  $(selector);
		date = footer_missing_key(data)
		$(parent).find('.tb-footer').html(frappe.render_template("footer_row",data));
	},
	render_body:function (selector,data) {		
		this.get_colors()
		var parent =  $(selector);
		data = body_missing_key(data)
		data['idx'] = $(parent).index()+1;
		data['colors'] = spData.colors;
		$(parent).find('.tb-body').append(frappe.render_template("row",data));
	},
	get_colors:function () {
		if (!('colors' in spData)) {
			frappe.call({
				method:'garments.merchandising.doctype.sewing_booking.sewing_booking.colors',
				async:false,
				callback:function (r) {
					if (r.message) {
						spData['colors']=r.message;
					};
				}
			});
		}
	},
	check_mandetory:function (sel) {
		if ($(sel).val() !="") {
			$(sel).parents('.form-group.frappe-control').removeClass('has-error');
		}else{
			$(sel).parents('.form-group.frappe-control').addClass('has-error');
		};
	},
	validate:function (doc) {
		if ($('#html').find('.has-error').length >0) {
			$('#html').find('.has-error').each(function(index, el) {
				msgprint(__("Plese fill mandetory field {0}",[$(el).attr('title')]))
			});
		};
		this.set_child_value(doc)
	},
	set_child_value:function (doc) {
		doc['childs']=[];		
		$('#html').find('.item').each(function(key, sel) {
			var item = {};
			item['reference']=$(sel).find('input[data-fieldname="reference"]').val();
			item['size']=$(sel).find('input[data-fieldname="size"]').val();
			var table={};
			// Header Value
			var rows = [];
			$(sel).find('.dync_row').each(function(index, el) {
				var row = {}
				row['size'] = $(el).text();
				row['val'] = $(el).parents('.tb-header').find('input[name="'+$(el).text()+'"]').val();
				rows.push(row);
			});
			var header = {};
				header['format_child'] = rows;
				header['first_rei_date'] = $(sel).find('.first_rei_date input').val()
				header['sec_rei_date'] = $(sel).find('.sec_rei_date input').val();
			table['header']=header;
			// Footer Value
			var footer = {};
			footer['format_child'] = []
			$(sel).find('.tb-footer input:not([name="ttl-total_col"])').each(function(fidx, fel) {
				var row = {}
				row['size'] = $(fel).attr('name').replace('ttl-','');
				row['val'] = $(fel).val();
				footer['format_child'].push(row);
			});
			footer['ttl_total_col']=$(sel).find('.tb-footer input[name="ttl-total_col"]').val();
			table['footer']=footer;
			// Body Value
			var body = [];
			var static_fields = ['desc_one','desc_two','color','total_col','first_pri','secound_pri','remark'];
			$(sel).find('.tb-body tr').each(function(bidx, bval) {
				b_row = {};
				// set static fields value
				$.each(static_fields,function(i,v) {
					b_row[v] = $(bval).find('input[name="'+v+'"], select[name="'+v+'"]').val();
				});
				// set format_child
				b_row['format_child']=[];

				$(bval).find('input').each(function(k,rv) {
					var row ={};
					if ((static_fields.indexOf($(rv).attr('name'))) == -1) {
						row['size'] = $(rv).attr('name');
						row['val'] = $(rv).val();
						b_row['format_child'].push(row);
					};
				});
				body.push(b_row);
			});
			table['body']={'body' : body};
			item['datas'] = JSON.stringify(table);
			doc['childs'].push(item)
		});
	},
	first_rei_date:function (event) {
		var parent = $(event).parents('tr');
		var total = $(parent).find('[name="total_col"]').val();
		if (parseInt($(event).val()) > parseInt(total)) {
			msgprint(__("1st Priority QTY must be equal or less then {0}",[total]));
			$(event).val("");
			$(parent).find('[name="secound_pri"]').val(total);
		};
	},
	print_childs:function (doc) {
		doc['childs']=[]
		for (var i = 0; i < doc.childs.length; i++) {
			doc['childs'][i]['datas'] = JSON.parse(doc.childs[i].datas)
		};
	}



});

cur_frm.cscript = new garments.merchandising.ZipperBookingController ({ frm:cur_frm })
