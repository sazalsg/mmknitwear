# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class BeneficiaryBank(Document):
	pass


		
@frappe.whitelist()
def bank_info(name):
	if name :
		# return filters
		sql = frappe.db.get_values("Beneficiary Bank", {"name":name}, ["address_line_1","tel","fax","swift_code","account_no"])
		return sql
	else :
		return []

