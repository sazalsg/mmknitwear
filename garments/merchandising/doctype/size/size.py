# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Size(Document):
	pass


@frappe.whitelist()
def size_all():
	sql = frappe.db.get_values("Size", {"name":("!=", "")}, ["size_name","size_name"], order_by="idx asc" ,as_dict=1)
	return sql
