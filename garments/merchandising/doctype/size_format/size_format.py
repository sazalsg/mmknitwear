# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class SizeFormat(Document):
	pass


		
@frappe.whitelist()
def size_info(name):
	if name :
		# return filters
		sql = frappe.db.get_values("Size Format Child", {"parent":name}, ["size"], order_by="idx asc")
		return sql
	else :
		return []
