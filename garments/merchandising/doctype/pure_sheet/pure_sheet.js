// -*- coding: utf-8 -*-
//  Copyright (c) 2015, Techbeeo Software Ltd. and contributors
//  For license information, please see license.txt

frappe.provide('garments.merchandising');
{% include "garments/public/js/controllers/pure_sheet_child_fields.js" %}
garments.merchandising.PureSheetController = frappe.ui.form.Controller.extend({
	onload:function (doc) {
		spData =[];
	},
	refresh:function (doc) {
		setTimeout(function (doc) {
			doc = cur_frm.doc;
			me = cur_frm.cscript;
			me.section_render(doc)

			if (!doc.__islocal) {
				var sel = $('#html').find('.grid-body');
				for (var i = 0; i < doc.childs.length; i++) {	
					me.add_new_table(doc,sel);
					var item = $(sel).find('.item:last');

					var parsejson = JSON.parse(doc.childs[i].datas)
					me.render_header(item,me.get_sizes(doc.childs[i].size))
					me.render_footer(item,doc.childs[i])
					// $(item).find('.tb-body').html("");
					for (var j = 0; j < parsejson.length; j++) {
						me.render_body(item,me.get_sizes(doc.childs[i].size),parsejson[j])
					};
					$(item).find('input[data-fieldname="size"]').val(doc.childs[i].size)
					me.check_mandetory($(item).find('input[data-fieldname="size"]'));
				};
			};
		},100);
	},
	section_render:function (doc) {
		var me = this;
		this.html = $('body').find('#html');
		this.html.html($(frappe.render_template("section_body", {}))
			.attr('date-fieldname', 'childs'));
		// Bind Add Table Event 
		$(this.html).find(".sec-add-table").click(function() {
			var sel = $(this).parents(".grid-body");
			me.add_new_table(doc,sel);
			return false;
		});	
	},
	add_new_table:function (doc,selector) {
		var me = this;
		var table = $($(selector).find('.rows').append(frappe.render_template("table_render",{})));
		// Create Size Field
		var size = frappe.ui.form.make_control({
			parent: table.find(".size:last"),
			df: {
				fieldtype: "Link",
				options: "Size Format",
				fieldname: "size",
				reqd:1
			},
			only_input: true
		});
		size.refresh();

		
		

		// SIze Onchange Event
		$(table).find('input[data-fieldname="size"]').change(function(event) {	
			var sel = $(this);
			var fields = me.get_sizes($(sel).val())
			me.render_header($(sel).parents('.item'),fields)
			me.render_footer($(sel).parents('.item'),fields)
			$(this).parents('.item').find('.tb-body').html("");
			me.render_body($(sel).parents('.item'),fields)
			me.check_mandetory(sel);
		});



	},
	get_sizes:function (sel) {
		var fields = [];
		var fields = new pure_sheet_child_fileds();
		frappe.call({
			method:"frappe.client.get",
			async:false,
			args:{
				doctype:"Size Format",
				name:sel
			},
			callback:function (r) {
				if (r.message) {
					spData['sizes'] = r.message
					for (var i = 0; i < r.message.format_child.length; i++) {
						fields.push({
							'fieldname':r.message.format_child[i].size,
							'label':r.message.format_child[i].size,
							'fieldtype':'Int',
							'h_cal':1
						})
					};
				};
			}
		});
		return fields
	},
	render_header:function (selector,data) {
		var parent =  $(selector);
		data['idx'] = $(parent).index()+1;
		$(parent).find('.tb-header').html('<tr class="data-row row sortable-handle"></tr>');
		var tr = $(parent).find('.tb-header tr');
		for (var i = 0; i < data.length; i++) {
			var td = $(tr).append('<td>'+data[i].label+'</td>');			
			if ('css' in data[i]) {
				$(tr).find('td:last').attr('style', data[i].css);
			};
		};
		$(tr).append('<td></td>')

	},
	render_footer:function (selector,data) {
		var parent =  $(selector);
		var total_td = $(parent).find('thead tr.data-row.row.sortable-handle:first td').length;
		data['total_td'] = total_td;
		$(parent).find('.tb-footer').html(frappe.render_template("footer_row",data));
	},
	render_body:function (selector,data,values) {	
		var me = this	
		var parent =  $(selector);
		// data = body_missing_key(data)
		// data['idx'] = $(parent).index()+1;
		$(parent).find('.tb-body').append('<tr></tr>');
		var tr = $(parent).find('.tb-body tr:last');
		for (var i = 0; i < data.length; i++) {
			var td = $(tr).append('<td></td>');
			var field = frappe.ui.form.make_control({
				parent: $(tr).find('td:last'),
				df: data[i],
				only_input: true
			});
			field.refresh();
			if (data[i].fieldname == "size_format") {
				$(field.input).val($(parent).find('input[data-fieldname="size"]').val())
				$(field.input).attr('disabled','1');
				me.check_mandetory($(field.input))
			};
			if (data[i].fieldname == "amount") {
				$(field.input).attr('disabled','1')
			};
			if (data[i].fieldname == "qty") {
				$(field.input).attr('disabled','1')
			};
			if (data[i].fieldname == "size_breakdown_for") {
				$(field.input).on('change', function(event) {
					me.changeV($(this))
					event.preventDefault();
					/* Act on the event */
				});
				
			};
			if ('css' in data[i]) {
				$(tr).find('td:last').attr('style', data[i].css);
			};
			if ('h_cal' in data[i] && data[i].h_cal == 1) {
				$(field.input).addClass('day_row').on('change', function(event) {
					totalHorizental(this)
					event.preventDefault();
					/* Act on the event */
				}).trigger('change');
			};			
			if ( typeof values !== "undefined" && data[i].fieldname in values) {
				$(field.input).val(values[data[i].fieldname]);
				if (data[i].reqd == '1') {
					me.check_mandetory($(field.input))
				};
			};
			
		};
		$(tr).append('<td><button onclick="javascript:remove_row(this);" class="button btn btn-xs btn-danger">remove</button></td>')

		$(tr).find('input[data-fieldtype="Link"]').on('change', function(event) {
			me.check_mandetory(this)
			event.preventDefault();
		});
		$(tr).find('input[data-fieldname="purchase_price"], input[data-fieldname="qty"]').on('change', function(event) {
			totalAmount(this)
			event.preventDefault();
		});
		$(tr).find('input[data-fieldname="qty"], input[data-fieldname="amount"], input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"], input[data-fieldname="solid_size_qty"]').on('change', function(event) {
			totalVartical(this)
			event.preventDefault();
		});
		
		$(tr).find('select[data-fieldname="size_breakdown_for"], .day_row').trigger('change')

	},
	changeV:function (e) {
		if ($(e).val()=="Standard Packing") {
			$(e).parents('tr').find('input[data-fieldname="std_packing_qty"], input[data-fieldname="solid_size_qty"]').attr('disabled','1');
			$(e).parents('tr').find('input[data-fieldname="solid_size_qty"]').val('');
			$(e).parents('tr').find('input[data-fieldname="std_parcel_number"]').removeAttr('disabled');
			$(e).parents('tr').find('input[data-fieldname="solid_size_qty"]').val('');
			totalVartical($(e).parents('tr').find('input[data-fieldname="solid_size_qty"]'));
		}else if ($(e).val()=="Solid Packing") {
			$(e).parents('tr').find('input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"]').val('').attr('disabled','1');
			$(e).parents('tr').find('input[data-fieldname="solid_size_qty"]').attr('disabled','1');
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_packing_qty"]'));
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_parcel_number"]'));
		}else {
			$(e).parents('tr').find('input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"], input[data-fieldname="solid_size_qty"]').val('').attr('disabled','1');
			totalVartical($(e).parents('tr').find('input[data-fieldname="solid_size_qty"]'));
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_packing_qty"]'));
			totalVartical($(e).parents('tr').find('input[data-fieldname="std_parcel_number"]'));
		};
	},
	check_mandetory:function (sel) {
		if ($(sel).val() !="") {
			$(sel).parents('.form-group.frappe-control').removeClass('has-error');
		}else{
			$(sel).parents('.form-group.frappe-control').addClass('has-error');
		};
	},
	validate:function (doc) {
		if ($('#html').find('.has-error').length >0) {
			$('#html').find('.has-error').each(function(index, el) {
				msgprint(__("Plese fill mandetory field {0}",[$(el).attr('data-fieldname')]))
			});
		};
		this.set_child_value(doc)
		console.log(doc.childs)
		return false
	},
	set_child_value:function (doc) {
		doc['childs']=[];		
		$('#html').find('.item').each(function(key, sel) {
			var item = {};
			item['size']=$(sel).find('input[data-fieldname="size"]').val();
			var table={};
			table['footer'] = [];
			// Body Value
			var body = [];
			$(sel).find('.tb-body tr').each(function(bidx, bval) {
				// set format_child
				var row ={};

				$(bval).find('input, select').each(function(k,rv) {
						row[$(rv).attr('data-fieldname')] = $(rv).val();
				});
				body.push(row);
			});
			item['datas'] = JSON.stringify(body);
			var static_fields = ['total_qty','total_amount','total_std_packing_qty','total_std_parcel_number','total_solid_size_qty']
				for (var i = 0; i < static_fields.length; i++) {
					item[static_fields[i]]=$(sel).find('.tb-footer input[data-fieldname="'+static_fields[i]+'"]').val()
				};
			doc['childs'].push(item)
		});
	}
});

cur_frm.cscript = new garments.merchandising.PureSheetController({frm:cur_frm});