# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.model.naming import make_autoname

class PureSheet(Document):
	def autoname(self):
		self.name = make_autoname("PS-{b}-{y}/{m}-.##".format(b=self.buyer or "", y = str(date.today().year)[2:4],m=date.today().month))
