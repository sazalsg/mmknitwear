# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class StyleFormat(Document):
	def validate(self):
		for f in self.format_child:
			self.check_unique_style(f.style)

	def check_unique_style(self,style):
		duplicates = []
		for el in self.format_child:
			if el.style==style: duplicates.append(el.idx)
		if len(duplicates) > 1:
			frappe.throw(_("Styel Name {0} appears multiple times in rows {1}").format(style, duplicates))
