# -*- coding: utf-8 -*-
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import msgprint, throw,_
from datetime import date
from frappe.model.naming import make_autoname
import time
from num2words import num2words

class PI(Document):
		
	def autoname(self):
		cname=frappe.db.get_value("Customer", {"name":self.customer}, ["customer_name"])
		self.name =  make_autoname('{0}-{1}-{2}{3}{4}-'.format("MNH",cname.split()[0], str(date.today().month), '/',str(date.today().year)[2:4] ) + ".#")
		
@frappe.whitelist()
def numToWords(num):

	if len(num.split(".")) == 1:
		return " and ".join(map(lambda x:num2words(int(x)),num.split(".")))

	else:
		return " and ".join(map(lambda x:num2words(int(x)),num.split("."))) + " cent"