cur_frm.cscript.incoterms = function(doc,doctype,docname){
	cur_frm.set_value("payment_terms", doc.incoterms +' / LC at sight' ); 
}

frappe.ui.form.on("PI Child", "pi_child_list_remove", function(frm){

        sumTotal(frm.doc,frm.doctype,frm.docname);
});

cur_frm.cscript.refresh=function(doc,doctype,docname){

	if(!doc.__islocal){

		benificiary(doc,doctype,docname);
		declaration(doc,doctype,docname);
	}else{
		cur_frm.set_value("company", "");
	}
}

cur_frm.cscript.onload=function(doc){


	this.frm.set_query("port_of_loading",function () {
			return {
				filters:[
					["Port Location","loading","=",1]
				]
			}
		})
	this.frm.set_query("port_of_discharge",function () {
			return {
				filters:[
					["Port Location","discharge","=",1]
				]
			}
		})

	this.frm.set_query("final_destination",function () {
			return {
				filters:[
					["Address","customer_name","=",doc.customer],
					["Address","address_type","=","Shipping"],
				]
			}
		})
}

cur_frm.cscript.benificiary_bank = function (doc,doctype,docname){

	benificiary(doc,doctype,docname);
}


// filter Benificiary Bank
cur_frm.set_query('benificiary_bank', function(){
	return{
		filters:{
			party_type: 'Supplier'
		}
	}
})

function benificiary(doc,doctype,docname){

	 var pro_name = doc.benificiary_bank;

	 //console.log(pro_name);

	/*if(pro_name.party_type == "Supplier"){*/
	if(pro_name){	
			frappe.call({

				method:"garments.merchandising.doctype.beneficiary_bank.beneficiary_bank.bank_info",
				args:{
						name:pro_name
				},
				callback:function (data, doc1) {

					console.log(data);

					if(data){

						data = data.message[0];

						var msg = "YOUR BANK address: "+ data[0]+"<br />Tel: ";
						msg += data[1] + "<br />Fax: ";
						msg += data[2]+"<br />ACCOUNT NO: "+data[4]+"<br />SWIFT CODE: "+data[3];

					$("#bank_info").html(msg);
					}
				}
			});
	}
	else{
		//setLocal.total_amount_discount = 0;
		//refresh_field("total_amount_discount", setLocal.name, setLocal.parentfield);
	} 		
}
cur_frm.cscript.declaration = function (doc,doctype,docname){
	declaration(doc,doctype,docname);
}

function declaration(doc,doctype,docname){

	 var pro_name = doc.declaration;

	if(pro_name){

			frappe.call({

				method:"garments.merchandising.doctype.declaration.declaration.dec_info",
				args:{
					
					name:pro_name
				},
				callback:function (data, doc1) {

					console.log(data);

					if(data){

						data = data.message[0];

					$("#decl_info").html(data);
					}
				}
			});

	}
	else{
		//setLocal.total_amount_discount = 0;
		//refresh_field("total_amount_discount", setLocal.name, setLocal.parentfield);
	} 		
}

cur_frm.cscript.proforma_order = function(doc,doctype,docname){

	var setLocal = locals[doctype][docname];

	 var pro_name = setLocal.proforma_order;

	if(pro_name){

			frappe.call({

				method:"garments.merchandising.doctype.proforma_order.proforma_order.order_query",
				args:{
					
					name:pro_name
				},
				callback:function (data, doc1) {

					if(data){

						var total_val = parseFloat(data.message[0][0]);

						var total_qty = parseInt(data.message[0][1]);

						var terms = parseFloat(doc.terms);

						if(terms){

							total_val = total_val - (total_val*terms)/100;

						}
					
						setLocal.total_amount_discount = total_val.toFixed(2);
						refresh_field("total_amount_discount", setLocal.name, setLocal.parentfield);

						setLocal.order_qty = total_qty;
						refresh_field("order_qty", setLocal.name, setLocal.parentfield);

						sumTotal(doc,doctype,docname);
					}
				}
			});
	}
	else{
		setLocal.total_amount_discount = 0;
		refresh_field("total_amount_discount", setLocal.name, setLocal.parentfield);
	} 		
}

function numToWords(amount){

	frappe.call({

				method:"garments.merchandising.doctype.pi.pi.numToWords",
				args:{
					num:amount
				},
				sync:false,
				callback:function (data, doc1) {

						console.log( 'For amount After asynch :: ' + data.message );

					if(data){

						var currency;

						currency = 'US DOLLAR ' + data.message + ' ONLY.';

						currency = currency.toUpperCase();
						cur_frm.set_value("amount_in_word", currency);
					}
				}
			});
}
// calculte child tables total field  
function sumTotal(doc,doctype,docname){

			var sum = 0;
			var sum_qty = 0;
			// Check Current Row Duplicate Entry
			for(var key in doc.pi_child_list){

				var res = parseFloat(doc.pi_child_list[key].total_amount_discount);

				if( res ){
					sum+=res;
				}
				var res = parseInt(doc.pi_child_list[key].order_qty);
				if( res ){
					sum_qty+= res;
				}
			}
			sum = sum.toFixed(2);

			cur_frm.set_value("total_amount_order", sum); 

			cur_frm.set_value("total_qty", sum_qty);

			numToWords(sum);
}