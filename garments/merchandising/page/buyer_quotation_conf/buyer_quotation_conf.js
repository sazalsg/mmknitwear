
frappe.breadcrumbs.add("Merchandising");

frappe.pages['buyer-quotation-conf'].on_page_load = function(wrapper) {
    var page = frappe.ui.make_app_page({
        parent: wrapper,
        title: 'Buyer Quotation Confirmation',
        single_column: true
    });


    page.main.css({
        "min-height": "300px",
        "padding-bottom": "25px"
    });


    page.tree_area = $('<div class="col-sm-12 bg-primary" style="padding:10px 10px 10px 30px;"><div class="col-sm-3">Reference/Style</div><div class="col-sm-3">Item Name</div><div class="col-sm-2">Total Qty</div><div class="col-sm-2">Per PC Price </div><div class="col-sm-2">Action</div></div><div class="clearfix"></div>').appendTo(page.main);



    page.set_secondary_action(__('Refresh'), function() {
        frappe.pages['buyer-quotation-conf'].on_page_show();
    });

}

var dataList = [];


frappe.pages['buyer-quotation-conf'].on_page_show = function(wrapper) {

  
   
   page: this.page;
   parent: $("<div class='info'></div>").appendTo(this.page.main);
   //$('.info').append("<div class='col-sm-12' style='padding:10px 10px 10px 30px;'><div class='col-sm-3 Reference'>Reference/Style</div><div class='col-sm-3'>Item Name</div><div class='col-sm-2'>Total Qty</div><div class='col-sm-2'>Per PC Price </div><div class='col-sm-2'>Action</div></div><div class='clearfix'></div>")



   frappe.call({

    method: "garments.merchandising.doctype.buyer_quotation.buyer_quotation.buyer_quation_data",
    args: {
        'name_qut':'Buyer Quotation Child',
        'status_qut':'Pending',
    }, 
    callback: function(r) { 
        
        
        
        for (var i = 0; i <=r.message.length; i++) {
            dataList.push(r)
          $('.info').append('<div class="col-sm-12 rowsList"><div class="col-sm-3"><a data-doctype="Style Name" href="#Form/Style Name/'+r.message[i].ref+'">'+r.message[i].ref+'</a></div><div class="col-sm-3">' +r.message[i].item+ '</div><div class="col-sm-2">' +r.message[i].t_prc+ '</div><div class="col-sm-2">' + r.message[i].per_pcs_price+'</div><div class="col-sm-2"><button type="button" class="confirm_qt btn btn-sm btn-success">Confirm</button> </div></div><div class="clearfix"><hr></div>');  
        }
        

        
        
        // set the returned value in a field
        // cur_frm.set_value(fieldname, r.message);


        }
    
    })

    // $('body').find('.frappe-list').remove();

    // this.page.list = new frappe.ui.BaseList({
    //     hide_refresh: true,
    //     page: this.page,
    //     method:"frappe.client.get_value",
    //     args: {
    //         doctype: "Buyer Quotation Child",
    //         filters: {
    //             status:"Pending"
            
    //         },
    //         fieldname:["name", "ref", "item", "t_prc", 'per_pcs_price']
    //     },
    //     parent: $("<div>asdasdasd</div>").appendTo(this.page.main),
    //     async: false,
    //     render_row: function(row, data) {
    //         // new frappe.activity.Feed(row, data);
    //         dataList.push(data);
    //         var dd = '<div class="col-sm-12 rowsList"><div class="col-sm-3"><a data-doctype="Style Name" href="#Form/Style Name/'+data['ref']+'">'+data['ref']+'</a></div><div class="col-sm-3">' + data['item'] + '</div><div class="col-sm-2">' + data['t_prc'] + '</div><div class="col-sm-2">' + data['per_pcs_price'] + '</div><div class="col-sm-2"><button type="button" class="confirm_qt btn btn-sm btn-success">Confirm</button> <button type="button" class="cancel_qt btn btn-sm btn-danger">Cancel</button></div></div><div class="clearfix"></div>'

    //         $(row).html(dd)
    //         // $(frappe.render_template("buyer_quot", {
    //         //       'data': data
    //         //   })).html(row);
    //     }
    // });

    // this.page.list.run();

};

$('body').on('click', '.confirm_qt', function() {

    var index = $("body").find('.confirm_qt').index(this);
    var val = $(this).html();
    console.log(dataList[0])
    
    
    update_status(dataList[0].message[index].name, val, index)

})

$('body').on('click', '.cancel_qt', function() {

    var index = $("body").find('.cancel_qt').index(this);
    var val = $(this).html();
    

    update_status(dataList[0].message[index].name, val, index);

})


function update_status(name, status, index) {
    
    var list = "";
    frappe.confirm(__(list),
        function() {
            frappe.call({
                method: "garments.merchandising.doctype.buyer_quotation.buyer_quotation.quotation_confirmation",
                args: {
                    'name': name,
                    'status': status,
                },
                cache: false,
                callback: function(r) {
                    if (r.message == "ok") {
                       
                         $('.info').find('.rowsList').eq(index).hide('slow');
                        

                    }
                }
            });

        }
    );
}





// frappe.breadcrumbs.add("Merchandising");

// frappe.pages['buyer-quotation-conf'].on_page_load = function(wrapper) {
//     var page = frappe.ui.make_app_page({
//         parent: wrapper,
//         title: 'Buyer Quotation Confirmation',
//         single_column: true
//     });


//     page.main.css({
//         "min-height": "300px",
//         "padding-bottom": "25px"
//     });


//     page.tree_area = $('<div class="col-sm-12 bg-primary" style="padding:10px 10px 10px 30px;"><div class="col-sm-3">Reference/Style</div><div class="col-sm-3">Item Name</div><div class="col-sm-2">Total Qty</div><div class="col-sm-2">Per PC Price </div><div class="col-sm-2">Action</div></div><div class="clearfix"></div>').appendTo(page.main);



//     page.set_secondary_action(__('Refresh'), function() {
//         frappe.pages['buyer-quotation-conf'].on_page_show();
//     });

// }

// var dataList = [];


// frappe.pages['buyer-quotation-conf'].on_page_show = function(wrapper) {


//     $('body').find('.frappe-list').remove();

//     this.page.list = new frappe.ui.Listing({
//         hide_refresh: true,
//         page: this.page,
//         method: "frappe.client.get_list",
//         args: {
//             doctype: "Buyer Quotation Child",
//             filters: [
//                 ["status", "=", 'Pending'],
//             ],
//             fields: ["name", "ref", "item", "t_prc", 'per_pcs_price']
//         },
//         parent: $("<div>asdasdasd</div>").appendTo(this.page.main),
//         async: false,
//         render_row: function(row, data) {
//             // new frappe.activity.Feed(row, data);
//             dataList.push(data);
//             var dd = '<div class="col-sm-12 rowsList"><div class="col-sm-3"><a data-doctype="Style Name" href="#Form/Style Name/'+data['ref']+'">'+data['ref']+'</a></div><div class="col-sm-3">' + data['item'] + '</div><div class="col-sm-2">' + data['t_prc'] + '</div><div class="col-sm-2">' + data['per_pcs_price'] + '</div><div class="col-sm-2"><button type="button" class="confirm_qt btn btn-sm btn-success">Confirm</button> <button type="button" class="cancel_qt btn btn-sm btn-danger">Cancel</button></div></div><div class="clearfix"></div>'

//             $(row).html(dd)
//             // $(frappe.render_template("buyer_quot", {
//             // 		'data': data
//             // 	})).html(row);
//         }
//     });

//     this.page.list.run();

// };

// $('body').on('click', '.confirm_qt', function() {

//     var index = $("body").find('.confirm_qt').index(this);
//     var val = $(this).html();


//     update_status(dataList[index]['name'], val, index)

// })

// $('body').on('click', '.cancel_qt', function() {

//     var index = $("body").find('.cancel_qt').index(this);
//     var val = $(this).html();


//     update_status(dataList[index]['name'], val, index);

// })


// function update_status(name, status, index) {

//     var list = "";
//     frappe.confirm(__(list),
//         function() {
//             frappe.call({
//                 method: "garments.merchandising.doctype.buyer_quotation.buyer_quotation.quotation_confirmation",
//                 args: {
//                     'name': name,
//                     'status': status,
//                 },
//                 cache: false,
//                 callback: function(r) {
//                     if (r.message == "ok") {
//                         $('body').find('.list-row').eq(index).hide('slow');
//                     }
//                 }
//             });

//         }
//     );
// }