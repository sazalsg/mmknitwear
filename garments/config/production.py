from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Documents"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Pattern Cutting",
					"description": _("Pattern Cutting")
				},
				{
					"type": "doctype",
					"name": "Size Set",
					"description": _("Create and manage size set")
				},
				{
					"type": "page",
					"name": "Size Set Approval",
					"label": "Size Set Approval",
					"description": _("Size set QC")
				},
				{
					"type": "doctype",
					"name": "Bundle",
					"description": _("Create and manage bundle")
				},
				# {
				# 	"type": "doctype",
				# 	"name": "Packing",
				# },
				{
					"type": "doctype",
					"name": "Embellishment",
					"description": _("Embellishment input")
				},
				{
					"type": "doctype",
					"name": "Embellishment Receive",
					"description": _("Embellishment Receive")
				},
				{
					"type": "page",
					"name": "embellishment-qc",
					"label": "Embellishment QC",
					"description": _("Embellishment QC")
				},
				{
					"type": "doctype",
					"name": "Finished Goods Process",
					"description": _("Finished Goods Process")
				},
				{
					"type": "doctype",
					"name": "Measurement Check",
					"description": _("Measurement Check")
				},
				{
					"type": "doctype",
					"name": "Inspection",
					"description": _("Inspection")
				}
			]
		},
		{
			"label": _("Cutting"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Test Cutting",
					"description": _("Test cutting input")
				},
				{
					"type": "page",
					"name": "Test Cutting Approval",
					"label": "Test Cutting Approval",
					"description": _("Test cutting QC")
				},
				{
					"type": "doctype",
					"name": "Bulk Cutting",
					"description": _("Bulk cutting input")
				},
				{
					"type": "doctype",
					"name": "Bulk Cutting QC",
					"description": _("Bulk cutting QC")
				},
				{
					"type": "doctype",
					"name": "Cutting Alter Entry",
					"description": _("Add Alter in Test Cutting/Bulk Cutting")
				}
			]
		},
		{
			"label": _("Production Line"),
			"icon": "icon-star",
			"items": [

					{
						"type": "doctype",
						"name": "Production Line",
						"description": _("Production line worker input")
					},
					{	
						"type": "doctype",
						"name": "Alter Entry",
						"description": _("Add Alter in Production Line")
					},
					{	
						"type": "page",
						"name": "daily-overtime-report",
						"label": "Daily Overtime Entry",
						"description": _("Daily Overtime Entry")
					}
			]
		},
		{
			"label": _("Setup"),
			"icon": "icon-cog",
			"items": [
				{
					"type": "doctype",
					"name": "Line No",
					"description": _("Create Line No.")
				},
				{
					"type": "doctype",
					"name": "Nature of Work",
					"description": _("Create and manage worker working natures")
				},
				{
					"type": "doctype",
					"name": "Production Line Setup",
					"description": _("Create production line")
				},
				# {
				# 	"type": "doctype",
				# 	"name": "Packing Type",
				# },
			]
		},
		{
			"label": _("Report"),
			"icon": "icon-list",
			"items": [
				{
					"type": "report",
					"name": "Hourly Production Report",
					"doctype":"Production Line",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Size Set",
					"doctype": "Size Set",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Embellishment",
					"doctype":"Embellishment",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Size Set Approval",
					"doctype":"Size Set",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Cutting Alter",
					"doctype":"Cutting Alter",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Test Cutting Approval",
					"doctype":"Test Cutting",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Test Cutting Order",
					"doctype":"Test Cutting Order",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Pattern Cutting",
					"doctype":"Pattern Cutting",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Bulk Cutting Order",
					"doctype":"Bulk Cutting Order",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Production Alter",
					"doctype":"Production Line",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Production Line Setup",
					"doctype": "Production Line Setup",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Bundle",
					"doctype":"Bundle",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Bulk Cutting",
					"doctype":"Bulk Cutting",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Measurement Check",
					"doctype": "Measurement Check",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Finished Goods Process",
					"doctype":"Finished Goods Process",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Daily Overtime",
					"doctype": "Daily Overtime",
					"is_query_report": True
				}
			]
		}

	]
