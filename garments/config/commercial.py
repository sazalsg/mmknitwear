from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Documents"),
			"icon": "icon-star",
			"items": [
				{
					"type":"doctype",
					"name":"LC or SC",
					"label":_("LC or SC"),
					"description":_("Letter of Credit / Sales Contact")
				},
				{
					"type":"doctype",
					"name":"Bill of Exchange",
				},
				{
					"type":"doctype",
					"name":"Packing List",
				},
				{
					"type":"doctype",
					"name":"Commercial Invoice",
				},
				{
					"type":"doctype",
					"name":"Short Shipment",
				},
				{
					"type":"doctype",
					"name":"Bill of Entry or Export",
					"label":_("Bill of Entry/Export")
				},
				{
					"type":"doctype",
					"name":"Beneficiary Certificate",
				},
				{
					"type":"doctype",
					"name":"Exp Form",
				},
				{
					"type":"doctype",
					"name":"Bill of Lading",
					"label":_("Bill of Lading")
				},
				{
					"type":"doctype",
					"name":"Certificate of Origin",
					"label":_("Certificate of Origin")
				},
				{
					"type":"doctype",
					"name":"Export Document",
				},
				{
					"type":"doctype",
					"name":"Additional Attachment",
				},
				{
					"type":"page",
					"name":"export_print_format",
					"label":_("Print all Document")
				},
				{
					"type":"doctype",
					"name":"Realization",
					"label":_("Realization")
				},
				{
					"type": "doctype",
					"name": "Undertaking",
					"label": _("Undertaking")

				},
				{
					"type":"doctype",
					"name":"Incentive",
					"label":_("Incentive")
				}
			]
		},
		{
			"label":_("Setup"),
			"icon":"icon-cog",
			"items":[
				{
					"type":"doctype",
					"name":"Serialize Print",
					"label": _("Serialize Print")
				},
				{
					"type": "doctype",
					"name": "Terms of Delivery",
					"description": _("Terms of Delivery"),
					"label": _("Terms of Delivery")
				},
				{
					"type": "doctype",
					"name": "Forwarding Agent",
					"description": _("Forwarding Agent"),
					"label": _("Forwarding Agent")
				},
				{
					"type": "doctype",
					"name": "BBLC Yarn",
					"description": _("BBLC Yarn Details"),
					"label": _("BBLC Yarn")
				},
				{
					"type": "doctype",
					"name": "Entry or Exit Office",
					"description": _("Entry or Exit Office"),
					"label": _("Entry or Exit Office")
				},
				{
					"type": "doctype",
					"name": "Place of Loading or Unloading",
					"description": _("Place of Loading or Unloading"),
					"label": _("Place of Loading or Unloading")
				},
				{
					"type": "doctype",
					"name": "Realization Deduction Type",
					"description": _("Realization Deduction Type"),
					"label": _("Realization Deduction Type")
				}
			]
		},
		{
			"label":_("Report"),
			"icon":"icon-list",
			"items":[
				{
					"type": "report",
					"name": "Import Statement",
					"description": _("Import related information"),
					"doctype":"LC or SC",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "LC or SC",
					"description": _("LC or SC related information"),
					"doctype":"LC or SC",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "LC or SC Expire",
					"description": _("LC or SC related information"),
					"doctype":"LC or SC",
					"is_query_report": True
				},
				# {
				# 	"type": "report",
				# 	"name": "LC or SC Expire Weekly",
				# 	"description": _("LC or SC related information"),
				# 	"doctype":"LC or SC",
				# 	"is_query_report": True
				# },
				# {
				# 	"type": "report",
				# 	"name": "LC or SC Expire Monthly",
				# 	"description": _("LC or SC related information"),
				# 	"doctype":"LC or SC",
				# 	"is_query_report": True
				# },
				# {
				# 	"type": "report",
				# 	"name": "LC or SC Expire Date to Date",
				# 	"description": _("LC or SC related information"),
				# 	"doctype":"LC or SC",
				# 	"is_query_report": True
				# },
				{
					"type": "report",
					"name": "Packing Detail",
					"description": _("Packing List related information"),
					"doctype":"Packing List",
					"is_query_report": True
				},

				{
					"type": "report",
					"name": "Bill of Exchange",
					"description": _("Bill of Exchange related information"),
					"doctype":"Bill of Exchange",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Beneficiary Certificate",
					"description": _("Beneficiary Certificate related information"),
					"doctype":"Beneficiary Certificate",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Exp Form detail",
					"description": _("Exp Form related information"),
					"doctype":"Exp Form",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Export Document",
					"description": _("Export Document related information"),
					"doctype":"Export Document",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Commercial Invoice Detail",
					"description": _("Commercial Invoice related information"),
					"doctype":"Commercial Invoice",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Daily Export Documents Follow Up",
					"description": _("Export Documents related information"),
					"doctype":"Export Document",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Daily Cash LC Follow Up",
					"description": _("Cash LC related information"),
					"doctype":"LC or SC",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Local Yarn",
					"description": _("Daily BBLC Follow Up-Local Yarn"),
					"doctype":"LC or SC",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Imported Yarn",
					"description": _("Daily Imported BBLC Follow Up-Imported Yarn"),
					"doctype":"LC or SC",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "Daily BBLC Documents Follow Up",
					"description": _("BBLC related information"),
					"doctype":"LC or SC",
					"is_query_report": True
				},
				{
					"type": "report",
					"name": "LC status note book",
					"description": _("LC related information"),
					"doctype":"LC or SC",
					"is_query_report": True
				}
				

			]
		}
	]