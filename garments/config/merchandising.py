from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Documents"),
			"icon": "icon-star",
			"items": [
				{
					"type":"doctype",
					"name":"Buyer Inquiry",
					"description":_("Buyer Inquiry")
				}
				,
				{
					"type": "doctype",
					"name": "Consumption Sheet",
					"description": _("Consumption Sheet")
				},
				{
					"type": "doctype",
					"name": "Consumption Workout",
					"description": _("Consumption Workout")
				},
				{
					"type": "doctype",
					"name": "Costing",
					"description": _("Costing")
				},
				{
					"type": "doctype",
					"name": "Buyer Quotation",
					"description": _("Buyer Quotation")
				},
				
				{
					"type": "page",
					"name": "buyer-quotation-conf",
					"label": _("Buyer Quotation Confirmation"),
					"icon": "icon-bar-chart",
				},

				{
					"type": "doctype",
					"name": "PO Sheet",
					"description": _("PO Sheet")
				},

				{
					"type": "doctype",
					"name": "Order ReCap",
					"description": _("Order ReCap")
				},
				
				{
					"type":"doctype",
					"name":"PO",
					"description":_("Purchase Order")
				},
				{
					"type": "doctype",
					"name": "PI",
					"description": _("Proforma Invoice")
				},
				{
					"type": "doctype",
					"name": "Job",
					"description": _("JOB Creation")
				},
				{
					"type": "doctype",
					"name": "Order Sheet",
					"description": _("Order Sheet")
				},
				# {
				# 	"type":"doctype",
				# 	"name":"Shipping Confirm",
				# 	"description":_("Shipping Confirm")
				# },

				# {
				# 	"type": "doctype",
				# 	"name": "Pattern Cutting",
				# 	"description": _("Pattern Cutting")
				# },


			]
		},
		{
			"label": _("Sampling"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "PP Sample",
					"description": _("Pre Production Sample")
				},
				
				{
					"type": "doctype",
					"name": "Sampling",
					"description": _("Sampling")
				},
				{
					"type": "doctype",
					"name": "Sample Type",
					"description": _("Sample Type")
				}

			]
		},
		{
			"label": _("Booking"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Fabric Order",
					"description" :_("Fabric Order")
				},

				{
					"type": "doctype",
					"name": "Sewing Booking",
					"description": _("Sewing Booking")
				},

				{
					"type": "doctype",
					"name": "Zipper Booking",
					"description": _("Zipper Booking")
				},

				{
					"type": "doctype",
					"name": "Others Booking",
					"label": _("Others Booking"),
					"description": _("Others Booking")
				}


			]
		},
		{
			"label": _("Setup"),
			"icon": "icon-cog",
			"items": [
				{
					"type": "doctype",
					"name": "Beneficiary Bank",
					"description": _("Beneficiary Bank")
				},
				{
					"type": "doctype",
					"name": "Color",
					"description": _("Color")
				},
				{
					"type": "doctype",
					"name": "Terms of Delivery",
					"description": _("Terms of Delivery")
				},


				{
					"type": "doctype",
					"name": "Port Location",
					"description": _("Port Location")
				},


				{
					"type": "doctype",
					"name": "Proforma Order",
					"description": _("Proforma Order")
				},

				{
					"type": "doctype",
					"name": "Style Name",
					"description": _("Style Name")
				},

				{
					"type": "doctype",
					"name": "Style Format",
					"description": _("Style Format")
				},

				{
					"type": "doctype",
					"name": "Theme",
					"description": _("Theme")
				},

				{
					"type": "doctype",
					"name": "Size",
					"description": _("Size")
				},

				{
					"type": "doctype",
					"name": "Size Format",
					"description": _("Size Format")
				}
			]
		},
		{
			"label":_("Report"),
			"icon":"icon-list-alt",
			"items":[
				{
					"type":"report",
					"name":"Costing",
					"description":_("Costing Report"),
					"is_query_report":1,
					"doctype":"Costing"
				},

				{
					"type":"report",
					"name":"PP Sample",
					"description":_("PP Sample Report"),
					"is_query_report":1,
					"doctype":"PP Sample"
				},

				{
					"type": "report",
					"name": "Fabric Order",
					"description": _("Fabric Order Report"),
					"is_query_report": 1,
					"doctype":"Fabric Order"
				},
		
				{
					"type":"report",
					"name":"Consumption Workout Report",
					"description":_("Consumption Workout Report"),
					"is_query_report":1,
					"doctype":"Consumption Workout"
				},

				{
					"type":"report",
					"name":"Order Sheet Report",
					"label":"Order Sheet",
					"description":_("Order Sheet Report"),
					"is_query_report":1,
					"doctype":"Order Sheet"
				},
				{
					"type":"report",
					"name":"PI Report",
					"label":"PI",
					"description":_("PI"),
					"is_query_report":1,
					"doctype":"PI"
				},
				{
					"type":"report",
					"name":"PO Report",
					"label":"PO",
					"description":_("Purchase Order Report"),
					"is_query_report":1,
					"doctype":"PO"
				},
				{
					"type":"report",
					"name":"Size Set",
					"label":"Size Set",
					"description":_("Size Set"),
					"is_query_report":1,
					"doctype":"Size Set"
				},
				{
					"type":"report",
					"name":"Buyer Quotation",
					"label":"Buyer Quotation",
					"description":_("Buyer Quotation"),
					"is_query_report":1,
					"doctype":"Buyer Quotation"
				},
				{
					"type":"report",
					"name":"Order ReCap Report",
					"label":"Order ReCap",
					"description":_("Order ReCap Report"),
					"is_query_report":1,
					"doctype":"Order ReCap"
				}
			]
		},

	]
