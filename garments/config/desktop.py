# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return {
		"production": {
			"color": "#3498db",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Production")
		},
		"Merchandising": {
			"color": "#1abc9c",
			"force_show": True,
			"icon": "octicon octicon-jersey",
			"type": "module",
			"is_help": True
		},
		"Commercial": {
			"color": "#1abc9c",
			#"force_show": True,
			"icon": "octicon octicon-shield",
			"type": "module",
			#"is_help": True
		}
	}
