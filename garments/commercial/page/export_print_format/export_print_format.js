frappe.pages['export_print_format'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Export Print Format',
		single_column: true
	});
	frappe.breadcrumbs.add(frappe.breadcrumbs.last_module || "Commercial");

	page.view_area = $('<div id="view_area"></div>').appendTo(page.main)

	// page.print_form = $('<form action="/export_print" method="POST" class="hide"><textarea name="html"></textarea></form>').appendTo(page.main)

	page.wrapper.prepend('<style type="text/css">@media print{.page-form,.page-head,.page-toolbar{display:none}}</style>')

	var datas = function () {
		new techbeeo.SerializePrint(page,page.view_area)
	}

	page.$print = wrapper.page.add_field({
		fieldname:"print",
		fieldtype:"Link",
		options:"Serialize Print",
		label:"Print Format"
	}).$input.change(function(event) {
		datas()
	});

	page.$invoice = wrapper.page.add_field({
		fieldname:"invoice",
		fieldtype:"Link",
		options:"Commercial Invoice",
		label:__("Invoice")
	}).$input.change(function(event) {
		datas()
	});

	page.$letter_head = wrapper.page.add_field({
		fieldname:"letter_head",
		fieldtype:"Check",
		label:"Letter Head"
	}).$input.change(function(event) {
		datas()
	});

	page.$print_button = wrapper.page.add_field({
		fieldname:"print_button",
		fieldtype:"Button",
		label:"Print"
	})/*.$input.click(function(event) {
		var $html = page.view_area.find('.print-format').html();
		page.print_form.find('[name="html"]').html(encodeURI($html));
		console.log(page.print_form)
		page.print_form.submit();
	});*/



}

frappe.provide('techbeeo');
techbeeo.SerializePrint = Class.extend({
	init:function (page,parent) {
		$(parent).empty()
		var me = this;
		me.$parent = parent
		me.print = page.$print.val();
		me.invoice = page.$invoice.val();
		me.$print_button = page.$print_button;
		// me.letter_head = page.$letter_head.checked;
		me.letter_head = (page.$letter_head.prop('checked'))?0:1;
		console.log(me.letter_head)
		if (me.print && me.invoice) {
			me.fieldnames()
		};
	},
	fieldnames:function () {
		var me = this;
		frappe.call({
			method:"garments.commercial.doctype.serialize_print.serialize_print.get_exsist_data",
			args:{
				print_name:me.print,
				invoice:me.invoice
			},
			callback:function (r) {
				if (r.message) {
					me.prints_docs = r.message
					me.datarender()
				};
			}
		})
	},
	datarender:function () {
		var me = this;
		for (var i = 0; i < this.prints_docs.length; i++) {
			this.prints_docs[i]['no_letterhead'] = this.letter_head
			$.ajax({
				url: '/print',
				type: 'GET',
				data: this.prints_docs[i],
				success:function (r) {
					var $html  = $(r);
					var $pr = $('<div class="print-format-gutter"></div>').appendTo(me.$parent)
					$pr.append($html.find('.print-format'))
				}
			})
		};
		// $(me.$parent).append('<div class="clearfix"></div>')
		var str = encodeURI(JSON.stringify(this.prints_docs))
		str = str.replace('&','%26')
		me.$print_button.$input.click(function(event) {
			window.location.assign('/export_print?docs='+str)
		});
	}
});
