cur_frm.cscript.to = function (doc) {
	if (!doc.to) {
		return
	};
	frappe.call({
		method:"frappe.client.get",
		args:{
			doctype:cur_frm.fields_dict.to.df.options,
			name:doc.to
		},
		callback:function (r) {
			if (r.message) {
				address = (r.message.address_line1 ? r.message.address_line1+"<br>" : "")
				address += (r.message.address_line2 ? r.message.address_line2+"<br>" : "")
				address += (r.message.city ? r.message.city+"<br>" : "")
				address += (r.message.state ?r.message.state+"<br>" : "")
				address += (r.message.postal_code ? r.message.postal_code+"<br>" : "")
				address += r.message.country || ""
				cur_frm.set_value('address',address)
			};
		}
	})
}
cur_frm.cscript.master_lc = function (doc) {
	if (!doc.master_lc) {
		return
	};
	frappe.call({
		method:"frappe.client.get",
		args:{
			doctype:cur_frm.fields_dict.master_lc.df.options,
			name:doc.master_lc
		},
		callback:function (r) {
			if (r.message) {
				cur_frm.set_value('reference',__("L/C No. ")+doc.master_lc+" "+r.message.date_of_issue)
				cur_frm.set_value('to',r.message.sender_bank_address)
			};
		}
	})
}

cur_frm.cscript.onload = function(doc,doctype,docname){
	// LC Filter
	this.frm.set_query('master_lc',function () {
		frappe.model.validate_missing(doc, "job");
		return {
			filters:{
				'job':doc.job
			}
		}
	})
}

cur_frm.cscript.job = function(doc){
	doc.master_lc = "";
	cur_frm.script_manager.trigger('master_lc')
	refresh_field('master_lc')
}
cur_frm.set_query('invoice', function () {
    frappe.model.validate_missing(cur_frm.doc,'job');
    return {
        filters:{
            job:cur_frm.doc.job
        }
    }
});

cur_frm.add_fetch('invoice','lc','master_lc');
