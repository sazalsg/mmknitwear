frappe.provide('garments.commercial')

{% include "garments/public/js/controllers/multicurrency_controller.js" %}
garments.commercial.ExpFormController = garments.MultiCurrencyController.extend({
		

	destination_port:function(frm)
	{

		 frappe.call({
            "method": "frappe.client.get",
            args: {
                doctype: "Port Location",
                name: me.frm.doc.destination_port,
            },
            callback: function (data) 
            {
				me.frm.set_value('destination_country',data.message.country)
				
            }
        });
	},



	onload:function (doc) {
		this.frm.add_fetch('lc','currency','currency')
		this.frm.add_fetch('lc','amount','total_amount')
		// cur_frm.add_fetch("styl","buyer","buyer")
		this.frm.set_query('lc',function () {
			frappe.model.validate_missing(doc, "job");
			return {
				filters:{
					'job':doc.job
				}
			}
		})
        if(this.frm.doc.company && !this.frm.doc.amended_from && this.frm.doc.__islocal) {
            cur_frm.script_manager.trigger("company");
        }
	},
	refresh:function (doc) {
		this.invoiced_amount(doc)
        this.change_form_labels(this.get_company_currency())
	},
	job:function(doc){
		doc.lc = "";
		this.frm.script_manager.trigger('lc')
		refresh_field('lc')
	},
	invoiced_amount:function (doc) {
		var me = this
		frappe.call({
			method:'garments.commercial.doctype.exp_form.exp_form.get_invoiced_amount',
			args:{
				fl:'invoice_value',
				doctype:doc.doctype,
				filters:{
					'lc':doc.lc,
					'docstatus':1,
					'name':['!=',doc.name]
				}
			},
			callback:function (r) {
				if (r.message[0].result)
					me.frm.set_value('invoiced_amount',r.message[0].result);
			}
		})
	},
    fields_of_company_currency:[
        'base_total_amount','base_invoiced_amount','base_invoice_value'
    ],
    fields_of_form_currency:[
        'total_amount','invoiced_amount','invoice_value'
    ],
    invoice_value: function () {
        this._set_base_field_value('base_invoice_value')
    },
    total_amount: function () {
        this._set_base_field_value('base_total_amount')
    },
	invoice: function (doc) {
        var me = this;
		me.get_linked_doc_value(this.frm.fields_dict.invoice.df.options,doc.invoice, function (data) {
            me.frm.set_value('invoice_value',data.total_amount);
            console.log(data.total_pcs)
            me.frm.set_value('quantity',data.total_pcs)

        })
	},
    get_linked_doc_value:function (doctype,docname,callback) {
        return frappe.call({
            method:'frappe.client.get',
            args:{
                doctype:doctype,
                name:docname
            },
            callback: function (r) {
                if(!r.exc && r.message) {
                    callback(r.message)
                }
            }
        })
    }
})

cur_frm.cscript = new garments.commercial.ExpFormController({frm: cur_frm})
