# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, json
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc

class ExpForm(Document):
	pass

@frappe.whitelist()
def get_invoiced_amount(fl, doctype,filters):
	conditions, values = frappe.db.build_conditions(json.loads(filters))
	# frappe.msgprint(conditions)
	return frappe.db.sql("select sum({0})as result from `tab{1}` where {2}".format(fl, doctype,
			conditions), values, as_dict=1)
