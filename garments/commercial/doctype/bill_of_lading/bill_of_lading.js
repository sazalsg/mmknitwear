// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Bill of Lading', {
		refresh: function(frm) {
		if(frm.doc.docstatus==0) {
			cur_frm.add_custom_button(__(frm.fields_dict.invoice.df.options),
				function() {
					frm.set_value('items',[])
					erpnext.utils.map_current_doc({
						method: "garments.commercial.doctype.commercial_invoice.commercial_invoice.get_data_for_bill_of_lading",
						source_doctype: frm.fields_dict.invoice.df.options
					})
				}, __("Get items from"));	
		}
	}
});
