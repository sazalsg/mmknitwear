// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.provide('garments.commercial')
{% include "garments/public/js/controllers/multicurrency_controller.js" %};

frappe.ui.form.on('Realization', {
	refresh: function(frm) {
		if(frm.doc.docstatus==0) {
			cur_frm.add_custom_button(__(frm.fields_dict.invoice_no.df.options),
				function() {
					// frm.set_value('items',[])
					erpnext.utils.map_current_doc({
						method: "garments.commercial.doctype.commercial_invoice.commercial_invoice.get_data_for_commercial_invoice",
						source_doctype: frm.fields_dict.invoice_no.df.options
					})
				}, __("Get items from"));	
		}
	},
});
frappe.ui.form.on("Deduction",{
	deduction_type:function (frm,doctype,docname) {
		cur_frm.cscript.total_calculate(cur_frm.doc);
		var local = locals[doctype][docname];
		frappe.call({
			method:"frappe.client.get",
			args:{
				doctype:"Realization Deduction Type",
				name:local.deduction_type
			},
			callback:function (data) {
				if (data.message) {
					// console.log(data)
					local.amount = data.message.amount
					local.details = data.message.details
				}else{
					local.amount = ""
					local.details = ""
				}
				refresh_field('amount',local.name,'deduction')
				refresh_field('details',local.name,'deduction')
			}
		})
	},
	deduction_remove:function (frm) {
		cur_frm.cscript.total_calculate(cur_frm.doc)
	}
});

garments.commercial.RealizationController = garments.MultiCurrencyController.extend({
	fields_of_company_currency:[
        'base_invoice_value', 'base_realized_amount','base_total_deduction_amount','base_short_amount'
    ],
    fields_of_form_currency:[
        'invoice_value', 'realized_amount','total_deduction_amount','short_amount'
    ],
	realized_amount:function (doc) {
		if (doc.realized_amount>doc.invoice_value) {
			msgprint(__("Realized Amount can't greater than Invoice Amount"));
			doc.realized_amount=""
			refresh_field('realized_amount');
		}
		this.frm.set_value("short_amount", doc.invoice_value - doc.realized_amount);
		this._set_base_field_value('base_realized_amount')
		this._set_base_field_value('base_short_amount')
	},
	amount:function (doc,dt,dn) {
		var local = locals[dt][dn];
		this._set_child_base_field_value(['amount'],local);
		cur_frm.cscript.total_calculate(cur_frm.doc);
	},
	refresh:function () {
		// this._super;
		this.set_dynamic_labels();
	},
    total_calculate:function (doc) {
		var totals = 0.0;
		var totals_base = 0.0;
	    // Check Current Row Duplicate Entry
	    for(var key in doc.deduction){
			if(parseInt(doc.deduction[key].amount)){
				totals+=flt(doc.deduction[key].amount);
				totals_base+=flt(doc.deduction[key].base_amount);
			}
		}
	    this.frm.set_value("total_deduction_amount", totals);
	    this.frm.set_value("base_total_deduction_amount", totals_base);
		this.set_dynamic_labels();
	},
	grid_from_base_fields: function () {
        return [
            {
                "base_label_change_fields":['base_amount'],
                "base_fields":['base_amount'],
                "label_change_fields":['amount'],
                "fields":['amount'],
                "table_field_name":"deduction"
            }
        ]
    },
});
cur_frm.cscript = new garments.commercial.RealizationController({frm: cur_frm});
