# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
# from frappe.model.naming import make_autoname

class SerializePrint(Document):
	pass
	# def autoname(self):
		# self.name = make_autoname(self.export_document+"-.##")


@frappe.whitelist()
def get_exsist_data(print_name,invoice):
	print_doc = frappe.get_doc("Serialize Print",print_name)
	rows = []

	for doc in print_doc.document:
		data = frappe.db.get_values(doc.sl_doctype,{("name" if doc.sl_doctype =="Commercial Invoice" else "invoice"):invoice},'name')
		if len(data)>0:
			# default_print = frappe.db.get_value("DocType",{"name":data[0][0]},'default_print_format')
			meta = frappe.get_meta(doc.sl_doctype)
			rows.append({'doctype':doc.sl_doctype,'name':data[0][0],'format':meta.default_print_format or "Standard"})

	return rows