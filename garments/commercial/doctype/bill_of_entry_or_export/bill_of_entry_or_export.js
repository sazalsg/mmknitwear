frappe.ui.form.on("Bill of Entry or Export",{
    refresh:function (frm) {
        if (frm.doc.docstatus == 0) {
            frm.add_custom_button(__(frm.fields_dict.invoice.df.options),
                function() {
                    frm.set_value('items',[]);
                    erpnext.utils.map_current_doc({
                        method: "garments.commercial.doctype.commercial_invoice.commercial_invoice.get_items_for_bill_of_entry",
                        source_doctype: frm.fields_dict.invoice.df.options
                        /*get_query_filters: {
                            docstatus: 1
                        }*/
                    })
            }, __("Get items from"));
        }
    }
});