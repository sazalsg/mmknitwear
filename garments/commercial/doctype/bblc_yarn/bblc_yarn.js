// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('BBLC Yarn', {
	refresh: function(frm) {

	}
});
cur_frm.set_query('bblc',function(){
    return {
        filters:{
            lc_type:'BBLC'
        }
    }
});

cur_frm.cscript.quantity_of_fabrics = function(doc,doctype,docname){
    sumTotal(doc,doctype,docname);
}
cur_frm.cscript.bblc_amount = function(doc,doctype,docname){
    cur_frm.set_value("usable_value", doc.bblc_amount);
}

frappe.ui.form.on("BBLC Yarn Child", "child_remove", function(frm){
    sumTotal(frm.doc,frm.doctype,frm.docname);
});

function sumTotal(doc,doctype,docname){

            var sum_qty = 0;
            // Check Current Row Duplicate Entry
            for(var key in doc.child){
                var res = parseInt(doc.child[key].quantity_of_fabrics);
                if( res ){
                    sum_qty+= res;
                }
            }
            cur_frm.set_value("total_yarn_qty", sum_qty); 
            cur_frm.set_value("available_quantity", sum_qty); 
}

cur_frm.cscript.use_quantity = function(doc,doctype,docname){
    if (doc.use_quantity > doc.total_yarn_qty) {
        msgprint(__("Can't greater than {0}", [doc.total_yarn_qty]));
        doc.use_quantity = "";
        refresh_field('use_quantity');
        sumTotal(doc, doctype, docname); 
    }
    if (doc.use_quantity ==0) {
        console.log("fdsfds");
    }
    
    if (!doc.use_quantity){
        cur_frm.set_value("available_quantity", doc.total_yarn_qty); 
        cur_frm.set_value("use_quantity", 0); 
    }else{
        cur_frm.set_value("available_quantity", doc.total_yarn_qty - doc.use_quantity); 
        cur_frm.set_value("usable_value", (doc.bblc_amount / doc.total_yarn_qty) * doc.use_quantity);
    }
    
}