// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt
cur_frm.set_query('invoice', function () {
    frappe.model.validate_missing(cur_frm.doc,'job');
    return {
        filters:{
            job:cur_frm.doc.job
        }
    }
});