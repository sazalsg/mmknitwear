// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Incentive', {
	refresh: function(frm) {

	}
});

/*
	Reference LC type filters
 */
cur_frm.set_query('reference_lc',function(){
    return {
        filters:{
            lc_type:'Master LC'
        }
    }
});
/*
	lc_date, lc_value, company, address field auto clean after reference_lc blank
 */
cur_frm.cscript.reference_lc = function(doc,doctype,docname){
	if (!doc.reference_lc) {
		doc.lc_date = "";
		doc.lc_value = "";
		doc.company = "";
		doc.address = "";
		refresh_field('lc_date');
		refresh_field('lc_value');
		refresh_field('company');
		refresh_field('address');
		
	}
}
/*
	export_registration_certificate_no field auto clean after company blank
 */
cur_frm.cscript.company = function(doc,doctype,docname){
	if (!doc.company) {
		doc.export_registration_certificate_no = "";
		refresh_field('export_registration_certificate_no');		
	}
}

//cur_frm.cscript.frm.add_fetch("company", "export_registration_certificate_no", "account_and_risk_of_measure");