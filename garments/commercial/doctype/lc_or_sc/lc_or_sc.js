frappe.provide('garments.commercial')
{% include "garments/public/js/controllers/multicurrency_controller.js" %}
garments.commercial.LcController = frappe.ui.form.Controller.extend({
    
   

    onload: function() {
        // FIltering Mastering LC
        this.frm.set_query('master_lc', function () {
            return {
                filters:{
                    'lc_type':'Master LC',
                    'docstatus':1
                }
            }
        })
        this.setup()
        if(this.frm.doc.company && !this.frm.doc.amended_from && this.frm.doc.__islocal) {
            cur_frm.script_manager.trigger("company");
        }
        // port_of_loading filtering
        this.frm.set_query('port_of_loading', function () {
            return {
                filters:{
                    'loading':true,
                }
            }
        })
        
    },
    setup:function() {
        fields_map = {'swift_output':'swift_input'}
        for (key in fields_map) {
            this.frm.add_fetch('master_lc',key,fields_map[key])
        }
        this.toggle_reqd_dep_on_type()
        this.discharge_filters('port_of_discharge');
        this.discharge_filters('port_of_final_destination');
        this.frm.set_query('sender_bank_address', function(){
            return{
                filters:{
                    party_type: 'Supplier'
                }
            }
        });
        this.frm.set_query('receiver_bank_address', function(){
            return{
                filters:{
                    party_type: 'Customer'
                }
            }
        });
    },
    discharge_filters:function (fieldname) {
        this.frm.set_query(fieldname, function () {
            return {
                filters:{
                    'discharge':true,
                }
            }
        })
    },
    type:function (doc) {
        this.toggle_reqd_dep_on_type()
    },
    toggle_reqd_dep_on_type:function () {
        var fields = ['sender_code','receiver_code','form_of_documentary_credit','documentary_credit_no']
        var me = this;
        $.each(fields,function (i,v) {
            me.frm.toggle_reqd(v,me.frm.doc.type=="LC")
        });
    },
    refresh: function(doc) {
        var me =this;
        if (doc.docstatus == 1) {
            this.frm.page.set_primary_action(__('Amend'), function() { me.frm.amend_doc(); });
        }
        // this.change_form_labels(this.get_company_currency())
        this.setConpanyAddress(doc)

    },

    lc_type:function(doc){
        frappe.model.validate_missing(doc,'lc_type')
        this.frm.set_df_property('master_lc','reqd',(doc.lc_type=='BBLC'?1:0))
        this.setConpanyAddress(doc)

    },
    master_lc: function(doc){

    },
    setConpanyAddress:function (doc) {
        if (doc.lc_type=="Master LC") {
            /*hide_field('beneficiary_address');
            this.frm.set_value('applicant','');
            this.frm.fields_dict.beneficiary.df.label = __("Beneficiary");
            unhide_field('applicant_address');
            this.frm.fields_dict.applicant.df.label = "";*/
            this.hideunhide('Applicant','Beneficiary')
            this.setAddress(doc,'beneficiary');
        }else if (doc.lc_type=="BBLC") {
            this.hideunhide('Beneficiary','Applicant')
            this.setAddress(doc,'applicant');
        }
    },
    hideunhide:function(field1,field2){
            hide_field(field2.toLowerCase()+'_address');
            this.frm.set_value(field1.toLowerCase(),'');
            this.frm.fields_dict[field2.toLowerCase()].df.label = __(field2);
            unhide_field(field1.toLowerCase()+'_address');
            this.frm.fields_dict[field1.toLowerCase()].df.label = "";
    },

    company:function (doc) {
        this._super
        this.setConpanyAddress(doc);
    },
    setAddress:function (doc,field_name) {
        frappe.model.validate_missing(doc,'company')

                console.log(this)
        var me = this;
        frappe.call({
            method:"frappe.client.get",
            args:{
                doctype:"Company",
                name:doc.company
            },
            callback:function (r) {
                me.frm.set_value(field_name,'<strong>'+r.message.name+'</strong><br>'+r.message.address)
            }
        })
    },




    sender_bank_address: function (doc) {
        this.set_address(doc,'sender_bank_address','sender_bank')
    },
    receiver_bank_address: function (doc) {
        this.set_address(doc,'receiver_bank_address','receiver_bank')
    },
    applicant_address: function (doc) {
        this.set_address(doc,'applicant_address','applicant','Address')
    },
    beneficiary_address: function (doc) {
        this.set_address(doc,'beneficiary_address','beneficiary','Address')
        
    },
    set_address: function (doc,source,target,doctype) {
        if (typeof doctype === 'undefined') {
            doctype = "Beneficiary Bank"
        }
        var me =this;
        frappe.call({
            method:"frappe.client.get",
            args:{
                doctype:doctype,
                name:doc[source]
            },
            callback:function (r) {
                if (r.message) {
                    var address = "";
                    address += r.message.address_line1 || "" + (r.message.address_line1?"<br />":"")
                    address += r.message.address_line2 || "" + (r.message.address_line1?"<br />":"")
                    address += r.message.city || "" + (r.message.address_line1?", ":"")
                    address += r.message.state || ""
                    me.frm.set_value(target,address)
                }
            }
        })
    },
    date_of_issue: function (doc) {
        if (doc.payment_in_days) {
            doc.payment_date = date.add_days(doc.date_of_issue,doc.payment_in_days)
            refresh_field('payment_date');
        }else if (doc.payment_date && doc.payment_date >= doc.date_of_issue) {
            doc.payment_in_days = date.get_diff(doc.payment_date,doc.date_of_issue);
            refresh_field('payment_in_days');
        }
    },
    payment_date: function (doc) {
        console.log(date.get_diff(doc.payment_date,doc.date_of_issue))
        frappe.model.validate_missing(doc,'date_of_issue')
        if (doc.payment_date && doc.payment_date >= doc.date_of_issue) {
            doc.payment_in_days = date.get_diff(doc.payment_date,doc.date_of_issue);
            refresh_field('payment_in_days');
        }
    },

    total_calculate: function(doc){
        var sum_qty = 0.0;
        // Check Current Row Duplicate Entry
        for(var key in doc.items){
            if(parseInt(doc.items[key].quantity)){
                sum_qty+=parseInt(doc.items[key].quantity);
            }
        }
        this.frm.set_value("total_item_quantity", sum_qty);

    },


    payment_in_days: function (doc) {
        frappe.model.validate_missing(doc,'date_of_issue')
        doc.payment_date = date.add_days(doc.date_of_issue,doc.payment_in_days)//date.get_diff(doc.date_of_issue,doc.payment_date);
        refresh_field('payment_date');
    },
    fields_of_company_currency:[
        'base_amount'
    ],
    fields_of_form_currency:[
        'amount'
    ],
}),




cur_frm.cscript = new garments.commercial.LcController({frm:cur_frm})


frappe.ui.form.on("LC or SC Item Child",{
    item:function (frm,doctype,docname) {
        // cur_frm.cscript.total_calculation_undertaking(cur_frm.doc)
        var local = locals[doctype][docname]
        frappe.call({
            method:"frappe.client.get",
            args:{
                doctype:"Item",
                filters:{
                    'item_name':local.item
                }
            },
            callback:function (data) {
                if (data.message) {
                    local.uom = data.message.stock_uom
                    local.description = data.message.description
                }else{
                    local.uom = "";
                    local.description = "";
                }
                refresh_field('uom',local.name,'items');
                refresh_field('description',local.name,'items');
            }
        })
    }
});

frappe.ui.form.on("LC or SC Item Child",{
    quantity:function (frm,doctype,docname) {
        cur_frm.cscript.total_calculate(cur_frm.doc)
    },
    items_remove:function (frm) {
        cur_frm.cscript.total_calculate(cur_frm.doc)
    }
});


// refresh_field('uom',local.name,'items')