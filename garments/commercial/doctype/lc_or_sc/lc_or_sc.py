# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname
from frappe.model.mapper import get_mapped_doc

class LCorSC(Document):
	def autoname(self):
		if self.type=="SC":
			self.name = make_autoname("SC-.YYYY.-.#####")
		else:
			self.name = self.documentary_credit_no

@frappe.whitelist()
def get_data_for_lc(source_name, target_doc=None):
	doc = get_mapped_doc("LC or SC", source_name,	{
		"LC or SC": {
			"doctype": "Undertaking",
			"field_map": [
				["name", "lc"],
				["date_of_issue", "lc_date"],
				["amount", "lc_value"],
				["applicant_address", "buyer_name"],
				["receiver_bank_address", "exporters_bank"]
			]
		}
	}, target_doc)

	return doc