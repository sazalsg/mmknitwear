# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.model.naming import make_autoname

class Job(Document):
		
	def autoname(self):
		self.name =  make_autoname('{0}#{1}-'.format("JOB", str(date.today().year) ) + ".####")
