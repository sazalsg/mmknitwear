# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PackingList(Document):
	pass


@frappe.whitelist()
def po_filters(doctype, txt, searchfield, filters, page_len=0, page_limit=20):
	return frappe.get_list(doctype,filters,searchfield,page_len,page_limit)