frappe.provide('garments.commercial')
garments.commercial.PackingListController = frappe.ui.form.Controller.extend({
	onload:function (doc) {
		// export_document Filters
		cur_frm.add_fetch("lc","job","job")	
		this.frm.set_query('invoice',function () {
			return {
				filters:{
					'lc':doc.lc
				}
			}
		})
		
		// LC Filter
		this.frm.set_query('lc',function () {
			frappe.model.validate_missing(doc, "job");
			return {
				filters:{
					'job':doc.job
				}
			}
		})
		// PO Filters
		this.frm.set_query("po", "items", function() {
			return {
				searchfield:"po_num"
			}
		});

	},
	totals:function (doc,source,target) {
		var tab = doc.items;
		var total = 0;
		for (var i = 0; i < tab.length; i++) {
			total += tab[i][source]
		};
		this.frm.set_value(target,total)
		refresh_field(target)
		this.frm.form_wrapper.find('.empty-section').removeClass('empty-section').addClass('visible-section '+(this.frm.form_wrapper.find('.empty-section').index()%2!=0?'shaded-section':''))
	},
	total_in_items:function (doc,doctype,docname) {
		var local = locals[doctype][docname]
		local.total_nt_wt = (local.nt_wt_ctn || 0) * (local.ctns)
		local.total_gr_wt = (local.gt_wt_ctn || 0) * (local.ctns)
		refresh_field('total_nt_wt',local.name,'items')
		refresh_field('total_gr_wt',local.name,'items')
	},
	nt_wt_ctn:function (doc,cdt,cdn) {
		this.total_in_items(doc,cdt,cdn)
		this.totals(doc,'total_nt_wt','total_nt_wt')
	},
	gt_wt_ctn:function (doc,cdt,cdn) {
		this.total_in_items(doc,cdt,cdn)
		this.totals(doc,'total_gr_wt','total_gtwt')
	},
	qty_sets:function (doc) {
		this.totals(doc,'qty_sets','total_set')
	},
	pcs:function (doc) {
		this.totals(doc,'pcs','total_pcs')
	},
	ctns:function (doc,cdt,cdn) {
		this.nt_wt_ctn(doc,cdt,cdn)
		this.gt_wt_ctn(doc,cdt,cdn)
	},
	cbm_per_item:function (doc) {
		this.totals(doc,'cbm_per_item','cbm_total')
	}
})

cur_frm.cscript.job = function (doc) {
	console.log("dasd")
	/*doc.lc = "";
	// cur_frm.script_manager.trigger('test_cutting_order')
	refresh_field('lc')*/
}

cur_frm.cscript = new garments.commercial.PackingListController({frm:cur_frm})

cur_frm.cscript.job = function (doc) {
	doc.lc = "";
	cur_frm.script_manager.trigger('lc')
	refresh_field('lc')
}