cur_frm.cscript.frm.add_fetch("invoice", "job", "job");
cur_frm.cscript.frm.add_fetch("invoice", "lc", "lc_no");
cur_frm.cscript.frm.add_fetch("invoice", "lc_date", "lc_date");
cur_frm.cscript.frm.add_fetch("invoice", "lc_issue_bank", "to");
cur_frm.cscript.frm.add_fetch("invoice", "account_and_risk_of_measure", "ac");
cur_frm.cscript.frm.add_fetch("invoice", "total_amount", "for");
cur_frm.cscript.frm.add_fetch("invoice", "company", "company");

frappe.provide('garments.commercial');

{% include "garments/public/js/controllers/multicurrency_controller.js" %}

garments.commercial.BillOfExchangeController = garments.MultiCurrencyController.extend({

    
    refresh:function (doc) {
        this.change_form_labels(this.get_company_currency())
    },
    fields_of_company_currency:[
        'base_for'
    ],
    fields_of_form_currency:[
        'for'
    ],
    currency:function (doc) {
        this._super();
        this.for(doc);
    },
    for: function (doc) {
        if (!doc.for) {
            return
        }
        var me = this;
        me.frm.call({
            method: "garments.commercial.doctype.commercial_invoice.commercial_invoice.set_total_amount",
            args: {
                amt: doc.for,
                currency: doc.currency || frappe.defaults.get_default('currency')
            },
            callback: function (r) {
                if (r.message) {
                    me.frm.set_value('in_words', r.message)
                }
            }
        })
    }
});


cur_frm.cscript = new garments.commercial.BillOfExchangeController({frm:cur_frm});



frappe.ui.form.on('Bill of Exchange', {

    lc_no:function (frm) {
        frappe.call({
            "method": "frappe.client.get",
            args: {
                doctype: "LC or SC",
                name: frm.doc.lc_no
            },
            callback: function (data)
            {
                console.log(data)
                 frm.set_value('supplier_bank', data.message.receiver_bank_address)

            }
        }) 
    }

   
});

