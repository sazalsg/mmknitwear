// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.provide('garments.commercial')
{% include "garments/public/js/controllers/multicurrency_controller.js" %};

frappe.ui.form.on('Undertaking', {
	refresh: function(frm) {
		if(frm.doc.docstatus==0) {
			cur_frm.add_custom_button(__(frm.fields_dict.lc.df.options),
				function() {
					// frm.set_value('items',[])
					erpnext.utils.map_current_doc({
						method: "garments.commercial.doctype.lc_or_sc.lc_or_sc.get_data_for_lc",
						source_doctype: frm.fields_dict.lc.df.options,
						get_query_filters: {
							lc_type:'Master LC',
							docstatus: 1
						}
					})
				}, __("Get items from"));	
		}
	}
});

garments.commercial.UndertakingController = garments.MultiCurrencyController.extend({
	fields_of_company_currency:[
        'base_lc_value','base_total_value','base_total_invoice'
    ],
    fields_of_form_currency:[
        'lc_value','total_value','total_invoice'
    ],
    grid_from_base_fields: function () {
        return [
            {
                "base_label_change_fields":['base_bblc_value','base_usable_value'],
                "base_fields":['base_bblc_value','base_usable_value'],
                "label_change_fields":['bblc_value','usable_value'],
                "fields":['bblc_value','usable_value'],
                "table_field_name":"yarn_description"
            }
        ]
    },

	//Total Yarn and Total Value Calculation in Undertaking Child table (yarn_description)
	total_calculate:function (doc) {
		console.log("Paichi")
		var sum_qty = 0.0;
		var sum_val = 0.0;
	    // Check Current Row Duplicate Entry
	    for(var key in doc.yarn_description){
			if(parseInt(doc.yarn_description[key].used_qty)){
				sum_qty+=flt(doc.yarn_description[key].used_qty);
				sum_val+=flt(doc.yarn_description[key].usable_value);
			}
		}
	    this.frm.set_value("total_yarn_qty", sum_qty); 
	    this.frm.set_value("total_value", sum_val); 

	},

	// // Invoice Value and quantity Calculation in Undertaking Child table (undertaking_child)
	total_calculation_undertaking:function(doc){
		var sum_qty = 0;
		var sum_val = 0.0;
	    // Check Current Row Duplicate Entry
	    for(var key in doc.undertaking_child){
			if(parseInt(doc.undertaking_child[key].quantity)){
				sum_qty+=parseInt(doc.undertaking_child[key].quantity);
				sum_val+=flt(doc.undertaking_child[key].invoice_value);
			}
		}
	    this.frm.set_value("total_quentity", sum_qty); 
	    this.frm.set_value("total_invoice", sum_val); 
	},

	lc:function (doc) {
		if (!doc.lc) {
			var me = this;
			$.each(["lc_date","lc_value","company","applicant"],function (k,v) {
				me.frm.set_value(v,"");
			})
		}
	}
});

frappe.ui.form.on("Incentive Child Description",{
	bblc:function (frm,doctype,docname) {
		var local = locals[doctype][docname]
		frappe.call({
			method:"frappe.client.get",
			args:{
				doctype:"BBLC Yarn",
				name:local.bblc
			},
			callback:function (data) {
				if (data.message) {
					local.bblc_value = data.message.bblc_amount
					local.usable_value = data.message.usable_value
				}else{
					local.bblc_value = ""
					local.usable_value = ""
				}
				refresh_field('bblc_value',local.name,'yarn_description')
				refresh_field('usable_value',local.name,'yarn_description')
			}
		})
		cur_frm.cscript.total_calculate(cur_frm.doc)
	},
	usable_value:function (frm, doctype,docname) {
		var local = locals[doctype][docname]
		this._set_child_base_field_value(['usable_value'],local);
	},
	bblc_value:function (frm, doctype,docname) {
		var local = locals[doctype][docname]
		this._set_child_base_field_value(['bblc_value'],local);
	},
	yarn_description_remove:function (frm) {
		cur_frm.cscript.total_calculate(cur_frm.doc)
	}
});

frappe.ui.form.on("Undertaking Child",{
	invoice_no:function (frm,doctype,docname) {
		cur_frm.cscript.total_calculation_undertaking(cur_frm.doc)
		var local = locals[doctype][docname]
		jQuery.ajax({
			url: '/api/resource/Bill of Lading/?fields=["name","date"]&filters=[["Bill of Lading","invoice","=","'+local.invoice_no+'"]]',
			type: 'GET',
			// data: {},
		})
		.done(function(r) {
			console.log(r.data[0].name)
			if (r.data) {
				local.bl_no = r.data[0].name
				local.bl_date = r.data[0].date
			}else{
				local.bl_no = ""
				local.bl_date = ""
			}
			refresh_field('bl_no',local.name,'undertaking_child')
			refresh_field('bl_date',local.name,'undertaking_child')
		})
		.fail(function() {
		})
		.always(function() {
		});
		
	},
	undertaking_child_remove:function (frm) {
		cur_frm.cscript.total_calculation_undertaking(cur_frm.doc)
	}
});

cur_frm.cscript = new garments.commercial.UndertakingController({frm:cur_frm});
cur_frm.add_fetch('lc','currency','currency');