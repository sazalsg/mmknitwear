frappe.provide('garments.commercial');

{% include "garments/public/js/controllers/multicurrency_controller.js" %}
garments.commercial.CommercialInvoiceController = frappe.ui.form.Controller.extend({
    onload: function (doc) {
        // LC Filter
        this.frm.set_query('lc', function () {
            frappe.model.validate_missing(doc, "job");
            return {
                filters: {
                    'job': doc.job
                }
            }
        });
        // PO Filters
        this.frm.set_query("po", "items", function () {
            return {
                searchfield: "po_num"
            }
        });

        this.frm.add_fetch('lc','company','company');
        this.frm.add_fetch('lc','currency','currency');

    },
    refresh:function () {
        // this.set_dynamic_labels();
    },
   
    totals: function (doc, source, target) {
        var tab = doc.items;
        var total = 0;
        for (var i = 0; i < tab.length; i++) {
            total += tab[i][source]
        }
        this.frm.set_value(target, total);
        refresh_field(target);
        var idx = this.frm.form_wrapper.find('.empty-section').index()
        if (idx >= 0) {
            this.frm.form_wrapper.find('.empty-section').removeClass('empty-section')
                .addClass('visible-section ' + (idx % 2 != 0 ? 'shaded-section' : ''))
        }
    },
    total_in_items: function (doc, doctype, docname) {
        var local = locals[doctype][docname];
        local.net_amount = (local.unit_price || 0) * (local.qty_sets || 0)
        refresh_field('net_amount', local.name, 'items')
    },
    unit_price: function (doc, cdt, cdn) {
        this.total_in_items(doc, cdt, cdn);
        this.totals(doc, 'net_amount', 'total_amount')
    },
    qty_sets: function (doc, cdt, cdn) {
        this.total_in_items(doc, cdt, cdn);
        this.totals(doc, 'qty_sets', 'total_set')
    },
    pcs: function (doc) {
        this.totals(doc, 'pcs', 'total_pcs')
    },
    ctns: function (doc) {
        this.totals(doc, 'ctns', 'total_ctns')
    },
    total_amount: function (doc) {
        var me = this;
        me.frm.call({
            method: "set_total_amount",
            args: {
                amt: doc.total_amount,
                currency: doc.currency || frappe.defaults.get_default('currency')
            },
            callback: function (r) {
                if (r.message) {
                    me.frm.set_value('in_words', r.message)
                }
            }
        })
    },
   /* grid_from_base_fields: function () {
        return [
            {
                "base_label_change_fields":['base_unit_price','base_net_amount'],
                "base_fields":['base_unit_price','base_net_amount'],
                "label_change_fields":['unit_price','net_amount'],
                "fields":['unit_price','net_amount'],
                "table_field_name":"items"
            }
        ]
    },*/
    fields_of_company_currency:[
        'base_total_amount'
    ],
    fields_of_form_currency:[
        'total_amount'
    ]
});
cur_frm.cscript = new garments.commercial.CommercialInvoiceController({frm:cur_frm});

cur_frm.cscript.job = function (doc) {
	doc.lc = "";
	cur_frm.script_manager.trigger('lc');
	refresh_field('lc')
};


// frappe.ui.form.on(cur_frm.doctype,'items_add', function(frm) { //cur_frm.fields_dict.items.grid.doctype
//     console.log("sdf");
// 	frm.cscript.set_dynamic_labels();
// });



frappe.ui.form.on('Commercial Invoice', {
     port_of_loading:function (frm) {


         frappe.call({
            "method": "frappe.client.get",
            args: {
                doctype: "Port Location",
                name: frm.doc.port_of_loading
            },
            callback: function (data)
            {
                
                frm.set_value('from', data.message.country)

            }
        });

    },
     final_destination:function (frm) {


         frappe.call({
            "method": "frappe.client.get",
            args: {
                doctype: "Port Location",
                name: frm.doc.final_destination
            },
            callback: function (data)
            {
                
                frm.set_value('to', data.message.country)

            }
        });

    }
   
});