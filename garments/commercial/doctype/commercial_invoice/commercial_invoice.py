# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import money_in_words
from datetime import date
from frappe.model.naming import make_autoname
from frappe.model.mapper import get_mapped_doc

class CommercialInvoice(Document):
	def onload (self):
		if not self.invoice_no:
			self.invoice_no = self.name


	def autoname(self):
		if not self.invoice_no:
			company = frappe.get_doc("Company",self.company)
			countData = frappe.db.sql("SELECT COUNT(name) FROM `tab{}` WHERE YEAR(creation) = '{}'".format(self.doctype,date.today().year))
			countData = str(countData[0][0]+1).zfill(2)
			name = str(company.abbr)+'/Exp. '+str("{}".format(countData))+"/"+str(date.today().year)
			self.name = name
		else:
			self.name = self.invoice_no


@frappe.whitelist()
def set_total_amount(amt, currency):
	return money_in_words(amt, currency)



@frappe.whitelist()
def get_data_for_bill_of_lading(source_name, target_doc=None):
	doc = get_mapped_doc("Commercial Invoice", source_name,	{
		"Commercial Invoice": {
			"doctype": "Bill of Lading"
		},
		"Commercial Invoice Child": {
			"doctype": "Commercial Invoice Child"
		}
	}, target_doc)

	return doc


@frappe.whitelist()
def get_data_for_certificate_of_origin(source_name, target_doc=None):
	def update_item(obj, target, source_parent):
		bl = frappe.db.get_values("Bill of Lading",{"name":("!=","")},['name','date'])
		if bl:
			target.bl_no = bl[0][0]
			target.bl_date = bl[0][1]

	doc = get_mapped_doc("Commercial Invoice", source_name,	{
		"Commercial Invoice": {
			"doctype": "Certificate of Origin",
			"postprocess": update_item
		},
		"Commercial Invoice Child": {
			"doctype": "Commercial Invoice Child"
		}
	}, target_doc)

	return doc

@frappe.whitelist()
def get_items_for_bill_of_entry(source_name, target_doc=None):
	doc = get_mapped_doc("Commercial Invoice", source_name,	{
		"Commercial Invoice": {
			"doctype": "Bill of Entry or Export"
		},
		"Commercial Invoice Child": {
			"doctype": "Commercial Invoice Child"
		}
	}, target_doc)

	return doc

@frappe.whitelist()
def get_data_for_commercial_invoice(source_name, target_doc=None):
	doc = get_mapped_doc("Commercial Invoice", source_name,	{
		"Commercial Invoice": {
			"doctype": "Realization",
			"field_map": [
				["name", "invoice_no"],
				["total_pcs", "quantity"],
				["total_amount", "invoice_value"],
				["exp_date", "date"]
			]
		}
	}, target_doc)

	return doc