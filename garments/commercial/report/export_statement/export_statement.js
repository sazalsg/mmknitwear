// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Export Statement"] = {
	"filters": [
		{
			fieldname:"job",
			fieldtype:"Link",
			label:"JOB",
			options:"Job"
		},
		{
			fieldname:"from_date",
			label: __("From Date"),
			fieldtype: "Date",
			"default": frappe.defaults.get_user_default("year_start_date")
		},
		{
			fieldname:"to_date",
			label: __("To Date"),
			fieldtype: "Date",
			"default": frappe.defaults.get_user_default("year_end_date")
		}
	],
	/*"formatter": function (row, cell, value, columnDef, dataContext, default_formatter) {
        value = default_formatter(row, cell, value, columnDef, dataContext);
        var $value = $(value).css("font-weight", "bold");
        value = $value.wrap("<p></p>").parent().html();
		return value
	},*/
	"tree": true,
	"name_field": "lc",
	"parent_field": "master_lc",
	"initial_depth": 2
}
