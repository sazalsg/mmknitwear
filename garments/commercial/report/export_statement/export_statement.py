# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import cint, flt, getdate, formatdate, cstr


def execute(filters=None):
    columns, data = [], []
    return get_columns(filters), get_data(filters)


def get_data(filters):
    newFilters = filters
    if filters.get('from_date'):
        newFilters['date'] = ('>=', filters['from_date'])
        del newFilters['from_date']

    if filters.get('to_date'):
        newFilters['date'] = ('<=', filters['to_date'])
        del newFilters['to_date']
    exps = frappe.db.get_values('Exp Form',newFilters,['name as exp_no','job','invoice','invoice_value','quantity'],as_dict=1)


    return exps




def get_columns(filters):
    col = [
        {
            "fieldname": "exp_no",
            "label": _("Exp No"),
            "fieldtype": "Link",
            "options": "Exp Form",
            "width": 230
        },
        {
            "fieldname": "invoice",
            "label": _("Invoice No"),
            "fieldtype": "Link",
            "options": "Commercial Invoice",
            "width": 120
        },
        {
            "fieldname": "invoice_value",
            "label": _("Invoice Value"),
            "fieldtype": "Currency",
            "width": 120
        },
        {
            "fieldname": "quantity",
            "label": _("Exp Qty"),
            "fieldtype": "Int",
            "width": 120
        },
        {
            "fieldname": "for_bblc",
            "label": _("For BBL/C"),
            "fieldtype": "Currency",
            "width": 150
        },
        {
            "fieldname": "fbc_fbps",
            "label": _("FBC/FBPS"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "date",
            "label": _("DATE"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "r_value",
            "label": _("Realized Value"),
            "fieldtype": "Currency",
            "width": 120
        },
        {
            "fieldname": "r_date",
            "label": _("Realized Date"),
            "fieldtype": "Date",
            "width": 120
        }
    ]

    if not filters.get('job'):
        col.insert(1, {
            "fieldname": "job",
            "label": "Job",
            "fieldtype": "Link",
            "options": "Job",
            "width": 130
        })

    return col
