# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	#columns, data = [], []
	#return columns, data
	return get_columns(filters), get_data(filters)



def get_columns(filters):
    col = [
        {
            "fieldname": "name",
            "label": _("LC No"),
            "fieldtype": "Link",
            "options": "LC or SC",
            "width": 230
        },
        {
            "fieldname": "date_of_issue",
            "label": _("LC Date"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "applicant_address",
            "label": _("Supplier"),
            "fieldtype": "Link",
            "options": "Job",
            "width": 120
        },
        {
            "fieldname": "item",
            "label": _("Item"),
            "fieldtype": "Link",
            "options": "Item",
            "width": 120
        },
        {
            "fieldname": "amount",
            "label": _("Value"),
            "fieldtype": "Currency",
            "width": 120
        },
        {
            "fieldname": "currency",
            "label": _("Currency"),
            "fieldtype": "Link",
            "options": "Currency",
            "width": 150
        },
        {
            "fieldname": "ltr_no",
            "label": _("LTR No"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "ltr_dt",
            "label": _("LTR Date"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "ltr_am_bdt",
            "label": _("LTR AM BDT"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "adjust_am_bdt",
            "label": _("Adjust AM.BDT"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "adjust_dt",
            "label": _("ADJ DT"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "balance_am",
            "label": _("BALANCE AM"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "remark",
            "label": _("Remark"),
            "fieldtype": "Data",
            "width": 120
        }
    
    ]
    return col


def get_data(filters):
    filters['is_cash'] = 1
    #frappe.msgprint(filters)
    lccashs = frappe.db.get_values("LC or SC", filters, "*", as_dict=1)
    rows = []

    for row in lccashs:
        items = frappe.db.get_values("LC or SC Item Child",{'parent':row.name},'item')
        tmp = []
        for item in items:
            tmp.append('<a href="/desk#Form/Item/'+item[0]+'">'+item[0]+"</a>")

        row['item'] = ','.join(tmp)

        rows.append(row)
        
    return rows
























def columns(filters):
	return [
		_("LC No")+":Link/LC or SC:100",
		_("LC Date")+":Date:120",
		_("Supplier")+":Link/Supplier:100",
		_("Item")+":Link/Item:100",
		_("Value")+":Currency:150",
		_("Currency")+":Data:120",
		_("LTR No")+":Data:120",
		_("LTR DT")+":Date:100",
		_("LTR AM.BDT")+":Currency:120",
		_("Adjust AM.BDT")+":Currency:150",
		_("ADJ DT")+":Date:150",
		_("BALANCE AM")+":Data:100",
		_("Remarks")+":Data:100",
	]


def data(filters):
	if len(filters) <= 0:
		filters['name']=("!=","")
	fields = ['name','date_of_issue','applicant_address','"" as item','amount','currency',
	'"" as ltr_no','"" as ltr_dt','"" as ltr_am_bdt','"" as adjust_am_bdt',
	'"" as adjust_dt','"" as balance_am','"" as remark'
	]
	return frappe.db.get_values("LC or SC",filters,fields)