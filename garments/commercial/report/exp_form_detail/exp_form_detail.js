// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Exp Form detail"] = {
	"filters": [
		{
			fieldname:"name",
			fieldtype:"Link",
			label:"Exp Form",
			options:"Exp Form"
		},
		{
			fieldname:"company",
			fieldtype:"Link",
			label:"Company",
			options:"Company"
		},
		{
			fieldname:"lc_no",
			fieldtype:"Link",
			label:"LC/SC No.",
			options:"LC or SC"
		},
		{
			fieldname:"Date",
			fieldtype:"Date",
			label:"Date",
			
		},

	]
}
