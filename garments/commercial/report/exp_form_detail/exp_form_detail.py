# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _
#import frappe

def execute(filters=None):
	# columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Exp No")+":Link/Exp Form:100",
		_("Company")+":Link/Company:100",
		_("LC/SC")+":Link/LC or SC:120",
		_("Date")+":Date:180",
		_("Total Amount on LC/SC")+":currency:180",
	]

def data(filters):
	if len(filters) <= 0:
		filters['name']=("!=","")
	fields = ['name','company','lc','date','total_amount']
	return frappe.db.get_values("Exp Form",filters,fields)



