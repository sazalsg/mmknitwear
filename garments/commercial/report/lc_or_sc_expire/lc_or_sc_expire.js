// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["LC or SC Expire"] = {
	"filters": [
		{
			fieldname:"name",
			fieldtype:"Link",
			label:"LC or SC",
			options:"LC or SC"
		},
		{
			fieldname:"applicant_address",
			fieldtype:"Link",
			label:"Applicant",
			options:"Address"
		},
		{
			fieldname:"beneficiary_address",
			fieldtype:"Link",
			label:"Beneficiary",
			options:"Address"
		},
		{
			fieldname:"days_range",
			label: __("Expire"),
			fieldtype: "Select",
			options: "\nToday\nThis Week\nThis Month\nThis Year",
			default: "Today"
		},
		{
			fieldname:"from_date",
			label: __("From Date"),
			fieldtype: "Date",
			// reqd: 1,
			// default: frappe.defaults.get_user_default("year_start_date"),
			
			
		},
		{
			fieldname:"to_date",
			label: __("To Date"),
			fieldtype: "Date",
			// reqd: 1,
			// default: frappe.datetime.month_end()
			// "default": frappe.defaults.get_user_default("year_end_date")
		}
	]
};


setTimeout(function () {
	var field = frappe.query_report.filters_by_name.days_range
	field.$input.on('change',function () {
		if (field.get_value()) {
			frappe.query_report.filters_by_name.from_date.$wrapper.hide()
			frappe.query_report.filters_by_name.from_date.set_input(null)
			frappe.query_report.filters_by_name.to_date.$wrapper.hide()
			frappe.query_report.filters_by_name.to_date.set_input(null)
		}else {
			frappe.query_report.filters_by_name.from_date.$wrapper.show()
			frappe.query_report.filters_by_name.to_date.$wrapper.show()
		}
	})
	field.$input.change()
},100)