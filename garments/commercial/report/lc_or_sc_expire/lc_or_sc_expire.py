# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from garments.commercial.report.lc_or_sc.lc_or_sc import columns, data
from frappe.utils.data import formatdate

def execute(filters=None):
	rows =[]
	col = columns(filters)
	col.insert(9,_("Expire Date")+":Data:100")
	for row in data(filters):
		row = list(row)
		expire_date = frappe.get_doc("LC or SC",row[0]).date
		if len(filters) >0:
			expire_date = '<span class="lc_expire_date_field_report">'+formatdate(expire_date)+'</span>'
		else:
			expire_date = formatdate(expire_date)
		row.insert(9,expire_date)
		rows.append(row)
	return col, rows
