# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from garments.commercial.report.local_yarn.local_yarn import columns, get_data

def execute(filters=None):
	return columns(filters),get_data(filters,1)