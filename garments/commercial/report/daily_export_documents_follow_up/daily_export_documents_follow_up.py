# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _
from frappe.utils.data import flt


def execute(filters=None):
    # columns, data = [], []
    # return columns, data
    return get_columns(filters), data(filters)  # [] #data_dummy(filters)


def columns(filters):
    return [
        _("Buyer") + ":Link/Address:100",
        _("Job No") + ":Link/Job:100",
        _("Invoice No") + ":Link/Commercial Invoice:100",
        # _("Invoice Date")+":Date:120",
        _("Quantity") + ":Data:100",
        _("Value") + ":Data:100",
        _("EX FAC") + ":Link/LC or SC:100",
        _("FDBO No") + ":Link/:180",
        _("FDBO Date") + ":Link/:100",
        _("N/Rate") + ":Currency:120",
        _("RCV PAY") + ":Currency:150",
        _("Date") + ":Date:150",
        _("Short Rea") + ":Data:100",
        _("FC Held") + ":Currency:100",
        _("LTR TK") + ":Currency:100",
        _("F/C A/C") + ":Data:120",
        _("STD A/C") + ":Data:150",
        _("CD A/C TK") + ":Currency:150",
        _("CURIER") + ":Data:100",
        _("Comm") + ":Currency:100",
        _("Tax") + ":Data:120",
        _("LC no") + ":Link/Commercial Invoice:100",
        _("Date") + ":Date:150",
        _("Remarks") + ":Data:100"

    ]


def get_columns(filters):
    col = [
        {
            "fieldname": "to",
            "label": _("Buyer"),
            "fieldtype": "Link",
            "options": "Address",
            "width": 230
        },
        {
            "fieldname": "job",
            "label": _("Job No"),
            "fieldtype": "Link",
            "options": "Job",
            "width": 120
        },
        {
            "fieldname": "invoice",
            "label": _("Invoice No"),
            "fieldtype": "Link",
            "options": "Commercial Invoice",
            "width": 120
        },
        {
            "fieldname": "total_pcs",
            "label": _("Quantity"),
            "fieldtype": "Int",
            "width": 120
        },
        {
            "fieldname": "invoice_value",
            "label": _("Value"),
            "fieldtype": "Currency",
            "width": 150
        },
        {
            "fieldname": "ex_fac",
            "label": _("EX FAC"),
            "fieldtype": "Data",
            "width": 120
        }

        ,
        {
            "fieldname": "fdbp_no",
            "label": _("FDBP/FBC/FBPS"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "fdbp_date",
            "label": _("FDBP/FBC/FBPS Date"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "realization_rate",
            "label": _("N/Rate"),
            "fieldtype": "Currency",
            "width": 120
        },
        {
            "fieldname": "realization_value",
            "label": _("RCV PAY"),
            "fieldtype": "Currency",
            "width": 150
        },
        {
            "fieldname": "realization_date",
            "label": _("Date"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "realization_short",
            "label": _("Short Realization"),
            "fieldtype": "Currency",
            "width": 120
        }
    ]

    deduction_types = frappe.db.get_values("Realization Deduction Type", {'name': ('!=', '')}, as_dict=1)
    for deduction_type in deduction_types:
        col.append({
            "fieldname": deduction_type.name.lower().replace(' ', '_'),
            "label": _(deduction_type.name),
            "fieldtype": "Currency",
            "options": "currency",
            "width": 120
        })


    col += [
        {
            "fieldname": "lc",
            "label": _("LC No"),
            "fieldtype": "Link",
            "options":"LC or SC",
            "width": 120
        },
        {
            "fieldname": "lc_date",
            "label": _("LC Date"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "remark",
            "label": _("Remark"),
            "fieldtype": "Date",
            "width": 120
        }
    ]
    return col

def data(filters):
    if len(filters) <= 0:
        filters['name'] = ('!=', '')
    exportDocuments = frappe.db.get_values("Export Document", filters, "*", as_dict=1)


    rows = []


    for exportDocument in exportDocuments:
        lc = frappe.get_doc("LC or SC", exportDocument.master_lc)
        invoice = frappe.get_doc("Commercial Invoice", exportDocument.invoice)
        realization = frappe.db.get_values("Realization", {'invoice_no': invoice.name},"*")
        row = {
            "to":lc.applicant_address,
            "job":exportDocument.job,
            "invoice":exportDocument.invoice,
            "total_pcs":invoice.total_pcs,
            "invoice_value":invoice.total_amount,
            "ex_fac":""
        }
        if realization:
            realization = frappe.get_doc("Realization",realization[0].name)
            row.update({
                "fdbp_no": realization.reference_no,
                "fdbp_date":realization.submitted_date,
                "realization_rate":realization.conversion_rate,
                "realization_value":realization.realized_amount,
                "realization_date":realization.realization_date,
                "realization_short": flt(realization.invoice_value) - flt(realization.realized_amount)
            })
            for realize_deduction in realization.deduction:
                row[realize_deduction.deduction_type.lower().replace(' ', '_')] = realize_deduction.amount

        row.update({
            "lc":exportDocument.master_lc,
            "lc_date":lc.date_of_issue,
            "remark":""
        })

        rows.append(row)

    return rows

