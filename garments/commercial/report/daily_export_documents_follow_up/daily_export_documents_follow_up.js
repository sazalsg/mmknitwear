// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Daily Export Documents Follow Up"] = {
	"filters": [

		{
			fieldname:"name",
			fieldtype:"Link",
			label:__("Export Document"),
			options:"Export Document"
		},
		{
			fieldname:"invoice",
			fieldtype:"Link",
			label:__("Invoice No."),
			options:"Commercial Invoice"
		},

		{
			fieldname:"master_lc",
			fieldtype:"Link",
			label:__("LC/SC No."),
			options:"LC or SC"
		},
		{
			fieldname:"date",
			fieldtype:"Date",
			label:__("Date")
		}


	]
}
