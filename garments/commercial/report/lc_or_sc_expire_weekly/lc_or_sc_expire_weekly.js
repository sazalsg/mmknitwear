// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt
frappe.provide('garments');

garments.getWeekStartDay = function (d) {
  d = new Date(d);
  var getD = d.getDate();
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}



garments.getWeekEndDay = function (d) {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1) + 6; // adjust when day is sunday
  return new Date(d.setDate(diff));
}




frappe.query_reports["LC or SC Expire Weekly"] = {
	"filters": [
		{
			fieldname:"from_date",
			label: __("From Date"),
			fieldtype: "Date",
			reqd: 1,
			//default: frappe.datetime.year_start()
			default: frappe.datetime.obj_to_str(garments.getWeekStartDay(frappe.datetime.nowdate()))
		},	
		{
			fieldname:"to_date",
			label: __("To Date"),
			fieldtype: "Date",
			reqd: 1,
			// default: frappe.datetime.year_end()
			default: frappe.datetime.obj_to_str(garments.getWeekEndDay(frappe.datetime.nowdate()))
		}
	]
}
