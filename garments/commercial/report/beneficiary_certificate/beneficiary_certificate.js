// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Beneficiary Certificate"] = {
	"filters": [

		{
			fieldname:"name",
			fieldtype:"Link",
			label:"Beneficiary Certificate",
			options:"Beneficiary Certificate"
		},
		
		{
			fieldname:"Date",
			fieldtype:"Date",
			label:"Date",
			
		},

	]
}
