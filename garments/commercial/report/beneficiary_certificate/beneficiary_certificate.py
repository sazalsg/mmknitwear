# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _
#import frappe

def execute(filters=None):
	# columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Name")+":Link/Beneficiary Certificate:100",
		_("JOB")+":Data:100",
		_("Invoice")+":data:120",
		_("Date")+":Date:180",
		
		
	]

def data(filters):
	if len(filters) <= 0:
		filters['name']=("!=","")
	fields = ['name','job','invoice','date']
	return frappe.db.get_values("Beneficiary Certificate",filters,fields)
