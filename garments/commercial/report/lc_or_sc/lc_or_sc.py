# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _
from frappe.utils.data import nowdate, get_last_day, get_first_day, add_days

def execute(filters=None):
	# columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Name")+":Link/LC or SC:100",
		_("Type")+":Data:100",
		_("Company")+":Link/Company:120",
		_("LC Type")+":Data:180",
		_("Sender Code")+":Data:100",
		_("Sender Bank")+":Link/Address:120",
		_("Receiver Code")+":Data:100",
		_("Receiver Bank")+":Link/Address:120",
		_("Issue Date")+":Date:150",
		_("Payment Date")+":Date:150",
		_("Amount")+":Currency:100",
		_("Port of Loading")+":Link/Port Location:100",
		_("Port of Discharge")+":Link/Port Location:100",
		_("Port of Final Destination")+":Link/Port Location:100",
		
	]

def data(filters):
	newFilters = []
	if len(filters) <= 0:
		newFilters.append(["name","!=",""])
	if filters.get('month'):
		newFilters.append(["date", ">=",filters.from_date])

	if filters.get('from_date'):
		newFilters.append(["date", ">=",filters.from_date])
			
	if filters.get('to_date'):
		newFilters.append(["date", "<=", filters.to_date])
	for key,val in filters.iteritems():
		if key not in ['from_date','to_date']:
			if key != 'days_range':
				newFilters.append([key,'=',val])

	if filters.get('from_date') and filters.get('to_date') and filters.get('from_date') > filters.get('to_date'):
		frappe.throw("Fromdate  should be less than Todate");

	if filters.get('days_range'):
		if filters['days_range'] == 'Today':
			newFilters.append(["date", "=",nowdate()])
		elif filters['days_range'] == 'This Week':
			newFilters.append(["date", ">=",add_days(nowdate(),6-datetime.datetime.today().weekday())])
		elif filters['days_range'] == 'This Month':
			newFilters.append(["date", ">=",get_first_day(nowdate())])
		elif filters['days_range'] == 'This Year':
			newFilters.append(["date", ">=",datetime.date(datetime.date.today().year,1,1)])

	# frappe.throw(newFilters)
	fields = ['name','type','company','lc_type','sender_code','sender_bank_address','receiver_code','receiver_bank_address','date_of_issue','payment_date','amount','port_of_loading','port_of_discharge','port_of_final_destination']
	return frappe.get_all("LC or SC",filters=newFilters,fields=fields, as_list=1)

def revers_format(date):
	ndate = date.split('-')
	return "{}-{}-{}".format(ndate[0],ndate[1],ndate[-1])