// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["LC or SC"] = {
	"filters": [
		{
			fieldname:"name",
			fieldtype:"Link",
			label:"LC or SC",
			options:"LC or SC"
		},
		{
			fieldname:"applicant_address",
			fieldtype:"Link",
			label:"Applicant",
			options:"Address"
		},
		{
			fieldname:"beneficiary_address",
			fieldtype:"Link",
			label:"Beneficiary",
			options:"Address"
		},
		{
			fieldname:"date_of_issue",
			label: __("Date of Issue"),
			fieldtype: "Date",
			default:frappe.datetime.nowdate()
			
		},

	]
}
