# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	# columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Invoice")+":Link/Commercial Invoice:100",
		_("Invoice Date")+":Date:120",
		_("Company")+":Link/Company:100",
		_("Exporter")+":Data:100",
		_("LC/SC No")+":Link/LC or SC:100",
		_("LC/SC Date")+":Date:180",
		_("EXP No")+":Link/Exp Form:100",
		_("EXP Date")+":Date:120",
		_("LC/SC Issue Bank")+":Data:150",
		_("Total Set")+":Data:150",
		_("Total Pcs")+":Data:100",
		_("Total Amount")+":Currency:100"
		
		

	]

def data(filters):
	if len(filters) <= 0:
		filters['name']=("!=","")
	fields = ['invoice_no','invoice_date','company','exporter','lc','lc_date','exp_no','exp_date','lc_issue_bank','total_set','total_pcs','total_amount']
	return frappe.db.get_values("Commercial Invoice",filters,fields)