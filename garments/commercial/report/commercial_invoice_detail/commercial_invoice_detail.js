// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Commercial Invoice Detail"] = {
	"filters": [
	{
			fieldname:"name",
			fieldtype:"Link",
			label:"Commercial Invoice detail",
			options:"Commercial Invoice"
			},
			{
			fieldname:"invoice_date",
			fieldtype:"Date",
			label:"Invoice date",
			},
			{
			fieldname:"company",
			fieldtype:"Link",
			label:"Company",
			options:"Company"
			},
			{
			fieldname:"exp_no",
			fieldtype:"Link",
			label:"EXP No",
			options:"Exp Form"
			},
			{
			fieldname:"lc",
			fieldtype:"Link",
			label:"LC/SC No",
			options:"LC or SC"
			},
			{
			fieldname:"lc_date",
			fieldtype:"Date",
			label:"LC/SC Date",
			},

	]
}
