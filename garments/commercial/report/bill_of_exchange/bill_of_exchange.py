# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _
#import frappe

def execute(filters=None):
	# columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Name")+":Link/Bill of Exchange:100",
		_("Reference No")+":Link/Commercial Invoice:100",
		_("Date")+":Date:120",
		_("Company")+":Data:180",
		_("Amouunt")+":Data:100",
		_("LC/SC No.")+":Link/LC or SC:120",
		_("To")+":Data:120",
		_("AC")+":Data:120"
	]


def data(filters):
	if len(filters) <= 0:
		filters['name']=("!=","")
	fields = ['name','invoice','date','company','for','lc_no','to','ac']
	return frappe.db.get_values("Bill of Exchange",filters,fields)