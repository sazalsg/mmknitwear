// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Bill of Exchange"] = {
	"filters": [
		{
			fieldname:"name",
			fieldtype:"Link",
			label:"LC or SC",
			options:"Bill of Exchange"
		},
		{
			fieldname:"company",
			fieldtype:"Link",
			label:"Company",
			options:"Company"
		},
		{
			fieldname:"lc_no",
			fieldtype:"Link",
			label:"LC/SC No.",
			options:"LC or SC"
		}

	]
}
