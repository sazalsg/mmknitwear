# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from garments.commercial.report.lc_or_sc.lc_or_sc import columns, data

def execute(filters=None):
	rows =[]
	col = columns(filters)
	col.insert(9,_("Expire Date")+":Date:100")
	for row in data(filters):
		row = list(row)
		row.insert(9,frappe.get_doc("LC or SC",row[0]).date)
		rows.append(row)
	return col, rows
