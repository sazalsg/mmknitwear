// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["LC or SC Expire Date to Date"] = {
	"filters": [
		{
			fieldname:"name",
			fieldtype:"Link",
			label:"LC or SC",
			options:"LC or SC"
		},
		{
			fieldname:"applicant_address",
			fieldtype:"Link",
			label:"Applicant",
			options:"Address"
		},
		{
			fieldname:"beneficiary_address",
			fieldtype:"Link",
			label:"Beneficiary",
			options:"Address"
		},
		{
			fieldname:"from_date",
			label: __("From Date"),
			fieldtype: "Date",
			reqd: 1,
			default: frappe.defaults.get_user_default("year_start_date"),
			
			
		},
		{
			fieldname:"to_date",
			label: __("To Date"),
			fieldtype: "Date",
			reqd: 1,
			// default: frappe.datetime.month_end()
			"default": frappe.defaults.get_user_default("year_end_date")
		}

	]
}


// frappe.query_report.filters_by_name.from_date.$input.on('change',function(){msgprint($(this).val())})