# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	return columns(filters),get_data(filters)

def columns(filters):
	col = [
		{
            "fieldname": "applicant_address",
            "label": _("Buyer"),
            "fieldtype": "Link",
            "options": "Address",
            "width": 150
        },
        {
            "fieldname": "job",
            "label": _("Job No"),
            "fieldtype": "Link",
            "options": "Job",
            "width": 120
        },
        {
            "fieldname": "date_of_issue",
            "label": _("BBLC Date"),
            "fieldtype": "Date",
            "width": 120
        },
        {
            "fieldname": "name",
            "label": _("BBLC No"),
            "fieldtype": "Link",
            "options":"LC or SC",
            "width": 120
        },
        {
        	"fieldname": "amount",
        	"label": _("VALUE"),
        	"fieldtype": "Currency",
        	"width": 120
        },
        {
            "fieldname": "date",
            "label": _("SHIP DT"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "sl",
            "label": _("SL"),
            "fieldtype": "Data",
            "width": 50
        },
        {
        	"fieldname": "beneficiary_address",
        	"label": _("SUPPLIER NAME"),
        	"fieldtype": "Link",
        	"options": "Address",
        	"width": 150
        },
         {
            "fieldname": "item",
            "label": _("Item"),
            "fieldtype": "Link",
            "options": "Item",
            "width": 120
        },
        {
            "fieldname": "quantity",
            "label": _("QTY"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "uom",
            "label": _("Unit"),
            "fieldtype": "Link",
            "options": "UOM",
            "width": 120
        },
        {
            "fieldname": "count",
            "label": _("Count"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "rcvd_qty",
            "label": _("RCVD Qnty"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "blnc_qty",
            "label": _("BLNC Qnty"),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "estm_del",
            "label": _("ESTM. DEL."),
            "fieldtype": "Data",
            "width": 120
        },
        {
            "fieldname": "remark",
            "label": _("Remark"),
            "fieldtype": "Data",
            "width": 120
        }

        	

	]
	return col

def data(filters):
	filters['lc_type'] = "BBLC"
	# frappe.msgprint(filters)
	lyarns = frappe.db.get_values("LC or SC", filters, "*", as_dict=1)
	rows=[]
	for lyarn in lyarns:
		lc = frappe.db.get_values("LC or SC", {"name":lyarn.master_lc})
		#boee = frappe.get_doc("Bill of Entry or Export", lyarn.lc)
		items = frappe.db.get_values("LC or SC Item Child",{'parent':lyarn.name},'item')
		row = {
			"applicant_address": lc.applicant_address,
			"job": lyarn.job,
			"date_of_issue": lyarn.date_of_issue,
			"name":lyarn.name,
			"amount":lyarn.amount,
			"beneficiary_address":lyarn.beneficiary_address
			
		}



	rows.append(row)
	frappe.msgprint(rows)
	return rows

def get_data(filters, importet = 0):
	filters['item.item_group']='Yarn'

	conditions = [];
	for key, val in  filters.iteritems():
		conditions.append('{} = "{}"'.format(key,val))

	if importet:
		conditions.append('supp.country != com.country')
	else:
		conditions.append('supp.country = com.country')

	return frappe.db.sql("""SELECT lc.applicant_address, bblc.job, bblc.date_of_issue,bblc.name,
		bblc.amount,boee.date, '' as sl, bblc.beneficiary_address,lcchild.item, lcchild.quantity ,
		lcchild.uom
		FROM `tabLC or SC` as bblc
		INNER JOIN `tabLC or SC` as lc
		ON bblc.master_lc = lc.name
		LEFT JOIN `tabBill of Entry or Export` as boee
		ON bblc.name=boee.lc
		left join `tabLC or SC Item Child` as lcchild
		on lcchild.parent=bblc.name
		left join `tabItem` as item
		on item.name=lcchild.item
		left join `tabAddress` as supp
		on supp.name = bblc.beneficiary_address
		left join `tabCompany` as com
		on com.name = bblc.company
		where {}
		""".format(' and '.join(conditions)),as_dict=1)