// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Daily BBLC Documents Follow Up"] = {
	"filters": [
	{
			fieldname:"bblc.name",
			fieldtype:"Link",
			label:__("BBLC"),
			options:"LC or SC",
			get_query:function () {
				return {
					filters:{
						'lc_type':"BBLC"
					}
				}
			}
		},
		{
			fieldname:"bblc.job",
			fieldtype:"Link",
			label:__("Job"),
			options:"Job"
		},
		/*{
			fieldname:"master_lc",
			fieldtype:"Link",
			label:__("Master LC"),
			options:"LC or SC",
			get_query:function () {
				return {
					filters:{
						'lc_type':"Master LC"
					}
				}
			}
		},*/
		{
			fieldname:"bblc.date_of_issue",
			fieldtype:"Date",
			label:__("Date"),
			default:frappe.datetime.nowdate()
		},



	]
}
