# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _

def execute(filters=None):
	#columns, data = [], []
	#return columns, data
	return columns(filters), data(filters)



def columns(filters):
	return [
		_("Buyer")+":Link/Customer:100",
		_("Job No")+":Link/Job:100",
		_("BBLC No")+":Link/LC or SC:100",
		_("BBLC Date")+":Date:120",
		_("Value")+":Currency:150",
		_("Shipment Date")+":Date:120",
		_("ABP SL")+":Data:100",
		_("Value 2")+":Currency:120",
		_("MAT Date")+":Date:150",
		_("Paid Amt")+":Currency:150",
		_("Date")+":Date:100",
		_("Supplier")+":Link/Supplier:100",
		_("Item")+":Link/Item:100",
		_("Quantity")+":Data:100",
		_("Remarks")+":Data:100",
	]



def data(filters):
	condition = ''
	if filters:	
		for key,val in filters.iteritems():
			if key == "bblc.name":
				condition += " AND {key}='{val}'".format(key=key, val=val)
			elif key == "bblc.job":
				condition += " AND {key}='{val}'".format(key=key, val=val)
			elif key == "bblc.date_of_issue":
				condition += " AND {key} LIKE '%{val}%'".format(key=key, val=val)
			else:
				condition += " AND {key}='{val}'".format(key=key, val=val)

	#condition = 'WHERE bblc.lc_type ="BBLC"'

	return frappe.db.sql("""SELECT lc.applicant_address,bblc.job,bblc.name as bblc,
		bblc.date_of_issue as bbls_date ,bblc.amount, boee.date as shipment_date,
		' ' as abp_sl, boe.for, rea.submitted_date, boe.for as boe_for,
		rea.realization_date as rea_dt, bblc.beneficiary_address,' ' as item ,
		'0' as qty, 'Nothing' as remark
		FROM `tabLC or SC` as bblc
		INNER JOIN `tabLC or SC` as lc
		ON bblc.master_lc = lc.name
		INNER JOIN `tabCommercial Invoice` as cinvoice
		ON bblc.name=cinvoice.lc
		LEFT JOIN `tabBill of Entry or Export` as boee
		ON boee.invoice=cinvoice.name
		LEFT JOIN `tabRealization` as rea
		ON rea.lc=bblc.name
		INNER JOIN `tabBill of Exchange` as boe
		ON boe.invoice=cinvoice.name
		WHERE ''='' {wh}""".format(wh=condition),as_list=1)


# def data(filters):
# 	condition = ''
# 	if filters:	
# 		for key,val in filters.iteritems():
# 			if key == "bblc.name":
# 				condition += " AND {key}='{val}'".format(key=key, val=val)
# 			elif key == "bblc.job":
# 				condition += " AND {key}='{val}'".format(key=key, val=val)
# 			elif key == "bblc.date_of_issue":
# 				condition += " AND {key} LIKE '%{val}%'".format(key=key, val=val)
# 			else:
# 				condition += " AND {key}='{val}'".format(key=key, val=val)

# 	#condition = 'WHERE bblc.lc_type ="BBLC"'

# 	return frappe.db.sql("""SELECT lc.beneficiary_address,bblc.job,bblc.name as bblc,bblc.date_of_issue as bbls_date ,bblc.amount, boee.date as shipment_date
# 		 ,' ' as abp_sl, boe.for, rea.submitted_date as mature_date, boe.for as boe_for,
# 		rea.realization_date as rea_dt, bblc.beneficiary_address,' ' as item ,'0' as qty, 'Nothing' as remark
# 		FROM `tabLC or SC` as bblc
# 		INNER JOIN `tabLC or SC` as lc
# 		ON bblc.master_lc = lc.name
# 		INNER JOIN `tabCommercial Invoice` as cinvoice
# 		ON bblc.name=cinvoice.lc
# 		LEFT JOIN `tabBill of Entry or Export` as boee
# 		ON boee.invoice=cinvoice.name
# 		LEFT JOIN `tabRealization` as rea
# 		ON rea.lc=lc.name
# 		INNER JOIN `tabBill of Exchange` as boe
# 		ON boe.invoice=cinvoice.name
# 		WHERE ''='' {wh}""".format(wh=condition),as_list=1)

# 		{}""".format(condition),as_list=1)

