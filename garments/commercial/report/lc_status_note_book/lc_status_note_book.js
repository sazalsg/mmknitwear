// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["LC status note book"] = {
	"filters": [
		{
			fieldname:"job.name",
			fieldtype:"Link",
			label:__("Job"),
			options:"Job"
		},	
		{
			fieldname:"lc.name",
			fieldtype:"Link",
			label:__("LC/SC No."),
			options:"LC or SC"
		},
		{
			fieldname:"cinvoice.name",
			fieldtype:"Link",
			label:__("Invoice No."),
			options:"Commercial Invoice"
		}
	]
}


