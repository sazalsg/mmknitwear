# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	#columns, data = [], []
	#return columns, data
	return get_columns(filters), blank_data(filters)


def get_columns(filters):
	col=[

		{
            "fieldname": "job",
            "label": _("Job No"),
            "fieldtype": "Link",
            "options": "Job",
            "width": 120
        },
		{
            "fieldname": "buyer",
            "label": _("Buyer"),
            "fieldtype": "Link",
            "options": "Address",
            "width": 230
        },
		
		{
			"fieldname": "lc_no",
	        "label": _("LC no"),
	        "fieldtype": "Link",
	        "options": "LC or SC",
	        "width": 120
		},
		{
			"fieldname": "lc_amount",
	        "label": _("LC Value"),
	        "fieldtype": "Currency",
	        "width": 150
		},
		{
			"fieldname": "lc_quantity",
	        "label": _("LC Quantity"),
	        "fieldtype": "Data",
	        "width": 150
		},
		{
			"fieldname": "lc_ship_dt",
	        "label": _("LC Ship Dt"),
	        "fieldtype": "Date",
	        "width": 150
		},
		{
			"fieldname": "bblc_no",
	        "label": _("BBLC no"),
	        "fieldtype": "Link",
	        "options": "LC or SC",
	        "width": 120
		},
		{
			"fieldname": "bblc_amount",
	        "label": _("BBLC Value"),
	        "fieldtype": "Currency",
	        "width": 150
		},
		{
		"fieldname": "bblc_quantity",
        "label": _("BBLC Quantity"),
        "fieldtype": "Data",
        "width": 150
		},
		{
		"fieldname": "bblc_ship_dt",
        "label": _("BBLC Ship Dt"),
        "fieldtype": "Date",
        "width": 150
		},
		{
		"fieldname": "imp_doc_value",
        "label": _("Imp Docs Value"),
        "fieldtype": "Currency",
        "width": 150
		},
		{
		"fieldname": "imp_doc_qty",
        "label": _("Imp Docs Quantity"),
        "fieldtype": "Data",
        "width": 150
		},
		{
		"fieldname": "ifbc_bill_no",
        "label": _("IFBC Bill No"),
        "fieldtype": "Link",
        "options": "",
        "width": 120
		},
		{
		"fieldname": "abp_dt",
        "label": _("ABP Date"),
        "fieldtype": "Date",
        "width": 150
		},
		{
		"fieldname": "mature_dt",
        "label": _("Maturity Date"),
        "fieldtype": "Date",
        "width": 150
		},
		{
		"fieldname": "payment_value",
        "label": _("payment Value"),
        "fieldtype": "Currency",
        "width": 150
		},
		{
		"fieldname": "payment_dt",
        "label": _("Payment Date"),
        "fieldtype": "Date",
        "width": 150
		},
		{
		"fieldname": "exp_no",
        "label": _("Export No"),
        "fieldtype": "Link",
        "options": "Exp Form",
        "width": 120
		},
		{
		"fieldname": "exp_invoice_value",
        "label": _("EXP Inv. val."),
        "fieldtype": "Currency",
        "width": 120
		},
		{
		"fieldname": "exp_qty",
        "label": _("Exp Quantity"),
        "fieldtype": "Data",
        "width": 120
		},
		{
		"fieldname": "fdbp_no",
        "label": _("FDBP/FBC/FBPS"),
        "fieldtype": "Data",
        "width": 120
		},
		{
		"fieldname": "rea_amt",
        "label": _("Realize Amt"),
        "fieldtype": "Currency",
        "width": 120
		},
		{
		"fieldname": "fc_amt",
        "label": _("FC Amt"),
        "fieldtype": "Currency",
        "width": 120
		},
		{
		"fieldname": "short_rea",
        "label": _("Short Realise"),
        "fieldtype": "Currency",
        "width": 120
		},

	]
	return col


def blank_data(filters):
	condition = ''
	if filters:	
		for key,val in filters.iteritems():
			condition += " AND {key}='{val}'".format(key=key, val=val)

	return	frappe.db.sql("""SELECT job.name as job, lc.applicant_address as buyer, lc.name as lc_no,
		lc.amount as lc_amount, lc.total_item_quantity as lc_quantity,boee.date as lc_ship_dt,bblc.name as bblc_no,
		bblc.amount as bblc_amount,bblc.total_item_quantity as bblc_quantity,bbboee.date as bblc_ship_dt,
		bbcinvoice.total_amount as imp_doc_value,bbcinvoice.total_pcs as imp_doc_qty, '' as ifbc_bill_no,
		'' as abp_dt,rea.realization_date as mature_dt, '' as payment_value,'' as payment_dt, '' as exp_no,'' as exp_invoice_value,
		'' as exp_qty,'' as fdbp_no, '' as rea_amt, '' as fc_amt, '' as short_rea
	FROM `tabJob` as job
	left JOIN `tabLC or SC` as lc
	ON job.name = lc.job
	left JOIN `tabLC or SC` as bblc
	ON lc.name = bblc.master_lc
	left JOIN `tabCommercial Invoice` as cinvoice
	ON lc.name = cinvoice.lc
	left JOIN `tabBill of Entry or Export` as boee
	ON cinvoice.name = boee.invoice_no
	left JOIN `tabCommercial Invoice` as bbcinvoice
	ON bblc.name = bbcinvoice.lc
	left JOIN `tabBill of Entry or Export` as bbboee
	ON bbcinvoice.name = bbboee.invoice_no
	left JOIN `tabRealization` as rea
	ON cinvoice.name = rea.invoice_no
	WHERE 'job.name'!='' {wh}""".format(wh=condition),as_dict=1)

