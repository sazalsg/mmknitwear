// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Packing Detail"] = {
	"filters": [

		{
			fieldname:"name",
			fieldtype:"Link",
			label:"Packing List",
			options:"Packing List"
		},
		{
			fieldname:"lc",
			fieldtype:"Link",
			label:"LC/SC No",
			options:"LC or SC"
		},
		
		{
			fieldname:"lc_date",
			fieldtype:"Date",
			label:"LC/SC Date",
			
		},
		{
			fieldname:"invoice_no",
			fieldtype:"Link",
			label:"Invoice No",
			options:"Commercial Invoice"
		},
		{
			fieldname:"invoice_date",
			fieldtype:"Date",
			label:"Invoice date",
			
		}
		
	]
}
