# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _
#import frappe

def execute(filters=None):
	# columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Name")+":Link/Export Document:100",
		_("Invoice")+":Data:100",
		_("Mater LC/SC")+":data:120",
		_("Date")+":Date:180",
		_("Reference")+":Data:180",
		_("to")+":Data:180",
		_("Address")+":Data:180",
		
	]

def data(filters):
	if len(filters) <= 0:
		filters['name']=("!=","")
	fields = ['name','invoice','master_lc','date','reference','to','address']
	return frappe.db.get_values("Export Document",filters,fields)
