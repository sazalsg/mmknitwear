// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Export Document"] = {
	"filters": [
		{
			fieldname:"name",
			fieldtype:"Link",
			label:"Export Document",
			options:"Export Document"
		},
		{
			fieldname:"invoice",
			fieldtype:"Link",
			label:"Invoice No.",
			options:"Commercial Invoice"
		},

		{
			fieldname:"lc_no",
			fieldtype:"Link",
			label:"LC/SC No.",
			options:"LC or SC"
		},
		{
			fieldname:"Date",
			fieldtype:"Date",
			label:"Date",
			
		},

	]
}
