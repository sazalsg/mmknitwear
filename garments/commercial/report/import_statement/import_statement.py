# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
    columns = get_columns(filters)
    return columns, get_data(filters)

def get_data(filters):
    rearange_gilter = {'name':('!=',''),'lc_type':'Master LC'}
    if filters.get('from_date'):
        rearange_gilter['date_of_issue'] = ('>=',filters['from_date'])
    if filters.get('to_date'):
        rearange_gilter['date_of_issue'] = ('<=',filters['to_date'])
    if filters.get('job'):
        rearange_gilter['job']=filters['job']


    fields = ['name','name as lc','master_lc','date_of_issue as date', 'date_of_issue as due_date','amount','payment_date',"'0' as indent"]
    if not filters.get('job'):
        fields.insert(2,'job')

    master_lc = frappe.db.get_values("LC or SC",rearange_gilter,fields,as_dict=1)

    data = []
    fields[-1] = "'1' as indent"
    fields.append('beneficiary_address as beneficiary')
    for master in master_lc :
        data.append(master)
        bblc = frappe.db.get_values("LC or SC",{'master_lc':master.name,'name':('!=',''),'lc_type':'BBLC'}, fields,as_dict=1)
        if bblc :
           data= data + bblc
    return data


def get_columns(filters):
    col = [
        {
        	"fieldname": "lc",
        	"label": _("LC/SC"),
        	"fieldtype": "Link",
        	"options": "LC or SC",
        	"width": 230,
            "is_tree":1
        },
        {
        	"fieldname": "date",
        	"label": _("Date"),
        	"fieldtype": "Date",
        	"width": 120
        },
        {
        	"fieldname": "amount",
        	"label": _("VALUE"),
        	"fieldtype": "Currency",
        	"width": 120
        },
        {
        	"fieldname": "beneficiary",
        	"label": _("SUPPLIER NAME"),
        	"fieldtype": "Link",
        	"options": "Address",
        	"width": 150
        },
        {
        	"fieldname": "payment_date",
        	"label": _("DUE DATE"),
        	"fieldtype": "Date",
        	"width": 120
        },
        {
        	"fieldname": "paid_date",
        	"label": _("PAID DATE"),
        	"fieldtype": "Date",
        	"width": 120
        }
    ]

    if not filters.get('job'):
        col.insert(1,{
            "fieldname":"job",
            "label":"Job",
            "fieldtype":"Link",
            "options":"Job",
            "width":130
        })

    return col