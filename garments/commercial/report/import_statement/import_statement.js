// Copyright (c) 2016, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Import Statement"] = {
	"filters": [
		{
			fieldname:"job",
			fieldtype:"Link",
			label:"JOB",
			options:"Job"
		},
		{
			fieldname:"from_date",
			label: __("From Date"),
			fieldtype: "Date",
			"default": frappe.defaults.get_user_default("year_start_date")
		},
		{
			fieldname:"to_date",
			label: __("To Date"),
			fieldtype: "Date",
			"default": frappe.defaults.get_user_default("year_end_date")
		}
	],
	"tree": true,
	"name_field": "lc",
	"parent_field": "master_lc",
	"initial_depth": 2
}

