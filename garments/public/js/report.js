frappe.provide('techbeeoReport');

techbeeoReport.ButtonControl = Class.extend({
	init:function (opts) {
		$.extend(this, opts);
		if (this.oparetion == "NewDoc") {
			this.new_doc()
		};
	},
	new_doc:function () {
		if (this.doctype) {
			frappe.route_options = this.args;
			new_doc(this.doctype)
		};
	}
});