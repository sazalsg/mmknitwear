// Total TTL
function totalttl (sec,table) {
	var total_col = 0;
	$(sec).parents('.tb-body').find('input[name="'+$(sec).attr('name')+'"]').each(function(i, v) {
		total_col += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
	});
	$(sec).parents('table').children('.tb-footer').find('input[name="ttl-'+$(sec).attr('name')+'"]').val(total_col)	
	// Total count on Total column 
	var total_col_field = 0;
	$(sec).parents('.tb-body').find('input[name="total_col"]').each(function(i, v) {
		total_col_field += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
	});
	$(sec).parents('table').children('.tb-footer').find('input[name="ttl-total_col"]').val(total_col_field);


}
// Remove row
function remove_row (evel) {
	frappe.confirm(__("Are you sure you want to delete the Row?"),
	function() {
			$(evel).parents('tr').find('input[data-fieldname="qty"], input[data-fieldname="amount"], input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"], input[data-fieldname="solid_size_qty"]')
				.val(0).trigger('change');
			for (var i = 0; i < cur_frm.doc.childs_child.length; i++) {
				if (cur_frm.doc.childs_child[i].name === $(evel).parents('tr').data('name')) {
					frappe.model.remove_from_locals(cur_frm.doc.childs_child[i].doctype, cur_frm.doc.childs_child[i].name);
					break;
				};
			};
			$(evel).parents('tr').remove();
			return false
		}
	);	
}
// Remove  Section
function remove_section (evel) {
	frappe.confirm(__("Are you sure you want to delete the Section?"),
	function() {
			for (var i = 0; i < cur_frm.doc.childs.length; i++) {
				if (cur_frm.doc.childs[i].name === $(evel).parents('.item').data('parent')) {
					for (var j = 0; j < cur_frm.doc.childs_child.length; j++) {
						if (cur_frm.doc.childs[i].pre_parent === cur_frm.doc.childs_child[j].pre_parent) {
							frappe.model.remove_from_locals(cur_frm.doc.childs_child[j].doctype, cur_frm.doc.childs_child[j].name);
						};
					};
					frappe.model.remove_from_locals(cur_frm.doc.childs[i].doctype, cur_frm.doc.childs[i].name);
					break;
				};
			};
			$(evel).parents('.item').remove();
			return false
		}
	);	
}
// Add New Body Row
function add_new_row (evel) {
	var sel = $(evel).parents('.item').find('input[data-fieldname="size"]').val();
	if (sel != "") {
		var fields = cur_frm.cscript.get_sizes(sel)
		cur_frm.cscript.render_body($(evel).parents('.item'),fields);
		return false;
	}else {
		msgprint(__("Please select size"))
	};
	return false
};
// Total Count
function totalAmount (evel) {
	var qty = $(evel).parents('tr').find('input[data-fieldname="qty"]').val()
	var purchase_price = $(evel).parents('tr').find('input[data-fieldname="purchase_price"]').val()
	qty = ($.isNumeric(parseFloat(qty.replace(/,/g,'')))? parseFloat(qty.replace(/,/g,'')):0);
	purchase_price = ($.isNumeric(parseFloat(purchase_price.replace(/,/g,'')))? parseFloat(purchase_price.replace(/,/g,'')):0);
	$(evel).parents('tr').find('input[data-fieldname="amount"]').val((purchase_price*qty)).trigger( "change" );	
};
function totalVartical (evel) {
	var total_row = 0
	$(evel).parents('.item').find('input[data-fieldname="'+$(evel).data('fieldname')+'"]').each(function(index, el) {
		total_row += ($.isNumeric(parseFloat($(el).val().replace(/,/g,'')))? parseFloat($(el).val().replace(/,/g,'')):0);
	});
	$(evel).parents('.item').find('input[data-fieldname="total_'+$(evel).data('fieldname')+'"]').val(total_row).trigger('change');
};
function totalHorizental (evel) {
	var parent = $(evel).parents('tr');
	var total_day_rows_val = 0;
	$(parent).find('.day_row').not('[data-fieldname="std_parcel_number"]').each(function(index, el) {
		total_day_rows_val +=($.isNumeric(parseFloat($(el).val().replace(/,/g,'')))? parseFloat($(el).val().replace(/,/g,'')):0);
	});
	var size_brackdown_for = $(parent).find('select[data-fieldname="size_breakdown_for"]').val();
	if (size_brackdown_for=="Standard Packing") {
		var std_parcel_number = $(parent).find('input[data-fieldname="std_parcel_number"]').val();
		if (!isNaN(std_parcel_number) && parseInt(Number(std_parcel_number)) > 0 && total_day_rows_val > 0) {
			var total_val = total_day_rows_val * std_parcel_number
			$(parent).find('input[data-fieldname="qty"], input[data-fieldname="std_packing_qty"]').val(total_val).trigger('change');
		};
	}else if (size_brackdown_for=="Solid Packing") {
		$(parent).find('input[data-fieldname="qty"], input[data-fieldname="solid_size_qty"]').val(total_day_rows_val).trigger('change');
	}else {
	};
};