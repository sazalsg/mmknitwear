// Total TTL
function totalttl (sec,table) {
	var total_col = 0;

	var bdy = 0;
	var dos = 0;
	$(sec).parents('.tb-body').find('input[name="'+$(sec).attr('name')+'"]').each(function(i, v) {
		
		if(i == 0){

			bdy= ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
		}
		if( i==2){

			dos= ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);

		}
	});

	if(dos != 0){
		total_col = bdy / dos;
	}

	// console.log( 'aa::' + total_col );
	$(sec).parents('table').children('.tb-footer').find('input[name="ttl-'+$(sec).attr('name')+'"]').val(total_col.toFixed(2))	
	// // Total count on Total column 
	// var total_col_field = 0;
	// $(sec).parents('.tb-body').find('input[name="total_col"]').each(function(i, v) {
	// 	total_col_field += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
	// });
	// $(sec).parents('table').children('.tb-footer').find('input[name="ttl-total_col"]').val(total_col_field);



}
// Remove row
function remove_row (evel) {
	frappe.confirm(__("Are you sure you want to delete the Row?"),
	function() {
			var parent = $(evel).parents('.tb-body');
			$(evel).parents('tr').remove();
			if (parent.find('tr').length > 0) {				
				var ignor_col = ['desc_one','desc_two','total_col','first_pri','secound_pri','remark'];
				$(parent).find('input').each(function(index, el) {
					if ((ignor_col.indexOf($(el).attr('name'))) == -1) {
						totalttl(el)
					};
				});
			}else {
				$(parent).parents('table').children('.tb-footer').find('input').val("");
			};
			return false
		}
	);	
}
// Remove  Section
function remove_section (evel) {
	frappe.confirm(__("Are you sure you want to delete the Section?"),
	function() {
			$(evel).parents('.item').remove();
			return false
		}
	);	
}
// Add New Body Row
function add_new_row (evel) {
	var sel = $(evel).parents('.item').find('input[data-fieldname="size"]').val();
	if (sel != "") {
		cur_frm.cscript.get_sizes(sel)
		cur_frm.cscript.render_body($(evel).parents('.item'),spData.sizes);
		return false;
	}else {
		msgprint(__("Please select size"))
	};
	return false
};
// Total Count
function sumTotal (evel) {

	setDefaultVal(evel);
	SumVal(evel);
	var total_row = 0;
	var ignor_col = ['desc_one','desc_two','total_col','first_pri','secound_pri','remark'];
	$(evel).parents('tr').find('input').each(function(index, el) {
		if ((ignor_col.indexOf($(el).attr('name'))) == -1) {
			total_row += ($.isNumeric(parseInt($(el).val()))? parseInt($(el).val()):0);
		};
	});
	$(evel).parents('tr').find('input[name="total_col"]').val(total_row);
	// set pri second val
	var first_pri_val = $(evel).parents('tr').find('input[name="first_pri"]').val();
		first_pri_val = ($.isNumeric(first_pri_val)? first_pri_val:0);
	$(evel).parents('tr').find('input[name="secound_pri"]').val((total_row-first_pri_val)>0?total_row-first_pri_val:0);
	// Set Footer Total
	totalttl(evel);
	
};

function SumVal(evel){

			// var column = $(evel).parent().parent().children().index($(evel).parent());

			// var row = $(evel).parent().parent().parent().children().index($(evel).parent().parent());

			//   console.log( column );

			//   if(column > 0 && (row == 0 || row ==2)){

			//   	console.log('ok....');
			//   	//$(evel).parent().parent().find('input').val($(evel).val());


			  	
			//   }


}

function setDefaultVal(evel){

	var column = $(evel).parent().parent().children().index($(evel).parent());
		if(column == 1){

			frappe.confirm(__("Set Same Value To All Column?"),
			function() {

					  	$(evel).parent().parent().find('input').not(evel).val($(evel).val()).trigger('change');
					
					return false
				}
			);	
	
	  }
			
}

// Footer Missing Key
function footer_missing_key (r) {
	if (!('ttl_total_col' in r)) {	
		r['ttl_total_col']='';
	};
	return r
}
// Body Missing Key
function body_missing_key (r) {
	if (!('desc_one' in r)) {	
		r['desc_one']='';
	};
	if (!('desc_two' in r)) {	
		r['desc_two']='';
	};
	if (!('color' in r)) {	
		r['color']='';
	};
	if (!('total_col' in r)) {	
		r['total_col']='';
	};
	if (!('first_pri' in r)) {	
		r['first_pri']='';
	};
	if (!('secound_pri' in r)) {	
		r['secound_pri']='';
	};
	if (!('remark' in r)) {	
		r['remark']='';
	};
	return r
}