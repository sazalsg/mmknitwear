/**
 * Created by mainul on 3/21/16.
 */

frappe.provide('garments')

garments.MultiCurrencyController = frappe.ui.form.Controller.extend({
    get_company_currency: function() {
		return erpnext.get_currency(this.frm.doc.company);
	},
    get_exchange_rate: function(from_currency, to_currency, callback) {
		return frappe.call({
			method: "erpnext.setup.utils.get_exchange_rate",
			args: {
				from_currency: from_currency,
				to_currency: to_currency
			},
			callback: function(r) {
				callback(flt(r.message));
			}
		});
	},
	
    company: function() {
		var me = this;
		var set_pricing = function() {
			if(me.frm.doc.company && me.frm.fields_dict.currency) {
				var company_currency = me.get_company_currency();
				var company_doc = frappe.get_doc(":Company", me.frm.doc.company);
				if (!me.frm.doc.currency) {
					me.frm.set_value("currency", company_currency);
				}

				if (me.frm.doc.currency == company_currency) {
					me.frm.set_value("conversion_rate", 1.0);
				}
				if (me.frm.doc.price_list_currency == company_currency) {
					me.frm.set_value('plc_conversion_rate', 1.0);
				}
				if (company_doc.default_letter_head) {
					if(me.frm.fields_dict.letter_head) {
						me.frm.set_value("letter_head", company_doc.default_letter_head);
					}
				}
				if (company_doc.default_terms && me.frm.doc.doctype != "Purchase Invoice") {
					me.frm.set_value("tc_name", company_doc.default_terms);
				}

                me.get_exchange_rate(me.frm.doc.currency, company_currency,
                    function(exchange_rate) {
                        me.frm.set_value("conversion_rate", exchange_rate);
                    });
                me.change_form_labels(company_currency);
                me._set_all_base_field_value();
                me.frm.refresh_fields();
				//me.frm.script_manager.trigger("currency");
				//me.apply_pricing_rule();
			}
		}
        set_pricing();
	},
    change_form_labels: function(company_currency) {
		var me = this;
		var field_label_map = {};

		var setup_field_label_map = function(fields_list, currency) {
			$.each(fields_list, function(i, fname) {
				var docfield = frappe.meta.docfield_map[me.frm.doc.doctype][fname];
				if(docfield) {
					var label = __(docfield.label || "").replace(/\([^\)]*\)/g, "");
					field_label_map[fname] = label.trim() + " (" + currency + ")";
				}
			});
		};
		setup_field_label_map(me.fields_of_company_currency, company_currency);

		setup_field_label_map(me.fields_of_form_currency, this.frm.doc.currency);

		cur_frm.set_df_property("conversion_rate", "description", "1 " + this.frm.doc.currency
			+ " = [?] " + company_currency)

		if(this.frm.doc.price_list_currency && this.frm.doc.price_list_currency!=company_currency) {
			cur_frm.set_df_property("plc_conversion_rate", "description", "1 " + this.frm.doc.price_list_currency
				+ " = [?] " + company_currency)
		}

		// toggle fields
        var hidden_fields = $.merge(['conversion_rate'],me.fields_of_company_currency);
		this.frm.toggle_display(hidden_fields,
			this.frm.doc.currency != company_currency);


		// set labels
		$.each(field_label_map, function(fname, label) {
			me.frm.fields_dict[fname].set_label(label);
		});

	},
    _set_base_field_value:function(field,source_field) {
        var changed_val = this.frm.get_field((source_field?source_field:field.replace('base_',''))).value * this.frm.doc.conversion_rate;
        frappe.model.set_value(this.frm.doc.doctype,this.frm.doc.name,field,changed_val,this.frm.fields_dict[field].df.fieldtype);
        refresh_field(field);
    },
    _set_all_base_field_value:function() {
        var me = this;
        $.each(this.fields_of_company_currency, function (i,field) {
            me._set_base_field_value(field)
        } );
    },
    _set_child_base_field_value:function(fields,doc,source_field) {
		var me = this;
		$.each(fields, function(i, f) {
			doc["base_"+f] = flt(flt(doc[f], precision(f, doc)) * me.frm.doc.conversion_rate, precision("base_" + f, doc));
			refresh_field("base_"+f,doc.name,doc.parentfield);
			me.set_dynamic_labels();
		});

        // var changed_val = this.frm.get_field((source_field?source_field:field.replace('base_',''))).value * this.frm.doc.conversion_rate;
        // frappe.model.set_value(this.frm.doc.doctype,this.frm.doc.name,field,changed_val,this.frm.fields_dict[field].df.fieldtype);
        // refresh_field(field);
    },
    _set_child_all_base_field_value:function() {
        var me = this;
		var chield_Info = me.grid_from_base_fields();
		for (var x = 0; x <chield_Info.length; x++) {
			$.each(me.chield_Info[x].fields, function (i,field) {
				me._set_child_base_field_value(field,frappe.get_doc())
			} );
		}
    },
    currency: function () {
        this.frm.script_manager.trigger('company');
        this.set_dynamic_labels();
    },
    conversion_rate: function () {
		this.set_dynamic_labels();
		this._set_all_base_field_value();
		this.frm.refresh_fields();
    },
	change_grid_labels: function(company_currency) {
		var me = this;
		var field_label_map = {};

		var setup_field_label_map = function(fields_list, currency, parentfield) {
			var grid_doctype = me.frm.fields_dict[parentfield].grid.doctype;
			$.each(fields_list, function(i, fname) {
				var docfield = frappe.meta.docfield_map[grid_doctype][fname];
				if(docfield) {
					var label = __(docfield.label || "").replace(/\([^\)]*\)/g, "");
					field_label_map[grid_doctype + "-" + fname] =
						label.trim() + " (" + __(currency) + ")";
				}
			});
		}

		if (! ('grid_from_base_fields' in me)) {
			return
		}
		var chield_Info = me.grid_from_base_fields();
		for (var x = 0; x <chield_Info.length; x++) {
			setup_field_label_map(chield_Info[x].base_label_change_fields,company_currency,chield_Info[x].table_field_name);
			setup_field_label_map(chield_Info[x].label_change_fields,this.frm.doc.currency,chield_Info[x].table_field_name);
			var item_grid = this.frm.fields_dict[chield_Info[x].table_field_name].grid;
			$.each(chield_Info[x].base_label_change_fields, function(i, fname) {
				if(frappe.meta.get_docfield(item_grid.doctype, fname))
					item_grid.set_column_disp(fname, me.frm.doc.currency != company_currency);
			});
			// toggle columns
			var item_grid = this.frm.fields_dict[chield_Info[x].table_field_name].grid;
			$.each(chield_Info[x].base_fields, function(i, fname) {
				if(frappe.meta.get_docfield(item_grid.doctype, fname))
					item_grid.set_column_disp(fname, me.frm.doc.currency != company_currency);
			});

			var show = 1;//(cint(cur_frm.doc.discount_amount)) ||
				//((cur_frm.doc.taxes || []).filter(function(d) {return d.included_in_print_rate===1}).length);

			console.log(show);

			$.each(chield_Info[x].base_label_change_fields, function(i, fname) {
				if(frappe.meta.get_docfield(item_grid.doctype, fname))
					item_grid.set_column_disp(fname, show);
			});

			$.each(chield_Info[x].base_fields, function(i, fname) {
				if(frappe.meta.get_docfield(item_grid.doctype, fname))
					item_grid.set_column_disp(fname, (show && (me.frm.doc.currency != company_currency)));
			});
		}


		// set labels
		var $wrapper = $(this.frm.wrapper);
		$.each(field_label_map, function(fname, label) {
			fname = fname.split("-");
			var df = frappe.meta.get_docfield(fname[0], fname[1], me.frm.doc.name);
			if(df) df.label = label;
		});
	},
	set_dynamic_labels: function() {
		// What TODO? should we make price list system non-mandatory?
		// this.frm.toggle_reqd("plc_conversion_rate",
		// 	!!(this.frm.doc.price_list_name && this.frm.doc.price_list_currency));

		var company_currency = this.get_company_currency();
		this.change_form_labels(company_currency);
		this.change_grid_labels(company_currency);
		this.frm.refresh_fields();
	}

});