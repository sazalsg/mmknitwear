function pure_sheet_child_fileds (){
	return [
		  {
		   "fieldname": "po_num", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Po Num", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "uvc", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "UVC", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "std_parcel_code", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "STD Parcel Code", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "product_name", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Product Name", 
		   "options": "Style Name", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "description", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Description", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "color", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Price Tag Color Name", 
		   "options": "Color", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "theme", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Theme", 
		   "options": "Theme", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "size_format", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Age", 
		   "options": "Size Format", 
		   "read_only": 0, 
		   "reqd": 1,
		   "css": "min-width:150px;"

		  }, 
		  {
		   "fieldname": "std", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Std", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "etd_shipment_date", 
		   "fieldtype": "Date", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "ETD Shipment Date", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "shipment_destination", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Shipment Destination", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "composition", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Composition", 
		   "options":"Composition",
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "purchase_price", 
		   "fieldtype": "Currency", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Purchase Price", 
		   "permlevel": 0, 
		   "precision": "2", 
		   "print_hide": 0, 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "qty", 
		   "fieldtype": "Int", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Qty", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "amount", 
		   "fieldtype": "Currency", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Amount", 
		   "permlevel": 0, 
		   "precision": "2", 
		   "print_hide": 0, 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "size_breakdown_for", 
		   "fieldtype": "Select", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Size Breakdown For", 
		   "options": "\nStandard Packing\nSolid Packing", 
		   "read_only": 0, 
		   "reqd": 1, 
		   "css": "border-left:4px solid #DFEBF3; min-width:200px;"

		  }, 
		  {
		   "fieldname": "std_packing_qty", 
		   "fieldtype": "Int", 
		   "hidden": 0, 
		   "in_list_view": 0, 
		   "label": "STD Packing Qty", 
		   "read_only": 0, 
		   "reqd": 0, 

		  }, 
		  {
		   "fieldname": "std_parcel_number", 
		   "fieldtype": "Int", 
		   "hidden": 0, 
		   "in_list_view": 0, 
		   "label": "STD Parcel Number", 
		   "read_only": 0, 
		   "reqd": 0,
		   "h_cal": 1

		  }, 
		  {
		   "fieldname": "solid_size_qty", 
		   "fieldtype": "Int", 
		   "hidden": 0, 
		   "in_list_view": 0, 
		   "label": "Solid Size Qty", 
		   "read_only": 0, 
		   "reqd": 0,
		   "css": "border-right:4px solid #DFEBF3;"

		  }
		 ];
}

// Total TTL
function totalttl (sec,table) {
	var total_col = 0;
	$(sec).parents('.tb-body').find('input[name="'+$(sec).attr('name')+'"]').each(function(i, v) {
		total_col += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
	});
	$(sec).parents('table').children('.tb-footer').find('input[name="ttl-'+$(sec).attr('name')+'"]').val(total_col)	
	// Total count on Total column 
	var total_col_field = 0;
	$(sec).parents('.tb-body').find('input[name="total_col"]').each(function(i, v) {
		total_col_field += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
	});
	$(sec).parents('table').children('.tb-footer').find('input[name="ttl-total_col"]').val(total_col_field);


}
// Remove row
function remove_row (evel) {
	frappe.confirm(__("Are you sure you want to delete the Row?"),
	function() {
			$(evel).parents('tr').find('input[data-fieldname="qty"], input[data-fieldname="amount"], input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"], input[data-fieldname="solid_size_qty"]')
				.val(0).trigger('change');
			$(evel).parents('tr').remove();
			return false
		}
	);	
}
// Remove  Section
function remove_section (evel) {
	frappe.confirm(__("Are you sure you want to delete the Section?"),
	function() {
			$(evel).parents('.item').remove();
			return false
		}
	);	
}
// Add New Body Row
function add_new_row (evel) {
	var sel = $(evel).parents('.item').find('input[data-fieldname="size"]').val();
	if (sel != "") {
		var fields = cur_frm.cscript.get_sizes(sel)
		cur_frm.cscript.render_body($(evel).parents('.item'),fields);
		return false;
	}else {
		msgprint(__("Please select size"))
	};
	return false
};
// Total Count
function totalAmount (evel) {
	var qty = $(evel).parents('tr').find('input[data-fieldname="qty"]').val()
	var purchase_price = $(evel).parents('tr').find('input[data-fieldname="purchase_price"]').val()
	qty = ($.isNumeric(parseFloat(qty.replace(/,/g,'')))? parseFloat(qty.replace(/,/g,'')):0);
	purchase_price = ($.isNumeric(parseFloat(purchase_price.replace(/,/g,'')))? parseFloat(purchase_price.replace(/,/g,'')):0);
	$(evel).parents('tr').find('input[data-fieldname="amount"]').val((purchase_price*qty)).trigger( "change" );	
};
function totalVartical (evel) {
	var total_row = 0
	$(evel).parents('.item').find('input[data-fieldname="'+$(evel).data('fieldname')+'"]').each(function(index, el) {
		total_row += ($.isNumeric(parseFloat($(el).val().replace(/,/g,'')))? parseFloat($(el).val().replace(/,/g,'')):0);
	});
	$(evel).parents('.item').find('input[data-fieldname="total_'+$(evel).data('fieldname')+'"]').val(total_row);
};
function totalHorizental (evel) {
	var parent = $(evel).parents('tr');
	var total_day_rows_val = 0;
	$(parent).find('.day_row').each(function(index, el) {
		total_day_rows_val +=($.isNumeric(parseFloat($(el).val().replace(/,/g,'')))? parseFloat($(el).val().replace(/,/g,'')):0);
	});
	var size_brackdown_for = $(parent).find('select[data-fieldname="size_breakdown_for"]').val();
	if (size_brackdown_for=="Standard Packing") {
		var std_parcel_number = $(parent).find('input[data-fieldname="std_parcel_number"]').val();
		if (!isNaN(std_parcel_number) && parseInt(Number(std_parcel_number)) > 0 && total_day_rows_val > 0) {
			var total_val = total_day_rows_val * std_parcel_number
			$(parent).find('input[data-fieldname="qty"], input[data-fieldname="std_packing_qty"]').val(total_val).trigger('change');
		};
	}else if (size_brackdown_for=="Solid Packing") {
		$(parent).find('input[data-fieldname="qty"], input[data-fieldname="solid_size_qty"]').val(total_day_rows_val).trigger('change');
	}else {
	};
	/*var purchase_price = $(evel).parents('tr').find('input[data-fieldname="purchase_price"]').val()
	qty = ($.isNumeric(parseFloat(qty.replace(/,/g,'')))? parseFloat(qty.replace(/,/g,'')):0);
	purchase_price = ($.isNumeric(parseFloat(purchase_price.replace(/,/g,'')))? parseFloat(purchase_price.replace(/,/g,'')):0);
	$(evel).parents('tr').find('input[data-fieldname="amount"]').val((purchase_price*qty)).trigger( "change" );	*/
};