// Import Excels
function update_sheet_from_file (doc) {
	if (doc.xls_file == "") {
		return
	};
	if (doc.__unsaved) {
		save_doc_with_confermation(doc,"Your document are not saved, you need saved document before Import. Did you want to save.")

	}else {
		frappe.confirm("Did you want to Update Sheet",function (){
			import_data(doc)
		})
	};
}
function save_doc_with_confermation (doc,msg) {
	frappe.confirm(__(msg),function (doc) {
		cur_frm.save()
	});
}

function import_data (doc) {
	frappe.call({
		method:"garments.merchandising.doctype.po_sheet.po_sheet.import_sheet",
		datatype:'json',
		args:{
			doc:doc
		},
		freeze:true,
		callback:function (data) {
			if (data.message !=="Sorry") {
				location.reload();
			};
		}
	})
}