function new_bundel () {
	msgprint("New");
	return false;
}
function link_field (selector) {
	fieldname = $(selector).data('fieldname');
	var make_field = frappe.ui.form.make_control({
		parent: selector,
		df: {
			fieldtype: $(selector).data('doctype'),
			options: $(selector).data('options'),
			fieldname: $(selector).data('fieldname'),
			reqd:1
		},
		only_input: true
	});
	make_field.refresh();
}

function make_fields (evel) {
	$(evel).children('td').each(function(index, el) {
		var fieldtype = $(el).data('fieldtype');
		if (fieldtype == "Link") {
			link_field($(el))
		};
	});	
}