function po_child_fileds (){
	return [
		  {
		   "fieldname": "po_num", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Po Num", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "uvc", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "UVC", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "std_parcel_code", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "STD Parcel Code", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "product_name", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Product Name", 
		   "options": "Style Name", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "description", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Description", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "color", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Color", 
		   "options": "Color", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "theme", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Theme", 
		   "options": "Theme", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "size_format", 
		   "fieldtype": "Link", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Age", 
		   "options": "Size Format", 
		   "read_only": 0, 
		   "reqd": 1,
		   "css": "min-width:150px;"

		  }, 
		  {
		   "fieldname": "std", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Std", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "etd_shipment_date", 
		   "fieldtype": "Date", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Ship Date", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "shipment_destination", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Shipment Destinat", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "composition", 
		   "fieldtype": "Data", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "Composition", 
		   "read_only": 0, 
		   "reqd": 1, 

		  }, 
		  {
		   "fieldname": "purchase_price", 
		   "fieldtype": "Currency", 
		   "hidden": 0, 
		   "in_list_view": 1, 
		   "label": "FOB", 
		   "permlevel": 0, 
		   "precision": "2", 
		   "print_hide": 0, 
		   "read_only": 0, 
		   "reqd": 1, 
		   "css": "border-right:4px solid #DFEBF3;"
		  }
		 ];
}
function po_childs_fields () {
	return 
	[
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "size", 
	   "fieldtype": "Data", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Size", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 1, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }, 
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "total_qty", 
	   "fieldtype": "Int", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Total Qty", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 0, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }, 
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "total_amount", 
	   "fieldtype": "Currency", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Total Amount", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "2", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 0, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }, 
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "size_for_break", 
	   "fieldtype": "Column Break", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Size For Break", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 0, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }, 
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "total_std_packing_qty", 
	   "fieldtype": "Int", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Total STD Packing Qty", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 0, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }, 
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "total_std_parcel_number", 
	   "fieldtype": "Int", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Total STD Parcel Number", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 0, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }, 
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "total_solid_size_qty", 
	   "fieldtype": "Int", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Total Solid Size Qty", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 0, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }, 
	  {
	   "allow_on_submit": 0, 
	   "bold": 0, 
	   "collapsible": 0, 
	   "fieldname": "pre_parent", 
	   "fieldtype": "Data", 
	   "hidden": 0, 
	   "ignore_user_permissions": 0, 
	   "in_filter": 0, 
	   "in_list_view": 0, 
	   "label": "Pre_parent", 
	   "no_copy": 0, 
	   "permlevel": 0, 
	   "precision": "", 
	   "print_hide": 0, 
	   "read_only": 0, 
	   "report_hide": 0, 
	   "reqd": 1, 
	   "search_index": 0, 
	   "set_only_once": 0, 
	   "unique": 0
	  }
	 ]
}

// Total TTL
function totalttl (sec,table) {
	var total_col = 0;
	$(sec).parents('.tb-body').find('input[name="'+$(sec).attr('name')+'"]').each(function(i, v) {
		total_col += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
	});
	$(sec).parents('table').children('.tb-footer').find('input[name="ttl-'+$(sec).attr('name')+'"]').val(total_col)	
	// Total count on Total column 
	var total_col_field = 0;
	$(sec).parents('.tb-body').find('input[name="total_col"]').each(function(i, v) {
		total_col_field += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
	});
	$(sec).parents('table').children('.tb-footer').find('input[name="ttl-total_col"]').val(total_col_field);


}
// Remove row
function remove_row (evel) {
	frappe.confirm(__("Are you sure you want to delete the Row?"),
	function() {
			$(evel).parents('tr').find('input[data-fieldname="qty"], input[data-fieldname="amount"], input[data-fieldname="std_packing_qty"], input[data-fieldname="std_parcel_number"], input[data-fieldname="solid_size_qty"]')
				.val(0).trigger('change');
			$(evel).parents('tr').remove();
			return false
		}
	);	
}
// Remove  Section
function remove_section (evel) {
	frappe.confirm(__("Are you sure you want to delete the Section?"),
	function() {
			$(evel).parents('.item').remove();
			return false
		}
	);	
}
// Add New Body Row
function add_new_row (evel) {
	var sel = $(evel).parents('.item').find('input[data-fieldname="size"]').val();
	if (sel != "") {
		var fields = cur_frm.cscript.get_sizes(sel)
		cur_frm.cscript.render_body($(evel).parents('.item'),fields);
		return false;
	}else {
		msgprint(__("Please select size"))
	};
	return false
};
// Total Count
function totalAmount (evel) {
	var qty = $(evel).parents('tr').find('input[data-fieldname="qty"]').val()
	var purchase_price = $(evel).parents('tr').find('input[data-fieldname="purchase_price"]').val()
	qty = ($.isNumeric(parseFloat(qty.replace(/,/g,'')))? parseFloat(qty.replace(/,/g,'')):0);
	purchase_price = ($.isNumeric(parseFloat(purchase_price.replace(/,/g,'')))? parseFloat(purchase_price.replace(/,/g,'')):0);
	$(evel).parents('tr').find('input[data-fieldname="amount"]').val((purchase_price*qty)).trigger( "change" );	
};
function totalVartical (evel) {
	var total_row = 0
	$(evel).parents('.item').find('input[data-fieldname="'+$(evel).data('fieldname')+'"]').each(function(index, el) {
		total_row += ($.isNumeric(parseFloat($(el).val().replace(/,/g,'')))? parseFloat($(el).val().replace(/,/g,'')):0);
	});
	$(evel).parents('.item').find('input[data-fieldname="total_'+$(evel).data('fieldname')+'"]').val(total_row);
};
function totalHorizental (evel) {
	var parent = $(evel).parents('tr');
	var total_day_rows_val = 0;
	$(parent).find('.day_row').each(function(index, el) {
		total_day_rows_val +=($.isNumeric(parseFloat($(el).val().replace(/,/g,'')))? parseFloat($(el).val().replace(/,/g,'')):0);
	});
	var size_brackdown_for = $(parent).find('select[data-fieldname="size_breakdown_for"]').val();
	if (size_brackdown_for=="Standard Packing") {
		var std_parcel_number = $(parent).find('input[data-fieldname="std_parcel_number"]').val();
		if (!isNaN(std_parcel_number) && parseInt(Number(std_parcel_number)) > 0 && total_day_rows_val > 0) {
			var total_val = total_day_rows_val * std_parcel_number
			$(parent).find('input[data-fieldname="qty"], input[data-fieldname="std_packing_qty"]').val(total_val).trigger('change');
		};
	}else if (size_brackdown_for=="Solid Packing") {
		$(parent).find('input[data-fieldname="qty"], input[data-fieldname="solid_size_qty"]').val(total_day_rows_val).trigger('change');
	}else {
	};
	/*var purchase_price = $(evel).parents('tr').find('input[data-fieldname="purchase_price"]').val()
	qty = ($.isNumeric(parseFloat(qty.replace(/,/g,'')))? parseFloat(qty.replace(/,/g,'')):0);
	purchase_price = ($.isNumeric(parseFloat(purchase_price.replace(/,/g,'')))? parseFloat(purchase_price.replace(/,/g,'')):0);
	$(evel).parents('tr').find('input[data-fieldname="amount"]').val((purchase_price*qty)).trigger( "change" );	*/
};
Number.prototype.pad = function(size) {
      var s = String(this);
      while (s.length < (size || 2)) {s = "0" + s;}
      return s;
    }