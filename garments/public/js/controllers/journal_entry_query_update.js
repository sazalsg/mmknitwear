/**
 * Created by mainul on 8/2/16.
 */
// make a field read-only after saving
frappe.ui.form.on("Journal Entry", {
    refresh: function(frm) {
		// Set Query in reference_name field
		frm.set_query("reference_name", "accounts", function(doc, cdt, cdn) {
			var jvd = frappe.get_doc(cdt, cdn);

			// expense claim
			if(jvd.reference_type==="Expense Claim") {
				return {};
			}

			// journal entry
			if(jvd.reference_type==="Journal Entry") {
				frappe.model.validate_missing(jvd, "account");
				return {
					query: "erpnext.accounts.doctype.journal_entry.journal_entry.get_against_jv",
					filters: {
						account: jvd.account,
						party: jvd.party
					}
				};
			}

			var out = {
				filters: [
					[jvd.reference_type, "docstatus", "=", 1]
				]
			};

			if(in_list(["Sales Invoice", "Purchase Invoice"], jvd.reference_type)) {
				out.filters.push([jvd.reference_type, "outstanding_amount", "!=", 0]);

				// account filter
				frappe.model.validate_missing(jvd, "account");

				party_account_field = jvd.reference_type==="Sales Invoice" ? "debit_to": "credit_to";
				out.filters.push([jvd.reference_type, party_account_field, "=", jvd.account]);
			}else if (in_list(['LC or SC','Commercial Invoice','Realization'],jvd.reference_type)) {
				//Write Condition here
            } else {
                // party_type and party mandatory
                frappe.model.validate_missing(jvd, "party_type");
                frappe.model.validate_missing(jvd, "party");

                out.filters.push([jvd.reference_type, "per_billed", "<", 100]);
            }

			if(jvd.party_type && jvd.party) {
				out.filters.push([jvd.reference_type,
					(jvd.reference_type.indexOf("Sales")===0 ? "customer" : "supplier"), "=", jvd.party]);
			}

			return out;
		});

    }
});