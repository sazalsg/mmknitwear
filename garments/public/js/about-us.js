frappe.provide('frappe.ui.misc');
frappe.ui.misc.about = function() {
    var d = new frappe.ui.Dialog({title: __('Techbeeo Software Ltd.')});

    $(d.body).html(repl("<div>\
		<p>" + __("Custom Applications for the Web") + "</p>  \
		<p><i class='icon-globe icon-fixed-width'></i>\
			 Website: <a href='https://techbeeo.io' target='_blank'>https://techbeeo.io</a></p>\
	 	<p><i class='icon-github icon-fixed-width'></i>\
			Source: <a href='https://bitbucket.org/beeo-erp/garments' target='_blank'>https://bitbucket.org/beeo-erp/garments</a></p>\
		<hr>\
		<h4>Installed Apps</h4>\
		<div id='about-app-versions'>Loading versions...</div>\
		<hr>\
		<p class='text-muted'>&copy; 2016 Techbeeo Software Ltd </p> \
		</div>", frappe.app));

    frappe.ui.misc.about_dialog = d;

    frappe.ui.misc.about_dialog.on_page_show = function () {
        if (!frappe.versions) {
            frappe.call({
                method: "frappe.utils.change_log.get_versions",
                callback: function (r) {
                    show_versions(r.message);
                }
            })
        }
    };

    var show_versions = function (versions) {
        var $wrap = $("#about-app-versions").empty();
        $.each(keys(versions).sort(), function (i, key) {
            var v = versions[key];
            $($.format('<p><b>{0}:</b> v{1}<br></p>',
                [v.title, v.version])).appendTo($wrap);
        });

        frappe.versions = versions;
    }
    frappe.ui.misc.about_dialog.show();

}
