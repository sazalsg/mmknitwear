// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt

frappe.provide('garments');

// add toolbar icon
$(document).bind('toolbar_setup', function() {
	frappe.app.name = "Garments";

	frappe.help_feedback_link = '<p><a class="text-muted" \
		href="https://discuss.erpnextplus.com">Feedback</a></p>'


	$('.navbar-home').html('<img class="erpnext-icon" src="'+
			frappe.urllib.get_base_url()+'/assets/garments/images/icon.png" />');

	$('[data-link="docs"]').attr("href", "https://manual.erpnextplus.com")
});
