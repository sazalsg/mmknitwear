function set_table_header (data) {
	// console.log(data)
	data['redarfor']="heade";
	$('body').find('#s_b_head').html(
		frappe.render_template("rows",data)
	);
}
function set_table_body (data,colors,selCo,sel) {
	data['redarfor']="body";
	data['colors']=colors;
	if (!('color' in data)) {	
		data['color']=selCo;
	};
	if (!('priority' in data)) {	
		data['priority']=sel;
	};
	if (!('cone' in data)) {	
		data['cone']='';
	};
	$('body').find('#s_b_body').append(frappe.render_template("rows",data))
	$('body').find(".sl").each(function(index,el){
		$(el).html(index+1);
	});
}
function sumTotal () {
	$('body').find("#s_b_body tr").each(function(index, el) {
		var total_row = 0;
		$(el).find('td input:not([disabled])').each(function(key, val) {
			total_row += ($.isNumeric(parseInt($(val).val()))? parseInt($(val).val()):0) ;
		});
		$(el).find('.cone').val(total_row);
		// Total Row
		$(el).find('td input').each(function(key, val) {
			var total_col = 0
			$('input[name^="'+$(val).attr('name')+'"]').each(function(k, v) {
				total_col += ($.isNumeric(parseInt($(v).val()))? parseInt($(v).val()):0);
			});
			$('input[name="total-'+$(val).attr('name')+'"]').val(total_col)
		});
	});
}
function set_table_footer (data) {
	data['redarfor']="footer";
	if (!('total_cone' in data)) {	
		data['total_cone']='';
	};
	$('body').find('#s_b_footer').html(
		frappe.render_template("rows",data)
	);

	$('body').find('#s_b_body').change(function(event) {
		sumTotal ()
	});
	// add Custon add button
	$('body').find('#sewing_booking_child')
		.after('<button class="btn btn-xs btn-default pull-right">Add New</button>');

	$('button.btn.btn-xs.btn-default.pull-right').click(function(event) {
		cur_frm.cscript.add_new_row(spData);
	});
		
}
// Remove Row
function remove_row (evel) {
	frappe.confirm(__("Are you sure you want to delete the Row?"),
	function() {
			$(evel).parents('tr').remove();
			sumTotal ();
		}
	);	
}
// Set Body value
function body_values () {
	var bodys = []
	$('body').find("#s_b_body tr").each(function(index, el) {
		var row = [];
		$(el).find('input').each(function(i, v) {
			if ($(v).attr('name')!="cone") {
				var temp_row ={}
				temp_row[$(v).attr('name')]=$(v).val();
				temp_row['name']=$(v).attr('name');
				row.push(temp_row);
			};
		});
		var rows = {'format_child':row};
			rows['color'] = $(el).find('.color').val();
			rows['cone'] = $(el).find('.cone').val();
			rows['priority'] = $(el).find('.priority').val();
		bodys.push(rows);		
	});
	return bodys;
}