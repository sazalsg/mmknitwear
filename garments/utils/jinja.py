# Copyright (c) 2015, Techbeeo Software Ltd. and Contributors
# MIT License. See license.txt


from __future__ import unicode_literals
import frappe, json

from frappe.utils.jinja import get_allowed_functions_for_jenv


def get_allowed_functions_for_jenv():
	data = get_allowed_functions_for_jenv()
	data['json']=json
	return data