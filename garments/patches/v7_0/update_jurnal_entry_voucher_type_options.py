from __future__ import unicode_literals
import frappe
import frappe.website.render
from frappe import _

def execute():
    meta = frappe.get_meta('Journal Entry Account').get_field('reference_type')
    frappe.throw(meta)
