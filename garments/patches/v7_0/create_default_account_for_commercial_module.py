from __future__ import unicode_literals
import frappe
from frappe.test_runner import make_test_objects

def execute():
    accounts = [
        ["Commercial(Assets)", "Current Assets", 1, None],
        ["LC But Not Invoiced", "Commercial(Assets)", 0, None],
        ["Commercial(Debtors)", "Accounts Receivable", 0, "Receivable"],
        ["Commercial Temporary", "Temporary Accounts", 0, "Temporary"],
        ["Commercial(Liabilities)", "Current Liabilities", 1, None],
        ["BBLC But Not Invoiced", "Commercial(Liabilities)", 0, None],
        ["Commercial(Creditors)", "Accounts Payable", 0, "Payable"],
        ["Commercial(Expenses)", "Expenses", 1, "Expense Account"],
        ["Commercial(Export)", "Income", 1, "Income Account"]
    ]
    for company, abbr, currency in frappe.db.get_values('Company', {"name": ("!=", "")}, ['name', 'abbr', 'default_currency']):
        account_entry = make_test_objects("Account", [{
                 "doctype": "Account",
             "account_name": account_name,
             "parent_account": parent_account + " - " + abbr,
             "company": company,
             "is_group": is_group,
             "account_type": account_type,
             "account_currency": currency
         } for account_name, parent_account, is_group, account_type in accounts])

    return account_entry