function update_status (event) {
	var val = $(event).data('value');
	var parent = $(event).parents('div[data-name]');
	var name = $(parent).data('name');
	var size_set_name = $(parent).data('parent');
	var size_range = $(parent).find('.size_range').html().trim();

	// Inserting Data In new Doctype Named Size et QC
	var size_breakdown_for_status = $(parent).find('select[name="sbdfs"]').val();
	frappe.call({
		method:"garments.production.page.size_set_approval.size_set_approval.save_data",
		args:{
			doctype:"Size Set QC",
			values:{
				"style":$(parent).find('.style').data('style'),
				"size_range":$(parent).find('.size_range').html().trim(),
				"tco_qty":$(parent).find('input[name="tco_qty"]').val(),
				"remark":$(parent).find('input[name="remark"]').val(),
				"status":$(parent).find('select[name="status"]').val(),
				"attach":$(parent).find('a.attached-file').attr('href'),
				"docstatus":1
			}
		},
		callback:function(date){
		}
	});

	// To change only select field of doctype Size Set
	frappe.call({
		method:"garments.production.page.size_set_approval.size_set_approval.bulk_update",
		args:{
			doctype:"Size Set",
			filters:{
				"style":$(parent).find('.style').data('style'),
				"size_range":$(parent).find('.size_range').html().trim()
			},
			values:{
				"ss_name":$(parent).val(),
				"status":$(parent).find('select[name="status"]').val(),
			}
		},
		callback:function(date){
			//console.log(doc.size_set_name)
		}
	});

	// To Save New Entry In Test Cutting Order Doctype
	if ($(parent).find('select[name="status"]').val()=='Pass') {
		frappe.call({
			method:"garments.production.page.size_set_approval.size_set_approval.save_data",
			args:{
				doctype:"Test Cutting Order",
				values:{
					"ss_name":size_set_name,
					"size_breakdown_for":(size_breakdown_for_status=="Standard Packing"?"Standard Packing":"Solid Packing"),
					"tco_qty":$(parent).find('input[name="tco_qty"]').val(),
					"docstatus":1,
				},
			},
		});
	};
}

frappe.pages['Size Set Approval'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Size Set Approval',
		single_column: true,
	});

	frappe.breadcrumbs.add("production");
	this.page = wrapper.page;
	this.page.set_secondary_action(__("Refresh"), function() {
			frappe.ui.toolbar.clear_cache()
		}, "octicon octicon-sync");

	 page.$style_select = wrapper.page.add_field({
            fieldname: "style",
            fieldtype: "Link",
            options: "Style Name",
            label: __("Reference/Style")
        }).$input
        .change(function() {
        	$('.result').html('');
            $('.no-result').remove();
        	onchange();
        });
	function onchange(){
		var styleName = ""
		    this.page = wrapper.page;
		    var me = this
            styleName = $('input[data-fieldname="style"]').val()

	this.page.list = new frappe.ui.Listing({
		hide_refresh:true,
		page:this.page,
		method:"garments.production.page.size_set_approval.size_set_approval.get_size_set_childs",
		parent:$('<div></div>').appendTo(this.page.main),
		render_row:function (row,data) {
			$(row).css('overflow', 'hidden').html(frappe.render_template('row',data));
			$(row).find('.btn.btn-primary').on('click',function(){
				var tco_qty = $(this).parents('div[data-name]').find('input[name="tco_qty"]').val();
				var status = $(this).parents('div[data-name]').find('select[name="status"]').val();
				if (tco_qty == "" && status == "Pass") {
					msgprint(__("Quantity is required for Pass Status"))
					return
				};
					var me = $(this);
					frappe.confirm(__("Are you realy want to save?"),function (){
						update_status(me)
						$(row).hide();
					});
			});

			// test Cutting mandetory Code here $(row).find('.btn.btn-primary').on('click',function(){
			$(row).find('select[name="status"],[name="tco_qty"]').on('change',function() {
				var status = $(row).find('select[name="status"]').val();
				var input_val = $(row).find('input[name="tco_qty"]').val();
				if (status != 'Fail') {
					if (input_val !="") {
						$(row).find('input[name="tco_qty"]').css("border-color","");
						
					}else{
						$(row).find('input[name="tco_qty"]').css("border-color","red").attr("required", "true");
					};
				}else{
					$(row).find('input[name="tco_qty"]').css("border-color","");
				};
			});
			$(row).find('select[name="status"]').trigger('change');
			// Upload File
			$(row).find('.attachment').click(function(event) {
				var selector = $(this)
				  dialog = frappe.ui.get_upload_dialog({
				    "args": {
				      "folder": frappe.boot.home_folder,
				      "from_form": 1
				    },
				    callback: function(r) {
				    	if (r.file_url) {
					    	$(selector).siblings('.text-ellipsis').show();
					    	$(selector).siblings('.close').show();
					    	$(selector).siblings('.text-ellipsis').children('a.attached-file')
					    		.attr('href', r.file_url)
					    		.html(r.file_name);
					    	$(selector).hide();
				    	};
				    }
				  });
			});
			// Close Attach File
			$(row).find('.attach .close').on('click', function(event) {
				event.preventDefault();
				var selector = $(this)
				$(selector).siblings('.attachment').show();
				$(selector).siblings('.text-ellipsis').hide();
		    	$(selector).siblings('.text-ellipsis').children('a.attached-file')
		    		.attr('href', "")
		    		.html("");
		    	$(selector).hide();
			});

		},
		get_args:function () {
			return {
				filters:{
					'style':page.$style_select.val()
				}
			}			
		}
	});
this.page.main.find('.list-headers').html(frappe.render_template('row_header',{}));
	this.page.list.run();//
}
onchange()
	this.page.main.find('.list-headers').html(frappe.render_template('row_header',{}));
	//this.page.list.run();

};
