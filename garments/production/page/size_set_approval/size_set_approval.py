# Copyright (c) 2015, Techbeeo Software Ltd. and Contributors
# License: See license.txt

from __future__ import unicode_literals
import frappe, ast

no_cache = True

@frappe.whitelist()
def get_size_set_childs (filters, limit_start=0, limit_page_length=20):
	if filters:
		filters = ast.literal_eval(filters)
	sql = frappe.db.sql("SELECT ss.name , ssc.name, ssc.parent, ssc.size, ss.color, ssc.qty, ss.style, ss.pc, ss.size_range, ss.date, ss.pattern_cutting_date, ss.status,ss.ttl\
		FROM `tabSize Set` as ss,`tabSize Set Child` as ssc\
		WHERE ssc.parent = ss.name AND ss.status = 'Pending' AND ss.docstatus = '1' {} GROUP BY ss.size_range, ss.style".format("AND ss.style = '{}'".format(filters['style']) if filters.get('style') else ""),as_dict=1)
	#frappe.throw(sql)
	return sql

@frappe.whitelist()
def save_data(doctype,values, name = None):
	values = ast.literal_eval(values)
	if name:
		doc = frappe.get_doc(doctype,name)
	else:
		doc = frappe.new_doc(doctype)
	if not doc:
		return "Sorry"
	for key,value in values.iteritems():
		doc.set(key,value)
	# frappe.throw(doc)
	doc.save()
	return doc.as_dict()	

@frappe.whitelist()
def bulk_update(doctype,values, name = None, filters=None):
	values = ast.literal_eval(values)
	if name:
		doc = frappe.get_doc(doctype,name)
	elif filters and not name:
		docs = frappe.get_all(doctype,filters=ast.literal_eval(filters))
	else:
		doc = frappe.new_doc(doctype)
	if docs and "doc" not in locals() :
		for doc in docs:
			doc = frappe.get_doc(doctype,doc.name)
			for key,value in values.iteritems():
				doc.set(key,value)
			doc.db_update()
		return doc.as_dict()
	elif "doc" not in locals() and "docs" not in locals():
		return "Sorry"
	for key,value in values.iteritems():
		doc.set(key,value)
	doc.save()
	return doc.as_dict()