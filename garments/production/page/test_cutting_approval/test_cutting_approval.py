# Copyright (c) 2015, Techbeeo Software Ltd. and Contributors
# License: See license.txt
from __future__ import unicode_literals
import frappe, ast

no_cache = True

@frappe.whitelist()
def get_test_cutting_childs (limit_start, limit_page_length, style=None):
	styl = ""
	if style!="":
		styl = "tc.styl='"+style+"' AND"
	sql = frappe.db.sql("SELECT tc.styl, tc.size_range, tc.name, tc.status\
			FROM `tabTest Cutting` as tc\
			LEFT JOIN `tabBundle` as bundle ON bundle.style = tc.styl\
			LEFT JOIN `tabBundle Child` as bc ON bc.parent = bundle.name\
			LEFT JOIN `tabProduction Line Bundle` as plb ON plb.bundle = bc.name\
			WHERE {s} plb.status='Completed' AND bundle.`for`='Test Cutting' AND tc.status ='Pending' GROUP BY tc.size_range, tc.styl". format(s = styl),as_dict=1)
	#frappe.throw(sql)
	return sql

@frappe.whitelist()
def bulk_status_updata(doctype,values, name = None, filters = None):
	values = ast.literal_eval(values)
	if name:
		doc = frappe.get_doc(doctype,name)
		if not doc:
			return "Sorry"
		for key,value in values.iteritems():
			doc.set(key,value)
		doc.save()
	elif filters and not name:
		filters = ast.literal_eval(filters)
		docs = frappe.db.get_values(doctype,filters)
		for name in docs:
			doc = frappe.get_doc(doctype,name[0])
			if not doc:
				return "Sorry"
			for key,value in values.iteritems():
				doc.set(key,value)
			doc.save()
	else:
		doc = frappe.new_doc(doctype)
		if not doc:
			return "Sorry"
		for key,value in values.iteritems():
			doc.set(key,value)
		doc.save()
	return "success"
	