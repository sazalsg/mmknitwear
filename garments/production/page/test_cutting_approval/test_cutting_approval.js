function update_status (event) {
	var val = $(event).data('value');
	var parent = $(event).parents('div[data-name]');
	var name = $(parent).data('name');
	var test_cutting_name = $(parent).data('parent');

	// To change only select field of doctype Size Set
	frappe.call({
		method:"garments.production.page.test_cutting_approval.test_cutting_approval.bulk_status_updata",
		datatype:'JSON',
		args:{
			doctype:"Test Cutting",
			filters: {
				styl:$(parent).find('.style').data('styl'),
				size_range:$(parent).find('.size_range').html().trim()
			},
			values:{
				"status":$(parent).find('select[name="status"]').val(),
			}
		},
		callback:function(data){

		}
	});

	// To Entry In test Cutting QC
	frappe.call({
		method:"garments.production.page.size_set_approval.size_set_approval.save_data",
		async:false,
		args:{
			doctype:"Test Cutting QC",
			values:{
				"test_cutting":$(parent).find('.tc_name').data('test-cutting'),
				"style":$(parent).find('.style').data('sytle'),
				"size_range":$(parent).find('.size_range').html().trim(),
				"remark":$(parent).find('input[name="remark"]').val(),
				"status":$(parent).find('select[name="status"]').val(),
				"attachment":$(parent).find('a.attached-file').attr('href')
				// "docstatus":1
			}
		},
		callback:function(data){
			test_cutting_qc = data.message.name
		}
	});


	//To Save New Entry In Bulk Cutting Order Doctype
	if ($(parent).find('select[name="status"]').val()=='Pass') {
		frappe.call({
			method:"garments.production.page.size_set_approval.size_set_approval.save_data",
			args:{
				doctype:"Bulk Cutting Order",
				values:{
					"test_cutting":$(parent).find('.tc_name').data('test-cutting'),
					"test_cutting_qc":test_cutting_qc,
					"status":'Pending',
					"docstatus":1,
				},
			},
		});
	};

}

frappe.pages['Test Cutting Approval'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Test Cutting Approval',
		single_column: true
	});

	frappe.breadcrumbs.add("production");
	this.page = wrapper.page;
	this.page.set_secondary_action(__("Refresh"), function() {
			frappe.ui.toolbar.clear_cache()
		}, "octicon octicon-sync");
	 page.$style_select = wrapper.page.add_field({
            fieldname: "style",
            fieldtype: "Link",
            options: "Style Name",
            label: __("Reference/Style")
        }).$input
        .change(function() {
        	$('.result').html('');
            $('.no-result').remove();
        	onchange();
        });
	function onchange(){
		var styleName = ""
		    this.page = wrapper.page;
		    var me = this
            styleName = $('input[data-fieldname="style"]').val()

	this.page.list = new frappe.ui.Listing({
		hide_refresh:true,
		page:this.page,
		args: {
			style: styleName
		},
		method:"garments.production.page.test_cutting_approval.test_cutting_approval.get_test_cutting_childs",
		parent:$('<div></div>').appendTo(this.page.main),
		render_row:function (row,data) {
			$(row).css('overflow', 'hidden').html(frappe.render_template('row',data));
			$(row).find('.btn.btn-primary').on('click',function(){
					var me = $(this);
					frappe.confirm(__("Are you realy want to save?"),function (){
						update_status(me)
						$(row).hide();
					});
			});

			$(row).find('select[name="status"]').trigger('change');
			// Upload File
			$(row).find('.attachment').click(function(event) {
				var selector = $(this)
				  dialog = frappe.ui.get_upload_dialog({
				    "args": {
				      "folder": frappe.boot.home_folder,
				      "from_form": 1
				    },
				    callback: function(r) {
				    	if (r.file_url) {
					    	$(selector).siblings('.text-ellipsis').show();
					    	$(selector).siblings('.close').show();
					    	$(selector).siblings('.text-ellipsis').children('a.attached-file')
					    		.attr('href', r.file_url)
					    		.html(r.file_name);
					    	$(selector).hide();
				    	};
				    }
				  });
			});
			// Close Attach File
			$(row).find('.attach .close').on('click', function(event) {
				event.preventDefault();
				var selector = $(this)
				$(selector).siblings('.attachment').show();
				$(selector).siblings('.text-ellipsis').hide();
		    	$(selector).siblings('.text-ellipsis').children('a.attached-file')
		    		.attr('href', "")
		    		.html("");
		    	$(selector).hide();
			});

		}
	});
	this.page.main.find('.list-headers').html(frappe.render_template('row_header',{}));

	this.page.list.run();//

}
onchange()
};
