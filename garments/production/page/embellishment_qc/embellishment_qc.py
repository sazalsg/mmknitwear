from __future__ import unicode_literals
import frappe, ast

# @frappe.whitelist()
# def have_embellishment():
# 	return frappe.db.sql("SELECT style_name FROM `tabStyle Name` WHERE have_embellishment=1")

@frappe.whitelist()
def embellishment_list(style=None):
	if style != "":
		bundle_style="and bun.style='%s'" % style	
	else:
		bundle_style=""
	return frappe.db.sql("SELECT emrc.name, bun.style, emrc.parent, emrc.bundle_child, emr.date,emrc.bundle_no,emrc.quantity,emrc.alter_qty,emrc.receive_qty,emrc.remark\
		FROM `tabEmbellishment Receive` as emr\
		INNER JOIN `tabEmbellishment Receive Child` as emrc ON emr.name = emrc.parent\
		INNER JOIN `tabBundle Child` as bc ON emrc.bundle_child = bc.name\
		INNER JOIN `tabBundle` as bun ON bc.parent = bun.name\
		WHERE emrc.status ='Pending' and  emr.docstatus='1' {} ORDER BY bun.style, CAST(emrc.bundle_no as UNSIGNED) LIMIT {}, {}".format(bundle_style, frappe.form_dict['limit_start'],frappe.form_dict['limit_page_length']),as_dict=1)

	# return frappe.db.sql("SELECT emc.name, bun.style, emc.parent, emc.bundle_child, em.date,emc.bundle_no,emc.quantity,emc.alter_qty,emc.receive_qty,emc.remark\
	# 	FROM tabEmbellishment as em\
	# 	INNER JOIN `tabEmbellishment Child` as emc ON em.name = emc.parent\
	# 	INNER JOIN `tabBundle Child` as bc ON emc.bundle_child = bc.name\
	# 	INNER JOIN `tabBundle` as bun ON bc.parent = bun.name\
	# 	WHERE emc.status ='Pending' and  em.docstatus='1' {} ORDER BY bun.style, CAST(emc.bundle_no as UNSIGNED) LIMIT {}, {}".format(bundle_style, frappe.form_dict['limit_start'],frappe.form_dict['limit_page_length']),as_dict=1)

@frappe.whitelist()
def save_data(doctype,values,name=None):
	
	values = ast.literal_eval(values)
	if name:
		doc = frappe.get_doc(doctype,name)
	else:
		doc = frappe.new_doc(doctype)
	if not doc:
		return "Failed"
	for key,value in values.iteritems():
		doc.set(key,value)
	doc.save()
	return "Success"
	
