frappe.breadcrumbs.add("production");
function update_status (event) {
	var val = $(event).data('value');
	var parent = $(event).parents('div[data-name]');
	var name = $(parent).data('name');
	frappe.call({
		method:"garments.production.page.embellishment_qc.embellishment_qc.save_data",
		args:{
			doctype:"Embellishment QC",
			values:{
				"bundle_no":$(parent).data('child-bundle'),
				"alter_qty":$(parent).find('input[name="alter_qty"]').val(),
				"receive_qty":$(parent).find('.re_qty').text(),
				"status":($(parent).find('select[name="status"]').val() !==""?$(parent).find('select[name="status"]').val():"Pending"),
				"comment":$(parent).find('input[name="remark"]').val(),
			}
		},
		callback:function (data) {
			console.log(data);
		}
	});

	frappe.call({
		method:"garments.production.page.embellishment_qc.embellishment_qc.save_data",
		args:{
			doctype:"Embellishment Receive Child",
			name:name,
			values:{
				"status":($(parent).find('select[name="status"]').val() !==""?$(parent).find('select[name="status"]').val():"Pending"),
			}
		},
		callback:function (data) {
			msgprint("Done!");
		}
	});
}


frappe.pages['embellishment-qc'].on_page_load = function(wrapper) {
    //~~~~~~~~
    var page = frappe.ui.make_app_page({
        parent: wrapper,
        title: 'Embellishment QC',
        single_column: true
    });

    page.main.css({
        "min-height": "300px",
        "padding-bottom": "25px"
    });
    frappe.breadcrumbs.add(frappe.breadcrumbs.last_module || "production");

    
        ///////////////////////
    page.$style_select = wrapper.page.add_field({
            fieldname: "style",
            fieldtype: "Link",
            options: "Style Name",
            filters:{
                "have_embellishment":"1"
                    },
            label: __("Reference/Style")
        }).$input
        .change(function() {
        	$('.result').html('');
            $('.no-result').remove();
        	onchange();
        });
	function onchange(){
		var styleName = ""
		    this.page = wrapper.page;
		    var me = this
            styleName = $('input[data-fieldname="style"]').val()
            me.page.list = new frappe.ui.Listing({

                hide_refresh: true,
                page: me.page,
                method: "garments.production.page.embellishment_qc.embellishment_qc.embellishment_list",
                args: {
                    style: styleName

                },
                parent: $('<div></div>').appendTo(me.page.main),
                render_row: function(row, data) {

                    $(row).css('overflow', 'hidden').html(frappe.render_template('row', data));

                    if ($(row).find('.re_qty').html() == '0') {
                        $(row).find('.re_qty').html(parseInt($(row).find('.qty').html()));

                    }
                    $(row).find('[name="alter_qty"]').on('change', function(event) {
                        event.preventDefault();
                        var qty = $(row).find('.qty').text();
                        var re_qty = $(row).find('.re_qty').text();
                        var value = $(this).val();
                        if (parseInt(qty) < value || value < 0 || qty == value) {
                            msgprint(__("Alter quantity must be less than quantity"));
                            $(this).val("0");
                            return
                        };
                        if ($.isNumeric(value) == false) {
                            msgprint(__("Alter quantity must be a number!"));
                            $(this).val("0");
                            return
                        }
                        if ($(this).val() !== "") {
                            $(row).find('.re_qty').html(parseInt(qty) - parseInt($(this).val()));
                        } else {
                            $(row).find('.re_qty').html(parseInt(qty));
                        }
                    });

                    $(row).find('.btn.btn-primary').on('click', function() {
                        var status = $(row).find('select[name="status"]').val();
                        var total = parseInt($(row).find('[name="alter_qty"]').val()) + parseInt($(row).find('.re_qty').html().trim());
                        var qty = $(row).find('.qty').html().trim();
                        if (status != "") {
                            var me = $(this);
                            frappe.confirm(__("Are you realy want to save?"), function() {
                                update_status(me)
                                $(row).hide();
                            });

                        } else {
                            msgprint(__("Make sure you have changed in action"))
                        };
                    });
                }
            });
            me.page.list.run()
        
	}
onchange()
    this.page.main.find('.list-headers').html(frappe.render_template('row_header', {

  }));
};

frappe.pages['embellishment-qc'].on_page_show  = function(wrapper){
    if (frappe.route_options) {
        if ('style' in frappe.route_options) {
            page.$style_select.val(frappe.route_options.style);
            page.$style_select.trigger('change')
        };
        frappe.route_options = null
    };
}