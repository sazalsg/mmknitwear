frappe.breadcrumbs.add(frappe.breadcrumbs.last_module || "production");
frappe.pages['daily-overtime-report'].on_page_load = function(wrapper) {
    var page = frappe.ui.make_app_page({
        parent: wrapper,
        title: 'Daily Overtime Entry',
        single_column: true
    });
    page.main.css({
        "min-height": "300px",
        "padding-bottom": "25px"
    });
    page.$style_select = wrapper.page.add_field({
        "fieldname": "worker",
        "fieldtype": "Link",
        "options": "Employee",
        "label": __("Worker Name")
    }).$input
        .change(function() {
            $('.result').html('');
            $('.no-result').remove();
            onchange();
        });
    page.$style_select = wrapper.page.add_field({
        "fieldname": "nature_of_work",
        "fieldtype": "Link",
        "options": "Nature of Work",
        "label": __("Nature of Work")
    }).$input
        .change(function() {
            $('.result').html('');
            $('.no-result').remove();
            onchange();
        });
    page.$style_select = wrapper.page.add_field({
        "fieldname": "date",
        "fieldtype": "Date",
        "options": "Date",
        "label": __("Date"),
        "default": frappe.datetime.get_today()
    }).$input
        .change(function() {
            $('.result').html('');
            $('.no-result').remove();
            onchange();
        });
    page.$style_select = wrapper.page.add_field({
        "fieldname": "create",
        "fieldtype": "Button",
        "options": "Button",
        "label": __("Create")
    })
    page.$style_select = wrapper.page.add_field({
        "fieldname": "cancel",
        "fieldtype": "Button",
        "options": "Button",
        "label": __("Cancel All")
    })
    page.$style_select = wrapper.page.add_field({
        "fieldname": "reset",
        "fieldtype": "Button",
        "options": "Button",
        "label": __("Reset All")
    })
    page.$style_select = wrapper.page.add_field({
        "fieldname": "save_all",
        "fieldtype": "Button",
        "options": "Button",
        "label": __("Save All")
    })

    function minutesToHour(a) {
        var hours = Math.trunc(a / 60);
        var minutes = a % 60;
        return hours + ":" + minutes;
    }
    var overtime_val = {};
    function toBeSave(name, overtime) {
        overtime_val[name] = overtime
    }
    var currentEmp = {};
    function onchange() {

        var worker_id;
        var nature_of_work;
        var date;
        this.page = wrapper.page;
        var me = this;
        worker_id = $('input[data-fieldname="worker"]').val();
        nature_of_work = $('input[data-fieldname="nature_of_work"]').val();
        date = $('input[data-fieldname="date"]').val();
        me.page.list = new frappe.ui.Listing({

            hide_refresh: true,
            page: me.page,
            method: "garments.production.page.daily_overtime_report.daily_overtime_report.overtime_report",
            args: {
                worker: worker_id,
                work_nature: nature_of_work,
                date: date

            },
            parent: $('<div></div>').appendTo(me.page.main),
            render_row: function(row, data) {
                $(row).css('overflow', 'hidden').html(frappe.render_template('row', data));
                $(row).find('input[data-name="overtime"]').on('change', function(event) {

                    $(row).attr('data-unsaved', '1');
                    var overtime = $(row).find('input[data-name="overtime"]').val()
                    
                    var name = $(row).find('[data-name="name"]').text()
                    
                    var time = minutesToHour(overtime)
                    toBeSave(name, time)
                    $(row).find('input[data-name="overtime"]').val(time)
                });
                var name = $(row).find('[data-name="name"]').text();
                var empId = $(row).find('[data-name="worker_id"]').text();
                currentEmp[name] = empId;

                $(row).find('button[data-fieldname="save"]').on("click", function() {
                    var name = $(row).find('[data-name="name"]').text()
                    var overtime = $(row).find('input[data-name="overtime"]').val()
                    frappe.call({
                        method: "garments.production.page.daily_overtime_report.daily_overtime_report.single_row_update",
                        args: {
                            name: name,
                            overtime: overtime
                        },
                        callback: function(data) {
                            msgprint("Successfully Updated!")
                            $('.no-result').remove();
                            $('.result').html('');
                            onchange()
                        }
                    })
                })

            }
        });
        this.page.main.find('.list-headers').html(frappe.render_template('row_header'));
        me.page.list.run()
    }
    onchange();

    page.main.find('button[data-fieldname="create"]').on("click", function() {
        date = $('input[data-fieldname="date"]').val();
        frappe.call({
            method: "garments.production.page.daily_overtime_report.daily_overtime_report.get_overtime",
            args: {
                date: date
            },
            callback: function(data) {
                if (data['message'] == 1) {
                    msgprint("Already have in this date!")
                } else if (data['message'] == 2) {
                    msgprint("No production data found in this date!")
                } else {
                    msgprint("Successfully Created!")
                    $('.no-result').remove();
                    $('.result').html('');
                    onchange()
                }
            }
        });
    });
    page.main.find('button[data-fieldname="cancel"]').on("click", function() {
        date = $('input[data-fieldname="date"]').val();
        frappe.call({
            method: "garments.production.page.daily_overtime_report.daily_overtime_report.cancel_all_overtime",
            args: {
                date: date
            },
            callback: function(data) {
                msgprint("Successfully Cancelled!")
                $('.no-result').remove();
                $('.result').html('');
                onchange()
            }
        });
    });
    page.main.find('button[data-fieldname="save_all"]').on("click", function() {

        if ($.isEmptyObject(overtime_val)) {
            msgprint("Nothing to save!")
        } else {
            var overtime_encoded = JSON.stringify(overtime_val)
            frappe.call({
                method: "garments.production.page.daily_overtime_report.daily_overtime_report.save_all",
                args: {
                    values: overtime_encoded
                },
                callback: function(data) {
                    overtime_val = {}
                    msgprint("Successfully Saved!");
                    $('.no-result').remove();
                    $('.result').html('');
                    onchange()
                }
            });
        }
    });
    page.main.find('button[data-fieldname="reset"]').on("click", function() {
        var date = $('input[data-fieldname="date"]').val();
        var emp = JSON.stringify(currentEmp);
        frappe.call({
            method: "garments.production.page.daily_overtime_report.daily_overtime_report.reset_overtime",
            args: {
                date: date,
                emp: emp
            },
            callback:function(data){
                currentEmp = {}
                msgprint("Successfully Reseted!");
                $('.no-result').remove();
                $('.result').html('');
                onchange()
            }
        });
    });
}