from __future__ import unicode_literals
import frappe, ast, datetime, json

@frappe.whitelist()
def get_overtime(date=None):

	if date !="":

		dateNtime = datetime.datetime.strptime(date, "%d-%m-%Y").strftime("%Y-%m-%d")

		condition = "WHERE pwi.creation LIKE '%"+dateNtime+"%' AND plw.creation LIKE '%"+dateNtime+"%'"
		val = frappe.db.sql("""SELECT pwi.worker_id, pwi.worker_name, pls.line_no, plw.enabled, DATE_FORMAT(max(pwi.creation), '%Y-%m-%d') as date, pwi.nat_work,plw.target, TIMESTAMPDIFF(MINUTE, min(pwi.creation), max(pwi.creation)) as fillup_minutes, CONCAT(FLOOR(TIMESTAMPDIFF(MINUTE, min(pwi.creation), max(pwi.creation))/60),': ',MOD(TIMESTAMPDIFF(MINUTE, min(pwi.creation), max(pwi.creation)),60),'') as fillup_hours_minutes , sum((pwi.amount)) as fillup_target,target*target_hour as total_target, plw.target_hour,plw.target_hour*60 as target_minutes, '0' as overtime
		FROM `tabProduction Worker Input` as pwi 
		INNER JOIN `tabProduction Line` as pl ON pwi.parent=pl.name
		INNER JOIN `tabProduction Line Worker` as plw ON plw.worker_id=pwi.worker_id
		INNER JOIN `tabProduction Line Setup` as pls ON pls.name=plw.parent
		{cond}
		GROUP by plw.worker_id,plw.nat_work""".format(cond=condition),as_dict=1)
		ret = val
		for x in range(len(ret)):
			total_target = ret[x]['total_target']
			fillup_target = ret[x]['fillup_target']
			target_minutes = ret[x]['target_minutes']
			fillup_minutes = ret[x]['fillup_minutes']
			overtime_minute = fillup_minutes - target_minutes
			if fillup_target > total_target:
				h, m = divmod(overtime_minute, 60)
				if fillup_minutes > target_minutes:
					ret[x]['overtime'] = "%d:%02d" % (h, m)
		
		checkDate = frappe.db.get_value("Daily Overtime", {"date":dateNtime,"status":0}, ["status"], as_dict=1)

		if checkDate != None:
			return 1 #Already have in this date
		elif len(ret) == 0:
			return 2 #No Production data found in this date
		else:
			for key in ret:
				insertIntoDailyOvertime = frappe.get_doc({ 
				"doctype": "Daily Overtime",
				"worker_id": key['worker_id'],
				"line_no": key['line_no'],
				"worker_name": key['worker_name'],
				"date":	key['date'],
				"nature_of_work": key['nat_work'],
				"hourly_target": key['target'],
				"total_hour": key['target_hour'],
				"fillup_target": key['fillup_target'],
				"total_target": key['total_target'],
				"fillup_time": key['fillup_hours_minutes'],
				"overtime": key['overtime'],
				"status":0
				}).insert(ignore_permissions=True) 		 
				insertIntoDailyOvertime.save()

	else:
		frappe.msgprint("Please select a date.")	
@frappe.whitelist()
def overtime_report(worker=None, work_nature=None, date=None):
	dateNtime = ""
	if date !="":
		dateNtime = datetime.datetime.strptime(date, "%d-%m-%Y").strftime("%Y-%m-%d")
	if worker !="" and work_nature !="" and date !="":
		condition="WHERE worker_id='"+worker+"' AND date LIKE '%"+dateNtime+"%' AND nature_of_work='"+work_nature+"' AND status=0"
	elif worker !="" and work_nature !="" and date=="" :
		condition="WHERE worker_id='%s' AND nature_of_work='%s' AND status=0" % (worker,work_nature)
	elif worker !="" and work_nature=="" and date !="":
		condition="WHERE worker_id='"+worker+"' AND date LIKE '%"+dateNtime+"%' AND status=0"
	elif worker !="" and work_nature=="" and date=="":
		condition="WHERE worker_id='%s' AND status=0" % worker
	elif worker =="" and work_nature!="" and date !="":
		condition="WHERE nature_of_work='"+work_nature+"' AND date LIKE '%"+dateNtime+"%' AND status=0"
	elif worker =="" and work_nature=="" and date !="":
		condition="WHERE date LIKE '%"+dateNtime+"%' AND status=0"
	elif worker =="" and work_nature !="":
		condition="WHERE nature_of_work='%s' AND status=0" % work_nature	
	else:
		condition=""
	return frappe.db.sql("""SELECT * FROM `tabDaily Overtime`
	{cond}""".format(cond=condition),as_dict=1)

@frappe.whitelist()
def reset_overtime(date, emp):
	if date !="":
		dateNtime = datetime.datetime.strptime(date, "%d-%m-%Y").strftime("%Y-%m-%d")
		json_string = json.loads(emp)
		condition = "WHERE pwi.creation LIKE '%"+dateNtime+"%' AND plw.creation LIKE '%"+dateNtime+"%'"
		val = frappe.db.sql("""SELECT pwi.worker_id, pls.line_no, pwi.worker_name, plw.enabled, DATE_FORMAT(max(pwi.creation), '%Y-%m-%d') as date, pwi.nat_work,plw.target, TIMESTAMPDIFF(MINUTE, min(pwi.creation), max(pwi.creation)) as fillup_minutes, CONCAT(FLOOR(TIMESTAMPDIFF(MINUTE, min(pwi.creation), max(pwi.creation))/60),': ',MOD(TIMESTAMPDIFF(MINUTE, min(pwi.creation), max(pwi.creation)),60),'') as fillup_hours_minutes , sum((pwi.amount)) as fillup_target,target*target_hour as total_target, plw.target_hour,plw.target_hour*60 as target_minutes, '0' as overtime
		FROM `tabProduction Worker Input` as pwi 
		INNER JOIN `tabProduction Line` as pl ON pwi.parent=pl.name
		INNER JOIN `tabProduction Line Worker` as plw ON plw.worker_id=pwi.worker_id
		INNER JOIN `tabProduction Line Setup` as pls ON pls.name=plw.parent
		{cond}
		GROUP by plw.worker_id,plw.nat_work""".format(cond=condition),as_dict=1)
		ret = val
		for x in range(len(ret)):
			total_target = ret[x]['total_target']
			fillup_target = ret[x]['fillup_target']
			target_minutes = ret[x]['target_minutes']
			fillup_minutes = ret[x]['fillup_minutes']
			overtime_minute = fillup_minutes - target_minutes
			if fillup_target > total_target:
				h, m = divmod(overtime_minute, 60)
				if fillup_minutes > target_minutes:
					ret[x]['overtime'] = "%d:%02d" % (h, m)
		
		for x in ret:
			for y in json_string:
			 	if json_string[y] == x['worker_id']:
			 		for doc in frappe.get_all("Daily Overtime", filters={"name": y,"status": 0}):
			 			doc = frappe.get_doc("Daily Overtime", doc.name)
			 			doc.line_no = x['line_no']
			 			doc.nature_of_work = x['nat_work']
						doc.hourly_target = x['target']
						doc.total_hour = x['target_hour']
						doc.fillup_target = x['fillup_target']
						doc.total_target = x['total_target']
						doc.fillup_time = x['fillup_hours_minutes']
						doc.overtime = x['overtime']
						doc.status = 0
						doc.save()
			else:
				checkWorker = frappe.db.get_value("Daily Overtime", {"date":dateNtime, "worker_id":x['worker_id'],"status":0}, ["status"], as_dict=1)
				if checkWorker =="":
					insertIntoDailyOvertime = frappe.get_doc({ 
					"doctype": "Daily Overtime",
					"worker_id": x['worker_id'],
					"line_no": x['line_no'],
					"worker_name": x['worker_name'],
					"date":	x['date'],
					"nature_of_work": x['nat_work'],
					"hourly_target": x['target'],
					"total_hour": x['target_hour'],
					"fillup_target": x['fillup_target'],
					"total_target": x['total_target'],
					"fillup_time": x['fillup_hours_minutes'],
					"overtime": x['overtime'],
					"status":0
					}).insert(ignore_permissions=True) 		 
					insertIntoDailyOvertime.save()
@frappe.whitelist()
def cancel_all_overtime(date=None):
	if date !="":
		dateNtime = datetime.datetime.strptime(date, "%d-%m-%Y").strftime("%Y-%m-%d")

		for doc in frappe.get_all("Daily Overtime", filters={"date": dateNtime, "status": 0}):
			doc = frappe.get_doc("Daily Overtime", doc.name)
			doc.status = 1
			doc.save()

@frappe.whitelist()
def save_all(values=None):
	json_string = json.loads(values)
	for x in json_string:
		for doc in frappe.get_all("Daily Overtime", filters={"name": x}):
			doc = frappe.get_doc("Daily Overtime", doc.name)
			doc.overtime = json_string[x]
			doc.save()

@frappe.whitelist()
def single_row_update(name=None, overtime=None):
	if name !="":
		docname = "Daily Overtime"
		for doc in frappe.get_all(docname, filters={"name":name}):
			doc = frappe.get_doc(docname, doc.name)
			doc.overtime = overtime
			doc.save()
