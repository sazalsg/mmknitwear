// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Test Cutting Order"] = {
	"filters": [
		{
			fieldname:"ss.style",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 1,
			// default: frappe.defaults.get_user_default("style")
		},
		{
			fieldname:"tco.creation",
			label: __("Date"),
			fieldtype: "Date",
			reqd: 0,
			// default: frappe.defaults.get_user_default("size_range")
		}
	]
}
