# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
import json

def execute(filters=None):
	#columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Reference/Style")+":Link/Style Name:98",
		_("Buyer")+":Link/Customer:98",
		_("Size Format")+":Link/Size Format:120",
		_("Test Cutting Order Quantity")+":Int:120",
		_("Date")+":Date:150",
		_("Creator")+":Data:150",
		_("Attachment")+":Image:150",
		_("Action")+":Data:200",
	]

def data(filters):
	fields = ['ss.style', 'bi.byr', 'ss.size_range', 'tco.tco_qty', 'tco.creation', 'tco.modified_by','qc.attach','tco.name']
	condition = ''
	if filters:
		item = 0
		for key,val in filters.iteritems():
			if key == 'tco.creation':
				condition+= "{con} DATE({key}) = DATE('{val}')".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
				item = item+1
				continue
			condition+= "{con} {key} = '{val}'".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
			item = item+1
	
	rows = frappe.db.sql("""select {fields} 
		FROM  `tabTest Cutting Order` as tco
		LEFT JOIN `tabSize Set` as ss ON tco.ss_name = ss.name
		LEFT JOIN `tabBuyer Inquiry` as bi ON ss.style = bi.reference
		LEFT JOIN `tabSize Set QC` as qc ON qc.style = ss.style
		{wh} GROUP BY ss.style, ss.size_range""".format(fields=','.join(fields),wh=condition),as_list=1)
	
	for x in xrange(len(rows)):
		dd = {
			"oparetion":"NewDoc",
			"doctype":"Test Cutting",
			"args":{
				"styl":rows[x][0],
				"test_cutting_order":rows[x][-1]
			}
		}
		del rows[x][-1]
		rows[x][-1] = '<a target="__blank" href="{0}"><i class="icon-paper-clip"></i> {0}</a>'.format(rows[x][-1])
		rows[x].append('<button class="btn btn-xs btn-info" onclick=\'javascript: new techbeeoReport.ButtonControl({0})\'>Make Test Cutting</button>'.format(json.dumps(dd)))
	return rows