# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _, throw
from frappe.utils.data import parse_val
from garments.production.report.hourly_production_report.hourly_production_report import hour_range, data

def execute(filters=None):
	# data = []
	return columns(filters), datas(filters)

def columns(filters):
	columns = []
	columns.append(_("Alter Type")+":Link/Alter Type:200")
	hours = hour_range(filters,"Production Alter","date")
	for index, hour in enumerate(hours):
		columns.append(str(hour)+"-"+str(hour+1)+":Int:90")

	columns.append(_("TTL")+":Int:100")
	return columns


def datas(filters):
	where="WHERE style='{}'".format(filters['style'])
	where+=" AND DATE(date)=DATE('{}')".format(filters['date'])

	query = frappe.db.sql(""" SELECT alter_type, SUM(qty) as qty, HOUR(date) as date FROM `tabProduction Alter`
		{wh} GROUP BY alter_type, HOUR(date)""".format(wh = where),as_dict=1)
	hours = hour_range(filters,"Production Alter","date")
	return rearange_data(query,hours)



def rearange_data(datas,hours):
	arange_data = {}
	rows = []
	for data in datas:
		if not data.alter_type in arange_data:
			arange_data[data.alter_type] = [data.alter_type]
			for hour in enumerate(hours):
				arange_data[data.alter_type].append(0)
		for index,hour in enumerate(hours):
			if data.date == hour:
				arange_data[data.alter_type][index+1] = data.qty

	for row in arange_data:
		total = sum(arange_data[row][1:])
		arange_data[row].append(total)
		rows.append(arange_data[row])

	return rows