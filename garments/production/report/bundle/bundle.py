# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from garments.production.report.test_cutting_approval.test_cutting_approval import conditions
def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Date")+":Datetime:200",
		_("Bundle No")+":Data:150",
		_("Color")+":Link/Color:200",
		_("Size")+":Link/Size:150",
		_("Total")+":Int:178"
	]


def data(filters):
	fields = ['B.date', 'BC.bundle_no', 'BC.color', 'BC.size', 'BC.total_size']
	
	return  frappe.db.sql("""select {fields} 
		FROM `tabBundle Child` as BC
		LEFT JOIN `tabBundle` as B
		ON B.name = BC.parent
		{wh}""".format(fields=','.join(fields),wh=conditions(filters,['style'])),as_list=1)