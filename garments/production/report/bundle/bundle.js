// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Bundle"] = {
	"filters": [
		{
			fieldname:"B.style",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 1,
		},
		{
			fieldname:"B.date",
			label: __("Date"),
			fieldtype: "Date",
			reqd: 0,
		}
	]
}
