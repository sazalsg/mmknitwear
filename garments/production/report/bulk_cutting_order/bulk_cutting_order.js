// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Bulk Cutting Order"] = {
	"filters": [
		{
			fieldname:"tc.styl",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 0
//			default: frappe.defaults.get_user_default("style")
		},
		{
			fieldname:"bco.creation",
			label: __("Date"),
			fieldtype: "Date",
		}

	]
}
