# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
import json

def execute(filters=None):
	return columns(filters), data (filters)

def columns(filters):
	return [
		_("Name")+":Data:160",
		_("Date")+":Date:160",
		_("Reference/Style")+":Link/Style Name:160",
		_("Buyer")+":Data:160",
		# _("Size Set")+":Data:140",
		_("Quantity")+":Data:140",
		_("Status")+":Data:120",
		_("Action")+":Data:200"
	]
def data (filters):
	fields = ['bco.name','tc.creation','tc.styl','tc.buyer','tc.ttl','bco.status','bco.name'] #,'tc.size_set'
	condition=''
	if filters:
		i=0;
		for key,val in filters.iteritems():
			if key == "bco.creation":
				if i!=0:
					condition+= "AND DATE({key}) = DATE('{val}')".format(key=key,val=val)
				else:
					condition+= "WHERE DATE({key}) = DATE('{val}')".format(key=key,val=val)
				i = i+2
				continue

			if i!=0:
				condition+= "AND {key} = '{val}'".format(key=key,val=val)
			else:
				condition+= "WHERE {key} = '{val}'".format(key=key,val=val)
			i = i+2

	rows = frappe.db.sql("""SELECT {fields} 
		FROM `tabBulk Cutting Order` AS bco
		INNER JOIN `tabTest Cutting` AS tc ON bco.test_cutting=tc.name
		{wh}""".format(fields=','.join(fields),wh=condition), as_list=1)

	for x in xrange(len(rows)):
		dd = {
			"oparetion":"NewDoc",
			"doctype":"Bulk Cutting",
			"args":{
				"style":rows[x][2],
				# "test_cutting_order":rows[x][-1]
			}
		}
		del rows[x][-1]
		rows[x].append('<button class="btn btn-xs btn-info" onclick=\'javascript: new techbeeoReport.ButtonControl({0})\'>Make Bulk Cutting</button>'.format(json.dumps(dd)))

	return rows