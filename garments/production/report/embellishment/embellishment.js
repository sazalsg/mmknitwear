// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Embellishment"] = {
	"filters": [
		{
			fieldname:"emb.style",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 0,
			default: frappe.defaults.get_user_default("style")
		},
		// {
		// 	fieldname:"emb.date",
		// 	label: __("Date"),
		// 	fieldtype: "Date",
		// 	default: get_today()
		// },
		{
			fieldname:"embc.status",
			label: __("Status"),
			fieldtype: "Select",
			options: ""+NEWLINE+"Pending"+NEWLINE+"Pass"+NEWLINE+"Fail",
			reqd: 0,
		}

	]
}
