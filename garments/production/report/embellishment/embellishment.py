# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
def execute(filters=None):
	#columns, data = [], []
	return columns(filters), data (filters)

def columns(filters):
	return [
		_("Bundle No")+":date:160",
		_("Reference/Style")+":Link/Style Name:98",
		_("Type")+":date:120",
		_("Color")+":Data:120",
		_("Size")+":Data:120",
		#_("Quantity")+":Int:100",
		_("Alter Quantity")+":Int:100",
		_("Receive Quantity")+":Int:120",
		_("Status")+":Data:100"
	]
def data (filters):
	fields = ['embc.bundle_child','emb.style','emb.type','bnc.color','bnc.size','embqc.alter_qty','embqc.receive_qty', 'embc.status']
	#,'embqc.quantity'
	condition=''

	if filters:
		i=0;
		for key,val in filters.iteritems():
			if i!=0:
				condition+= "AND {key} = '{val}'".format(key=key,val=val)
			else:
				condition+= "WHERE {key} = '{val}'".format(key=key,val=val)
			i = i+2


	return  frappe.db.sql("""select {fields} 
		FROM `tabEmbellishment QC` as embqc
		RIGHT JOIN `tabEmbellishment Child` as embc ON embqc.bundle_no = embc.bundle_child
        LEFT JOIN `tabBundle Child` as bnc ON embc.bundle_child = bnc.name
        LEFT JOIN `tabEmbellishment` as emb ON emb.name=embc.parent
		{wh} GROUP BY embc.bundle_child""".format(fields=','.join(fields),wh=condition),as_list=1)
