# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _, msgprint, throw
from frappe.utils import getdate

def execute(filters=None):
	# columns, data = [], []
	return columns(filters), data(filters)


def columns(filters):
	columns = []
	columns.append(_("WOrker ID")+":Link/Employee:120")
	columns.append(_("WOrker Name")+":Data:130")
	columns.append(_("Nature of Work")+":Data:120")
	columns.append(_("Target")+":Int:100")
	hours = hour_range(filters)
	for index, hour in enumerate(hours):
		columns.append(str(hour)+"-"+str(hour+1)+":Int:100")

	columns.append(_("G.TTL")+":Int:100")
	return columns

def data(filters):
	fields = [
	'pwi.worker_id','pwi.worker_name','pwi.nat_work','plw.target','HOUR(pwi.creation) as hour','pwi.action',
	"SUM(CASE WHEN pwi.action = 'plus' THEN amount ELSE 0 END) AS plus",
	"SUM(CASE WHEN pwi.action = 'minus' THEN amount ELSE 0 END) AS minus"
	]
	datas = frappe.db.sql("""select {field} 
		from `tabProduction Worker Input` as pwi
		LEFT JOIN `tabProduction Line` as pl
		ON pl.name = pwi.parent
		LEFT JOIN `tabProduction Line Worker` as plw
		ON plw.parent = pl.production_line_setup
		LEFT JOIN `tabProduction Line Setup` as pls
		ON pls.name = pl.production_line_setup
		where DATE(pwi.creation) = DATE('{date}') and pls.style = '{style}' GROUP BY pwi.worker_id,hour""".format(field= ','.join(fields) , date = filters['date'], style = filters['style']),as_dict=1)

	return row_render(datas,filters)



def get_min_max_value(select,filters,doctype,field):
	return frappe.db.sql_list(""" select {select}
		from `tab{doc}` as pwi where DATE({field}) = DATE('{date}')""".format(select= select, doc=doctype , date = filters['date'], field=field))

def hour_range(filters,doctype="Production Worker Input",field='creation'):
	start = get_min_max_value("MIN(HOUR({}))".format(field),filters,doctype,field)
	end = get_min_max_value("MAX(HOUR({}))".format(field),filters,doctype,field)
	hour_list = []
	if start[0] and end[0]:
		for x in xrange(int(start[0]),int(end[0])+1):
			hour_list.append(x)

	return hour_list


def row_render(datas,filters):
	rows = []
	temp_rows = {}
	hours = hour_range(filters)
	for data in datas:
		if data.worker_id not in temp_rows:
			temp_rows[data.worker_id]={}
			for key,val in data.iteritems():
				if key not in ['hour']:
					temp_rows[data.worker_id][key]=val
				elif key == "hour":
					temp_rows[data.worker_id]['hour-'+str(val)] = data.plus-data.minus
		else:
			if data.hour:
				temp_rows[data.worker_id]['hour-'+str(data.hour)] = data.plus-data.minus

	for data in temp_rows:
		satatic_col= ['worker_id','worker_name','nat_work','target']
		tmp_row = []
		for st_col in satatic_col:
			tmp_row.append(temp_rows[data][st_col])
		# tmp_row.append("")
		gttl = 0
		for hour in hours:
			if ("hour-"+str(hour)) in temp_rows[data]:
				tmp_row.append(temp_rows[data]["hour-"+str(hour)]/3)
				gttl += temp_rows[data]["hour-"+str(hour)]/3
			else:
				tmp_row.append(0)
		tmp_row.append(gttl)
		rows.append(tmp_row)
	return rows

