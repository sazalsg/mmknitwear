// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Hourly Production Report"] = {
	"filters": [
		{
			fieldname:"style",
			fieldtype:"Link",
			label:"Reference/Style",
			options:"Style Name",
			reqd:1
		},
		{
			fieldname:"date",
			fieldtype:"Date",
			label:"Day",
			reqd:1,
			default:frappe.datetime.get_today()
		}
	]
}
