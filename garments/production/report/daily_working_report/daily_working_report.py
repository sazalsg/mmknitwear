# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _, throw
from frappe.utils.data import parse_val
from garments.production.report.hourly_production_report.hourly_production_report import hour_range, data

def execute(filters=None):
	# data = []
	return columns(filters), datas(filters)

def columns(filters):
	columns = []
	columns.append(_("WOrker ID")+":Link/Employee:120")
	columns.append(_("WOrker Name")+":Data:130")
	columns.append(_("Nature of Work")+":Data:120")
	columns.append(_("Target")+":Int:100")
	hours = hour_range(filters)
	for index, hour in enumerate(hours):
		columns.append("<span class='col-xs-12 text-center report-border-bottom'>"+str(hour)+"-"+str(hour+1)+"</span><span class='col-xs-4 report-col-separetor'> OK</span>\
			<span class='col-xs-4 report-col-separetor'>ALTER</span><span class='col-xs-4 report-col-separetor'>SP</span>:Data:200")

	columns.append(_("G.TTL")+":Int:100")
	return columns


def datas(filters):
	datas = []
	dynamic_col_start = 4
	dynamic_col_end = 10
	hours = hour_range(filters)
	alt = 0
	for item in data(filters):
		item = parse_val(item)
		alters = get_alter(filters['date'],item[0])
		for index,val in enumerate(item):
			if index >= dynamic_col_start and index < dynamic_col_end:
				for alter in alters:
					if alter['hour'] in hours:
						for i,h in enumerate(hours):
							if i == index-dynamic_col_start:
								alt = alter['qty']
				item[index]="<span class='{cm_class}'>{ok}</span><span class='{cm_class}'>{alter}</span> <span class='{cm_class}'>{sp}</span>".format(ok=item[index], alter=(alt or 0), sp=3, cm_class="col-xs-4 report-col-separetor")
		datas.append(item)

	return datas



def get_alter(date,worker):
	return frappe.db.sql("select hour(creation) as hour, sum(qty) as qty from `tabProduction line Bundle Alter`\
		where worker_id = '{worker_id}' and date(creation) = date('{date}')\
		group by hour(creation)".format(worker_id=worker,date=date),as_dict=1)