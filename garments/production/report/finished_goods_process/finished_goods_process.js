// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Finished Goods Process"] = {
	"filters": [
		{
			fieldname:"f.stl",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 0,
		},
		{
			fieldname:"f.typ",
			label: __("Type"),
			fieldtype: "Select",
			options: ""+NEWLINE+"Iron"+NEWLINE+"Poly"+NEWLINE+"Carton",
			reqd: 0,
		}
	]
}
