# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Date")+":Date:100",
		_("Worker Name")+":Data:200",
		_("Worker ID")+":Data:100",
		_("Type")+":Data:150",
		_("Reference/Style")+":Data:150",
		_("Color")+":Link/Color:100",
		_("Size")+":Link/Size:100",
		_("Total")+":Int:100"
	]


def data(filters):
	fields = ['DATE(f.date)','c.worker_name','c.worker','f.typ','f.stl','c.color','c.size','f.ttl']
	
	condition=''

	if filters:
		i=0;
		for key,val in filters.iteritems():
			if key == "style":
				if i!=0:
					condition+= "AND {key} = '{val}'".format(key=key,val=val)
				else:
					condition+= "WHERE {key} =  '{val}'".format(key=key,val=val)
				i = i+2
				continue
			if i!=0:
				condition+= "AND {key} = '{val}'".format(key=key,val=val)
			else:
				condition+= "WHERE {key} = '{val}'".format(key=key,val=val)
			i = i+2

	return  frappe.db.sql("""SELECT {fields} 
		FROM `tabFinished Goods Process` as f
		INNER JOIN `tabFinished Goods Process Child` as c
		ON f.name = c.parent
		{wh}""".format(fields=','.join(fields),wh=condition),as_list=1)