# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Reference/Style")+":Link/Style Name:120",
		_("Size Range")+":Link/Size Format:140",
		#_("Size")+":Link/Size:138",
		_("Date")+":Datetime:140",
		_("Color")+":Link/Color:120",
		#_("PCS")+":Int:120",
		_("Status")+":Data:100",
		_("Remark")+":Data:200",
		_("Created By")+":Data:100",
		_("Attachment")+":Data:158"
	]

def data(filters):
	fields = ['tc.styl', 'tc.size_range', 'tc.date', 'tcc.color', 'tc.status', 'tcqc.remark', 'tcqc.modified_by', 'tcqc.attachment']
	
	rows = frappe.db.sql("""select {fields} 
		FROM `tabTest Cutting Child` as tcc
		LEFT JOIN `tabTest Cutting` as tc ON tc.name = tcc.parent
		LEFT JOIN `tabTest Cutting QC` as tcqc ON tcqc.test_cutting = tc.name
		{wh} GROUP BY tc.size_range, tc.styl """.format(fields=','.join(fields),wh=conditions(filters,['tc.date'])),as_list=1)
	for x in xrange(len(rows)):
		rows[x][-1] = '<a target="__blank" href="{0}"><i class="icon-paper-clip"></i> {0}</a>'.format(rows[x][-1])
	return rows


def conditions(filters,datefiltereqelval):
	condition = ''
	if filters:
		item = 0
		for key,val in filters.iteritems():
			if key in datefiltereqelval:
				condition+= "{con} DATE({key}) = DATE('{val}')".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
				item = item+1
				continue
			condition+= "{con} {key} = '{val}'".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
			item = item+1

	return condition