// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Test Cutting Approval"] = {
	"filters": [
		{
			fieldname:"tc.styl",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 1,
			default: frappe.defaults.get_user_default("styl")
		},
		{
			fieldname:"tc.date",
			label: __("Date"),
			fieldtype: "Date",
			// options: "date",
			reqd: 0,
			// default: frappe.defaults.get_user_default("size_range")
		},
		{
			fieldname:"tcc.status",
			label: __("Status"),
			fieldtype: "Select",
			options: ""+NEWLINE+"Pending"+NEWLINE+"Pass"+NEWLINE+"Fail",
			reqd: 0,
		}
	]
}
