// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Cutting Alter"] = {
	"filters": [
		{
			fieldname:"style",
			fieldtype:"Link",
			label:"Style",
			options:"Style Name",
			reqd:1
		},
		{
			fieldname:"alter_for",
			label: __("Alter For"),
			fieldtype: "Select",
			options: ""+NEWLINE+"Test Cutting"+NEWLINE+"Bulk Cutting",
			reqd: 0,
		},
		{
			fieldname:"date",
			fieldtype:"Date",
			label:"Day",
			reqd:0,
			default:frappe.datetime.get_today()
		}
	]
}
