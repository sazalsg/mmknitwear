# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	#columns, data = [], []
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Reference/Style")+":Link/Style Name:120",
		_("Size Range")+":Link/Size Format:120",
		_("Date")+":Datetime:140",
		_("Cutting For")+":Data:118",
		_("Alter Type")+":Link/Alter Type:180",
		_("Worker")+":Link/Employee:100",
		_("Alter Qty")+":Int:100",
		_("Comment")+":Data:200"
	]


def data(filters):
	fields = ['style', 'size_range', 'date', 'alter_for', 'alter_type', 'worker', 'qty', 'comment']
	condition = ''
	if filters:
		item = 0
		for key,val in filters.iteritems():
			if key == 'date':
				condition+= "{con} DATE({key}) = DATE('{val}')".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
				item = item+1
				continue
			condition+= "{con} {key} = '{val}'".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
			item = item+1

	return  frappe.db.sql("""select {fields}
		FROM `tabCutting Alter` as ca
		{wh}""".format(fields=','.join(fields),wh=condition),as_list=1)
