# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
def execute(filters=None):
	#columns, data = [], []
	return columns(filters), data (filters)

def columns(filters):
	return [
		_("Pattern No")+":Data:160",
		_("Date")+":Date:160",
		_("Style")+":Link/Style Name:98",
		_("Color")+":Data:120",
		_("Size")+":Data:120",
		_("Quantity")+":Int:100"
	]
def data (filters):
	fields = ['pc.name', 'pc.date', 'pc.reference', 'pcc.color', 'pcc.size', 'pcc.qty']
	condition=''
	if filters:
		i=0;
		for key,val in filters.iteritems():
			if i!=0:
				condition+= "AND {key} = '{val}'".format(key=key,val=val)
			else:
				condition+= "WHERE {key} = '{val}'".format(key=key,val=val)
			i = i+2

	return  frappe.db.sql("""select {fields} 
		FROM `tabPattern Cutting` as pc
		LEFT JOIN `tabPattern Cutting Child` as pcc ON pc.name = pcc.parent
		{wh}""".format(fields=','.join(fields),wh=condition),as_list=1)
