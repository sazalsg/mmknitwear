// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Pattern Cutting"] = {
	"filters": [
		{
			fieldname:"pc.reference",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 0,
			default: frappe.defaults.get_user_default("style")
		},
		{
			fieldname:"pcc.size",
			label: __("Size"),
			fieldtype: "Link",
			options: "Size",
			reqd: 0
		},
		{
			fieldname:"pcc.color",
			label: __("Color"),
			fieldtype: "Link",
			options: "Color",
			reqd: 0
		}
	]
}