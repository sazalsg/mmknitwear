// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Production Line Setup"] = {
	"filters": [
		{
			fieldname:"style",
			fieldtype:"Link",
			label:"Reference/Style",
			options:"Style Name"
		},
		{
			fieldname:"line_no",
			label:__("Line No"),
			fieldtype:"Link",
			options:"Line No"
		},
		{
			fieldname:"pls.creation",
			label: __("Date"),
			fieldtype: "Date"
		}
	]
}
