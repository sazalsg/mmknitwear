# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _

def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Name")+":Link/Production Line Setup:120",
		_("Date")+":Date:150",
		_("Line No")+":Data:100",
		_("Reference/Style")+":Link/Style Name:100",
		_("Worker")+":Data:100",
		_("Supervisor")+":Data:100"
		
	]

def data(filters):
	condition = ''
	if filters:	
		for key,val in filters.iteritems():
			if key=='style':
				condition+= " AND {key} = '{val}'".format(key=key,val=val)
			elif key=='pls.creation':
				condition+= " AND {key} LIKE '%{val}%'".format(key=key,val=val)
			elif key=='line_no':
				condition+= " AND {key} = '{val}'".format(key=key,val=val)
			else:
				condition+= "WHERE {key} = '{val}'".format(key=key,val=val)

	return frappe.db.sql("""SELECT pls.name, DATE_FORMAT(pls.creation, "%Y-%m-%d") as date, pls.line_no, pls.style,
COUNT(plw.worker_id) as worker,
(SELECT COUNT(emp_id) FROM `tabProduction Line Supervisor` AS sv WHERE sv.parent=pls.name) as supervisor
FROM `tabProduction Line Setup` AS pls
INNER JOIN `tabProduction Line Worker` AS plw ON pls.name=plw.parent
{wh}
GROUP BY pls.creation""".format(wh=condition),as_list=1)

