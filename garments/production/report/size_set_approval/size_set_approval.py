# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Reference/Style")+":Link/Style Name:120",
		_("Size Range")+":Link/Size Format:110",
		_("Color")+":Link/Color:80",
		_("Date")+":Datetime:130",
		_("Size Breakdown For")+":Data:100",
		_("Test Cutting Order Qty")+":Int:120",
		_("Status")+":Data:70",
		_("Remark")+":Data:200",
		_("Created By")+":Data:100",
		_("Attachment")+":Data:150"
	]

def data(filters):
	fields = ['ss.style', 'ss.size_range', 'ss.color', 'ss.date', 'tco.size_breakdown_for', 'tco.tco_qty', 'ss.status', 'ssqc.remark', 'ssqc.modified_by', 'ssqc.attach', 'tco.creation']
	condition = ''
	if filters:
		item = 0
		for key,val in filters.iteritems():
			if key == 'tco.creation':
				condition+= "{con} DATE({key}) = DATE('{val}')".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
				item = item+1
				continue
			condition+= "{con} {key} = '{val}'".format(con=" AND" if item !=0 else "WHERE", key=key, val=val)
			item = item+1

	rows = frappe.db.sql("""select {fields}
		FROM `tabSize Set` as ss\
		LEFT JOIN `tabTest Cutting Order` as tco ON ss.name = tco.ss_name\
		LEFT JOIN `tabSize Set QC` as ssqc ON ss.style = ssqc.style AND ss.size_range = ssqc.size_range\
		{wh} GROUP BY ss.size_range, ss.style """.format(fields=','.join(fields),wh=condition),as_list=1)

	for x in xrange(len(rows)):
		rows[x][-2] = '<a target="__blank" href="{0}"><i class="icon-paper-clip"></i> {0}</a>'.format(rows[x][-2])
	return rows
