// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Size Set Approval"] = {
	"filters": [
		{
			fieldname:"ss.style",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 1,
			default: frappe.defaults.get_user_default("style")
		},
		{
			fieldname:"ss.size_range",
			label: __("Size Range"),
			fieldtype: "Link",
			options: "Size Format",
			reqd: 0,
			default: frappe.defaults.get_user_default("size_range")
		},
		{
			fieldname:"ss.status",
			label: __("Status"),
			fieldtype: "Select",
			options: ""+NEWLINE+"Pending"+NEWLINE+"Pass"+NEWLINE+"Fail",
			reqd: 0,
		},
		{
			fieldname:"tco.size_breakdown_for",
			label: __("Size BreakDown For"),
			fieldtype: "Select",
			options: ""+NEWLINE+"Standard Packing"+NEWLINE+"Solid Packing",
			reqd: 0,
		},
		{
			fieldname:"tco.creation",
			label: __("Date"),
			fieldtype: "Date",
			reqd: 0,
			// default: frappe.defaults.get_user_default("size_range")
		}
	]
}
