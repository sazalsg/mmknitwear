// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Daily Overtime"] = {
	"filters": [
		{
			fieldname:"date",
			fieldtype:"Date",
			label:"Date",
			options:"Daily Overtime",
			default:frappe.datetime.get_today()
		}
	]
}