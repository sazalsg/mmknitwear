# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Worker Name")+":Data:150",
		_("Date")+":Date:120",
		_("Line")+":Data:80",
		_("Nature of Work")+":Data:120",
		_("Hourly Target")+":Data:100",
		_("Total Hour")+":Link/Alter Type:100",
		_("Total Target")+":Link/Employee:100",
		_("Fillup Target")+":Int:100",
		_("Fillup Time")+":Data:100",
		_("Overtime")+":Data:100"
	]

def data(filters):
	fields = ['worker_name', 'date', 'line_no', 'nature_of_work', 'hourly_target', 'total_hour', 'total_target', 'fillup_target', 'fillup_time', 'overtime']
	condition = ''
	if filters:
		for key,val in filters.iteritems():
			if key == 'date':
				condition+= "WHERE {key} = '{val}' AND status= 0".format(key=key, val=val)
				continue
	return  frappe.db.sql("""SELECT {fields}
		FROM `tabDaily Overtime` {wh}""".format(fields=','.join(fields),wh=condition),as_list=1)
