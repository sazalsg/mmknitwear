# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Reference/Style")+":Data:100",
		_("Date")+":Date:100",
		_("Checking Type")+":Data:200",
		_("Plus")+":Int:100",
		_("Minus")+":Int:100",
		_("Ok")+":Int:100",
		_("Total")+":Int:100"
	]


def data(filters):
	fields = ['m.style','m.date','c.measure_checking_type','c.plus','c.minus','c.ok','c.total']
	
	condition=''

	if filters:
		i=0;
		for key,val in filters.iteritems():
			if key == "style":
				if i!=0:
					condition+= "AND {key} = '{val}'".format(key=key,val=val)
				else:
					condition+= "WHERE {key} =  '{val}'".format(key=key,val=val)
				i = i+2
				continue
			if i!=0:
				condition+= "AND {key} = '{val}'".format(key=key,val=val)
			else:
				condition+= "WHERE {key} = '{val}'".format(key=key,val=val)
			i = i+2

	return  frappe.db.sql("""SELECT {fields} 
		FROM `tabMeasurement Check` as m
		INNER JOIN `tabMeasurement Check Child` as c
		ON m.name = c.parent
		{wh}""".format(fields=','.join(fields),wh=condition),as_list=1)