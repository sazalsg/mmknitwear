// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Measurement Check"] = {
	"filters": [
		{
			fieldname:"m.style",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 0,
		},
		{
			fieldname:"c.measure_checking_type",
			label: __("Checking Type"),
			fieldtype: "Link",
			options: "Measurement Checking Type",
			reqd: 0,
		}		
	]
}
