# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, datetime
from frappe import _

def execute(filters=None):
	return columns(filters), data(filters)

def columns(filters):
	return [
		_("Name")+":Link/Size Set:100",
		_("Style")+":Link/Style Name:100",
		_("Date")+":Date:150",
		_("Patter Cutting")+":Link/Pattern Cutting:180",
		_("Size Range")+":Link/Size Format:120",
		_("Status")+":Data:100",
		_("Color")+":Link/Color:100",
		_("Total")+":Data:100"
	]

def data(filters):
	condition = ''
	if filters:	
		for key,val in filters.iteritems():
			if key == "style":
				condition += " AND {key}='{val}'".format(key=key, val=val)
			elif key == "date":
				condition += " AND {key} LIKE '%{val}%'".format(key=key, val=val)
			else:
				condition += " AND {key}='{val}'".format(key=key, val=val)

	return frappe.db.sql("""SELECT `name`,`style`, DATE_FORMAT(date,'%Y-%m-%d') as date, `pc`, `size_range`, `status`, `color`, `ttl`
		FROM `tabSize Set` WHERE ''='' {wh}""".format(wh=condition),as_list=1)
