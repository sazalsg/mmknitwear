// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Size Set"] = {
	"filters": [
		{
			fieldname:"style",
			fieldtype:"Link",
			label:"Reference/Style",
			options:"Style Name"
		},
		{
			fieldname:"date",
			label: __("Date"),
			fieldtype: "Date"
		},
		{
			fieldname:"status",
			label: __("Status"),
			fieldtype: "Select",
			options: ""+NEWLINE+"Pending"+NEWLINE+"Pass"+NEWLINE+"Fail"
		}
	]
}