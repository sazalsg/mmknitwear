# Copyright (c) 2013, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
def execute(filters=None):
	#columns, data = [], []
	return columns(filters), data (filters)

def columns(filters):
	return [
		_("Reference/Style")+":Link/Style Name:98",
		_("Buyer")+":Data:160",
		_("Date")+":date:160",
		_("Size Set")+":Data:150",
		_("Size")+":Data:100",
		_("Color")+":Data:120",
		_("Pcs")+":Int:100"
	]
def data (filters):
	fields = ['bc.style','bc.buyer', 'bc.date', 'bc.size_set', 'bcc.size', 'bcc.color', 'bcc.pcs']
	#,'embqc.quantity'
	condition=''

	if filters:
		i=0;
		for key,val in filters.iteritems():
			if key == "bc.date":
				if i!=0:
					condition+= "AND DATE({key}) = DATE('{val}')".format(key=key,val=val)
				else:
					condition+= "WHERE DATE({key}) = DATE('{val}')".format(key=key,val=val)
				i = i+2
				continue
			if i!=0:
				condition+= "AND {key} = '{val}'".format(key=key,val=val)
			else:
				condition+= "WHERE {key} = '{val}'".format(key=key,val=val)
			i = i+2


	return  frappe.db.sql("""SELECT {fields} 
		FROM `tabBulk Cutting` as bc
		INNER JOIN `tabBulk Cutting Child` as bcc ON bc.name=bcc.parent
		{wh}""".format(fields=','.join(fields),wh=condition),as_list=1)
