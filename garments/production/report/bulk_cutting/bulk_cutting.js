// Copyright (c) 2013, Techbeeo Software Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Bulk Cutting"] = {
	"filters": [
		{
			fieldname:"bc.style",
			label: __("Reference/Style"),
			fieldtype: "Link",
			options: "Style Name",
			reqd: 0,
		//	default: frappe.defaults.get_user_default("style")
		},
		{
			fieldname:"bc.date",
			label: __("Date"),
			fieldtype: "Date",
		}
	]
}
