cur_frm.cscript.onload = function (doc) {
	doc.style = "";
	doc.production_line = "";
	doc.worker="";
	refresh_field('worker');
	refresh_field('style');
	refresh_field('production_line');
	cur_frm.set_query('production_line',function () {
		return {
			query:"garments.production.doctype.alter_entry.alter_entry.production_line_filter",
			filters:{
				style:doc.style,
				jo_doc:"Production Line Setup"
			}
			/*filters:[
				["Production Line","production_line_setup","=",['style',"Production Line Setup"]],
				["Production Line Setup","style","=",doc.style]

			]*/
		}
	});
}

cur_frm.cscript.refresh = function (doc) {
	cur_frm.disable_save();
	cur_frm.page.clear_icons();
	if (jQuery.inArray( "__unsaved", doc ) == -1) {
		doc.alter_type="";
		doc.worker="";
		doc.date=frappe.datetime.now_datetime();
		refresh_field('alter_type');
		refresh_field('date');
		refresh_field('worker');
	};
}

