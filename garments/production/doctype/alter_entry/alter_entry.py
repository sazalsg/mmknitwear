# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class AlterEntry(Document):
	def alter_entry_on_update(self):
		self.alter_entry()
		fields = ['date','alter_type','comment','worker']
		frappe.db.sql("DELETE FROM `tabSingles` where doctype='Alter Entry' and field in ('{}')".format("','".join(fields)));


	def alter_entry(self):
		val_map = {
			"style":self.style,
			"production_line":self.production_line,
			"date":self.date,
			"alter_type":self.alter_type,
			"qty":self.qty,
			"comment":self.comment,
			"worker":self.worker,
			# "parent":True,
			# "parenttype":True
		}
		doc = frappe.get_doc({"doctype":"Production Alter"})

		for key, val in val_map.iteritems():
			doc.set(key,val)

		doc.insert()


def production_line_filter(doctype,txt,searchfield,start,page_len,filters):
	return frappe.db.sql("SELECT `tab{doc}`.name FROM `tab{doc}` LEFT JOIN `tab{jo_doc}` ON `tab{jo_doc}`.name = `tab{doc}`.production_line_setup WHERE `tab{jo_doc}`.style = '{style}'".format(doc=doctype,jo_doc=filters['jo_doc'] or "", style=filters['style']))
