cur_frm.cscript.total =function (doc,cdt,cdn) {
	var local = locals[cdt][cdn]
	local.total = local.plus+local.minus+local.ok
	refresh_field('total',local.name,'measurement_check_child')
	cur_frm.cscript.totals(cur_frm.doc)
	// console.log(local)
}
cur_frm.cscript.totals=function (doc) {
	var total = 0;
	for (var i = 0; i < doc.measurement_check_child.length; i++) {
		total += doc.measurement_check_child[i].total
	};
	doc.totals = total;
	refresh_field('totals')
}

cur_frm.cscript.ok = function (doc,cdt,cdn) {
	cur_frm.cscript.total(doc,cdt,cdn)
}

cur_frm.cscript.plus = function (doc,cdt,cdn) {
	cur_frm.cscript.total(doc,cdt,cdn)
}

cur_frm.cscript.minus = function (doc,cdt,cdn) {
	cur_frm.cscript.total(doc,cdt,cdn)
}

frappe.ui.form.on("Measurement Check Child","measurement_check_child_remove",function (frm) {
	frm.cscript.totals(frm.doc)
})