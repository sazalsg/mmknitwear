# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _

class TestCuttingOrder(Document):
	def validate(self):
		if not self.tco_qty:
			frappe.throw(_("Qty is mandetory"))
	pass


