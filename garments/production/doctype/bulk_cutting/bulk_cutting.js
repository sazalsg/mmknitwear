cur_frm.cscript.frm.add_fetch("style", "buyer", "buyer");
cur_frm.cscript.style = function(doc, doctype, docname){
			frappe.call({
			method:"garments.production.doctype.bulk_cutting.bulk_cutting.filters_size_set",
			args:{
				doctype:"Test Cutting",
				searchfield:"size_set",
				filters:{
					"parent":doc.style
				}
			},
			callback:function (r) {
				
				if (r.message) {
					doc.size_set = r.message[0][0];
					refresh_field('size_set');
				}
				else{
					cur_frm.set_value("size_set", "");
					refresh_field('size_set');
				}
			}
		})
}

frappe.provide('garments.production');

frappe.ui.form.on("Bulk Cutting Child", "bc_remove", function(frm){
		sumTotal(frm.doc);
});
cur_frm.cscript.cc = function(doc,doctype,docname){

		sumTotal(doc,doctype,docname);
		calcTotal(doc,doctype,docname);
	}

function sumTotal(doc,doctype,docname){
			var t_pcs = 0;
			// Check Current Row Duplicate Entry
			for(var key in doc.bc){

				if(parseFloat(doc.bc[key].pcs)){
					t_pcs+=(parseFloat(doc.bc[key].pcs));
				}
			}
			cur_frm.set_value("ttl", t_pcs);
}

function calcTotal(doc,doctype,docname){

	var setLocal = locals[doctype][docname];

	var b_pcs = parseInt(setLocal.pcs);

	var b_cc = parseFloat(setLocal.cc);

	var bkn_cns = setLocal.bk_cns

	if( b_pcs && b_cc ){
		setLocal.ttl_cc = (b_pcs*b_cc);
		refresh_field("ttl_cc", setLocal.name, setLocal.parentfield);

		if (bkn_cns < setLocal.ttl_cc) {
			
			msgprint('Total Cutting Consumption is Greater than Booking Consumption')
			setLocal.ttl_cc = 0;

			setLocal.cc = "";
			refresh_field("cc", setLocal.name, setLocal.parentfield);
		}
	}
	else {
		setLocal.ttl_cc = 0;
		refresh_field("ttl_cc", setLocal.name, setLocal.parentfield);
	}
}

cur_frm.cscript.process = function (doc, doctype, docname) {

doc.bc = ""
     field = ["size", "color"]
     for (var i = 0; i < field.length; i++) {
         if (!(field[i] in doc) || doc[field[i]] == "") {
            msgprint(__("You must have to be fill up size, color"))
            return 0;
         };
     };
	frappe.call({
		method:"garments.production.doctype.bulk_cutting.bulk_cutting.size_format",
		args:{
			doctype:"Size Format Child",
			fields:"size",
			searchfield: doc.size
			},
			callback:function (data) {
			 if (data.message) {
			 	for(var i=0; i < data.message.length; i++){     
			 		var row = frappe.model.add_child(doc,"Bulk Cutting Child","bc");
                    row.size = data.message[i].size
                    row.color = doc.color
                    row.pcs =doc.pcs
                }
			 };
			 sumTotal(doc,doctype,docname);                
             refresh_field('bc');
		}
	});
}