# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import ast


class BulkCutting(Document):
	pass

@frappe.whitelist()
def consumptionCalculate(consumption,size):

	sql = frappe.db.get_values("Consumption Workout Child",{"name":consumption},["consumption_data"], as_dict=True)
	data = ast.literal_eval(sql[0]['consumption_data'])

	if size in data['all_cols'] :
		getSizeIndex = data['all_cols'].index(size)
		return data['CONSUMPTION :'][getSizeIndex]
	else :
		return 0

@frappe.whitelist()
def filters_size_set(doctype, searchfield, filters):
	filters = ast.literal_eval(filters)
	cus_filter = ""
	if len(filters) > 0:
		if filters.get('parent'):
			cus_filter += "WHERE styl ='{}'".format(filters['parent'])

	return frappe.db.sql("""
		SELECT {sf} FROM `tab{doc}` {filt}
		""".format(sf = searchfield, doc = doctype, filt = cus_filter))

@frappe.whitelist()
def size_format(doctype, fields, searchfield):
		return frappe.db.sql("""
		SELECT `{fields}` FROM `tab{doc}` WHERE `parent`='{range}' ORDER BY `{fields}`
		""".format(fields = fields, doc= doctype,range= searchfield), as_dict=1)