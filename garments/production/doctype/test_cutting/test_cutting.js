
frappe.provide('garments.production');

frappe.ui.form.on("Test Cutting Child", "t_cut_remove", function(frm){
		sumTotal(frm.doc);
});

// Auto Filled Child table
frappe.ui.form.on("Test Cutting","size_range",function (frm) {
	frappe.call({
		method:"garments.production.doctype.size_set.size_set.size_range_filter",
		args:{
			doctype:"Test Cutting",
			fields:'name',
			filters:{
				test_cutting_order:frm.doc.test_cutting_order,
				size_range:frm.doc.size_range
				// select:["in", ["Pass", "Pending"]]
			}
		},
		callback:function (data) {
			//console.log(data)
			if (data.message) {
				msgprint(__("ALready Entry"))
				frm.doc.siz_set_cld = "";
				refresh_field('siz_set_cld');
				sumTotal(frm.doc);
				return
			};

			// Row render
			frappe.model.with_doc("Size Format",frm.doc.size_range,function () {
				size_format = frappe.model.get_doc("Size Format",frm.doc.size_range);
				frm.doc.t_cut = "";
				frappe.call({
					method:"garments.production.doctype.test_cutting.test_cutting.get_posheet_size_breakdown_for",
					args:{
						filters:{
							test_cutting_order:frm.doc.test_cutting_order
						}
					},
					callback:function(r){
						if (!r.message){
							msgprint(__("Didn't found Any match in PO Sheet with this style and size range for Standard Packing"));
							console.log(frm.doc.test_cutting_order)
							frm.doc.test_cutting_order = "";
							frm.doc.size_range = "";
							frm.doc.t_cut = "";
							sumTotal(frm.doc);
							refresh_field('test_cutting_order');
							refresh_field('t_cut');
							refresh_field('size_range');
						} else if (r.message && r.message == "Fixed") {
							cur_frm.cscript.render_child_row(frm,size_format.format_child)
						} else if (r.message){
							cur_frm.cscript.render_child_row(frm,size_format.format_child,r.message[0])
						};
					}
				}) //size_format.format_child
				
			})
		}
	})
});

cur_frm.cscript.render_child_row = function (frm,values,pcs) {
	if (pcs) {
		pcs = JSON.parse(pcs)
	};
	$.each(values, function(index, val) {
		var row = frappe.model.add_child(frm.doc,"Test Cutting Child","t_cut");
		row.size = val.size
		row.color = frm.doc.color
		row.pcs = (pcs?pcs[val.size]*frm.doc.tmp_pcs:frm.doc.tmp_pcs)
	});
	refresh_field('t_cut');
	sumTotal(frm.doc);
}

cur_frm.cscript.test_cutting_order = function (doc) {
	if (!doc.test_cutting_order) {
		doc.size_range = "";
		doc.t_cut = "";
		sumTotal(doc);
		refresh_field('t_cut');
		refresh_field('size_range');
		return
	};
	frappe.call({
		method:"garments.production.doctype.test_cutting.test_cutting.size_range_value",
		args:{
			filters:{
				test_cutting_order:doc.test_cutting_order
			}
		},
		callback:function (r) {
			if (r.message.length > 0) {
				//console.log(r.message);
				doc['tmp_pcs']= r.message[0][1];
				doc.size_range = r.message[0][0];
				doc['color'] = r.message[0][2]
				//console.log(doc.size_range);
				refresh_field('size_range');
				cur_frm.script_manager.trigger('size_range')
			};
		}
	})
}

cur_frm.cscript.onload = function(doc,doctype,docname){
	cur_frm.add_fetch("styl","buyer","buyer")

// For style and size set filtering
	cur_frm.set_query("test_cutting_order", function(doc) {
		frappe.model.validate_missing(doc, "styl");
	    return {
	    	query:"garments.production.doctype.test_cutting.test_cutting.filter_test_cutting_order",
	    	searchfield: 'tco.name',
	        filters: {
	        	"child_doc": 'Size Set',
	            "style": doc.styl
	        }
	    };
	});

}

cur_frm.cscript.pcs = function(doc,doctype,docname){
 	sumTotal(doc,doctype,docname);
}

function sumTotal(doc,doctype,docname){
			var t_pcs = 0;
			// Check Current Row Duplicate Entry
			for(var key in doc.t_cut){

				if(parseInt(doc.t_cut[key].pcs)){
					t_pcs+=(parseInt(doc.t_cut[key].pcs));
				}
			}
			cur_frm.set_value("ttl", t_pcs);
}

cur_frm.cscript.styl = function (doc) {
	console.log("dasd")
	doc.test_cutting_order = "";
	cur_frm.script_manager.trigger('test_cutting_order')
	refresh_field('test_cutting_order')
}