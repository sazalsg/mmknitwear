# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, ast
from frappe.model.document import Document

class TestCutting(Document):
	pass
# Size set filter with style
@frappe.whitelist()
def filters_size(doctype, txt, searchfield, start, page_len, filters):
	cus_filter = ""
	if len(filters) > 0:
		if filters.get('parent'):
			cus_filter += "WHERE ssc.parent ='{}'".format(filters['parent'])

	return frappe.db.sql("""
		SELECT {sf} FROM tab{doc} as s
		LEFT JOIN `tab{cdoc}` as ssc
		ON ssc.size = s.name
		{filt}
		""".format(sf = searchfield, doc = doctype, cdoc = filters['child_doc'],filt = cus_filter))

# test Cutting filter with Style
@frappe.whitelist()
def filter_test_cutting_order(doctype, txt, searchfield, start, page_len, filters):
	return frappe.db.sql("""
		SELECT {sf} FROM `tab{doc}` as tco
		LEFT JOIN `tab{adoc}` as ss
		ON tco.ss_name = ss.name
		WHERE ss.style = '{filt}'
		""".format(sf = searchfield, doc = doctype, adoc = filters['child_doc'],filt = filters['style']))

@frappe.whitelist()
def size_range_value(filters):
	filters = ast.literal_eval(filters)
	return frappe.db.sql("""SELECT ss.size_range, tco.tco_qty, ss.color
		FROM `tabSize Set` as ss
		LEFT JOIN `tabTest Cutting Order` as tco ON tco.ss_name = ss.name
		WHERE tco.name = '{filt}' """.format(filt = filters['test_cutting_order']))

@frappe.whitelist()
def get_posheet_size_breakdown_for(filters):
	filters = ast.literal_eval(filters)
	test_cutting_order = frappe.get_doc("Test Cutting Order", filters['test_cutting_order'])
	# frappe.msgprint(test_cutting_order.size_breakdown_for)
	if test_cutting_order.size_breakdown_for == "Standard Packing":
		return frappe.db.sql("""SELECT po_sheet_ch_ch.datas 
			FROM `tabPO Sheet Child Child` as po_sheet_ch_ch
			LEFT JOIN `tabSize Set` as ss
			ON ss.style = po_sheet_ch_ch.product_name AND ss.color = po_sheet_ch_ch.color AND ss.size_range = po_sheet_ch_ch.size_format
			WHERE ss.name = '{ss_name}' AND po_sheet_ch_ch.size_breakdown_for = '{sbf}'
			""".format(ss_name = test_cutting_order.ss_name, sbf = test_cutting_order.size_breakdown_for))
	return "Fixed"