# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
import ast
from frappe.utils import parse_val

no_cache = 1

class ProductionLine(Document):
	def on_trash(self):
		frappe.db.sql("DELETE FROM `tabProduction Worker Input` WHERE parent = '{}'".format(self.name))
		frappe.db.sql("DELETE FROM `tabProduction line Bundle Alter` WHERE parent = '{}'".format(self.name))


@frappe.whitelist()
def get_total_qty(doctype,parent,emp_id,bundle):
	val = 0;
	bn = ""
	if bundle:
		bn = "AND bundle_no = '{}'".format(bundle)
	sql = frappe.db.sql("SELECT SUM(CASE WHEN action = 'plus' THEN amount ELSE 0 END) AS plus,\
		SUM(CASE WHEN action = 'minus' THEN amount ELSE 0 END) AS minus\
		FROM `tab{doc}` WHERE parent='{parent}' AND worker_id='{worker_id}' {bn}".format(doc=doctype,\
			parent=parent,worker_id=emp_id, bn = bn),as_dict=1)[0];
	if sql.plus and sql.minus:
		val = sql.plus - sql.minus
	elif sql.plus and not sql.minus:
		val = sql.plus
	return val



def filter_bundle(doctype, txt, searchfield, start, page_len,filters):
	fil_bundle=""
	if filters.get('bundles'):
		i = '("'
		i += '","'.join(filters['bundles'])
		i += '")'
		fil_bundle = 'AND bun_ch.name not in {}'.format(i)
	style = frappe.get_doc("Style Name",filters['style'])
	if style.have_embellishment:
		return frappe.db.sql("""SELECT bun_ch.name, bun_ch.bundle_no
			FROM `tabBundle Child` as bun_ch
			LEFT JOIN `tabBundle` as bun
			ON bun.name = bun_ch.parent
			LEFT JOIN `tabEmbellishment QC` as emqc
			ON emqc.bundle_no = bun_ch.name
			WHERE emqc.status ="Pass" AND bun.style = '{}' {}""".format(filters['style'],fil_bundle))
	else:
		return frappe.db.sql("""SELECT bun_ch.name, bun_ch.bundle_no
			FROM `tabBundle Child` as bun_ch
			LEFT JOIN `tabBundle` as bun
			ON bun.name = bun_ch.parent
			WHERE bun.style = '{}' {}""".format(filters['style'],fil_bundle))



@frappe.whitelist()
def worker_input_status(worker_id, bundle,parent):
	
	bundle = ast.literal_eval(bundle)
	bundle_process =   worker_id, "", 0, 0, 0, 0
	for i in bundle:
		worker_bundle = worker_bundles(i,worker_id,parent)
		if not worker_bundle[0].parent :
			return i
	return ""

def worker_bundles(bundle,worker,parent = None):
	wh_parent = ""
	if parent:
		wh_parent = "AND `tabProduction Worker Input`.`parent` = '{}'".format(parent)

	return frappe.db.sql("SELECT `tabBundle Child`.`total_size`, `tabProduction Worker Input`.`parent`, SUM(CASE WHEN `tabProduction Worker Input`.`action` = 'plus' THEN `tabProduction Worker Input`.`amount` ELSE 0 END) as plus, SUM(CASE WHEN `tabProduction Worker Input`.`action` = 'minus' THEN `tabProduction Worker Input`.`amount` ELSE 0 END) as minus FROM `tabBundle Child` LEFT JOIN `tabProduction Worker Input` ON `tabProduction Worker Input`.`bundle_no` = `tabBundle Child`.`name` WHERE `tabProduction Worker Input`.`bundle_no` = '{}' and `tabProduction Worker Input`.`worker_id` = '{}' {wh}".format(bundle, worker,wh=wh_parent), as_dict = 1)


@frappe.whitelist()
def save_worker_input(prev_check_point, check_point, pro_line_setup, datas):
	filters = {"idx":(">", prev_check_point),"idx":("<=",check_point),"parent":pro_line_setup,"enabled":'1'}
	workers = frappe.db.get_values("Production Line Worker",filters,['worker_id','work_nm','nat_work'])

	datas = ast.literal_eval(datas)
	# frappe.throw(datas)
	for worker in workers:
		doc = frappe.get_doc({"doctype":"Production Worker Input"})
		doc.set('comment',datas['comment'])
		doc.set('parentfield',datas['parentfield'])
		doc.set('parenttype',datas['parenttype'])
		doc.set('parent',datas['parent'])
		doc.set('nat_work',worker[2])
		doc.set('amount',datas['amount'])
		doc.set('bundle_no',datas['bundle_no'])
		doc.set('action',datas['action'])
		doc.set('worker_id',worker[0])
		doc.set('worker_name',worker[1])
		doc.insert()

	return "Success"