// -*- coding: utf-8 -*-
// Copyright (c) 2015, Techbeeo and contributors
// For license information, please see license.txt

frappe.provide('garments.production')
frappe.ui.form.on ('Production Line',{
	onload_post_render: function(frm) {
		cur_frm.cscript.onload_render(frm)
	}	
})

// Cscript
garments.production.ProductionLineController = frappe.ui.form.Controller.extend({
	refresh:function (doc) {
		this.production_line_setup(doc)
		if (!doc.__islocal) {
			this.frm.set_df_property("production_line_setup","read_only",1);
			this.frm.disable_save();
			this.get_pro_line_setup(doc);
			// this.onload_render(cur_frm);
			setTimeout(function () {
				cur_frm.get_field("bundles").$wrapper.find('.grid-body').find("div.grid-row").remove();
			},100);
		};
	},
	onload_render:function (frm) {
		var me = frm.cscript;
		// Bundle
		// Body
		added_bundle=[]
		if (frm.get_field("bundles").$wrapper.find('.grid-body.table-responsive').length <1 ) {
			frm.get_field("bundles").$wrapper.find('.grid-body').addClass('table-responsive').prepend('<table class="grid-body table"><thead class="bundle_header"></thead><tfoot class="bundle_footer"></tfoot><tbody class="rows"></tbody></table>');
		};
		frm.get_field("bundles").$wrapper.find('.grid-body').find('.grid-footer .row div.col-sm-6:first').html($('<button href="#" class="btn btn-xs btn-default"> Add new bundle</button>').on('click', function(me) {
			cur_frm.cscript.new_bundle(frm.doc);		}));
		frm.get_field("bundles").$wrapper.find('.grid-body').children('div.rows').remove();
		if ("bundles" in frm.doc) {
			frm.get_field("bundles").$wrapper.find('.rows').html("");			
			$.each(frm.doc.bundles, function(index, val) {
				added_bundle.push(val.bundle);
				var data = {};
				data['keys']=frappe.get_meta(val.doctype);
				data['ignor_fields']=me.ignor_fields_in_bundels();
				data['val']=val;
				me.render_row(frm.get_field("bundles").$wrapper.find('.rows'),data)
			});
		};

		// supervisor
		if (!frm.doc.__islocal) {
			frappe.call({
				method:"frappe.client.get",
				args:{
					doctype:"Production Line Setup",
					name:frm.doc.production_line_setup
				},
				cache:false,
				callback:function (r) {
					if (r.message) {
						frm.get_field("supervisor").$wrapper.html(frappe.render_template('supervisor',r.message))
					};
				}
			})
		};

		// Worker Input
		pro_bundle_list = [];
		frm.get_field("production_worker_input").$wrapper.addClass('table-responsive').html('<table class="grid-body table table-bordered"><thead class="bundle_header"></thead><tfoot class="bundle_footer"></tfoot><tbody class="rows"></tbody></table>');
		if ("bundles" in frm.doc) {
			for (var i = 0; i < frm.doc.bundles.length; i++) {
				if (frm.doc.bundles[i].status == "Processing") {
					pro_bundle_list.push(frm.doc.bundles[i].bundle);
					// break
				};
			};
		};
		// msgprint(pro_bundle_list)
		if (!frm.doc.__islocal) {
			frm.get_field("production_worker_input").$wrapper.find(".bundle_header").html(frappe.render_template("worker_input_header",{}))
			if (workers.length > 0) {
				for (var j = 0; j < workers.length; j++) {
					pro_bundle = "";

					frappe.call({
						method:"garments.production.doctype.production_line.production_line.worker_input_status",
						async:false,
						cache:false,
						args:{
							worker_id : workers[j].worker_id,
							bundle : pro_bundle_list,
							parent:frm.doc.name
						},
						callback:function (r) {
							pro_bundle = r.message
						}
					});

					workers[j]['bundle_no']=pro_bundle;
					workers[j]['total_qty'] = me.get_total_qty(frm.doc.name,workers[j].worker_id,pro_bundle);
					workers[j]['sl'] = j+1;
					frm.get_field("production_worker_input").$wrapper.find('.rows').append(frappe.render_template("worker_input",{"val":workers[j]}));
					
				};
			};
		};
	},

	production_line_setup:function (doc) {
		if (doc.production_line_setup) {
			frappe.call({
				method:"frappe.client.get",
				async:false,
				args:{
					doctype:"Production Line Setup",
					name:doc.production_line_setup
				},
				callback:function (r) {
					if(r.message) {
						style = r.message.style;
						frappe.call({
							method:"frappe.client.get",
							async:false,
							args:{
								doctype:"Style Name",
								name:style
							},
							callback:function (data) {
								if(r.message) {
									doc['have_embellishment']=data.message.have_embellishment
								}
							}
						})
					}
				}
			});
		};
	},

	get_pro_line_setup:function (doc) {
		frappe.call({
			method:"frappe.client.get",
			async:false,
			args:{
				doctype:"Production Line Setup",
				name:doc.production_line_setup
			},
			callback:function (r) {
				if (r.message) {
					workers = [];
					for (var i = 0; i < r.message.worker.length; i++) {
						if (r.message.worker[i].enabled && r.message.worker[i].check_point) {
							workers.push(r.message.worker[i])
						};
					};
				};
			}
		});
	},

	get_total_qty:function (parent,emp_id,bundle) {
		var total = 0;
		frappe.call({
			method:"garments.production.doctype.production_line.production_line.get_total_qty",
			async:false,
			args:{
				doctype:"Production Worker Input",
				parent:parent,
				emp_id:emp_id,
				bundle:bundle || ""
			},
			callback:function (r) {
				if (r.message) {
					total = r.message
				};
			}
		})
		return total;
	},

	new_bundle:function (doc) {
		this.frm.get_field("bundles").$wrapper.find('.grid-empty').hide();
		var me = cur_frm.cscript;

		// for (var i = 0; i < doc.bundles.length; i++) {
		// 	if ("__islocal" in doc.bundles[i] ) {
		// 		msgprint(__("You can't add more then one bundle at a tiem"))
		// 		return
		// 	}else if (doc.bundles[i].status =="Processing") {
		// 		msgprint(__("Bundle '{0}' is Processing Please complete that before add",[doc.bundles[i].bundle]))
		// 		return
		// 	};
		// };

		row = frappe.model.add_child(this.frm.doc, "Production Line Bundle", "bundles");
		var data = {};
		data['keys']=frappe.get_meta(row.doctype);
		data['ignor_fields']=me.ignor_fields_in_bundels();
		data['val']=row;
		me.render_row(cur_frm.get_field("bundles").$wrapper.find('.rows'),data)
	},
	
	render_row:function (selector,data) {
		$(selector).append(frappe.render_template("bundle_row",data))
		this.make_fields($(selector).find('tr:last'),data)
	},
	make_fields: function (selector,data) {
		var me = this;
		var val = data.val
		$(selector).children('td').each(function(index, el) {
			
			var fieldtype = $(el).data('read-only') == '1' ? "Read Only" : $(el).data('fieldtype');
			var df = {};
			df['fieldtype']= fieldtype;
			df['fieldname']= $(el).data('fieldname');			
			df['options']= $(el).data('options')
			df['default']= $(el).data('default')
			df['reqd']= $(el).data('reqd')
			if (df.fieldname=="bundle") {
				df['get_query'] = function() {
					return {
						query:"garments.production.doctype.production_line.production_line.filter_bundle",
						filters: {
							"style": style,
							"bundles":added_bundle
						}
					}
				}
				if ($(el).data('unsaved')==0) {
					df['fieldtype']="Read Only";
				};
			} else if (df.fieldname=="save" && $(el).data('unsaved')==1) {
				df['label'] = "Save";
			} else if (df.fieldname=="save" && $(el).data('unsaved')==0) {
				df['label'] = "Complete";
			};
			var make_field = frappe.ui.form.make_control({
				parent: el,
				df:df,
				render_input: true
			});
			if (df.fieldname=="bundle" && df.fieldtype !="Read Only") {
				make_field.$input.on('change', function() {
					me.bundle(this);
					me.checkMandetory(this)
					var filters = {}
					var doc = cur_frm.doc;
					
					if (doc.have_embellishment) {
						filters = {
							"bundle_no":$(this).val()
						}
					}else{
						filters = {
							"name":$(this).val()
						}
					};
					if ($(this).val() !="") {
						frappe.call({
							method:"frappe.client.get",
							args:{
								doctype:(doc.have_embellishment?"Embellishment QC":"Bundle Child"),
								filters:filters
							},
							callback:function (r) {
								if (r.message) {
									console.log(r.message)
									$(selector).find('[data-fieldname="qty"]').html('<label>'+(doc.have_embellishment?r.message.receive_qty:r.message.total_size)+'</label>').css('vertical-align', 'middle')
								};
							}
						})
					};
				});
			} else if (df.fieldname=="save") {
				make_field.$input.on('click', function() {
					me.save(this);
				});
			}
			make_field.refresh();
			if (df.fieldname in val) {
				$(el).find('[data-fieldname="'+df.fieldname+'"]').val(val[df.fieldname]);
				// var read_only_val = $(el).data('fieldtype') == "Link" ? '<a href="#Form/'+df.options+'/'+val[df.fieldname]+'">'+val[df.fieldname]+'</a>' : val[df.fieldname] ;
				$(el).find('.control-value').html(val[df.fieldname]);
				me.checkMandetory($(el).find('[data-fieldname="'+df.fieldname+'"]'));
			};

		});
	},
	bundle:function (evel) {
		var d = locals[$(evel).parents('td').data('doctype')][$(evel).parents('tr').data('name')];
		var bundles = cur_frm.doc.bundles
		for (var i = 0; i < bundles.length; i++) {
			if (!("__islocal" in bundles[i]) && bundles[i].bundle == $(evel).val()) {
				msgprint(__("Bunble '{0}' already used.",[bundles[i].bundle]));
				return
			};
		};
		d.bundle = $(evel).val();
		frappe.call({
			method:"frappe.client.get",
			args:{
				doctype:$(evel).parents('td').data('options'),
				name:$(evel).val()
			},
			callback:function (r) {
				if (r.message) {
					d.bundle_no = r.message.bundle_no
					$(evel).parents('tr').find('div[data-fieldname="bundle_no"] .control-value').html(d.bundle_no)
				};
			}
		});
	},
	save:function (evel) {
		var me = this
		var d = locals[$(evel).parents('td').data('doctype')][$(evel).parents('tr').data('name')];
		if ($(evel).text()=="Complete") {
			if (d.status == "Completed") {
				return
			};
			d.status = "Completed";			
			cur_frm.save();
			location.reload();
		}else {	
			/*if (cur_frm.doc.bundles.length <=1) {
				d.status = "Processing";
				d.qty = me.bundle_child_max_qty(d.bundle);
				cur_frm.save();
				location.reload();
			}else if (cur_frm.doc.bundles.length > 1) {
				// Check Have any bundle Processing
				for (var i = 0; i < cur_frm.doc.bundles.length; i++) {
					
					if (!("__islocal" in cur_frm.doc.bundles[i]) && cur_frm.doc.bundles[i].status =="Processing") {
						msgprint(__("Please Complete bundle '{0}' before new bundle add",[cur_frm.doc.bundles[i].bundle]));
						return
					};
				};
				d.status = "Processing";
				d.qty = me.bundle_child_max_qty(d.bundle);
				cur_frm.save();
				location.reload();
			};*/
			d.status = "Processing";
			d.qty = me.bundle_child_max_qty(d.bundle);
			cur_frm.save();
			location.reload();
		};
	},
	checkMandetory:function (selector) {
		if ($(selector).data('fieldtype') == "Read Only" && $(selector).parents('td').data('reqd') == 1) {
			$(selector).removeClass('has-error');
		}
		if ($(selector).val()!="" && $(selector).parents('td').data('reqd') == 1) {
			$(selector).parents('div.frappe-control').removeClass('has-error');
		}else if ( $(selector).parents('td').data('reqd') == 1 && $(selector).parents('div.frappe-control').data('fieldtype') != "Read Only")  {
			$(selector).parents('div.frappe-control').addClass('has-error');
		};
	},
	ignor_fields_in_bundels:function () {
		return ["Column Break","Section Break","Code"];
	},

	bundle_child_max_qty:function (bundle) {
		var total = 0;
		var filters = {}
		var doc = cur_frm.doc;
		if (doc.have_embellishment) {
			filters = {
				"bundle_no":bundle
			}
		}else{
			filters = {
				"name":bundle
			}
		};
		frappe.call({
			method:"frappe.client.get",
			args:{
				doctype:(doc.have_embellishment?"Embellishment QC":"Bundle Child"),
				filters:filters
			},
			async:false,
			callback:function (r) {
				if (r.message) {
					total = (doc.have_embellishment?r.message.receive_qty:r.message.total_size)
				};
			}
		});
		return total;
	},
	worker_input:function (evel) {
		var me = this
		var parent = $(evel).parents('tr');
		var data = {};
		parent.find('input').each(function(index, el) {
			data[$(el).attr('name')]=$(el).val()
		});


		//  Check Worker on hand Bundle
		if (data['bundle_no'] == "") {
			msgprint(__("Didn't get any Bundle Processing"))
			return
		};

		// auto complete amount
		var max_bundle = me.bundle_child_max_qty(data.bundle_no);
		var total = me.get_total_qty(cur_frm.doc.name,data.worker_id,data.bundle_no);
		if ($(evel).data('com')) {
			var amount = parseInt(max_bundle) - parseInt(total)
			if (amount > 0) {
				data['amount'] = amount
			}else{
				return
			};
		};
		if (data.amount =="") {
			msgprint(__("Please entry Qty"));
			return
		};
		data['action'] = $(evel).data('value');
		if (data.action == "minus") {
			if (parseInt(total) < parseInt(data.amount)) {
				msgprint(__("Qty must bee equel or less than {0}",[total]));
				return
			};
		}else{
			if ((parseInt(max_bundle) - parseInt(total)) < parseInt(data.amount)) {
				msgprint(__("Qty must bee equel or less than {0}",[(parseInt(max_bundle) - parseInt(total))]));
				return
			};
		};
		var comment_html = '<textarea class="form-control" name="comment" id="comment">'+(data.action == 'plus' ? "Complete Bundle '"+ data['bundle_no']+"'" :"")+'</textarea>';
		comment_html += '<button class="btn btn-primary save_worker_input">Save</button>';
		msgprint(comment_html)
		data['comment'] = $("#comment").val();
		$("#comment").on('change', function(event) {
			event.preventDefault();
			data['comment']=$(this).val();
		});
		$(".save_worker_input").on('click', function(event) {
			event.preventDefault();
			if (!('comment' in data) || data.comment == "") {
				alert("Pelase erter some comment");
				return
			};
			data['doctype'] = "Production Worker Input";
			data['parentfield'] = "production_worker_input";
			cur_frm.cscript.worker_input_save(parent,data,$(this))
		});
		
	},
	worker_input_save:function (parent,data,selector) {
		var prev_check_point = 0;
		var worker_index = $(parent).data('worker-index');
		// alert(worker_index)
		// return
		if ( worker_index > 0) {
			prev_check_point = workers[worker_index-1].idx
		};
		data['parent']=cur_frm.doc.name;
		data['parenttype']=cur_frm.doc.doctype;

		frappe.call({
			method:"garments.production.doctype.production_line.production_line.save_worker_input",
			args:{
				"prev_check_point":prev_check_point,
				"check_point":workers[worker_index].idx,
				"pro_line_setup":cur_frm.doc.production_line_setup,
				"datas":data
			},
			callback:function (r) {
				if (r.message && r.message == "Success") {
					$(selector).parents('.modal-content').children('.modal-header').find('.btn-modal-close').click();
					// cur_frm.cscript.worker_input_succ(parent,data,selector,"Success");
				};
			}
		})
	},
	/*save_data:function (parent,data,evel) {
		var doc = cur_frm.doc;
		data['parent'] = doc.name;
		data['parenttype'] = "Production Line";
		// msgprint(data)
		frappe.call({
			method:"garments.production.doctype.production_line.production_line.save_data",
			args:data,
			async:false,
			callback:function (r) {
				// msgprint(r)


				if (r.message) {
					if(r.message[0] == 'complete'){

						msgprint(__("Reloading ..."));
						location.reload();

					}else if(r.message[0] == 'over'){

						msgprint(__("Can't submit more then {0} Qty.",[r.message[1]]));

					}else if (data.doctype == "Production Worker Input") {

						cur_frm.cscript.worker_input_succ(parent,data,evel,"Success");

					}else if (data.doctype == "Production line Bundle Alter") {

						cur_frm.cscript.alter_success(parent,data,evel,r.message);

					};
				};
			}
		});
	},*/
	worker_input_succ:function (parent,data,evel,message) {
		if (message =="Success") {
			var old_val = $(parent).find('.total_qty').html().trim();
			if (data.action == "plus") {
				$(parent).find('.total_qty').html(parseInt(old_val)+parseInt(data.amount))
			}else if (data.action == "minus") {
				$(parent).find('.total_qty').html(parseInt(old_val)-parseInt(data.amount))
			};
			var old_val = $(parent).find('input[name="amount"]').val("")
		};
		$(evel).parents('.modal-content').children('.modal-header').find('.btn-modal-close').click();
	},


});

cur_frm.cscript = new garments.production.ProductionLineController({frm:cur_frm})