# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json,ast

class SizeSet(Document):
	pass

@frappe.whitelist()
def size_filter(doctype, txt, searchfield, start, page_len, filters):
	return frappe.db.get_values(doctype,filters,searchfield)

@frappe.whitelist()
def size_range_filter(doctype,fields,filters):
	filters = ast.literal_eval(filters)
	return frappe.db.get_values(doctype,filters,fields)

@frappe.whitelist()
def size_range_value(filters):
	filters = ast.literal_eval(filters)
	return frappe.db.sql("""SELECT pc.size_format
		FROM `tabPattern Cutting` as pc
		WHERE pc.name = '{filt}' """.format(filt = filters['pc']))

