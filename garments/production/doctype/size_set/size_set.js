frappe.provide('garments.production');

frappe.ui.form.on("Size Set Child","siz_set_cld_remove", function(frm){
	sumTotal(frm.doc);
});

frappe.ui.form.on("Size Set Child","siz_set_cld_add", function(frm){
	//console.log("loo")
	cur_frm.form_wrapper.find('[data-fieldname="siz_set_cld"] .grid-footer-toolbar').remove();
});


function sumTotal(doc,doctype,docname){
	var t_qty = 0;
	// Check Current Row Duplicate Entry
	for(var key in doc.siz_set_cld){
		if(parseInt(doc.siz_set_cld[key].qty)){
			t_qty+=(parseInt(doc.siz_set_cld[key].qty));
		}
	}
	cur_frm.set_value("ttl", t_qty);
}

// pattern cutting filter according to style/ref

garmentsProductionSizeSetControler = frappe.ui.form.Controller.extend({
	onload:function (doc,doctype,docname) {
		this.frm.set_query("pc", function() {
			frappe.model.validate_missing(doc, "style");
	        return {
	            filters: {
	                "reference": doc.style,
	                "pattern_type": "Size Set"
	            }
	        };
	    });
	},
	refresh:function (doc,cdt,cdn) {
		var me = this;
		setTimeout(function(){me.remove_add_new()},100)

	}, 
	remove_add_new:function () {
		cur_frm.form_wrapper.find('[data-fieldname="siz_set_cld"] .grid-footer').remove();
	},

// Quentity changeing
	qty:function (doc,doctype,docname) {
		sumTotal(doc,doctype,docname);
	}
});

cur_frm.cscript = new garmentsProductionSizeSetControler({frm:cur_frm})

// To get size data from size range
frappe.ui.form.on("Size Set","size_range",function (frm) {
	frappe.call({
		method:"garments.production.doctype.size_set.size_set.size_range_filter",
		args:{
			doctype:"Size Set",
			fields:'name',
			filters:{
				pc:frm.doc.pc,
				size_range:frm.doc.size_range,
				status:["in", ["Pass", "Pending"]]
			}
		},
		callback:function (data) {
			//console.log(data)
			if (data.message) {
				msgprint(__("ALready Entry"))
				frm.doc.pc = "";
				frm.doc.size_range = "";
				frm.doc.siz_set_cld = "";
				refresh_field('pc');
				refresh_field('siz_set_cld');
				refresh_field('size_range');
				sumTotal(frm.doc);
				return
			}
			// Row render
			frappe.model.with_doc("Size Format",frm.doc.size_range,function () {
				size_format = frappe.model.get_doc("Size Format",frm.doc.size_range);
				frm.doc.siz_set_cld = "";
				$.each(size_format.format_child, function(index, val) {
					 var row = frappe.model.add_child(frm.doc,"Size Set Child","siz_set_cld");
					 row.size = val.size
				});
				refresh_field('siz_set_cld');
				sumTotal(frm.doc);
							
			})
		}
	})	
});

cur_frm.cscript.pc = function (doc) {
	if (!doc.pc) {
		doc.size_range = "";
		doc.siz_set_cld = "";
		sumTotal(doc);
		refresh_field('siz_set_cld');
		refresh_field('size_range');
		return
	};
	frappe.call({
		method:"garments.production.doctype.size_set.size_set.size_range_value",
		args:{
			filters:{
				pc:doc.pc

			}
		},
		callback:function (r) {
			if (r.message.length > 0) {
				doc.size_range = r.message[0][0];
				refresh_field('size_range');
				cur_frm.script_manager.trigger('size_range')
			};
		}
	})
}

cur_frm.cscript.style = function (doc) {
	doc.pc = "";
	cur_frm.script_manager.trigger('pc')
	refresh_field('pc')
}