frappe.provide('garments.production');

frappe.ui.form.on("Finished Goods Process Child", "finish_goods_process_child_remove", function(frm){
		sumTotal(frm.doc);
});
frappe.ui.form.on("Finished Goods Process Child", "finish_goods_process_child_add", function(frm){

});

cur_frm.cscript.quantity = function(doc,doctype,docname){

		sumTotal(doc);
	}
function sumTotal(doc){

		var t_qty = 0;
		
		for(var key in doc.finish_goods_process_child){

			if(parseFloat(doc.finish_goods_process_child[key].quantity)){
				t_qty+=(parseFloat(doc.finish_goods_process_child[key].quantity));
			}
		}
		cur_frm.set_value("ttl", t_qty);
}
cur_frm.cscript.typ = function(doc){
	doc.finish_goods_process_child = "";
	refresh_field("finish_goods_process_child");
	doc.ttl = "";
	refresh_field("ttl");
}