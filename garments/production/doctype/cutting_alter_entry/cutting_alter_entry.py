# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class CuttingAlterEntry(Document):
	def alter_entry_on_update(self):
		self.alter_entry()
		fields = ['date','alter_for','alter_type','comment','worker','qty']
		frappe.db.sql("DELETE FROM `tabSingles` where doctype='Cutting Alter Entry' and field in ('{}')".format("','".join(fields)));


	def alter_entry(self):
		val_map = {
			"style":self.style,
			"size_range":self.size_range,
			"alter_for":self.alter_for,
			"date":self.date,
			"alter_type":self.alter_type,
			"qty":self.qty,
			"comment":self.comment,
			"worker":self.worker
		}
		doc = frappe.get_doc({"doctype":"Cutting Alter"})

		for key, val in val_map.iteritems():
			doc.set(key,val)

		doc.insert()


def size_range_filter(doctype,txt,searchfield,start,page_len,filters):
	return frappe.db.sql("SELECT `tab{doc}`.name FROM `tab{doc}` LEFT JOIN `tab{jo_doc}` ON `tab{jo_doc}`.size_range = `tab{doc}`.name WHERE `tab{jo_doc}`.style = '{style}'".format(doc=doctype,jo_doc=filters['jo_doc'] or "", style=filters['style']))
