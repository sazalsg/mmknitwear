cur_frm.cscript.onload = function (doc) {
	doc.style = "";
	doc.size_range = "";
	doc.alter_for = "";
	doc.alter_type=""
	doc.worker="";
	doc.qty="1";
	refresh_field('worker');
	refresh_field('alter_type');
	refresh_field('alter_for');
	refresh_field('size_range');
	refresh_field('style');
	refresh_field('qty');

	cur_frm.set_query('size_range',function () {
		frappe.model.validate_missing(doc, "style");
		return {
			query:"garments.production.doctype.cutting_alter_entry.cutting_alter_entry.size_range_filter",

			filters:{
				style:doc.style,
				jo_doc:"Size Set"
			}
		}
	});
}

cur_frm.cscript.refresh = function (doc) {
	cur_frm.disable_save();
	cur_frm.page.clear_icons();
	if (jQuery.inArray( "__unsaved", doc ) == -1) {
		//doc.alter_for="";
		doc.alter_type="";
		doc.worker="";
		doc.date=frappe.datetime.now_datetime();
		//refresh_field('alter_for');
		refresh_field('alter_type');
		refresh_field('date');
		refresh_field('worker');
	};
}

