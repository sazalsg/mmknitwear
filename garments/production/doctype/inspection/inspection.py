# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import msgprint, _, throw


class Inspection(Document):
	pass

@frappe.whitelist()
def get_carton(doctype, txt, searchfield, start, page_len, filters):
	sql=frappe.db.sql("""
		SELECT fgpc.name, {se} FROM `tabFinished Goods Process` as fgp
		INNER JOIN `tabFinished Goods Process Child` as fgpc ON fgp.name=fgpc.parent
		WHERE fgp.stl='{stylename}' AND fgp.docstatus=1 AND fgp.typ='Carton'""".format(stylename=filters['carton'], se='fgpc.carton_no'))
	return sql