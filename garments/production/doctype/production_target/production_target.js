frappe.ui.form.on("Production Target Child", "pt_child_remove", function(frm){
		sumTotal(frm.doc);
});

cur_frm.cscript.qty = function(doc,doctype,docname){

	sumTotal(doc,doctype,docname);
}

function sumTotal(doc,doctype,docname){

	var total = 0;
	
	for(var key in doc.pt_child){

		if(parseInt(doc.pt_child[key].qty)){
			total+=(parseInt(doc.pt_child[key].qty));
		}
	}
	cur_frm.set_value("t_qty", total);
}

cur_frm.cscript.oh_cost = function(doc,doctype,docname){
	
	var setLocal = locals[doctype][docname];

	var ohCost = parseFloat(setLocal.oh_cost);

	var cM = parseFloat(setLocal.cm); 

	var pReq = parseFloat((ohCost*cM)/12);

	setLocal.p_req = parseFloat(pReq); 

	refresh_field("p_req",setLocal.name,"pt_child",0);
}

cur_frm.cscript.stl = function(doc,doctype,docname){
	
	var setLocal = locals[doctype][docname];

	frappe.call({
		method:"frappe.client.get_value",
		args: {
			doctype: "PO Sheet Child Child",
			filters: {
				"product_name":setLocal.stl,
			},
			fieldname : ["description","qty",]
		},
		callback: function(r) {
			//msgprint(r.message)
			$.each(doc.pt_child, function(i, d) {
			 	if(d.stl){
			 		d.itm = r.message["description"];
			 		d.qty = r.message["qty"];
			 	}
			 });
			 refresh_field("pt_child");
		}
	});

	frappe.call({
		method:"frappe.client.get_value",
		args: {
			doctype: "Costing",
			filters: {
				"reference":setLocal.stl,
			},
			fieldname : ["name"]
		},
		cache:false,
		callback: function(r) {

			frappe.call({
				method:"frappe.client.get_value",
				args: {
					doctype: "CostingChild",
					filters: {
						"parentfield":'fabric_table',
						"parent":r.message['name'],
						"costing_child_item":'CM',
					},
					fieldname : ["costing_child_unit_price"]
				},
				cache:false,
				async:false,
				callback: function(r) {
					//console.log(r)
					//msgprint(r)
					$.each(doc.pt_child, function(i, d) {
					 	if(d.stl){
					 		d.cm = r.message["costing_child_unit_price"];
					 	}
					 });
					 refresh_field("pt_child");


					 var setLocal = locals[doctype][docname];

					var ohCost = parseFloat(setLocal.oh_cost);

					var cM = parseFloat(setLocal.cm); 

					var pReq = parseFloat((ohCost*cM)/12);

					setLocal.p_req = parseFloat(pReq); 

					refresh_field("p_req",setLocal.name,"pt_child",0);
				 }
			});
		 }
	});		
}