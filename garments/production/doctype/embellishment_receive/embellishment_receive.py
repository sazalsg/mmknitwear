# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo Software Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class EmbellishmentReceive(Document):
	pass


@frappe.whitelist()
def bundle_filters(doctype, txt, searchfield, start, page_len, filters):
	lmt = "{st}, {pgl}".format(st=start or 0, pgl=page_len or 0)
	where ="WHERE doc.docstatus != '2'"
	if filters:
		for k,v in filters.iteritems():
			if k !="child_doc":
				where += "AND {k} = '{v}'".format(k= k, v=v)
				
	return frappe.db.sql("""
			SELECT {se} FROM `tab{doc}` as doc
			LEFT JOIN `tab{cdoc}` as cdoc
			ON cdoc.parent = doc.name
			{where} LIMIT {lmt}""".format(se=searchfield,doc=doctype,
				cdoc=filters['child_doc'],where=where,lmt=lmt))

@frappe.whitelist()
def bundle_range(doctype,searchfield,bundle_start,bundle_end,stylename):

	return frappe.db.sql("""
		SELECT {se} FROM `tab{doc}` as cdoc
		INNER JOIN `tabBundle` as bundle ON cdoc.parent=bundle.name
		LEFT JOIN `tabEmbellishment Receive Child` as embrc ON embrc.bundle_child = cdoc.name
		WHERE embrc.bundle_child IS NULL AND bundle.style='{stylename}' AND cdoc.bundle_no>{start}-1 AND cdoc.bundle_no<{end}+1
		""".format(doc=doctype,se=searchfield,start=bundle_start, end=bundle_end, stylename=stylename), as_dict = 1)
