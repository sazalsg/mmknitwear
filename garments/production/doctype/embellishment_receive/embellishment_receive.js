cur_frm.set_query('style',function () {
    return {
        filters:{
            "have_embellishment":"1"
        }
    }
})

cur_frm.cscript.refresh = function (doc, doctype, docname) {
	cur_frm.fields_dict['embellishment_recv_child'].grid.get_field('bundle_child').get_query = function(doc) {
        return {
            query: "garments.production.doctype.embellishment_receive.embellishment_receive.bundle_filters",
            searchfield: "cdoc.name , cdoc.bundle_no",
            doctype: "Bundle",
            filters: {
                "child_doc":"Bundle Child",
                "style": doc.style
            }
        }
        
	};
}

cur_frm.cscript.bundle_child = function (doc,doctype,docname) {
    var local = locals[doctype][docname];
    frappe.call({
        method:"frappe.client.get",
        args:{
            doctype:"Bundle Child",
            name:local.bundle_child
        },
        callback:function (r) {
            if (r.message) {
                local.quantity = r.message.total_size;
                local.bundle_no = r.message.bundle_no;

                refresh_field('quantity',local.name,'embellishment_recv_child');
                refresh_field('bundle_no',local.name,'embellishment_recv_child');

                cur_frm.script_manager.trigger("quantity");
            };
        }
    });
}
frappe.ui.form.on("Embellishment Receive Child", "embellishment_recv_child_remove", function(frm) {
    sumTotal();
});
cur_frm.cscript.quantity = function () {
    sumTotal()
}

function sumTotal() {

    var sum_qty = 0;
    // Check Current Row Duplicate Entry
    for (var key in cur_frm.doc.embellishment_recv_child) {


        var res = parseInt(cur_frm.doc.embellishment_recv_child[key].quantity);
        if (res) {
            sum_qty += res;
        }

    }

    cur_frm.set_value("total_delivery_qty", sum_qty);
    cur_frm.refresh_field("total_delivery_qty")
    
}


cur_frm.cscript.process = function (doc, doctype, docname) {


     field = ["style", "bundle_from", "bundle_to"]
     for (var i = 0; i < field.length; i++) {
         if (!(field[i] in doc) || doc[field[i]] == "") {
            msgprint(__("You must have to be fill up Style, From & To"))
            return
         };
     };
     total_bundle = doc.bundle_to-doc.bundle_from+1;

     doc.embellishment_recv_child=""

        frappe.call({
            method: "garments.production.doctype.embellishment_receive.embellishment_receive.bundle_range",
            args:{
                doctype:"Bundle Child",
                searchfield: "cdoc.name, cdoc.bundle_no, cdoc.total_size, bundle.style",
                bundle_start: doc.bundle_from,
                bundle_end: doc.bundle_to,
                stylename: doc.style
            },
            callback:function (r) {
                if (r.message) {
                for(var i=0; i < r.message.length; i++){               
                    var row = frappe.model.add_child(doc,"Embellishment Receive Child","embellishment_recv_child");
                    row.bundle_child = r.message[i].name
                    row.bundle_no = r.message[i].bundle_no
                    row.quantity = r.message[i].total_size
                }
                };
                sumTotal();                
                refresh_field('embellishment_recv_child');
            }
        });     

}