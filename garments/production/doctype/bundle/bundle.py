# -*- coding: utf-8 -*-
# Copyright (c) 2015, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import throw, _
class Bundle(Document):
	
	def validate(self):
		total = 0
		for x in xrange(len(self.bund_child)):
			total += self.bund_child[x].total_size
		if self.available_qty < total :
			throw(_("You can not add maximum {} Bundle".format(self.available_qty)))


@frappe.whitelist()
def get_prev_bundels(style = 0, types=None):
	if style != "":		
		if types == "Test Cutting":
			test_cutting = frappe.db.get_values("Test Cutting", {"styl":style}, ['name', 'ttl'])
			test_cutting_bundle = len(test_cutting)
			test_cutting_total_qty = 0
			for x in range(test_cutting_bundle):
				test_cutting_total_qty += int(test_cutting[x][1])

			bundle = frappe.db.get_values("Bundle", {"style":style, "for":types}, ['total_qty', 'gn_total_bundle'])
			bundle_bundles = 0
			bundle_qty= 0
			for x in range(len(bundle)):
				bundle_bundles += bundle[x][1]
				bundle_qty += int(bundle[x][0])
			available_qty = test_cutting_total_qty - bundle_qty
			#Below code for bundle_no serial maintain
			xbundle = frappe.db.get_values("Bundle", {"style":style}, ['gn_total_bundle'])
			xbundle_bundles = 0
			for x in range(len(xbundle)):
				xbundle_bundles += xbundle[x][0]
			return {'bundles':bundle_bundles, 'available_qty':available_qty, 'bundle_qty':bundle_qty, 'xbundle':xbundle_bundles}
			

		if types == "Bulk Cutting":
			bulk_cutting = frappe.db.get_values("Bulk Cutting",{"style":style},['name', 'ttl'])
			bulk_cutting_bundle = len(bulk_cutting)
			bulk_cutting_total_qty = 0
			for x in range(bulk_cutting_bundle):
				bulk_cutting_total_qty += int(bulk_cutting[x][1])
			bundle = frappe.db.get_values("Bundle", {"style":style, "for":types}, ['total_qty', 'gn_total_bundle'])
			bundle_bundles = 0
			bundle_qty= 0
			for x in range(len(bundle)):
				bundle_bundles += bundle[x][1]
				bundle_qty += int(bundle[x][0])
			available_qty = bulk_cutting_total_qty - bundle_qty
			#Below code for bundle_no serial maintain
			xbundle = frappe.db.get_values("Bundle", {"style":style}, ['gn_total_bundle'])
			xbundle_bundles = 0
			for x in range(len(xbundle)):
				xbundle_bundles += xbundle[x][0]
			return {'bundles':bundle_bundles, 'available_qty':available_qty, 'bundle_qty':bundle_qty, 'xbundle':xbundle_bundles}