cur_frm.cscript.frm.add_fetch("style", "buyer", "buyer");

// calculte child tables total field  

frappe.ui.form.on("Bundle Child", "bund_child_remove", function(frm) {

    sumTotal();
});

function sumTotal() {

    var sum_qty = 0;
    // Check Current Row Duplicate Entry
    for (var key in cur_frm.doc.bund_child) {


        var res = parseInt(cur_frm.doc.bund_child[key].total_size);
        if (res) {
            sum_qty += res;
        }

    }

    cur_frm.set_value("total_qty", sum_qty);
    refresh_field('total_qty');
}
function previous_total(doc, for_type) {
	frappe.call({
		method:"garments.production.doctype.bundle.bundle.get_prev_bundels",
		args:{
            types:for_type,
			style:doc.style
		},
		callback:function (data) {
			if (data.message) {
            doc.gn_start = data.message.bundle_qty+1
            doc['prev_bundle'] = data.message.bundles
            doc['available_qty'] = data.message.available_qty
            doc['bundle_serial'] = data.message.xbundle
			cur_frm.form_wrapper.find('[data-fieldname="previous_bundle"]').html('<div class="control-value like-disabled-input"> <label style="padding-right: 0px;" class="control-label">Total Bundles</label><br/>'+data.message.bundles+'<br/><label style="padding-right: 0px;" class="control-label">Available Quantity</label><br/>'+data.message.available_qty+'<br/><label style="padding-right: 0px;" class="control-label">Previous Bundle Quantity</label><br/>'+data.message.bundle_qty+'</div>')
			}else{
                cur_frm.form_wrapper.find('[data-fieldname="previous_bundle"]').html("")
            };
		}
	})
}
cur_frm.cscript.for = function(doc, doctype, docname) {
    var type = doc.for;
    previous_total(doc, type);

}

 cur_frm.cscript.style = function(doc, doctype, docname) {
 	previous_total(doc, doc.for);
 }

cur_frm.cscript.proccess = function (doc) {
     field = ["gn_size","gn_start","gn_bundle","gn_total_bundle"]
     for (var i = 0; i < field.length; i++) {
         if (!(field[i] in doc) || doc[field[i]] == "") {
            msgprint(__("Please fill all field in Genarate Section and Style"))
            return
         };
     };
     doc.bund_child=""
     
     for (var i = 0; i < doc.gn_total_bundle; i++) {
         var row = frappe.model.add_child(doc,"Bundle Child","bund_child");

         row.bundle_no = doc.bundle_serial+i+1;
         row.color = doc.gn_color
         row.size = doc.gn_size
         row.total_size = (parseInt(((doc.gn_start+(doc.gn_bundle*i))))+doc.gn_bundle)-((doc.gn_start+(doc.gn_bundle*i))+(i<0?1:0))
         
     };
     sumTotal();
     refresh_field('bund_child')
}

