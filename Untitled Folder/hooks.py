# -*- coding: utf-8 -*-
from __future__ import unicode_literals

app_name = "garments"
app_title = "Garments"
app_publisher = "Techbeeo"
app_description = "Garments..."
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "abir5001@gmail.com"
app_version = "0.0.1"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/garments/css/garments.css"
# app_include_js = "/assets/garments/js/garments.js"

# include js, css files in header of web template
# web_include_css = "/assets/garments/css/garments.css"
# web_include_js = "/assets/garments/js/garments.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "garments.install.before_install"
# after_install = "garments.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "garments.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"garments.tasks.all"
# 	],
# 	"daily": [
# 		"garments.tasks.daily"
# 	],
# 	"hourly": [
# 		"garments.tasks.hourly"
# 	],
# 	"weekly": [
# 		"garments.tasks.weekly"
# 	]
# 	"monthly": [
# 		"garments.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "garments.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "garments.event.get_events"
# }

